#!/usr/bin/python3

import argparse

import aes
import phk
from model import Model
from phk import PHKMode


# ----------------------------------------------------------
#
# ----------------------------------------------------------

class Context():

    def __init__(self,
                 num_steps,
                 num_substates,
                 permutation,
                 allow_related_tweaks,
                 allow_related_tweakeys,
                 use_tau_rotation,
                 use_kappa_over_full_state,
                 use_256_bit_key,
                 use_full_tweak,
                 output_path,
                 mode=1,
                 model=None):
        self.num_steps = num_steps
        self.num_substates = num_substates
        self.permutation = permutation
        self.allow_related_tweaks = allow_related_tweaks
        self.allow_related_tweakeys = allow_related_tweakeys
        self.use_tau_rotation = use_tau_rotation
        self.use_kappa_over_full_state = use_kappa_over_full_state
        self.use_256_bit_key = use_256_bit_key
        self.use_full_tweak = use_full_tweak
        self.output_path = output_path
        self.mode = mode
        self.model = model

    # ----------------------------------------------------------

    def __str__(self):
        return "# steps {}\n".format(self.num_steps) + \
               "# substates {}\n".format(self.num_substates) + \
               "# permutation {}\n".format(self.permutation) + \
               "# allow_related_tweaks {}\n"\
                   .format(self.allow_related_tweaks) + \
               "# allow_related_tweakeys {}\n"\
                   .format(self.allow_related_tweakeys) + \
               "# use_tau_rotation {}\n".format(self.use_tau_rotation) + \
               "# use_kappa_over_full_state {}\n".format(
                   self.use_kappa_over_full_state) + \
               "# use_256_bit_key {}\n".format(self.use_256_bit_key) + \
               "# use_full_tweak {}\n".format(self.use_full_tweak) + \
               "# output_path {}\n".format(self.output_path) + \
               "# mode {}\n".format(self.mode)


# ----------------------------------------------------------
# Functions
# ----------------------------------------------------------

def _as_unsigned_int(val):
    value_as_int = int(val)

    if value_as_int < 0:
        raise argparse.ArgumentTypeError(
            "{} is an invalid unsigned int value.".format(val)
        )

    return value_as_int


# ----------------------------------------------------------

def _as_positive_int(val):
    ivalue = int(val)

    if ivalue <= 0:
        raise argparse.ArgumentTypeError(
            "{} is an invalid positive int value.".format(val)
        )

    return ivalue


# ----------------------------------------------------------

def _as_bool(val):
    return int(val) != 0


# ----------------------------------------------------------

def _parse_arguments():
    parser = argparse.ArgumentParser(
        description=
        "Find differential trail with minimum count of active S-boxes."
    )

    parser.add_argument(
        "--steps",
        type=_as_positive_int,
        help="Number of subsequent steps of the selected cipher."
    )
    parser.add_argument(
        "--substates",
        type=_as_positive_int,
        help="Parallel AES substates."
    )
    parser.add_argument(
        "--permutation",
        type=_as_unsigned_int,
        nargs="+",
        help="Permutation."
    )
    parser.add_argument(
        "--related-tweak",
        type=_as_bool,
        help="Allow related tweaks.",
        default=False
    )
    parser.add_argument(
        "--related-tweakey",
        type=_as_bool,
        help="Allow related tweakeys.",
        default=False
    )
    parser.add_argument(
        "--use-tau-rotation",
        type=_as_bool,
        help="Use tau as left rotation.",
        default=False
    )
    parser.add_argument(
        "--use-kappa-over-full-state",
        type=_as_bool,
        help="Rotate kappa over the full state. "
             "(only effective with --use-tau-rotation)",
        default=False
    )
    parser.add_argument(
        "--use-256-bit-key",
        type=_as_bool,
        help="Use a 256-bit key.",
        default=False
    )
    parser.add_argument(
        "--use-full-tweak",
        type=_as_bool,
        help="Uses k-bit tweak if True. Uses 128-bit tweak if False.",
        default=False
    )
    parser.add_argument(
        "--output-path",
        type=str,
        help="Output path for the constraints.",
        default="diff_phk.lp"
    )

    args = parser.parse_args()
    return Context(
        args.steps,
        args.substates,
        args.permutation,
        args.related_tweak,
        args.related_tweakey,
        args.use_tau_rotation,
        args.use_kappa_over_full_state,
        args.use_256_bit_key,
        args.use_full_tweak,
        args.output_path
    )


# ----------------------------------------------------------

def _is_permutation_correct(num_permutation_elements,
                            num_expected_elements):
    return num_permutation_elements > 0 and \
           (num_permutation_elements == num_expected_elements)


# ----------------------------------------------------------

def main():
    context = _parse_arguments()
    context.model = Model.get_instance()
    print("{}".format(context))

    num_permutation_elements = len(context.permutation)
    num_expected_elements = context.num_substates * aes.AES_NUM_COLS

    if not _is_permutation_correct(num_permutation_elements,
                                   num_expected_elements):
        raise argparse.ArgumentTypeError(
            "Unexpected permutation length {} for Phk. "
                .format(num_permutation_elements)
        )

    if context.allow_related_tweakeys:
        context.allow_related_tweakeys = context.allow_related_tweaks or \
                                         context.allow_related_tweakeys
        context.allow_related_key = context.allow_related_tweakeys

        if context.allow_related_key and context.use_256_bit_key:
            context.use_256_bit_key = context.use_256_bit_key

    if context.use_tau_rotation is not None:
        context.mode = 2

    phk.find_differential_trail(
        context.model,
        context.output_path,
        context.num_steps,
        context.num_substates,
        context.permutation,
        context.allow_related_tweaks,
        context.allow_related_tweakeys,
        context.mode,
        context.use_kappa_over_full_state,
        context.use_256_bit_key,
        context.use_full_tweak
    )


# ----------------------------------------------------------

if __name__ == '__main__':
    main()

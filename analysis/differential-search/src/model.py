#!/usr/bin/python3

import gurobipy as grb


# ----------------------------------------------------------
# Model class
# ----------------------------------------------------------

class Model:

    _instance = None

    # ----------------------------------------------------------

    def __init__(self):
        self._model = grb.Model()

    # ----------------------------------------------------------

    @staticmethod
    def get_instance():
        if Model._instance is None:
            Model._instance = Model()

        return Model._instance

    # ----------------------------------------------------------

    def get_model(self):
        return self._model

    # ----------------------------------------------------------

    def xor(self, bin_out, *bin_in):
        m = self._model
        bin_out_last = bin_in[1]

        for i in range(2, len(bin_in)):
            bin_out_cur = m.addVar(vtype=grb.GRB.BINARY)

            m.addConstr(bin_out_cur >= bin_out_last - bin_in[i])
            m.addConstr(bin_out_cur >= bin_in[i] - bin_out_last)
            m.addConstr(bin_out_cur <= bin_in[i] + bin_out_last)
            m.addConstr(bin_out_cur <= 2 - bin_in[i] - bin_out_last)

            bin_out_last = bin_out_cur

        m.addConstr(bin_out >= bin_out_last - bin_in[0])
        m.addConstr(bin_out >= bin_in[0] - bin_out_last)
        m.addConstr(bin_out <= bin_in[0] + bin_out_last)
        m.addConstr(bin_out <= 2 - bin_in[0] - bin_out_last)

    # ----------------------------------------------------------

    def xor_allow_cancel(self, bin_out, *bin_in):
        m = self._model
        bin_out_last = bin_in[1]

        for i in range(2, len(bin_in)):
            bin_out_cur = m.addVar(vtype=grb.GRB.BINARY)

            m.addConstr(bin_out_cur >= bin_out_last - bin_in[i])
            m.addConstr(bin_out_cur >= bin_in[i] - bin_out_last)
            m.addConstr(bin_out_cur <= bin_in[i] + bin_out_last)

            bin_out_last = bin_out_cur

        m.addConstr(bin_out >= bin_out_last - bin_in[0])
        m.addConstr(bin_out >= bin_in[0] - bin_out_last)
        m.addConstr(bin_out <= bin_in[0] + bin_out_last)

    # ----------------------------------------------------------

    def bin(self, bin_out, var_in):
        m = self._model
        d = m.addVar(vtype=grb.GRB.BINARY)

        m.addConstr(-100 * bin_out <= var_in)
        m.addConstr(var_in <= 100 * bin_out)
        m.addConstr(0.001 * bin_out - 100.001 * d <= var_in)
        m.addConstr(var_in <= -0.001 * bin_out + 100.001 * (1 - d))

    # ----------------------------------------------------------

    def mod_add(self, int_out, int_in, add, mod):
        m = self._model
        x = m.addVar(vtype=grb.GRB.INTEGER)

        m.addConstr(int_out >= 0)
        m.addConstr(int_out <= mod - 1)
        m.addConstr(int_out + x * mod == int_in + add)

    # ----------------------------------------------------------

    def xor_byte(self, bin_out, int_in0, int_in1):
        m = self._model

        diff_var = m.addVar(vtype=grb.GRB.INTEGER)
        m.addConstr(diff_var == int_in0 - int_in1)

        abs_var = m.addVar(vtype=grb.GRB.INTEGER)
        m.addGenConstrAbs(abs_var, diff_var)

        m.addConstr(int_in0 <= int_in1 + bin_out * 256)
        m.addConstr(int_in1 <= int_in0 + bin_out * 256)
        m.addConstr(bin_out <= abs_var)

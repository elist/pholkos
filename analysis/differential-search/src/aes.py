#!/usr/bin/python3

from model import *


# ----------------------------------------------------------
# Constants
# ----------------------------------------------------------

AES_NUM_ROWS = 4
AES_NUM_COLS = 4
AES_NUM_STATE_CELLS = AES_NUM_ROWS * AES_NUM_COLS
AES_NUM_STATE_WORDS = 4
AES_SBOX_DEG = 7
AES_WORD_SIZE = 8


# ----------------------------------------------------------

def create_word(model):
    """
    Internal model
    m = Model.get_instance().get_model()
    :param m:
    :return:
    """
    m = model.get_model()
    word = m.addVar(vtype=grb.GRB.BINARY)
    m.addConstr(word >= 0)
    m.addConstr(word <= AES_WORD_SIZE)

    return word


# ----------------------------------------------------------

def shift_rows(model, state):
    next_state = [None] * AES_NUM_STATE_CELLS

    for r in range(AES_NUM_ROWS):
        for c in range(AES_NUM_COLS):
            next_state[c * AES_NUM_ROWS + r] = \
                state[((c + r) % AES_NUM_COLS) * AES_NUM_ROWS + r]

    return next_state


# ----------------------------------------------------------

def mix_columns(model, state):
    m = model.get_model()
    next_state = [create_word(model) for _ in range(AES_NUM_STATE_CELLS)]

    for c in range(AES_NUM_COLS):
        ctr_in = m.addVar(vtype=grb.GRB.INTEGER)
        ctr_out = m.addVar(vtype=grb.GRB.INTEGER)

        m.addConstr(ctr_in == grb.quicksum(
            state[c * AES_NUM_ROWS:(c + 1) * AES_NUM_ROWS])
        )
        m.addConstr(ctr_out == grb.quicksum(
            next_state[c * AES_NUM_ROWS:(c + 1) * AES_NUM_ROWS])
        )

        col_active = m.addVar(vtype=grb.GRB.BINARY)

        m.addGenConstrOr(
            col_active, state[c * AES_NUM_ROWS:(c + 1) * AES_NUM_ROWS]
        )
        m.addConstr(ctr_in + ctr_out >= col_active * 5)
        m.addConstr(ctr_out <= col_active * AES_NUM_ROWS)

    return next_state


# ----------------------------------------------------------

def add_key(model, state, key):
    m = model.get_model()
    next_state = [None] * AES_NUM_STATE_CELLS

    for c in range(AES_NUM_COLS):
        for r in range(AES_NUM_ROWS):
            word = create_word(model)
            model.xor_allow_cancel(word,
                                   state[c * AES_NUM_ROWS + r],
                                   key[c * AES_NUM_ROWS + r])
            next_state[c * AES_NUM_ROWS + r] = word

    return next_state


# ----------------------------------------------------------

def aes_round(model, state, key=None):
    m = model.get_model()

    sr_state = shift_rows(model, state)
    mc_state = mix_columns(model, sr_state)

    next_state = mc_state
    intermediate_states = [sr_state]

    if key is not None:
        next_state = add_key(model, mc_state, key)
        intermediate_states.append(mc_state)

    return next_state, intermediate_states

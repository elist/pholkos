#!/usr/bin/python3

from enum import Enum

import gurobipy as grb

import aes


# ----------------------------------------------------------
# Constants
# ----------------------------------------------------------

PHK_NUM_ROUNDS_PER_STEP = 2

PHK_PI_K_PERMUTATION = [8, 1, 7, 15, 10, 4, 2, 3, 6, 9, 11, 0, 5, 12, 14, 13]
PHK_KHOO_PERMUTATION = [11, 12, 1, 2, 15, 0, 5, 6, 3, 4, 9, 10, 7, 8, 13, 14]


# ----------------------------------------------------------

class PHKMode(Enum):
    PHK_TAU_PI_K = 1
    PHK_TAU_ROT = 2
    PHK_MODES = [PHK_TAU_PI_K,PHK_TAU_ROT]

    @staticmethod
    def is_valid(mode):
        return mode in PHKMode.PHK_MODES


# ----------------------------------------------------------
# expand 256-bit key
# def expand(state, size):
#     model = Model.get_instance()
#     m = model.get_model()
#     key_exp = [None] * size
#
#     key_exp[0:32] = state[0:32]
#
#     if size > 32:
#         for i in range(32):
#             xor_val = m.addVar(vtype=grb.GRB.BINARY)
#
#             model.xor_allow_cancel(xor_val,
#                       state[(i + 4) % 32],
#                       state[(i + 8) % 32],
#                       state[(i + 12) % 32],
#                       state[(i + 16) % 32],
#                       state[(i + 20) % 32],
#                       state[(i + 24) % 32],
#                       state[(i + 28) % 32])
#
#             key_exp[32 + i] = xor_val
#
#     if size > 64:
#         for i in range(64):
#             xor_val = m.addVar(vtype=grb.GRB.BINARY)
#
#             model.xor_allow_cancel(xor_val,
#                       state[(i // 4) + (((i % 4) + 1) % 4)],
#                       state[(i // 4) + (((i % 4) + 2) % 4)],
#                       state[(i // 4) + (((i % 4) + 3) % 4)])
#
#             key_exp[64 + i] = xor_val
#
#     return key_exp


# ----------------------------------------------------------

def expand(model, state, size):
    m = model.get_model()
    key_exp = [None] * size

    key_exp[0:32] = state[0:32]

    if size > 32:
        for i in range(32):
            xor_val = m.addVar(vtype=grb.GRB.BINARY)
            model.xor_allow_cancel(xor_val,
                                   key_exp[i],
                                   key_exp[(i + 16) % 32],
                                   key_exp[(i + 28) % 32])
            key_exp[32 + i] = xor_val

    if size > 64:
        for i in range(32):
            xor_val = m.addVar(vtype=grb.GRB.BINARY)
            model.xor_allow_cancel(xor_val,
                                   key_exp[32 + i],
                                   key_exp[32 + ((i + 16) % 32)],
                                   key_exp[32 + ((i + 24) % 32)])
            key_exp[64 + i] = xor_val

    if size > 96:
        for i in range(32):
            xor_val = m.addVar(vtype=grb.GRB.BINARY)
            model.xor_allow_cancel(xor_val,
                                   key_exp[64 + i],
                                   key_exp[64 + ((i + 16) % 32)],
                                   key_exp[64 + ((i + 20) % 32)])
            key_exp[96 + i] = xor_val

    return key_exp


# ----------------------------------------------------------

def _apply_cell_permutation(state, permutation):
    return [state[permutation[i]] for i in range(aes.AES_NUM_STATE_CELLS)]


# ----------------------------------------------------------

def _phk_pi_k(state):
    """
    Applies permutation pi_k to 16-byte state.
    :param state:
    :return:
    """
    return _apply_cell_permutation(state, PHK_PI_K_PERMUTATION)
    # return [state[8],
    #         state[1],
    #         state[7],
    #         state[15],
    #         state[10],
    #         state[4],
    #         state[2],
    #         state[3],
    #         state[6],
    #         state[9],
    #         state[11],
    #         state[0],
    #         state[5],
    #         state[12],
    #         state[14],
    #         state[13]]


# ----------------------------------------------------------

def _phk_khoo_permutation(state):
    """
    Apply permutation pi_k to 16-byte state.
    :param state:
    :return:
    """
    return _apply_cell_permutation(state, PHK_KHOO_PERMUTATION)
    # return [state[11],
    #         state[12],
    #         state[1],
    #         state[2],
    #         state[15],
    #         state[0],
    #         state[5],
    #         state[6],
    #         state[3],
    #         state[4],
    #         state[9],
    #         state[10],
    #         state[7],
    #         state[8],
    #         state[13],
    #         state[14]]


# ----------------------------------------------------------

# left rotate state
def _phk_rotate_left(state):
    return state[1:] + [state[0]]


# ----------------------------------------------------------

# tweak update function
def _phk_tau(state):
    return _phk_khoo_permutation(state)


# ----------------------------------------------------------

# Application of tweak-update function in key-update function
def _phk_key_schedule_tau(tweak, mode, use_full_rotation):
    if mode == PHKMode.PHK_TAU_ROT and use_full_rotation:
        return _phk_rotate_left(tweak)

    res = []

    for i in range(len(tweak) // aes.AES_NUM_STATE_CELLS):
        start_cell_index = i * aes.AES_NUM_STATE_CELLS
        end_cell_index = (i + 1) * aes.AES_NUM_STATE_CELLS
        res += _phk_tau(tweak[start_cell_index:end_cell_index])

    return res


# ----------------------------------------------------------

# Apply 32-bit word-wise permutation to state
def _phk_mix(state, permutation):
    next_state = []

    for p in range(len(permutation)):
        next_state += state[
                      permutation[p] * aes.AES_NUM_ROWS:
                      (permutation[p] + 1) * aes.AES_NUM_ROWS
                      ]

    return next_state


# ----------------------------------------------------------

def _phk_mix_sparx(model, state):
    """
    Mixing layer derived from SPARX (deprecated).
    """
    m = model.get_model()
    next_state = [None] * len(state)

    for i in range(len(state) // 2):
        next_state[i + (len(state) // 2)] = state[i]

    xor_col = []

    for i in range(4):
        xor_word = m.addVar(vtype=grb.GRB.BINARY)
        model.xor(xor_word, *state[i:(len(state) // 2):4])
        xor_col.append(xor_word)

    xor_col = xor_col[1:] + [xor_col[0]]
    xor_words = [m.addVar(vtype=grb.GRB.BINARY) for _ in range(len(state) // 2)]

    for i in range(len(state) // 2):
        model.xor(xor_words[i], state[i], xor_col[i % 4])

    xor_words = xor_words[0:4] + \
                xor_words[20:24] + \
                xor_words[8:12] + \
                xor_words[28:32] + \
                xor_words[16:20] + \
                xor_words[4:8] + \
                xor_words[24:28] + \
                xor_words[12:16]

    for i in range(len(state) // 2):
        xor_word = m.addVar(vtype=grb.GRB.BINARY)
        model.xor(xor_word, state[(len(state) // 2) + i], xor_words[i])
        next_state[i] = xor_word

    return next_state


# ----------------------------------------------------------

def _phk_tweak_schedule_lfsr(model, word):
    """
    LFSR developed for key schedule (deprecated).
    :param word:
    :return:
    """
    m = model.get_model()

    word = [word[3]] + word[0:3]

    word1 = m.addVar(vtype=grb.GRB.BINARY)
    model.xor(word1, word[1], word[0])

    word[1] = word1

    return word


# ----------------------------------------------------------

def _phk_kappa(key, permutation, mode, use_full_rotation):
    """
    Key-update function.
    :param key:
    :param permutation:
    :param mode:
    :param use_full_rotation:
    :return:
    """
    next_key = _phk_mix(key, permutation)
    next_key = _phk_key_schedule_tau(next_key, mode, use_full_rotation)
    return next_key


# ----------------------------------------------------------

# phk round
def _phk_round(model, state, key=None):
    next_state = []
    num_substates = len(state) // aes.AES_NUM_STATE_CELLS

    for s in range(num_substates):
        substate_key = None

        if key is not None:
            substate_key = key[
                           s * aes.AES_NUM_STATE_CELLS:
                           (s + 1) * aes.AES_NUM_STATE_CELLS
                           ]

        aes_state, intermediate_states = \
            aes.aes_round(
                model,
                state[
                s * aes.AES_NUM_STATE_CELLS:
                (s + 1) * aes.AES_NUM_STATE_CELLS
                ],
                substate_key
            )

        next_state += aes_state

    return next_state


# ----------------------------------------------------------

# phk step
def _phk_step(model, state, permutation, keys=None):
    round_states = []

    for r in range(PHK_NUM_ROUNDS_PER_STEP):
        key = None

        if keys is not None:
            key = keys[r]

        state = _phk_round(model, state, key)
        round_states.append(state)

    next_state = _phk_mix(state, permutation)
    return next_state, round_states


# ----------------------------------------------------------

def _phk_prepare_related_keys(model,
                              num_steps,
                              num_substates,
                              permutation,
                              use_256_bit_key):
    key = None
    keys = None

    if use_256_bit_key:
        # Create a 256-bit key
        key = [aes.create_word(model)
               for _ in range(2 * aes.AES_NUM_STATE_CELLS)]

        key = expand(
            model,
            key,
            aes.AES_NUM_STATE_CELLS * num_substates
        )

        # Expand the 256-bit key to the state size
        keys = [key]
    else:
        # Start with full-sized key
        key = [aes.create_word(model)
               for _ in range(num_substates * aes.AES_NUM_STATE_CELLS)]

        keys = [key]

    # Tracks the byte order in the round keys
    num_key_cells = len(keys[0])
    key_indices = [[i for i in range(num_key_cells)]]
    num_rounds = num_steps * PHK_NUM_ROUNDS_PER_STEP

    for r in range(num_rounds):
        # Permutes the current key
        num_cells_in_permutation = len(permutation)
        start_cell_index = (r * num_substates * aes.AES_NUM_ROWS) \
                           % num_cells_in_permutation
        end_cell_index = start_cell_index + num_substates * aes.AES_NUM_ROWS

        # Permutes the current key
        key = _phk_kappa(keys[r],
                         permutation[start_cell_index:end_cell_index],
                         PHKMode.PHK_TAU_PI_K,
                         False)

        # Permutes the key-byte indices
        key_index = _phk_kappa(key_indices[r],
                               permutation[start_cell_index:end_cell_index],
                               PHKMode.PHK_TAU_PI_K,
                               False)

        keys.append(key)
        key_indices.append(key_index)

    return keys, key_indices, key


# ----------------------------------------------------------
# Change for full tweaks
# ----------------------------------------------------------

def _phk_prepare_full_related_tweaks(model,
                                     num_steps,
                                     num_substates,
                                     permutation):
    num_tweak_words = aes.AES_NUM_STATE_CELLS * num_substates
    tweaks = [[aes.create_word(model)
               for _ in range(num_tweak_words)]]
    num_tweak_cells = len(tweaks[0])

    # Tracks the byte order in the round tweaks
    tweak_indices = [[i for i in range(num_tweak_cells)]]
    num_rounds = num_steps * PHK_NUM_ROUNDS_PER_STEP
    num_cells_in_permutation = len(permutation)

    for r in range(num_rounds):
        start_cell_index = (r * num_substates * aes.AES_NUM_ROWS) \
                           % num_cells_in_permutation
        end_cell_index = start_cell_index + num_substates * aes.AES_NUM_ROWS

        # Permutes the current key
        tweak = _phk_kappa(tweaks[r],
                           permutation[start_cell_index:end_cell_index],
                           PHKMode.PHK_TAU_PI_K,
                           False)

        # Permutes the key-byte indices
        tweak_index = _phk_kappa(tweak_indices[r],
                                 permutation[start_cell_index:end_cell_index],
                                 PHKMode.PHK_TAU_PI_K,
                                 False)

        tweaks.append(tweak)
        tweak_indices.append(tweak_index)

    return tweaks, tweak_indices


# ----------------------------------------------------------

def _phk_prepare_128_bit_related_tweaks(model, num_steps):
    """
    Creates 128-bit tweaks
    :param model:
    :param num_steps:
    :return:
    """
    # Create 128-bit tweak
    tweaks = [[aes.create_word(model) for _ in range(aes.AES_NUM_STATE_CELLS)]]
    num_tweak_cells = len(tweaks[0])

    # Tracks the byte order in the round tweaks
    tweak_indices = [[i for i in range(num_tweak_cells)]]

    for r in range(num_steps * PHK_NUM_ROUNDS_PER_STEP):
        tweak = _phk_tau(tweaks[r])
        tweak_index = _phk_tau(tweak_indices[r])

        tweaks.append(tweak)
        tweak_indices.append(tweak_index)

    return tweaks, tweak_indices


# ----------------------------------------------------------

def _create_counter_id(left, right):
    return "{}#{}".format(left, right)


# ----------------------------------------------------------

def _get_counter_id_left_index(counter_id):
    return int(counter_id.split('#')[0])


# ----------------------------------------------------------

def _phk_generate_keyless_tweakeys(num_steps,
                                   num_substates,
                                   tweaks):
    tweakeys = []

    for r in range(num_steps * PHK_NUM_ROUNDS_PER_STEP + 1):
        num_tweak_cells = len(tweaks[r])

        tweakey = [tweaks[r][i % num_tweak_cells]
                   for i in range(num_substates * aes.AES_NUM_STATE_CELLS)]

        # Each round tweakey is composed only of the tweak
        tweakeys.append(tweakey)

    cancellation_counters = None
    return tweakeys, cancellation_counters


# ----------------------------------------------------------

def _phk_generate_tweakeys_with_keys(model,
                                     num_steps,
                                     keys,
                                     key_indices,
                                     tweaks,
                                     tweak_indices):
    m = model.get_model()
    tweakeys = []
    cancellation_counters = {}
    num_rounds = num_steps * PHK_NUM_ROUNDS_PER_STEP + 1

    for r in range(num_rounds):
        # Each round tweakey consists of the round key and round tweak
        num_keys_cells = len(keys[r])
        tweakey = [m.addVar(vtype=grb.GRB.BINARY)
                   for _ in range(num_keys_cells)]

        # print("# len(keys[r]):   {}".format(len(keys[r])))
        # print("# len(tweaks[0]): {}".format(len(tweaks[0])))

        for i in range(num_keys_cells):
            num_tweak_cells = len(tweaks[r])

            # Key and tweak are XORed and can cancel each other
            model.xor_allow_cancel(tweakey[i],
                                   keys[r][i],
                                   tweaks[r][i % num_tweak_cells])

            counter_id = _create_counter_id(str(i % len(tweaks[0])), str(i))

            if counter_id not in cancellation_counters:
                cancellation_counters[counter_id] = []
                # print("# Added cancel_ctr {}".format(counter_id))

            num_cells_in_tweak_index = len(tweak_indices[r])
            final_counter_id = _create_counter_id(
                str(tweak_indices[r][i % num_cells_in_tweak_index]),
                str(key_indices[r][i])
            )

            # ----------------------
            # Hic sunt dracones!
            # ----------------------

            # print("# foo: {}".format(tweak_indices[r][i % len(tweak_indices[r])]))
            # print("# r: {}, i: {} final_counter_id: {} {}".format(
            #     r,
            #     i,
            #     final_counter_id,
            #     str(tweak_indices[r][i % len(tweak_indices[r])]) + '#' + str(key_indices[r][i])
            # ))
            # print("# tweakey[{}]: {}".format(i, tweakey[i]))

            cancellation_counters[final_counter_id].append(tweakey[i])
            # print("# Set cancel_ctr {}".format(final_counter_id))

        tweakeys.append(tweakey)

    for counter_id in cancellation_counters:
        index = _get_counter_id_left_index(counter_id)

        # But cancel at most once because of the doublings in the Tweakey
        # framework
        m.addConstr(
            grb.quicksum(cancellation_counters[counter_id]) >=
            tweaks[0][index] * num_steps * PHK_NUM_ROUNDS_PER_STEP
        )

    return tweakeys, cancellation_counters


# ----------------------------------------------------------

def _phk_prepare_tweakeys(model,
                          num_steps,
                          num_substates,
                          keys,
                          key_indices,
                          tweaks,
                          tweak_indices):
    if keys is None:
        return _phk_generate_keyless_tweakeys(
            num_steps,
            num_substates,
            tweaks
        )

    return _phk_generate_tweakeys_with_keys(
        model,
        num_steps,
        keys,
        key_indices,
        tweaks,
        tweak_indices
    )


# ----------------------------------------------------------

def _print_results(states, allow_related_tweaks, tweaks, keys):
    key_index = 0
    num_states = len(states)

    for s in range(num_states - 2):
        in_aes = False

        if s > 0:
            if (s + 2) % 3 == 0:
                print('AES ' + str((s + 2) // 3))
                print('')

                in_aes = True
            elif (s + 2) % 3 == 1:
                print('AES ' + str((s + 2) // 3))
                print('')

                in_aes = True
            else:
                print('MIX ' + str((s + 2) // 3))
                print('')

        for r in range(aes.AES_NUM_ROWS):
            row_str = ''
            tweak_row_str = ''

            for c in range(len(states[s + 1]) // aes.AES_NUM_ROWS):
                if states[s + 1][c * aes.AES_NUM_ROWS + r].xn == 0:
                    row_str += '. '
                else:
                    row_str += 'x '

                if (c % aes.AES_NUM_COLS) == (aes.AES_NUM_COLS - 1):
                    row_str += ' '

                if (s == 0 or in_aes) and allow_related_tweaks:
                    if keys is None or \
                            keys[key_index][c * aes.AES_NUM_ROWS + r].xn == 0:
                        tweak_row_str += '. '
                    else:
                        tweak_row_str += 'x '

                    if (c % aes.AES_NUM_COLS) == (aes.AES_NUM_COLS - 1):
                        tweak_row_str += ' '

            if (s == 0 or in_aes) and allow_related_tweaks:
                tweak_str = ''

                for c in range(len(tweaks[key_index]) // aes.AES_NUM_ROWS):
                    if tweaks[key_index][c * aes.AES_NUM_ROWS + r].xn == 0:
                        tweak_str += '. '
                    else:
                        tweak_str += 'x '
                    if (c % aes.AES_NUM_COLS) == (aes.AES_NUM_COLS - 1):
                        tweak_str += ' '

                row_str += '|  ' + tweak_row_str + '|  ' + tweak_str

            print(row_str)

        if s == 0 or in_aes:
            key_index += 1

        print('')


# ----------------------------------------------------------

def find_differential_trail(
        model,
        output_path,
        num_steps,
        num_substates,
        permutation,
        allow_related_tweaks=False,
        allow_related_keys=False,
        mode=PHKMode.PHK_TAU_PI_K,
        use_kappa_over_full_state=False,
        use_256_bit_key=False,
        use_full_tweak=False):
    # ----------------------------------------------------------
    # Create initial states
    # ----------------------------------------------------------

    num_state_cells = aes.AES_NUM_STATE_CELLS * num_substates
    states = [[aes.create_word(model) for _ in range(num_state_cells)]]

    tweaks = None
    tweak_indices = None

    original_key = None
    keys = None
    key_indices = None

    tweakeys = None
    cancellation_counters = None

    # ----------------------------------------------------------
    # Create tweaks and relations
    # ----------------------------------------------------------

    if allow_related_tweaks:
        # ----------------------------------------------------------
        # Change for full tweaks
        # ----------------------------------------------------------

        if use_full_tweak:
            tweaks, tweak_indices = _phk_prepare_full_related_tweaks(
                model,
                num_steps,
                num_substates,
                permutation
            )
        else:
            tweaks, tweak_indices = _phk_prepare_128_bit_related_tweaks(
                model,
                num_steps
            )

    if allow_related_keys:
        keys, key_indices, original_key = _phk_prepare_related_keys(
            model,
            num_steps,
            num_substates,
            permutation,
            use_256_bit_key
        )

    if allow_related_tweaks or allow_related_keys:
        tweakeys, cancellation_counters = _phk_prepare_tweakeys(
            model,
            num_steps,
            num_substates,
            keys,
            key_indices,
            tweaks,
            tweak_indices
        )

    m = model.get_model()
    m.Params.Threads = 8

    if tweakeys is not None:
        # Add first tweakey
        states.append([m.addVar(vtype=grb.GRB.BINARY)
                       for _ in range(num_state_cells)])

        num_cells_in_last_state = len(states[-1])

        for i in range(num_cells_in_last_state):
            model.xor_allow_cancel(
                states[-1][i],
                states[-2][i],
                tweakeys[0][i]
            )
    else:
        states.append([states[0][i] for i in range(num_state_cells)])

    # ----------------------------------------------------------
    # Create state relations over the steps
    # ----------------------------------------------------------

    for s in range(num_steps):
        step_keys = None

        if tweakeys is not None:
            start_tweakey_index = PHK_NUM_ROUNDS_PER_STEP * s + 1
            end_tweakey_index = PHK_NUM_ROUNDS_PER_STEP * (s + 1) + 1
            step_keys = tweakeys[start_tweakey_index:end_tweakey_index]

        num_permutation_cells = len(permutation)
        start_index = (s * num_substates * aes.AES_NUM_COLS) \
                      % num_permutation_cells
        end_index = start_index + num_substates * aes.AES_NUM_COLS

        state, round_states = _phk_step(
            model,
            states[-1],
            permutation[start_index:end_index],
            step_keys
        )

        states += round_states
        states.append(state)

    # ----------------------------------------------------------
    # Collect the active Sboxes and build the sum
    # ----------------------------------------------------------

    active_sboxes = []
    num_states = len(states)

    for i in range(1, num_states - 1):
        if ((i - 1) % (PHK_NUM_ROUNDS_PER_STEP + 1)) != PHK_NUM_ROUNDS_PER_STEP:
            active_sboxes += states[i]

    # ----------------------------------------------------------
    # At constraints that there must be at least one active cell
    # Depending on the model
    # ----------------------------------------------------------

    if allow_related_tweaks and allow_related_keys:
        # In states, keys, or tweaks
        m.addConstr(grb.quicksum(states[0]) +
                    grb.quicksum(keys[0]) +
                    grb.quicksum(tweaks[0]) >= 1)
    elif allow_related_tweaks:
        # In states or tweaks
        m.addConstr(grb.quicksum(states[0]) +
                    grb.quicksum(tweaks[0]) >= 1)
    elif allow_related_keys:
        # In states or keys
        m.addConstr(grb.quicksum(states[0]) +
                    grb.quicksum(keys[0]) >= 1)
    else:
        # Only in states
        m.addConstr(grb.quicksum(states[0]) >= 1)

    # ----------------------------------------------------------
    # Optimizer work
    # ----------------------------------------------------------

    m.setObjective(grb.quicksum(active_sboxes), grb.GRB.MINIMIZE)

    num_states = len(states)
    print("# States: {}".format(num_states))

    m.update()
    m.write(output_path)
    m.optimize()

    # ----------------------------------------------------------
    # Output
    # ----------------------------------------------------------

    _print_results(states, allow_related_tweaks, tweaks, keys)

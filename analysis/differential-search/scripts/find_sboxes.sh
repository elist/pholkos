#!/usr/bin/env bash

# ----------------------------------------------------------
# Variables
# ----------------------------------------------------------

GUROBI_PATH=gurobi.sh
SCRIPT_PATH=../src/find_min_num_active_sboxes.py
RESULTS_PATH=../results
VARIANT=256
NUM_STEPS=2
NUM_SUBSTATES=2
PERMUTATION="0 5 2 7 4 1 6 3"
USE_RELATED_TWEAK=1
USE_RELATED_TWEAKEY=1
USE_256_BIT_KEY=1
USE_FULL_TWEAK=1

# ----------------------------------------------------------
# Usage
# ----------------------------------------------------------

usage() {
    echo "Usage: $0 -v <variant> -s <num_steps> -t <use_related_tweak> "
    echo " -k <use_related_tweakey>"
    echo "- v: Variant, in [256,512,1024]"
    echo "- s: #steps, positive integer"
    echo "- t: 0 if no related tweaks allowed, 1 otherwise"
    echo "- k: 0 if no related tweakeys allowed, 1 otherwise"
    exit 1
}

# ----------------------------------------------------------
# Options parsing
# ----------------------------------------------------------

while getopts v:s:t:k:h flag
do
    case "${flag}" in
        v) VARIANT=${OPTARG};;
        s) NUM_STEPS=${OPTARG};;
        t) USE_RELATED_TWEAK=${OPTARG};;
        k) USE_RELATED_TWEAKEY=${OPTARG};;
        *) usage;;
    esac
done

# ----------------------------------------------------------
# Variants
# ----------------------------------------------------------

if [[ $VARIANT -eq 512 ]]
then
    NUM_SUBSTATES=4
    PERMUTATION="0 5 10 15 4 9 14 3 8 13 2 7 12 1 6 11"
elif [[ $VARIANT -eq 1024 ]]
then
    NUM_SUBSTATES=8
    PERMUTATION="0 9 18 27 4 13 22 31 8 17 26 3 12 21 30 7 16 25 2 11 20 29 6 15 24 1 10 19 28 5 14 23 0 5 2 7 4 9 6 11 8 13 10 15 12 17 14 19 16 21 18 23 20 25 22 27 24 29 26 31 28 1 30 3"
fi

# ----------------------------------------------------------
# The function calls
# ----------------------------------------------------------

RESULT_FILE_NAME=$RESULTS_PATH/pholkos_${VARIANT}_${USE_256_BIT_KEY}_${USE_RELATED_TWEAKEY}_${USE_FULL_TWEAK}_${NUM_STEPS}

stdbuf -oL nohup ${GUROBI_PATH} $SCRIPT_PATH \
--steps ${NUM_STEPS} \
--substates ${NUM_SUBSTATES} \
--permutation ${PERMUTATION} \
--related-tweak ${USE_RELATED_TWEAK} \
--related-tweakey ${USE_RELATED_TWEAKEY} \
--use-tau-rotation 0 \
--use-kappa-over-full-state 0 \
--use-256-bit-key ${USE_256_BIT_KEY} \
--use-full-tweak ${USE_FULL_TWEAK} \
--output-path ${RESULT_FILE_NAME}_constraints.lp \
> ${RESULT_FILE_NAME}.lp &


# ----------------------------------------------------------
# 256-bit version
# ----------------------------------------------------------
# --substates 2 \
# --permutation 0 5 2 7 4 1 6 3  \
# --related-tweak 1 \
# --related-tweakey 1 \
# --use-full-tweak 1

# ----------------------------------------------------------
# 512-bit version
# ----------------------------------------------------------
# --substates 4 \
# --permutation 0 5 10 15 4 9 14 3 8 13 2 7 12 1 6 11 \
# --related-tweak 1 \
# --related-tweakey 1 \
# --use-full-tweak 1

# ----------------------------------------------------------
# 1024-bit version
# ----------------------------------------------------------
# --substates 8 \
# --permutation 0 9 18 27 4 13 22 31 8 17 26 3 12 21 30 7 16 25 2 11 20 29 6 15 24 1 10 19 28 5 14 23 0 5 2 7 4 9 6 11 8 13 10 15 12 17 14 19 16 21 18 23 20 25 22 27 24 29 26 31 28 1 30 3 \
# --related-tweakey 1 \
# --use-full-tweak 1
# ----------------------------------------------------------



# ----------------------------------------------------------
# Parameter combinations
# ----------------------------------------------------------

#VARIANTS_OPTIONS=(256 512 1024)
#NUM_STEPS_OPTIONS=(3 4 5 6)
#USE_RELATED_TWEAKEYS_OPTIONS=(0 1)
#
#for VARIANT in ${VARIANTS_OPTIONS[*]}
#do
#for NUM_STEPS in ${NUM_STEPS_OPTIONS[*]}
#do
#for USE_RELATED_TWEAKEY in ${USE_RELATED_TWEAKEYS_OPTIONS[*]}
#do
#done
#done
#done

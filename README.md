# Pholkos

# <span style="font-variant:small-caps;">Phk</span> Reference Implementation

This is the reference implementation of the high-security family of block ciphers
<span style="font-variant:small-caps;">Pholkos</span>. It has been developed using AVX2 intrinsics for performance on x86 platforms.

## Requirements

In order to run this implementation the CPU is required to support SSE4.1, AVX2 and AES-NI.

## Authors and acknowledgment
Jannis Bossert
Eik List
Stefan Lucks
Sebastian Schmitz

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

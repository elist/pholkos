
#include <cstdint>
#include <cstring>
#include <iostream>
#include <string>
#include "misc.h"

extern "C" {
#include "aes.h"
#include "utils.h"
}


// ---------------------------------------------------------

static bool run_encryption_test(const aes_state_t key,
                                const aes_state_t plaintext,
                                const aes_state_t expected_ciphertext) {
    aes_state_t ciphertext;
    aes_encrypt_round(plaintext, ciphertext, key);

    const bool was_successful = misc::assert_equals(expected_ciphertext,
                                                    ciphertext,
                                                    AES_NUM_STATE_BYTES);

    if (was_successful) {
        std::cout << "OK" << std::endl;
    } else {
        std::cout << "FAIL" << std::endl;
    }

    return !was_successful;
}

// ---------------------------------------------------------

static bool run_decryption_test(const aes_state_t key,
                                const aes_state_t ciphertext,
                                const aes_state_t expected_plaintext) {
    aes_state_t plaintext;
    aes_decrypt_round(ciphertext, plaintext, key);

    const bool was_successful = misc::assert_equals(expected_plaintext,
                                                    plaintext,
                                                    AES_NUM_STATE_BYTES);

    if (was_successful) {
        std::cout << "OK" << std::endl;
    } else {
        std::cout << "FAIL" << std::endl;
    }

    return !was_successful;
}

// ---------------------------------------------------------

bool test_encrypt() {
    const aes_state_t key = {
        0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f
    };
    const aes_state_t plaintext = {
        0x52, 0x00, 0x00, 0x00, 0x00, 0x09, 0x00, 0x00,
        0x00, 0x00, 0x6a, 0x00, 0x00, 0x00, 0x00, 0xd5
    };
    const aes_state_t expected_ciphertext = {
        // 2, 7, 0, 5
        0x02, 0x06, 0x02, 0x06, 0x67, 0x66, 0x65, 0x64,
        0x6b, 0x6a, 0x69, 0x68, 0x6f, 0x6e, 0x6d, 0x6c
    };

    return run_encryption_test(key, plaintext, expected_ciphertext);
}

// ---------------------------------------------------------

bool test_decrypt() {
    const aes_state_t key = {
        0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f
    };
    const aes_state_t ciphertext = {
        // 2, 7, 0, 5
        0x02, 0x06, 0x02, 0x06, 0x67, 0x66, 0x65, 0x64,
        0x6b, 0x6a, 0x69, 0x68, 0x6f, 0x6e, 0x6d, 0x6c
    };
    const aes_state_t expected_plaintext = {
        0x52, 0x00, 0x00, 0x00, 0x00, 0x09, 0x00, 0x00,
        0x00, 0x00, 0x6a, 0x00, 0x00, 0x00, 0x00, 0xd5
    };

    return run_decryption_test(key, ciphertext, expected_plaintext);
}

// ---------------------------------------------------------

int main() {
    bool result = false;

    result |= test_encrypt();
    result |= test_decrypt();

    return result;
}

// ---------------------------------------------------------

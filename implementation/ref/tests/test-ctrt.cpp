
#include <cstdint>
#include <cstring>
#include <fstream>
#include <iostream>
#include <jsoncpp/json/json.h>
#include <string>

#include "ctrt.h"
#include "misc.h"

extern "C" {
#include "phk-256.h"
}

// ---------------------------------------------------------


uint8_t HalfByteToDez(char const& h) {
  uint8_t ret = 0;
  switch (h) {
    case '0' : return 0;
    case '1' : return 1;
    case '2' : return 2;
    case '3' : return 3;
    case '4' : return 4;
    case '5' : return 5;
    case '6' : return 6;
    case '7' : return 7;
    case '8' : return 8;
    case '9' : return 9;
    case 'a' : return 10;
    case 'b' : return 11;
    case 'c' : return 12;
    case 'd' : return 13;
    case 'e' : return 14;
    case 'f' : return 15;
  }
  return ret;
}


uint8_t ByteToDez(char lhs, char rhs) {
  return 16 * HalfByteToDez(lhs) + HalfByteToDez(rhs);
}


void HexToDez(std::string const& hexstr, uint8_t* const& out) {
  uint64_t len = (hexstr.length() + 1) / 2;
  if (hexstr.length() % 2 == 1) {
    out[0] = HalfByteToDez(static_cast<char>(hexstr[0]));
    for (uint64_t i = 1; i <= len; ++i) {
      out[i] = ByteToDez(hexstr[(i << 1)-1], hexstr[(i << 1)]);
    }
  } else {
    for (uint64_t i = 0; i < len; ++i) {
      out[i] = ByteToDez(hexstr[i << 1], hexstr[(i << 1)+1]);
    }
  }
}

// ---------------------------------------------------------

bool test_ctrt_phk256_128_ref() {
  bool ret = true;
  std::ifstream test_file("../testcases/ctrt-phk256-128.json", std::ifstream::binary);
  
  if (!test_file.good()) {
    std::cout << "\033[91mTEST FILE ";
    std::cout << " NOT FOUND\n";
    std::cout << "You either changed the directory structure or called the";
    std::cout << " program from the wrong directory!\n\033[0m";
    return false;
  }

  Json::Value tests;

  test_file >> tests;

  int size;

  for (auto test : tests) {
    std::cout << "------------------"<<std::endl;
    std::string tmp = test["Plaintext"].asString();
    size = tmp.size() / 2;
    uint8_t plaintext[size];
    uint8_t ciphertext[size];
    char* tmp2 = &tmp[0u];
    HexToDez(tmp2, plaintext);

    tmp = test["Key"].asString();
    phk256_key_t key;
    tmp2 = &tmp[0u];
    HexToDez(tmp2, key);

    tmp = test["Tweak"].asString();
    phk256_tweak_t tweak;
    tmp2 = &tmp[0u];
    HexToDez(tmp2, tweak);

    tmp = test["Nonce"].asString();
    phk256_state_t nonce;
    tmp2 = &tmp[0u];
    HexToDez(tmp2, nonce);

    tmp = test["Ciphertext"].asString();
    uint8_t expected[size];
    tmp2 = &tmp[0u];
    HexToDez(tmp2, expected);

    ctrt_phk_256_128_enc(key, tweak, nonce, plaintext, ciphertext, size / PHK256_NUM_STATE_BYTES);

    bool was_successful = misc::assert_equals(expected,
                                              ciphertext,
                                              size);
    if (!was_successful) {
      std::cout << "\033[91mEncryption failed!\033[0m" << std::endl;
      std::cout << "Expected: "<< std::hex;
      for (int i=0; i < size; ++i) {
        std::cout << std::setw(2) << std::setfill('0') << (int) expected[i];
      }
      std::cout << std::endl;
      std::cout << "But was: "<< std::hex;
      for (int i=0; i < size; ++i) {
        std::cout << std::setw(2) << std::setfill('0') << (int) ciphertext[i];
      }
      std::cout << std::endl;
      std::cout << std::endl;
      ret = false;
    }

    std::cout << "------------------"<<std::endl;
  }
  return ret;
}

// ---------------------------------------------------------

int main () {
  bool success = true;
  success &= test_ctrt_phk256_128_ref();
  std::cout << (success ? "All tests passed." : "Failure.") <<std::endl;
  return 0;
}

// ---------------------------------------------------------

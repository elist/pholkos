
#include <cstdint>
#include <cstring>
#include <fstream>
#include <iostream>
#include <jsoncpp/json/json.h>
#include <string>

#include "misc.h"

extern "C" {
#include "phk-256.h"
}

// ---------------------------------------------------------

uint8_t HalfByteToDez(char const &h) {
    uint8_t ret = 0;
    switch (h) {
        case '0' :
            return 0;
        case '1' :
            return 1;
        case '2' :
            return 2;
        case '3' :
            return 3;
        case '4' :
            return 4;
        case '5' :
            return 5;
        case '6' :
            return 6;
        case '7' :
            return 7;
        case '8' :
            return 8;
        case '9' :
            return 9;
        case 'a' :
            return 10;
        case 'b' :
            return 11;
        case 'c' :
            return 12;
        case 'd' :
            return 13;
        case 'e' :
            return 14;
        case 'f' :
            return 15;
    }
    return ret;
}

// ---------------------------------------------------------

uint8_t ByteToDez(char lhs, char rhs) {
    return 16 * HalfByteToDez(lhs) + HalfByteToDez(rhs);
}

// ---------------------------------------------------------

void HexToDez(std::string const &hexstr, uint8_t *const &out) {
    uint64_t len = (hexstr.length() + 1) / 2;
    if (hexstr.length() % 2 == 1) {
        out[0] = HalfByteToDez(static_cast<char>(hexstr[0]));
        for (uint64_t i = 1; i <= len; ++i) {
            out[i] = ByteToDez(hexstr[(i << 1) - 1], hexstr[(i << 1)]);
        }
    } else {
        for (uint64_t i = 0; i < len; ++i) {
            out[i] = ByteToDez(hexstr[i << 1], hexstr[(i << 1) + 1]);
        }
    }
}

// ---------------------------------------------------------

bool test_phk_256_ref() {
    bool ret = true;
    std::ifstream test_file("../testcases/phk256.json", std::ifstream::binary);
    
    if (!test_file.good()) {
        std::cout << "\033[91mTEST FILE ";
        std::cout << " NOT FOUND\n";
        std::cout << "You either changed the directory structure or called the";
        std::cout << " program from the wrong directory!\n\033[0m";
        return false;
    }
    
    Json::Value tests;
    
    test_file >> tests;
    
    for (auto test : tests) {
        std::cout << "------------------" << std::endl;
        std::string tmp = test["Plaintext"].asString();
        phk256_state_t plaintext;
        char *tmp2 = &tmp[0u];
        HexToDez(tmp2, plaintext);
        
        tmp = test["Key"].asString();
        phk256_state_t key;
        tmp2 = &tmp[0u];
        HexToDez(tmp2, key);
        std::cout << std::hex;
        for (int i = 0; i < 32; ++i) {
            std::cout << std::setw(2) << std::setfill('0') << (int) key[i]
                      << "  ";
        }
        std::cout << std::endl;
        
        tmp = test["Tweak"].asString();
        phk256_state_t tweak;
        tmp2 = &tmp[0u];
        HexToDez(tmp2, tweak);
        
        tmp = test["Ciphertext"].asString();
        phk256_state_t ciphertext;
        tmp2 = &tmp[0u];
        HexToDez(tmp2, ciphertext);
        
        phk256_ctx ctx;
        phk256_state_t test_val;
        phk256_set_key_encrypt(&ctx, key);
        for (int round = 0; round < 17; ++round)
            print_round_key(&ctx, round);
        phk256_set_tweak_encrypt(&ctx, tweak);
        for (int round = 0; round < 17; ++round)
            print_round_tweakey(&ctx, round);
        std::memcpy(test_val, plaintext, PHK256_NUM_STATE_BYTES);
        phk256_encrypt(&ctx, test_val);
        
        bool was_successful = misc::assert_equals(ciphertext,
                                                  test_val,
                                                  PHK256_NUM_STATE_BYTES);
        if (!was_successful) {
            std::cout << "\033[91mEncryption failed!\033[0m" << std::endl;
            std::cout << "Expected: " << std::hex;
            
            for (int i = 0; i < PHK256_NUM_STATE_BYTES; ++i) {
                std::cout << std::setw(2) << std::setfill('0')
                          << (int) ciphertext[i];
            }
            
            std::cout << std::endl;
            std::cout << "But was: " << std::hex;
            
            for (int i = 0; i < PHK256_NUM_STATE_BYTES; ++i) {
                std::cout << std::setw(2) << std::setfill('0')
                          << (int) test_val[i];
            }
            
            std::cout << std::endl;
            std::cout << std::endl;
            ret = false;
        }
        
        phk256_set_key_decrypt(&ctx, key);
        phk256_set_tweak_decrypt(&ctx, tweak);
        std::memcpy(test_val, ciphertext, PHK256_NUM_STATE_BYTES);
        phk256_decrypt(&ctx, test_val);
        
        misc::print_buffer(tweak, 16, true);
        was_successful = misc::assert_equals(plaintext,
                                             test_val,
                                             PHK256_NUM_STATE_BYTES);
        if (!was_successful) {
            std::cout << "\033[91mDecryption failed!\033[0m" << std::endl;
            std::cout << "Expected: " << std::hex;
            
            for (int i = 0; i < PHK256_NUM_STATE_BYTES; ++i) {
                std::cout << std::setw(2) << std::setfill('0')
                          << (int) plaintext[i];
            }
            
            std::cout << std::endl;
            std::cout << "But was: " << std::hex;
            
            for (int i = 0; i < PHK256_NUM_STATE_BYTES; ++i) {
                std::cout << std::setw(2) << std::setfill('0')
                          << (int) test_val[i];
            }
            
            std::cout << std::endl;
            ret = false;
        }
        
        std::cout << "------------------" << std::endl;
    }
    
    return ret;
}

// ---------------------------------------------------------

int main() {
    bool success = true;
    success &= test_phk_256_ref();
    std::cout << (success ? "All tests passed." : "Failure.") << std::endl;
    return 0;
}

// ---------------------------------------------------------

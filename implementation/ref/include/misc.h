#ifndef MISC_H
#define MISC_H

// --------------------------------------------------------------------

#include <cstddef>
#include <cstdint>
#include <iostream>
#include <iomanip>
#include <math.h>
#include <limits>
#include <sstream>

// --------------------------------------------------------------------

namespace misc {

    void print_progress_bar(std::ostream &out, uint8_t progress);

    // --------------------------------------------------------------------

    void print_buffer(uint8_t *buf, size_t len, bool only_words);

    // --------------------------------------------------------------------

    bool assert_equals(const uint8_t* expected,
                       const uint8_t* actual,
                       size_t num_bytes);

}

// --------------------------------------------------------------------

#endif

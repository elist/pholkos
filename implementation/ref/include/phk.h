#ifndef PHK_H
#define PHK_H

// --------------------------------------------------------------------

#include <stdint.h>
#include <stdlib.h>

// --------------------------------------------------------------------

#define PHK_NUM_AES_ROUNDS_PER_STEP               2
#define PHK_NUM_SUBSTATE_BYTES                   16

static const uint8_t PHK_GF2_8_MODULUS = 0x1b;

// --------------------------------------------------------------------

typedef uint8_t phk_substate_t[PHK_NUM_SUBSTATE_BYTES];

// --------------------------------------------------------------------

void phk_encryption_key_schedule(uint8_t* round_keys,
                                 const uint8_t* key,
                                 size_t num_substates);

// --------------------------------------------------------------------

void phk_encrypt_round(const uint8_t* round_key,
                       uint8_t* state,
                       size_t num_substates);

// --------------------------------------------------------------------

void phk_encrypt_step(uint8_t* state,
                      size_t num_substates);

// --------------------------------------------------------------------

void phk_decrypt_round(uint8_t* round_key,
                       uint8_t* state,
                       size_t num_substates);

// --------------------------------------------------------------------

void phk_decrypt_step(uint8_t* state,
                      size_t num_substates);

// --------------------------------------------------------------------

void phk_permute_state(uint8_t* state,
                       const uint8_t* permutation,
                       size_t num_substates);

// --------------------------------------------------------------------

void phk_invert_permute_state(uint8_t* state,
                              const uint8_t* permutation,
                              size_t num_substates);

// --------------------------------------------------------------------

void phk_expand_key_ma(uint8_t* key);

// --------------------------------------------------------------------

void phk_expand_key_mb(uint8_t* key);

// --------------------------------------------------------------------

void phk_expand_key_mc(uint8_t* key);

// --------------------------------------------------------------------

#endif // PHK_H

/**
// This is free and unencumbered software released into the public domain.
//
// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a compiled
// binary, for any purpose, commercial or non-commercial, and by any
// means.
//
// In jurisdictions that recognize copyright laws, the author or authors
// of this software dedicate any and all copyright interest in the
// software to the public domain. We make this dedication for the benefit
// of the public at large and to the detriment of our heirs and
// successors. We intend this dedication to be an overt act of
// relinquishment in perpetuity of all present and future rights to this
// software under copyright law.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
// OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//
// For more information, please refer to <http://unlicense.org/>
*/

#ifndef _UTILS_H_
#define _UTILS_H_

// --------------------------------------------------------------------

#include <stdint.h>
#include <stdlib.h>

// --------------------------------------------------------------------

int assert_equal_uint32(const uint32_t *left,
                        const uint32_t *right,
                        size_t num_words);

// --------------------------------------------------------------------

int assert_equal_uint64(const uint64_t *left,
                        const uint64_t *right,
                        size_t num_words);

// --------------------------------------------------------------------

void print_hex_uint32(const uint32_t *array, size_t num_words);

// --------------------------------------------------------------------

void print_hex_uint64(const uint64_t *array, size_t num_words);

// --------------------------------------------------------------------

void print_hex(const uint8_t *array, size_t num_bytes);

// --------------------------------------------------------------------

uint64_t uint32_to_uint64(const uint32_t *input);

// ---------------------------------------------------------

void uint64_to_uint32(uint32_t *output, uint64_t input);

// ---------------------------------------------------------

void swap_words32(uint32_t* left, uint32_t* right);

// --------------------------------------------------------------------

void swap_words64(uint64_t *left, uint64_t *right);

// --------------------------------------------------------------------

void swap_words(uint64_t *array, size_t i, size_t j);

// --------------------------------------------------------------------

uint32_t rotate_left32(uint32_t x, size_t r);

// --------------------------------------------------------------------

uint32_t rotate_right32(uint32_t x, size_t r);

// --------------------------------------------------------------------

uint64_t rotate_left48(uint64_t x, size_t r);

// --------------------------------------------------------------------

uint64_t rotate_right48(uint64_t x, size_t r);

// --------------------------------------------------------------------

uint64_t rotate_left64(uint64_t x, size_t r);

// --------------------------------------------------------------------

uint64_t rotate_right64(uint64_t x, size_t r);

// --------------------------------------------------------------------

uint32_t bytes_to_uint32(const uint8_t *input, size_t num_bytes);

// --------------------------------------------------------------------

void bytes_to_uint32_words(uint32_t *output_words,
                           const uint8_t *input_bytes,
                           size_t num_bytes,
                           size_t num_bytes_per_word);

// --------------------------------------------------------------------

void uint32_to_bytes(uint8_t *result,
                     uint32_t input_word,
                     size_t num_bytes);

// --------------------------------------------------------------------

void uint32_words_to_bytes(uint8_t *result,
                           const uint32_t *input_words,
                           size_t num_words,
                           size_t num_bytes_per_word);

// --------------------------------------------------------------------

void bytes_to_uint64_words(uint64_t *output_words,
                           const uint8_t *input_bytes,
                           size_t num_bytes,
                           size_t num_bytes_per_word);

// --------------------------------------------------------------------

uint64_t bytes_to_uint64(const uint8_t *input, size_t num_bytes);

// --------------------------------------------------------------------

void uint64_words_to_bytes(uint8_t *result,
                           const uint64_t *input_words,
                           size_t num_words,
                           size_t num_bytes_per_word);

// --------------------------------------------------------------------

void uint64_word_to_bytes(uint8_t *result,
                          uint64_t input_word,
                          size_t num_bytes);

// --------------------------------------------------------------------

void shift_words_left(uint64_t *target,
                      size_t num_words,
                      size_t num_shifted_bits);

// --------------------------------------------------------------------

void xor_arrays(uint8_t *target,
                const uint8_t *left,
                const uint8_t *right,
                size_t num_bytes);

// --------------------------------------------------------------------

/**
 *
 * @param value
 * @param modulus A primitive polynomial in GF(2^9) that
 * @return
 */
uint8_t gf2_8_double(uint8_t value, uint8_t modulus);

// --------------------------------------------------------------------

void gf2_8_double_array(uint8_t* array, uint8_t modulus, size_t num_bytes);

// --------------------------------------------------------------------

void zeroize(uint8_t *target, size_t num_bytes);

// --------------------------------------------------------------------

#endif // _UTILS_H_

#ifndef PHK_CTRT
#define PHK_CTRT


#include <cstdint>
#include <cstring>
#include <fstream>
#include <iostream>
#include <jsoncpp/json/json.h>
#include <string>

#include "misc.h"
extern "C" {
#include "phk-256.h"
#include "utils.h"
}

void add_uint8_t_arr(uint8_t* arr, uint8_t val, int block=PHK256_NUM_TWEAK_BYTES);

void ctrt_phk_256_128_enc(phk256_key_t const key, phk256_tweak_t tweak,\
                      uint8_t const* nonce, uint8_t const* plain,\
                      uint8_t *ciphertext, size_t length);

#endif

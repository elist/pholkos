/****************************************/
/* Compute pi to arbitrary precision    */
/* Author Roy Williams February 1994    */
/* Uses Machin's formula...             */
/* pi/4 = 4 arctan(1/5) - arctan(1/239) */
/****************************************/
/* compile with cc -O -o pi pi.c        */
/* run as "pi 1000"                     */
/****************************************/
/* The last few digits may be wrong.......... */

#ifndef PI_H
#define PI_H

// --------------------------------------------------------------------

#include <stdio.h>
#include <cstdlib>

// --------------------------------------------------------------------

#define BASE 10000

// --------------------------------------------------------------------

void copy(int nblock, int *result, int *from);

int zero(int nblock, int *result);

void add(int nblock, int *result, int *increm);

void sub(int nblock, int *result, int *decrem);

void mult(int nblock, int *result, int factor);

void div(int nblock, int *result, int denom);

void set(int nblock, int *result, int rhs);

void print(int nblock, int *result);

void arctan(int nblock, int *result, int *w1, int *w2, int denom, int onestep);

// --------------------------------------------------------------------

#endif // PI_H

#include <memory.h>
#include "phk-1024.h"
#include "aes.h"
#include "utils.h"

// --------------------------------------------------------------------

static void add_round_tweak(const phk1024_ctx *ctx,
                            phk1024_state_t state,
                            const size_t round_index) {
    const uint8_t *round_tweak = ctx->round_tweaks[round_index];

    for (size_t i = 0; i < PHK1024_NUM_SUBSTATES; ++i) {
        uint8_t *state_word = state + i * PHK_NUM_SUBSTATE_BYTES;
        xor_arrays(state_word, state_word, round_tweak, PHK_NUM_SUBSTATE_BYTES);
    }
}

// --------------------------------------------------------------------

static void add_round_tweak_256(const phk1024_256_ctx *ctx,
                            phk1024_state_t state,
                            const size_t round_index) {
    const uint8_t *round_tweak = ctx->round_tweaks[round_index];

    for (size_t i = 0; i < PHK1024_NUM_SUBSTATES; ++i) {
        uint8_t *state_word = state + i * PHK_NUM_SUBSTATE_BYTES;
        xor_arrays(state_word, state_word, round_tweak, PHK_NUM_SUBSTATE_BYTES);
    }
}

// --------------------------------------------------------------------

static void add_round_key(const phk1024_ctx *ctx,
                          phk1024_state_t state,
                          const size_t round_index) {
    const uint8_t *round_key = ctx->round_keys[round_index];
    xor_arrays(state, state, round_key, PHK1024_NUM_STATE_BYTES);
}

// --------------------------------------------------------------------

static void add_round_key_256(const phk1024_256_ctx *ctx,
                          phk1024_state_t state,
                          const size_t round_index) {
    const uint8_t *round_key = ctx->round_keys[round_index];
    xor_arrays(state, state, round_key, PHK1024_NUM_STATE_BYTES);
}

// --------------------------------------------------------------------

static void add_round_constant(phk1024_state_t state,
                               const size_t round_index) {
    const uint8_t *round_constant = PHK1024_ROUND_CONSTANTS[round_index];
    xor_arrays(state, state, round_constant, PHK_NUM_SUBSTATE_BYTES);
}

// --------------------------------------------------------------------

static void add_round_tweakey(const phk1024_ctx *ctx,
                              phk1024_state_t state,
                              const size_t round_index) {
    add_round_key(ctx, state, round_index);
    add_round_tweak(ctx, state, round_index);
    add_round_constant(state, round_index);
}

// --------------------------------------------------------------------

static void add_round_tweakey_256(const phk1024_256_ctx *ctx,
                              phk1024_state_t state,
                              const size_t round_index) {
    add_round_key_256(ctx, state, round_index);
    add_round_tweak_256(ctx, state, round_index);
    add_round_constant(state, round_index);
}

// --------------------------------------------------------------------

static size_t get_round_index(const size_t step_index,
                              const size_t round_index) {
    return step_index * PHK_NUM_AES_ROUNDS_PER_STEP + round_index;
}

// --------------------------------------------------------------------

static void permute_words(uint8_t *state,
                          const uint8_t *permutation,
                          const size_t num_words) {
    uint8_t temp[4 * num_words];

    for (size_t i = 0; i < num_words; ++i) {
        const size_t to_index = permutation[i];
        uint8_t *word_start = &(state[4 * to_index]);
        uint8_t *word_to = &(temp[4 * i]);
        memcpy(word_to, word_start, 4);
    }

    memcpy(state, temp, PHK1024_NUM_STATE_BYTES);
}

// --------------------------------------------------------------------

static void phk1024_permute_state_0(phk1024_state_t state) {
    permute_words(state, PHK1024_PERMUTATION_0, PHK1024_NUM_WORDS);
}

// --------------------------------------------------------------------

static void phk1024_invert_permute_state_0(phk1024_state_t state) {
    permute_words(state, PHK1024_INVERSE_PERMUTATION_0, PHK1024_NUM_WORDS);
}

// --------------------------------------------------------------------

static void phk1024_permute_state_1(phk1024_state_t state) {
    permute_words(state, PHK1024_PERMUTATION_1, PHK1024_NUM_WORDS);
}

// --------------------------------------------------------------------

static void phk1024_invert_permute_state_1(phk1024_state_t state) {
    permute_words(state, PHK1024_INVERSE_PERMUTATION_1, PHK1024_NUM_WORDS);
}

// --------------------------------------------------------------------

static void phk1024_encrypt_round(const phk1024_ctx *ctx,
                                 phk1024_state_t state,
                                 const size_t round_index) {
    for (size_t i = 0; i < PHK1024_NUM_SUBSTATES; ++i) {
        const uint8_t *round_key = ctx->round_keys[round_index]
                                   + i * PHK_NUM_SUBSTATE_BYTES;
        uint8_t *substate = &(state[i * PHK_NUM_SUBSTATE_BYTES]);
        aes_encrypt_round(substate, substate, round_key);
    }

    add_round_tweak(ctx, state, round_index);
    add_round_constant(state, round_index);
}

// --------------------------------------------------------------------

static void phk1024_256_encrypt_round(const phk1024_256_ctx *ctx,
                                 phk1024_state_t state,
                                 const size_t round_index) {
    for (size_t i = 0; i < PHK1024_NUM_SUBSTATES; ++i) {
        const uint8_t *round_key = ctx->round_keys[round_index]
                                   + i * PHK_NUM_SUBSTATE_BYTES;
        uint8_t *substate = &(state[i * PHK_NUM_SUBSTATE_BYTES]);
        aes_encrypt_round(substate, substate, round_key);
    }

    add_round_tweak_256(ctx, state, round_index);
    add_round_constant(state, round_index);
}

// --------------------------------------------------------------------

static void phk1024_encrypt_last_round(const phk1024_ctx *ctx,
                                      phk1024_state_t state,
                                      const size_t round_index) {
    for (size_t i = 0; i < PHK1024_NUM_SUBSTATES; ++i) {
        const uint8_t *round_key = ctx->round_keys[round_index]
                                   + i * PHK_NUM_SUBSTATE_BYTES;
        uint8_t *substate = &(state[i * PHK_NUM_SUBSTATE_BYTES]);
        aes_encrypt_last_round(substate, substate, round_key);
    }

    add_round_tweak(ctx, state, round_index);
    add_round_constant(state, round_index);
}

// --------------------------------------------------------------------

static void phk1024_256_encrypt_last_round(const phk1024_256_ctx *ctx,
                                      phk1024_state_t state,
                                      const size_t round_index) {
    for (size_t i = 0; i < PHK1024_NUM_SUBSTATES; ++i) {
        const uint8_t *round_key = ctx->round_keys[round_index]
                                   + i * PHK_NUM_SUBSTATE_BYTES;
        uint8_t *substate = &(state[i * PHK_NUM_SUBSTATE_BYTES]);
        aes_encrypt_last_round(substate, substate, round_key);
    }

    add_round_tweak_256(ctx, state, round_index);
    add_round_constant(state, round_index);
}

// --------------------------------------------------------------------

static void phk1024_encrypt_step(const phk1024_ctx *ctx,
                                phk1024_state_t state,
                                const size_t step_index) {
    for (size_t i = 0; i < PHK_NUM_AES_ROUNDS_PER_STEP; ++i) {
        const size_t round_index = get_round_index(step_index, i + 1);
        phk1024_encrypt_round(ctx, state, round_index);
    }

    if ((step_index & 1) == 0) {
        phk1024_permute_state_0(state);
    } else {
        phk1024_permute_state_1(state);
    }
}

// --------------------------------------------------------------------

static void phk1024_256_encrypt_step(const phk1024_256_ctx *ctx,
                                phk1024_state_t state,
                                const size_t step_index) {
    for (size_t i = 0; i < PHK_NUM_AES_ROUNDS_PER_STEP; ++i) {
        const size_t round_index = get_round_index(step_index, i + 1);
        phk1024_256_encrypt_round(ctx, state, round_index);
    }

    if ((step_index & 1) == 0) {
        phk1024_permute_state_0(state);
    } else {
        phk1024_permute_state_1(state);
    }
}

// --------------------------------------------------------------------

static void phk1024_encrypt_last_step(const phk1024_ctx *ctx,
                                     phk1024_state_t state,
                                     const size_t step_index) {
    for (size_t i = 0; i < PHK_NUM_AES_ROUNDS_PER_STEP - 1; ++i) {
        const size_t round_index = get_round_index(step_index, i + 1);
        phk1024_encrypt_round(ctx, state, round_index);
    }

    const size_t round_index = get_round_index(
        step_index, PHK_NUM_AES_ROUNDS_PER_STEP - 1
    );
    phk1024_encrypt_last_round(ctx, state, round_index + 1);
}

// --------------------------------------------------------------------

static void phk1024_256_encrypt_last_step(const phk1024_256_ctx *ctx,
                                     phk1024_state_t state,
                                     const size_t step_index) {
    for (size_t i = 0; i < PHK_NUM_AES_ROUNDS_PER_STEP - 1; ++i) {
        const size_t round_index = get_round_index(step_index, i + 1);
        phk1024_256_encrypt_round(ctx, state, round_index);
    }

    const size_t round_index = get_round_index(
        step_index, PHK_NUM_AES_ROUNDS_PER_STEP - 1
    );
    phk1024_256_encrypt_last_round(ctx, state, round_index + 1);
}

// --------------------------------------------------------------------

static void phk1024_decrypt_last_round(const phk1024_ctx *ctx,
                                      phk1024_state_t state,
                                      const size_t round_index) {
    add_round_tweak(ctx, state, round_index);
    add_round_constant(state, round_index);

    for (size_t i = 0; i < PHK1024_NUM_SUBSTATES; ++i) {
        const uint8_t *round_key = ctx->round_keys[round_index]
                                   + i * PHK_NUM_SUBSTATE_BYTES;
        uint8_t *substate = state + i * PHK_NUM_SUBSTATE_BYTES;
        aes_decrypt_last_round(substate, substate, round_key);
    }
}

// --------------------------------------------------------------------

static void phk1024_256_decrypt_last_round(const phk1024_256_ctx *ctx,
                                      phk1024_state_t state,
                                      const size_t round_index) {
    add_round_tweak_256(ctx, state, round_index);
    add_round_constant(state, round_index);

    for (size_t i = 0; i < PHK1024_NUM_SUBSTATES; ++i) {
        const uint8_t *round_key = ctx->round_keys[round_index]
                                   + i * PHK_NUM_SUBSTATE_BYTES;
        uint8_t *substate = state + i * PHK_NUM_SUBSTATE_BYTES;
        aes_decrypt_last_round(substate, substate, round_key);
    }
}

// --------------------------------------------------------------------

static void phk1024_decrypt_round(const phk1024_ctx *ctx,
                                 phk1024_state_t state,
                                 const size_t round_index) {
    add_round_tweak(ctx, state, round_index);
    add_round_constant(state, round_index);

    for (size_t i = 0; i < PHK1024_NUM_SUBSTATES; ++i) {
        const uint8_t *round_key = ctx->round_keys[round_index]
                                   + i * PHK_NUM_SUBSTATE_BYTES;
        uint8_t *substate = &(state[i * PHK_NUM_SUBSTATE_BYTES]);
        aes_decrypt_round(substate, substate, round_key);
    }
}

// --------------------------------------------------------------------

static void phk1024_256_decrypt_round(const phk1024_256_ctx *ctx,
                                 phk1024_state_t state,
                                 const size_t round_index) {
    add_round_tweak_256(ctx, state, round_index);
    add_round_constant(state, round_index);

    for (size_t i = 0; i < PHK1024_NUM_SUBSTATES; ++i) {
        const uint8_t *round_key = ctx->round_keys[round_index]
                                   + i * PHK_NUM_SUBSTATE_BYTES;
        uint8_t *substate = &(state[i * PHK_NUM_SUBSTATE_BYTES]);
        aes_decrypt_round(substate, substate, round_key);
    }
}

// --------------------------------------------------------------------

static void phk1024_decrypt_step(const phk1024_ctx *ctx,
                                phk1024_state_t state,
                                const size_t step_index) {
    if ((step_index & 1) == 0) {
        phk1024_invert_permute_state_0(state);
    } else {
        phk1024_invert_permute_state_1(state);
    }

    for (int i = PHK_NUM_AES_ROUNDS_PER_STEP - 1; i >= 0; --i) {
        const size_t round_index = get_round_index(step_index, (size_t) i + 1);
        phk1024_decrypt_round(ctx, state, round_index);
    }
}

// --------------------------------------------------------------------

static void phk1024_256_decrypt_step(const phk1024_256_ctx *ctx,
                                phk1024_state_t state,
                                const size_t step_index) {
    if ((step_index & 1) == 0) {
        phk1024_invert_permute_state_0(state);
    } else {
        phk1024_invert_permute_state_1(state);
    }

    for (int i = PHK_NUM_AES_ROUNDS_PER_STEP - 1; i >= 0; --i) {
        const size_t round_index = get_round_index(step_index, (size_t) i + 1);
        phk1024_256_decrypt_round(ctx, state, round_index);
    }
}

// --------------------------------------------------------------------

static void phk1024_decrypt_last_step(const phk1024_ctx *ctx,
                                     phk1024_state_t state,
                                     const size_t step_index) { // 7
    size_t round_index = get_round_index(
        step_index, PHK_NUM_AES_ROUNDS_PER_STEP - 1
    );
    phk1024_decrypt_last_round(ctx, state, round_index + 1);

    for (int i = PHK_NUM_AES_ROUNDS_PER_STEP - 2; i >= 0; --i) {
        round_index = get_round_index(step_index, (size_t)i + 1);
        phk1024_decrypt_round(ctx, state, round_index);
    }
}

// --------------------------------------------------------------------

static void phk1024_256_decrypt_last_step(const phk1024_256_ctx *ctx,
                                     phk1024_state_t state,
                                     const size_t step_index) { // 7
    size_t round_index = get_round_index(
        step_index, PHK_NUM_AES_ROUNDS_PER_STEP - 1
    );
    phk1024_256_decrypt_last_round(ctx, state, round_index + 1);

    for (int i = PHK_NUM_AES_ROUNDS_PER_STEP - 2; i >= 0; --i) {
        round_index = get_round_index(step_index, (size_t)i + 1);
        phk1024_256_decrypt_round(ctx, state, round_index);
    }
}

// --------------------------------------------------------------------

static void phk1024_tau(phk1024_tweak_t tweak) {
    phk1024_tweak_t result;

    for (size_t i = 0; i < PHK1024_NUM_TWEAK_BYTES; ++i) {
        result[i] = tweak[PHK1024_TAU_PERMUTATION[i]];
    }

    memcpy(tweak, result, PHK1024_NUM_TWEAK_BYTES);
}

// --------------------------------------------------------------------

static void phk1024_kappa(phk1024_key_t key, const size_t index) {
    if ((index & 1) == 0) {
        phk1024_permute_state_0(key);
    } else {
        phk1024_permute_state_1(key);
    }

    for (size_t i = 0; i < PHK1024_NUM_SUBSTATES; ++i) {
        uint8_t *substate = key + i * PHK_NUM_SUBSTATE_BYTES;
        phk1024_tau(substate);
    }

    gf2_8_double_array(key, PHK_GF2_8_MODULUS, PHK1024_NUM_STATE_BYTES);
}

// --------------------------------------------------------------------
// API
// --------------------------------------------------------------------

// PHK-1024-1024

void phk1024_set_key_encrypt(phk1024_ctx *ctx, const phk1024_key_t key) {
    phk1024_key_t k;
    memcpy(k, key, PHK1024_NUM_KEY_BYTES);

    uint8_t *round_key;

    for (size_t i = 0; i < PHK1024_NUM_ROUNDS; ++i) {
        round_key = ctx->round_keys[i];
        memcpy(round_key, k, PHK1024_NUM_STATE_BYTES);
        phk1024_kappa(k, i);
    }

    round_key = ctx->round_keys[PHK1024_NUM_ROUNDS];
    memcpy(round_key, k, PHK1024_NUM_STATE_BYTES);
}

// --------------------------------------------------------------------

void phk1024_set_key_decrypt(phk1024_ctx *ctx, const phk1024_key_t key) {
    phk1024_set_key_encrypt(ctx, key);
}

// --------------------------------------------------------------------

void phk1024_set_tweak_encrypt(phk1024_ctx *ctx, const phk1024_tweak_t tweak) {
    phk1024_tweak_t t;
    memcpy(t, tweak, PHK1024_NUM_TWEAK_BYTES);

    uint8_t *round_tweak;

    for (size_t i = 0; i < PHK1024_NUM_ROUNDS; ++i) {
        round_tweak = ctx->round_tweaks[i];
        memcpy(round_tweak, t, PHK1024_NUM_TWEAK_BYTES);

        phk1024_tau(t);
    }

    round_tweak = ctx->round_tweaks[PHK1024_NUM_ROUNDS];
    memcpy(round_tweak, t, PHK1024_NUM_TWEAK_BYTES);
}

// --------------------------------------------------------------------

void phk1024_set_tweak_decrypt(phk1024_ctx *ctx, const phk1024_tweak_t tweak) {
    phk1024_set_tweak_encrypt(ctx, tweak);
}

// --------------------------------------------------------------------

void phk1024_encrypt(const phk1024_ctx *ctx, phk1024_state_t state) {
    add_round_tweakey(ctx, state, 0);

    for (size_t i = 0; i < PHK1024_NUM_STEPS - 1; ++i) {
        phk1024_encrypt_step(ctx, state, i);
    }

    phk1024_encrypt_last_step(ctx, state, PHK1024_NUM_STEPS - 1);
}

// --------------------------------------------------------------------

void phk1024_decrypt(const phk1024_ctx *ctx, phk1024_state_t state) {
    phk1024_decrypt_last_step(ctx, state, PHK1024_NUM_STEPS - 1);

    for (int i = PHK1024_NUM_STEPS - 2; i >= 0; --i) {
        phk1024_decrypt_step(ctx, state, (size_t) i);
    }

    add_round_tweakey(ctx, state, 0);
}

// --------------------------------------------------------------------

// PHK-1024-256

void phk1024_256_set_key_encrypt(phk1024_256_ctx *ctx, const phk1024_256_key_t key) {
    phk1024_key_t k;
    memcpy(k, key, PHK1024_256_NUM_KEY_BYTES);

    phk_expand_key_ma(k);
    phk_expand_key_mb(k);
    phk_expand_key_mc(k);


    uint8_t *round_key;

    for (size_t i = 0; i < PHK1024_256_NUM_ROUNDS; ++i) {
        round_key = ctx->round_keys[i];
        memcpy(round_key, k, PHK1024_NUM_STATE_BYTES);
        phk1024_kappa(k, i);
    }

    round_key = ctx->round_keys[PHK1024_256_NUM_ROUNDS];
    memcpy(round_key, k, PHK1024_NUM_STATE_BYTES);
}

// --------------------------------------------------------------------

void phk1024_256_set_key_decrypt(phk1024_256_ctx *ctx, const phk1024_256_key_t key) {
    phk1024_256_set_key_encrypt(ctx, key);
}

// --------------------------------------------------------------------

void phk1024_256_set_tweak_encrypt(phk1024_256_ctx *ctx, const phk1024_tweak_t tweak) {
    phk1024_tweak_t t;
    memcpy(t, tweak, PHK1024_NUM_TWEAK_BYTES);

    uint8_t *round_tweak;

    for (size_t i = 0; i < PHK1024_256_NUM_ROUNDS; ++i) {
        round_tweak = ctx->round_tweaks[i];
        memcpy(round_tweak, t, PHK1024_NUM_TWEAK_BYTES);

        phk1024_tau(t);
    }

    round_tweak = ctx->round_tweaks[PHK1024_256_NUM_ROUNDS];
    memcpy(round_tweak, t, PHK1024_NUM_TWEAK_BYTES);
}

// --------------------------------------------------------------------

void phk1024_256_set_tweak_decrypt(phk1024_256_ctx *ctx, const phk1024_tweak_t tweak) {
    phk1024_256_set_tweak_encrypt(ctx, tweak);
}

// --------------------------------------------------------------------

void phk1024_256_encrypt(const phk1024_256_ctx *ctx, phk1024_state_t state) {
    add_round_tweakey_256(ctx, state, 0);

    for (size_t i = 0; i < PHK1024_256_NUM_STEPS - 1; ++i) {
        phk1024_256_encrypt_step(ctx, state, i);
    }

    phk1024_256_encrypt_last_step(ctx, state, PHK1024_256_NUM_STEPS - 1);
}

// --------------------------------------------------------------------

void phk1024_256_decrypt(const phk1024_256_ctx *ctx, phk1024_state_t state) {
    phk1024_256_decrypt_last_step(ctx, state, PHK1024_256_NUM_STEPS - 1);

    for (int i = PHK1024_256_NUM_STEPS - 2; i >= 0; --i) {
        phk1024_256_decrypt_step(ctx, state, (size_t) i);
    }

    add_round_tweakey_256(ctx, state, 0);
}


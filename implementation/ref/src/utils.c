/**
// This is free and unencumbered software released into the public domain.
//
// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a compiled
// binary, for any purpose, commercial or non-commercial, and by any
// means.
//
// In jurisdictions that recognize copyright laws, the author or authors
// of this software dedicate any and all copyright interest in the
// software to the public domain. We make this dedication for the benefit
// of the public at large and to the detriment of our heirs and
// successors. We intend this dedication to be an overt act of
// relinquishment in perpetuity of all present and future rights to this
// software under copyright law.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
// OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//
// For more information, please refer to <http://unlicense.org/>
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>

#include "utils.h"

// --------------------------------------------------------------------

int assert_equal_uint32(const uint32_t *left,
                        const uint32_t *right,
                        const size_t num_words) {
    int result = 1;

    for (size_t i = 0; i < num_words; ++i) {
        result &= left[i] == right[i];
    }

    return result;
}

// --------------------------------------------------------------------

int assert_equal_uint64(const uint64_t *left,
                        const uint64_t *right,
                        const size_t num_words) {
    int result = 1;

    for (size_t i = 0; i < num_words; ++i) {
        result &= left[i] == right[i];
    }

    return result;
}

// --------------------------------------------------------------------

void print_hex_uint32(const uint32_t *array, const size_t num_words) {
    for (size_t i = 0; i < num_words; ++i) {
        printf("%08x ", array[i]);
    }

    puts("");
}

// --------------------------------------------------------------------

void print_hex_uint64(const uint64_t *array, const size_t num_words) {
    for (size_t i = 0; i < num_words; ++i) {
        printf("%016lx ", array[i]);
    }

    puts("");
}

// --------------------------------------------------------------------

void print_hex(const uint8_t* array, const size_t num_bytes) {
    for (size_t i = 0; i < num_bytes; ++i) {
        printf("%02x  ", array[i]);
    }

    puts("");
}

// ---------------------------------------------------------

uint64_t uint32_to_uint64(const uint32_t *input) {
    return ((uint64_t) (input[0]) << 32) |
           ((uint64_t) input[1]);
}

// ---------------------------------------------------------

void uint64_to_uint32(uint32_t *output, const uint64_t input) {
    output[0] = (uint32_t) ((input >> 32) & 0xFFFFFFFF);
    output[1] = (uint32_t) (input & 0xFFFFFFFF);
}

// --------------------------------------------------------------------

void swap_words32(uint32_t* left, uint32_t* right) {
    *left ^= *right; // i ^ j, j
    *right ^= *left; // i ^ j, i
    *left ^= *right; // j, i
}

// --------------------------------------------------------------------

void swap_words64(uint64_t *left, uint64_t *right) {
    *left ^= *right; // i ^ j, j
    *right ^= *left; // i ^ j, i
    *left ^= *right; // j, i
}

// --------------------------------------------------------------------

void swap_words(uint64_t *array, const size_t i, const size_t j) {
    array[i] ^= array[j]; // i ^ j, j
    array[j] ^= array[i]; // i ^ j, i
    array[i] ^= array[j]; // j, i
}

// --------------------------------------------------------------------

uint32_t rotate_left32(uint32_t x, size_t r) {
    return (x >> (32 - r)) | (x << r);
}

// --------------------------------------------------------------------

uint32_t rotate_right32(uint32_t x, size_t r) {
    return (x << (32 - r)) | (x >> r);
}

// --------------------------------------------------------------------

uint64_t rotate_left64(const uint64_t x, const size_t r) {
    return (x >> (64 - r)) | (x << r);
}

// --------------------------------------------------------------------

uint64_t rotate_right64(const uint64_t x, const size_t r) {
    return (x << (64 - r)) | (x >> r);
}

// --------------------------------------------------------------------

uint32_t bytes_to_uint32(const uint8_t *input, size_t num_bytes) {
    uint32_t result = 0;
    size_t shift = 8 * num_bytes;

    for (size_t i = 0; i < num_bytes; ++i) {
        shift -= 8;
        result |= (uint32_t) input[i] << shift;
    }

    return result;
}

// --------------------------------------------------------------------

void bytes_to_uint32_words(uint32_t *output_words,
                           const uint8_t *input_bytes,
                           size_t num_bytes,
                           size_t num_bytes_per_word) {
    const size_t num_words = num_bytes / num_bytes_per_word;
    const uint8_t *input_position = input_bytes;

    for (size_t i = 0; i < num_words; ++i) {
        output_words[i] = bytes_to_uint32(input_position, num_bytes_per_word);
        input_position += num_bytes_per_word;
    }
}

// --------------------------------------------------------------------

void uint32_to_bytes(uint8_t *result,
                     const uint32_t input_word,
                     const size_t num_bytes) {
    size_t shift = 8 * num_bytes;

    for (size_t i = 0; i < num_bytes; ++i) {
        shift -= 8;
        result[i] = (uint8_t) ((input_word >> shift) & 0xff);
    }
}

// --------------------------------------------------------------------

void uint32_words_to_bytes(uint8_t *result,
                           const uint32_t *input_words,
                           const size_t num_words,
                           const size_t num_bytes_per_word) {
    uint8_t *result_position = result;

    for (size_t i = 0; i < num_words; ++i) {
        uint32_to_bytes(
            result_position, input_words[i], num_bytes_per_word
        );
        result_position += num_bytes_per_word;
    }
}

// --------------------------------------------------------------------

void bytes_to_uint64_words(uint64_t *output_words,
                           const uint8_t *input_bytes,
                           const size_t num_bytes,
                           const size_t num_bytes_per_word) {
    const size_t num_words = num_bytes / num_bytes_per_word;
    const uint8_t* input_position = input_bytes;

    for (size_t i = 0; i < num_words; ++i) {
        output_words[i] = bytes_to_uint64(input_position, num_bytes_per_word);
        input_position += num_bytes_per_word;
    }
}

// --------------------------------------------------------------------

uint64_t bytes_to_uint64(const uint8_t *input, const size_t num_bytes) {
    uint64_t result = 0;
    size_t shift = 8 * num_bytes;

    for (size_t i = 0; i < num_bytes; ++i) {
        shift -= 8;
        result |= (uint64_t)input[i] << shift;
    }

    return result;
}

// --------------------------------------------------------------------

void uint64_words_to_bytes(uint8_t *result,
                           const uint64_t *input_words,
                           const size_t num_words,
                           const size_t num_bytes_per_word) {
    uint8_t* result_position = result;

    for (size_t i = 0; i < num_words; ++i) {
        uint64_word_to_bytes(result_position, input_words[i],
                             num_bytes_per_word);
        result_position += num_bytes_per_word;
    }
}

// --------------------------------------------------------------------

void uint64_word_to_bytes(uint8_t *result,
                          const uint64_t input_word,
                          const size_t num_bytes) {
    size_t shift = 8 * num_bytes;

    for (size_t i = 0; i < num_bytes; ++i) {
        shift -= 8;
        result[i] = (uint8_t)((input_word >> shift) & 0xff);
    }
}

// --------------------------------------------------------------------

void shift_words_left(uint64_t* target,
                      const size_t num_words,
                      const size_t num_shifted_bits) {
    if (num_words == 0) {
        return;
    }

    uint64_t next;

    for (size_t i = 0; i < num_words - 1; ++i) {
        next = (target[i + 1] >> (64 - num_shifted_bits));
        target[i] = (target[i] << num_shifted_bits) | next;
    }

    target[num_words - 1] <<= num_shifted_bits;
}

// --------------------------------------------------------------------

void xor_arrays(uint8_t* target,
                const uint8_t* left,
                const uint8_t* right,
                const size_t num_bytes) {
    for (size_t i = 0; i < num_bytes; ++i) {
        target[i] = left[i] ^ right[i];
    }
}

// --------------------------------------------------------------------

uint8_t gf2_8_double(uint8_t value, uint8_t modulus) {
    const uint8_t msb = value >> 7;
    return ((uint8_t)(value & 0x7F) << 1) ^ (modulus * msb);
}

// --------------------------------------------------------------------

void gf2_8_double_array(uint8_t* array, uint8_t modulus, size_t num_bytes) {
    for (size_t i = 0; i < num_bytes; ++i) {
        array[i] = gf2_8_double(array[i], modulus);
    }
}

// --------------------------------------------------------------------

void zeroize(uint8_t* target, size_t num_bytes) {
    memset(target, 0x00, num_bytes);
}

// --------------------------------------------------------------------

#include <memory.h>
#include "phk-512.h"
#include "aes.h"
#include "utils.h"

#include <stdio.h>
// --------------------------------------------------------------------

static void add_round_tweak(const phk512_ctx *ctx,
                            phk512_state_t state,
                            const size_t round_index) {
    const uint8_t *round_tweak = ctx->round_tweaks[round_index];

    for (size_t i = 0; i < PHK512_NUM_SUBSTATES; ++i) {
        uint8_t *state_word = state + i * PHK_NUM_SUBSTATE_BYTES;
        xor_arrays(state_word, state_word, round_tweak, PHK_NUM_SUBSTATE_BYTES);
    }
}

// --------------------------------------------------------------------

static void add_round_key(const phk512_ctx *ctx,
                          phk512_state_t state,
                          const size_t round_index) {
    const uint8_t *round_key = ctx->round_keys[round_index];
    xor_arrays(state, state, round_key, PHK512_NUM_STATE_BYTES);
}

// --------------------------------------------------------------------

static void add_round_constant(phk512_state_t state,
                               const size_t round_index) {
    const uint8_t *round_constant = PHK512_ROUND_CONSTANTS[round_index];
    xor_arrays(state, state, round_constant, PHK_NUM_SUBSTATE_BYTES);
}

// --------------------------------------------------------------------

static void add_round_tweakey(const phk512_ctx *ctx,
                              phk512_state_t state,
                              const size_t round_index) {
    add_round_key(ctx, state, round_index);
    add_round_tweak(ctx, state, round_index);
    add_round_constant(state, round_index);
}

// --------------------------------------------------------------------

static size_t get_round_index(const size_t step_index,
                              const size_t round_index) {
    return step_index * PHK_NUM_AES_ROUNDS_PER_STEP + round_index;
}

// --------------------------------------------------------------------

static void permute_words(uint8_t *state,
                          const uint8_t *permutation,
                          const size_t num_words) {
    uint8_t temp[4 * num_words];

    for (size_t i = 0; i < num_words; ++i) {
        const size_t to_index = permutation[i];
        uint8_t *word_start = &(state[4 * to_index]);
        uint8_t *word_to = &(temp[4 * i]);
        memcpy(word_to, word_start, 4);
    }

    memcpy(state, temp, PHK512_NUM_STATE_BYTES);
}

// --------------------------------------------------------------------

static void phk512_permute_state(phk512_state_t state) {
    permute_words(state, PHK512_PERMUTATION, PHK512_NUM_WORDS);
}

// --------------------------------------------------------------------

static void phk512_invert_permute_state(phk512_state_t state) {
    permute_words(state, PHK512_INVERSE_PERMUTATION, PHK512_NUM_WORDS);
}

// --------------------------------------------------------------------

static void phk512_encrypt_round(const phk512_ctx *ctx,
                                 phk512_state_t state,
                                 const size_t round_index) {
    for (size_t i = 0; i < PHK512_NUM_SUBSTATES; ++i) {
        const uint8_t *round_key = ctx->round_keys[round_index]
                                   + i * PHK_NUM_SUBSTATE_BYTES;
        uint8_t *substate = &(state[i * PHK_NUM_SUBSTATE_BYTES]);
        aes_encrypt_round(substate, substate, round_key);
    }

    add_round_tweak(ctx, state, round_index);
    add_round_constant(state, round_index);
}

// --------------------------------------------------------------------

static void phk512_encrypt_last_round(const phk512_ctx *ctx,
                                      phk512_state_t state,
                                      const size_t round_index) {
    for (size_t i = 0; i < PHK512_NUM_SUBSTATES; ++i) {
        const uint8_t *round_key = ctx->round_keys[round_index]
                                   + i * PHK_NUM_SUBSTATE_BYTES;
        uint8_t *substate = &(state[i * PHK_NUM_SUBSTATE_BYTES]);
        aes_encrypt_last_round(substate, substate, round_key);
    }

    add_round_tweak(ctx, state, round_index);
    add_round_constant(state, round_index);
}

// --------------------------------------------------------------------

static void phk512_encrypt_step(const phk512_ctx *ctx,
                                phk512_state_t state,
                                const size_t step_index) {
    for (size_t i = 0; i < PHK_NUM_AES_ROUNDS_PER_STEP; ++i) {
        const size_t round_index = get_round_index(step_index, i + 1);
        phk512_encrypt_round(ctx, state, round_index);
    }

    phk512_permute_state(state);
}

// --------------------------------------------------------------------

static void phk512_encrypt_last_step(const phk512_ctx *ctx,
                                     phk512_state_t state,
                                     const size_t step_index) {
    for (size_t i = 0; i < PHK_NUM_AES_ROUNDS_PER_STEP - 1; ++i) {
        const size_t round_index = get_round_index(step_index, i + 1);
        phk512_encrypt_round(ctx, state, round_index);
    }

    const size_t round_index = get_round_index(
        step_index, PHK_NUM_AES_ROUNDS_PER_STEP - 1
    );
    phk512_encrypt_last_round(ctx, state, round_index + 1);
}

// --------------------------------------------------------------------

static void phk512_decrypt_last_round(const phk512_ctx *ctx,
                                      phk512_state_t state,
                                      const size_t round_index) {
    add_round_tweak(ctx, state, round_index);
    add_round_constant(state, round_index);

    for (size_t i = 0; i < PHK512_NUM_SUBSTATES; ++i) {
        const uint8_t *round_key = ctx->round_keys[round_index]
                                   + i * PHK_NUM_SUBSTATE_BYTES;
        uint8_t *substate = state + i * PHK_NUM_SUBSTATE_BYTES;
        aes_decrypt_last_round(substate, substate, round_key);
    }
}

// --------------------------------------------------------------------

static void phk512_decrypt_round(const phk512_ctx *ctx,
                                 phk512_state_t state,
                                 const size_t round_index) {
    add_round_tweak(ctx, state, round_index);
    add_round_constant(state, round_index);

    for (size_t i = 0; i < PHK512_NUM_SUBSTATES; ++i) {
        const uint8_t *round_key = ctx->round_keys[round_index]
                                   + i * PHK_NUM_SUBSTATE_BYTES;
        uint8_t *substate = &(state[i * PHK_NUM_SUBSTATE_BYTES]);
        aes_decrypt_round(substate, substate, round_key);
    }
}

// --------------------------------------------------------------------

static void phk512_decrypt_step(const phk512_ctx *ctx,
                                phk512_state_t state,
                                const size_t step_index) {
    phk512_invert_permute_state(state);

    for (int i = PHK_NUM_AES_ROUNDS_PER_STEP - 1; i >= 0; --i) {
        const size_t round_index = get_round_index(step_index, (size_t) i + 1);
        phk512_decrypt_round(ctx, state, round_index);
    }
}

// --------------------------------------------------------------------

static void phk512_decrypt_last_step(const phk512_ctx *ctx,
                                     phk512_state_t state,
                                     const size_t step_index) {
    size_t round_index = get_round_index(
        step_index, PHK_NUM_AES_ROUNDS_PER_STEP - 1
    );
    phk512_decrypt_last_round(ctx, state, round_index + 1);

    for (int i = PHK_NUM_AES_ROUNDS_PER_STEP - 2; i >= 0; --i) {
        round_index = get_round_index(step_index, (size_t)i + 1);
        phk512_decrypt_round(ctx, state, round_index);
    }
}

// --------------------------------------------------------------------

static void phk512_tau(phk512_tweak_t tweak) {
    phk512_tweak_t result;

    for (size_t i = 0; i < PHK512_NUM_TWEAK_BYTES; ++i) {
        result[i] = tweak[PHK512_TAU_PERMUTATION[i]];
    }

    memcpy(tweak, result, PHK512_NUM_TWEAK_BYTES);
}

// --------------------------------------------------------------------

static void phk512_kappa(phk512_key_t key) {
    phk512_permute_state(key);

    for (size_t i = 0; i < PHK512_NUM_SUBSTATES; ++i) {
        uint8_t *substate = key + i * PHK_NUM_SUBSTATE_BYTES;
        phk512_tau(substate);
    }

    gf2_8_double_array(key, PHK_GF2_8_MODULUS, PHK512_NUM_STATE_BYTES);
}

// --------------------------------------------------------------------
// API
// --------------------------------------------------------------------

void phk512_set_key_encrypt(phk512_ctx *ctx, const phk512_key_t key) {
    phk512_key_t k;
    memcpy(k, key, PHK512_NUM_KEY_BYTES);

    uint8_t *round_key;

    for (size_t i = 0; i < PHK512_NUM_ROUNDS; ++i) {
        round_key = ctx->round_keys[i];
        memcpy(round_key, k, PHK512_NUM_STATE_BYTES);
        phk512_kappa(k);
    }

    round_key = ctx->round_keys[PHK512_NUM_ROUNDS];
    memcpy(round_key, k, PHK512_NUM_STATE_BYTES);
}

// --------------------------------------------------------------------

void phk512_256_set_key_encrypt(phk512_ctx *ctx, const phk512_key_t key) {
    phk512_key_t k;
    memcpy(k, key, PHK512_NUM_KEY_BYTES);
    phk_expand_key_ma(k);


    uint8_t *round_key;

    for (size_t i = 0; i < PHK512_NUM_ROUNDS; ++i) {
        round_key = ctx->round_keys[i];
        memcpy(round_key, k, PHK512_NUM_STATE_BYTES);
        phk512_kappa(k);
    }

    round_key = ctx->round_keys[PHK512_NUM_ROUNDS];
    memcpy(round_key, k, PHK512_NUM_STATE_BYTES);
}

// --------------------------------------------------------------------

void phk512_set_key_decrypt(phk512_ctx *ctx, const phk512_key_t key) {
    phk512_set_key_encrypt(ctx, key);
}

// --------------------------------------------------------------------

void phk512_256_set_key_decrypt(phk512_ctx *ctx, const phk512_key_t key) {
    phk512_256_set_key_encrypt(ctx, key);
}

// --------------------------------------------------------------------

void phk512_set_tweak_encrypt(phk512_ctx *ctx, const phk512_tweak_t tweak) {
    phk512_tweak_t t;
    memcpy(t, tweak, PHK512_NUM_TWEAK_BYTES);

    uint8_t *round_tweak;

    for (size_t i = 0; i < PHK512_NUM_ROUNDS; ++i) {
        round_tweak = ctx->round_tweaks[i];
        memcpy(round_tweak, t, PHK512_NUM_TWEAK_BYTES);

        phk512_tau(t);
    }

    round_tweak = ctx->round_tweaks[PHK512_NUM_ROUNDS];
    memcpy(round_tweak, t, PHK512_NUM_TWEAK_BYTES);
}

// --------------------------------------------------------------------

void phk512_set_tweak_decrypt(phk512_ctx *ctx, const phk512_tweak_t tweak) {
    phk512_set_tweak_encrypt(ctx, tweak);
}

// --------------------------------------------------------------------

void phk512_encrypt(const phk512_ctx *ctx, phk512_state_t state) {
    add_round_tweakey(ctx, state, 0);

    for (size_t i = 0; i < PHK512_NUM_STEPS - 1; ++i) {
        phk512_encrypt_step(ctx, state, i);
    }

    phk512_encrypt_last_step(ctx, state, PHK512_NUM_STEPS - 1);
}

// --------------------------------------------------------------------

void phk512_256_encrypt(const phk512_ctx *ctx, phk512_state_t state) {
    add_round_tweakey(ctx, state, 0);

    for (size_t i = 0; i < PHK512_256_NUM_STEPS - 1; ++i) {
        phk512_encrypt_step(ctx, state, i);
    }

    phk512_encrypt_last_step(ctx, state, PHK512_256_NUM_STEPS - 1);
}

// --------------------------------------------------------------------

void phk512_decrypt(const phk512_ctx *ctx, phk512_state_t state) {

    phk512_decrypt_last_step(ctx, state, PHK512_NUM_STEPS - 1);

    for (int i = PHK512_NUM_STEPS - 2; i >= 0; --i) {
        phk512_decrypt_step(ctx, state, (size_t) i);
    }

    add_round_tweakey(ctx, state, 0);
}

// --------------------------------------------------------------------

void phk512_256_decrypt(const phk512_ctx *ctx, phk512_state_t state) {

    phk512_decrypt_last_step(ctx, state, PHK512_256_NUM_STEPS - 1);

    for (int i = PHK512_256_NUM_STEPS - 2; i >= 0; --i) {
        phk512_decrypt_step(ctx, state, (size_t) i);
    }

    add_round_tweakey(ctx, state, 0);
}


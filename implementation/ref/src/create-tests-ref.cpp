
#include <cstdint>
#include <cstring>
#include <iostream>
#include <string>
#include <time.h>
#include <stdlib.h>

extern "C" {
  #include "phk-256.h"
  #include "phk-512.h"
  #include "phk-1024.h"
}

// --------------------------------------------------------------------

#include "ctrt.h"
#include "misc.h"

// --------------------------------------------------------------------

void create_test_vectors_128bit_addition (int const num_tests) {
  std::cout << "[" << std::endl;


  int i;
  int testcounter;
  uint8_t lhs[16];
  uint8_t rhs;

  srand(time(NULL));

  for (testcounter = 0; testcounter < num_tests; ++testcounter) {
    for (i = 0; i < 16; ++i) {
      lhs[i] = (uint8_t) rand();
    }
    rhs = (uint8_t) rand() % 8;

    std::cout << "\t{" << std::endl;
    std::cout << "\t\t\"lhs\" : \"" << std::hex;
    for (i=0; i < 16; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) lhs[i];
    }
    std::cout << "\"," << std::endl;

    std::cout << "\t\t\"rhs\" : \"" << std::hex;
    std::cout << std::setw(2) << std::setfill('0') << (int) rhs;
    std::cout << "\"," << std::endl;


    add_uint8_t_arr(lhs, rhs, 16);

    std::cout << "\t\t\"res\" : \"" << std::hex;
    for (i=0; i < 16; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) lhs[i];
    }
    std::cout << "\"" << std::endl;

    std::cout << (testcounter == num_tests - 1 ? "\t}" : "\t},") << std::endl;
  }


  std::cout << "]" << std::endl;
}

// --------------------------------------------------------------------

void create_test_vectors_ctrt_phk_256_128 (int const num_tests) {
  std::cout << "[" << std::endl;


  uint8_t nonce[PHK256_NUM_STATE_BYTES];
  phk256_key_t key;     // uint8_t[]
  phk256_tweak_t tweak; // uint8_t[]
  int i, size, testcounter;

  srand(time(NULL));

  for (testcounter = 0; testcounter < num_tests; ++testcounter) {
    //size = (rand() % 1000) * PHK256_NUM_STATE_BYTES;
    size = (4*(rand() % 250 + 1)) * PHK256_NUM_STATE_BYTES;

    uint8_t plaintext[size];
    uint8_t ciphertext[size];

    for (i = 0; i < size; ++i) {
      plaintext[i] = (uint8_t) rand();
    }

    for (i = 0; i < PHK256_NUM_KEY_BYTES; ++i) {
      key[i] = (uint8_t) rand();
      nonce[i] = (uint8_t) rand();
    }

    for (i = 0; i < PHK256_NUM_TWEAK_BYTES; ++i) {
      tweak[i] = (uint8_t) rand();
    }

    std::cout << "\t{" << std::endl;
    std::cout << "\t\t\"Plaintext\" : \"" << std::hex;
    for (i=0; i < size; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) plaintext[i];
    }
    std::cout << "\"," << std::endl;

    std::cout << "\t\t\"Key\" : \"" << std::hex;
    for (i=0; i < PHK256_NUM_KEY_BYTES; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) key[i];
    }
    std::cout << "\"," << std::endl;

    std::cout << "\t\t\"Tweak\" : \"" << std::hex;
    for (i=0; i < PHK256_NUM_TWEAK_BYTES; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) tweak[i];
    }
    std::cout << "\"," << std::endl;

    std::cout << "\t\t\"Nonce\" : \"" << std::hex;
    for (i=0; i < PHK256_NUM_STATE_BYTES; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) nonce[i];
    }
    std::cout << "\"," << std::endl;

    ctrt_phk_256_128_enc(key, tweak, nonce, plaintext, ciphertext, size / PHK256_NUM_STATE_BYTES);

    std::cout << "\t\t\"Ciphertext\" : \"" << std::hex;
    for (i=0; i < size; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) ciphertext[i];
    }
    std::cout << "\"" << std::endl;

    std::cout << (testcounter == num_tests - 1 ? "\t}" : "\t},") << std::endl;
  }

  std::cout << "]" << std::endl;
}

// --------------------------------------------------------------------

void create_test_vectors_phk_1024_256 (int const num_tests) {
  std::cout << "[" << std::endl;


  phk1024_state_t plaintext; // uint8_t[]
  phk1024_state_t ciphertext;
  phk1024_256_key_t key;     // uint8_t[]
  phk1024_tweak_t tweak; // uint8_t[]
  phk1024_256_ctx ctx;
  int i;
  int testcounter;

  srand(time(NULL));

  for (testcounter = 0; testcounter < num_tests; ++testcounter) {
    for (i = 0; i < PHK1024_NUM_STATE_BYTES; ++i) {
      plaintext[i] = (uint8_t) rand();
    }

    for (i = 0; i < PHK1024_256_NUM_KEY_BYTES; ++i) {
      key[i] = (uint8_t) rand();
    }

    for (i = 0; i < PHK1024_NUM_TWEAK_BYTES; ++i) {
      tweak[i] = (uint8_t) rand();
    }

    std::cout << "\t{" << std::endl;
    std::cout << "\t\t\"Plaintext\" : \"" << std::hex;
    for (i=0; i < PHK1024_NUM_STATE_BYTES; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) plaintext[i];
    }
    std::cout << "\"," << std::endl;

    std::cout << "\t\t\"Key\" : \"" << std::hex;
    for (i=0; i < PHK1024_256_NUM_KEY_BYTES; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) key[i];
    }
    std::cout << "\"," << std::endl;

    std::cout << "\t\t\"Tweak\" : \"" << std::hex;
    for (i=0; i < PHK1024_NUM_TWEAK_BYTES; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) tweak[i];
    }
    std::cout << "\"," << std::endl;


    phk1024_256_set_key_encrypt(&ctx, key);
    phk1024_256_set_tweak_encrypt(&ctx, tweak);
    std::memcpy(ciphertext, plaintext, PHK1024_NUM_STATE_BYTES);
    phk1024_256_encrypt(&ctx, ciphertext);


    std::cout << "\t\t\"Ciphertext\" : \"" << std::hex;
    for (i=0; i < PHK1024_NUM_STATE_BYTES; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) ciphertext[i];
    }
    std::cout << "\"" << std::endl;

    std::cout << (testcounter == num_tests - 1 ? "\t}" : "\t},") << std::endl;
  }


  std::cout << "]" << std::endl;
}

// --------------------------------------------------------------------

void create_test_vectors_phk_1024_1024 (int const num_tests) {
  std::cout << "[" << std::endl;


  phk1024_state_t plaintext; // uint8_t[]
  phk1024_state_t ciphertext;
  phk1024_key_t key;     // uint8_t[]
  phk1024_tweak_t tweak; // uint8_t[]
  phk1024_ctx ctx;
  int i;
  int testcounter;

  srand(time(NULL));

  for (testcounter = 0; testcounter < num_tests; ++testcounter) {
    for (i = 0; i < PHK1024_NUM_STATE_BYTES; ++i) {
      plaintext[i] = (uint8_t) rand();
    }

    for (i = 0; i < PHK1024_NUM_KEY_BYTES; ++i) {
      key[i] = (uint8_t) rand();
    }

    for (i = 0; i < PHK1024_NUM_TWEAK_BYTES; ++i) {
      tweak[i] = (uint8_t) rand();
    }

    std::cout << "\t{" << std::endl;
    std::cout << "\t\t\"Plaintext\" : \"" << std::hex;
    for (i=0; i < PHK1024_NUM_STATE_BYTES; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) plaintext[i];
    }
    std::cout << "\"," << std::endl;

    std::cout << "\t\t\"Key\" : \"" << std::hex;
    for (i=0; i < PHK1024_NUM_STATE_BYTES; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) key[i];
    }
    std::cout << "\"," << std::endl;

    std::cout << "\t\t\"Tweak\" : \"" << std::hex;
    for (i=0; i < PHK1024_NUM_TWEAK_BYTES; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) tweak[i];
    }
    std::cout << "\"," << std::endl;


    phk1024_set_key_encrypt(&ctx, key);
    phk1024_set_tweak_encrypt(&ctx, tweak);
    std::memcpy(ciphertext, plaintext, PHK1024_NUM_STATE_BYTES);
    phk1024_encrypt(&ctx, ciphertext);


    std::cout << "\t\t\"Ciphertext\" : \"" << std::hex;
    for (i=0; i < PHK1024_NUM_STATE_BYTES; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) ciphertext[i];
    }
    std::cout << "\"" << std::endl;

    std::cout << (testcounter == num_tests - 1 ? "\t}" : "\t},") << std::endl;
  }


  std::cout << "]" << std::endl;
}

// --------------------------------------------------------------------

void create_test_vectors_phk_512_512 (int const num_tests) {
  std::cout << "[" << std::endl;

  phk512_state_t plaintext; // uint8_t[]
  phk512_state_t ciphertext;
  phk512_key_t key;     // uint8_t[]
  phk512_tweak_t tweak; // uint8_t[]
  phk512_ctx ctx;
  int i;
  int testcounter;

  srand(time(NULL));

  for (testcounter = 0; testcounter < num_tests; ++testcounter) {
    for (i = 0; i < PHK512_NUM_STATE_BYTES; ++i) {
      plaintext[i] = (uint8_t) rand();
    }

    for (i = 0; i < PHK512_NUM_KEY_BYTES; ++i) {
      key[i] = (uint8_t) rand();
    }

    for (i = 0; i < PHK512_NUM_TWEAK_BYTES; ++i) {
      tweak[i] = (uint8_t) rand();
    }

    std::cout << "\t{" << std::endl;
    std::cout << "\t\t\"Plaintext\" : \"" << std::hex;
    for (i=0; i < PHK512_NUM_STATE_BYTES; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) plaintext[i];
    }
    std::cout << "\"," << std::endl;

    std::cout << "\t\t\"Key\" : \"" << std::hex;
    for (i=0; i < PHK512_NUM_STATE_BYTES; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) key[i];
    }
    std::cout << "\"," << std::endl;

    std::cout << "\t\t\"Tweak\" : \"" << std::hex;
    for (i=0; i < PHK512_NUM_TWEAK_BYTES; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) tweak[i];
    }
    std::cout << "\"," << std::endl;


    phk512_set_key_encrypt(&ctx, key);
    phk512_set_tweak_encrypt(&ctx, tweak);
    std::memcpy(ciphertext, plaintext, PHK512_NUM_STATE_BYTES);
    phk512_encrypt(&ctx, ciphertext);


    std::cout << "\t\t\"Ciphertext\" : \"" << std::hex;
    for (i=0; i < PHK512_NUM_STATE_BYTES; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) ciphertext[i];
    }
    std::cout << "\"" << std::endl;

    std::cout << (testcounter == num_tests - 1 ? "\t}" : "\t},") << std::endl;
  }


  std::cout << "]" << std::endl;
}

// --------------------------------------------------------------------

void create_test_vectors_phk_512_256 (int const num_tests) {
  std::cout << "[" << std::endl;


  phk512_state_t plaintext; // uint8_t[]
  phk512_state_t ciphertext;
  phk512_key_t key;     // uint8_t[]
  phk512_tweak_t tweak; // uint8_t[]
  phk512_ctx ctx;
  int i;
  int testcounter;

  srand(time(NULL));

  for (testcounter = 0; testcounter < num_tests; ++testcounter) {
    for (i = 0; i < PHK512_NUM_STATE_BYTES; ++i) {
      plaintext[i] = (uint8_t) rand();
    }

    for (i = 0; i < PHK512_256_NUM_KEY_BYTES; ++i) {
      key[i] = (uint8_t) rand();
    }

    for (i = 0; i < PHK512_NUM_TWEAK_BYTES; ++i) {
      tweak[i] = (uint8_t) rand();
    }

    std::cout << "\t{" << std::endl;
    std::cout << "\t\t\"Plaintext\" : \"" << std::hex;
    for (i=0; i < PHK512_NUM_STATE_BYTES; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) plaintext[i];
    }
    std::cout << "\"," << std::endl;

    std::cout << "\t\t\"Key\" : \"" << std::hex;
    for (i=0; i < PHK512_256_NUM_KEY_BYTES; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) key[i];
    }
    std::cout << "\"," << std::endl;

    std::cout << "\t\t\"Tweak\" : \"" << std::hex;
    for (i=0; i < PHK512_NUM_TWEAK_BYTES; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) tweak[i];
    }
    std::cout << "\"," << std::endl;


    phk512_256_set_key_encrypt(&ctx, key);
    phk512_set_tweak_encrypt(&ctx, tweak);
    std::memcpy(ciphertext, plaintext, PHK512_NUM_STATE_BYTES);
    phk512_256_encrypt(&ctx, ciphertext);


    std::cout << "\t\t\"Ciphertext\" : \"" << std::hex;
    for (i=0; i < PHK512_NUM_STATE_BYTES; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) ciphertext[i];
    }
    std::cout << "\"" << std::endl;

    std::cout << (testcounter == num_tests - 1 ? "\t}" : "\t},") << std::endl;
  }

  std::cout << "]" << std::endl;
}

// --------------------------------------------------------------------

void create_test_vectors_phk_256(int const num_tests) {
  std::cout << "[" << std::endl;


  phk256_state_t plaintext; // uint8_t[]
  phk256_state_t ciphertext;
  phk256_key_t key;     // uint8_t[]
  phk256_tweak_t tweak; // uint8_t[]
  phk256_ctx ctx;
  int i;
  int testcounter;

  srand(time(NULL));

  for (testcounter = 0; testcounter < num_tests; ++testcounter) {
    for (i = 0; i < PHK256_NUM_STATE_BYTES; ++i) {
      plaintext[i] = (uint8_t) rand();
    }

    for (i = 0; i < PHK256_NUM_KEY_BYTES; ++i) {
      key[i] = (uint8_t) rand();
    }

    for (i = 0; i < PHK256_NUM_TWEAK_BYTES; ++i) {
      tweak[i] = (uint8_t) rand();
    }

    std::cout << "\t{" << std::endl;
    std::cout << "\t\t\"Plaintext\" : \"" << std::hex;
    for (i=0; i < PHK256_NUM_STATE_BYTES; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) plaintext[i];
    }
    std::cout << "\"," << std::endl;

    std::cout << "\t\t\"Key\" : \"" << std::hex;
    for (i=0; i < PHK256_NUM_KEY_BYTES; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) key[i];
    }
    std::cout << "\"," << std::endl;

    std::cout << "\t\t\"Tweak\" : \"" << std::hex;
    for (i=0; i < PHK256_NUM_TWEAK_BYTES; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) tweak[i];
    }
    std::cout << "\"," << std::endl;


    phk256_set_key_encrypt(&ctx, key);
    phk256_set_tweak_encrypt(&ctx, tweak);
    std::memcpy(ciphertext, plaintext, PHK256_NUM_STATE_BYTES);
    phk256_encrypt(&ctx, ciphertext);


    std::cout << "\t\t\"Ciphertext\" : \"" << std::hex;
    for (i=0; i < PHK256_NUM_STATE_BYTES; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) ciphertext[i];
    }
    std::cout << "\"" << std::endl;

    std::cout << (testcounter == num_tests - 1 ? "\t}" : "\t},") << std::endl;
  }

  std::cout << "]" << std::endl;
}

// --------------------------------------------------------------------

void create_test_vectors_phk_256_256(int const num_tests) {
  std::cout << "[" << std::endl;

  phk256_state_t plaintext; // uint8_t[]
  phk256_state_t ciphertext;
  phk256_key_t key;     // uint8_t[]
  phk256_256_tweak_t tweak; // uint8_t[]
  phk256_256_ctx ctx;
  int i;
  int testcounter;

  srand(time(NULL));

  for (testcounter = 0; testcounter < num_tests; ++testcounter) {
    for (i = 0; i < PHK256_NUM_STATE_BYTES; ++i) {
      plaintext[i] = (uint8_t) rand();
    }

    for (i = 0; i < PHK256_NUM_KEY_BYTES; ++i) {
      key[i] = (uint8_t) rand();
    }

    for (i = 0; i < PHK256_256_NUM_TWEAK_BYTES; ++i) {
      tweak[i] = (uint8_t) rand();
    }

    std::cout << "\t{" << std::endl;
    std::cout << "\t\t\"Plaintext\" : \"" << std::hex;

    for (i=0; i < PHK256_NUM_STATE_BYTES; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) plaintext[i];
    }

    std::cout << "\"," << std::endl;

    std::cout << "\t\t\"Key\" : \"" << std::hex;

    for (i=0; i < PHK256_NUM_KEY_BYTES; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) key[i];
    }

    std::cout << "\"," << std::endl;

    std::cout << "\t\t\"Tweak\" : \"" << std::hex;

    for (i=0; i < PHK256_256_NUM_TWEAK_BYTES; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) tweak[i];
    }

    std::cout << "\"," << std::endl;

    phk256_256_set_key_encrypt(&ctx, key);
    phk256_256_set_tweak_encrypt(&ctx, tweak);
    std::memcpy(ciphertext, plaintext, PHK256_NUM_STATE_BYTES);

    phk256_256_encrypt(&ctx, ciphertext);

    std::cout << "\t\t\"Ciphertext\" : \"" << std::hex;

    for (i=0; i < PHK256_NUM_STATE_BYTES; ++i) {
      std::cout << std::setw(2) << std::setfill('0') << (int) ciphertext[i];
    }

    std::cout << "\"" << std::endl;

    std::cout << (testcounter == num_tests - 1 ? "\t}" : "\t},") << std::endl;
  }

  std::cout << "]" << std::endl;
}

// --------------------------------------------------------------------

int main() {
  create_test_vectors_phk_256_256(500);
  //create_test_vectors_128bit_addition(5000);

  return 0;
}

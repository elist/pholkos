
#include "ctrt.h"

// --------------------------------------------------------------------

void add_uint8_t_arr(uint8_t* arr, uint8_t val, int block) {
  bool carry = true;
  uint8_t old_val;
  while (carry && block >= 1) {
    old_val = arr[block - 1];
    arr[block - 1] += val;
    if (arr[block - 1] < old_val){
      block -= 1;
      val = 1;
    } else {
      carry = false;
    }
  }
}

// --------------------------------------------------------------------

void ctrt_phk_256_128_enc(phk256_state_t const key, phk256_tweak_t tweak,\
                      uint8_t const* nonce, uint8_t const* plain,\
                      uint8_t *ciphertext, size_t length) {
  phk256_tweak_t orig_tweak;
  memcpy(orig_tweak, tweak, 16);

  for (size_t block = 0; block < length; ++block) {
    phk256_ctx ctx;
    phk256_state_t state;

    memcpy(state, nonce, PHK256_NUM_STATE_BYTES);
    phk256_set_key_encrypt(&ctx, key);
    phk256_set_tweak_encrypt(&ctx, tweak);

    phk256_encrypt(&ctx, state);

    for (size_t i=0; i < PHK256_NUM_STATE_BYTES; ++i) {
      ciphertext[i + block*PHK256_NUM_STATE_BYTES] = state[i]\
                                  ^ plain[i + block * PHK256_NUM_STATE_BYTES];
    }

    add_uint8_t_arr(tweak, 1, 16);
  }
}

// --------------------------------------------------------------------

void ctrt_phk_256_256_enc(phk256_state_t const key, \
                          phk256_256_tweak_t tweak,\
                          uint8_t const* nonce,\
                          uint8_t const* plain,\
                          uint8_t *ciphertext,\
                          size_t length) {
  phk256_256_tweak_t orig_tweak;
  memcpy(orig_tweak, tweak, PHK256_256_NUM_TWEAK_BYTES);

  for (size_t block = 0; block < length; ++block) {
    phk256_256_ctx ctx;
    phk256_state_t state;

    memcpy(state, nonce, PHK256_NUM_STATE_BYTES);

    phk256_256_set_key_encrypt(&ctx, key);
    phk256_256_set_tweak_encrypt(&ctx, tweak);
    phk256_256_encrypt(&ctx, state);

    for (size_t i=0; i < PHK256_NUM_STATE_BYTES; ++i) {
      ciphertext[i + block*PHK256_NUM_STATE_BYTES] = state[i]\
                                  ^ plain[i + block * PHK256_NUM_STATE_BYTES];
    }

    add_uint8_t_arr(tweak, 1, PHK256_256_NUM_TWEAK_BYTES);
  }
}

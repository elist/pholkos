#include <memory.h>
#include <stdio.h>

#include "phk-256.h"
#include "aes.h"
#include "utils.h"

// --------------------------------------------------------------------

static void add_round_tweak(const phk256_ctx *ctx,
                            phk256_state_t state,
                            const size_t round_index) {
    const uint8_t* round_tweak = ctx->round_tweaks[round_index];

    for (size_t i = 0; i < PHK256_NUM_SUBSTATES; ++i) {
        uint8_t* state_word = state + i * PHK_NUM_SUBSTATE_BYTES;
        xor_arrays(state_word, state_word, round_tweak, PHK_NUM_SUBSTATE_BYTES);
    }
}

// --------------------------------------------------------------------

static void add_round_tweak_256(const phk256_256_ctx *ctx,
                                phk256_state_t state,
                                const size_t round_index) {
    const uint8_t* round_tweak = ctx->round_tweaks[round_index];
    xor_arrays(state, state, round_tweak, PHK256_NUM_STATE_BYTES);
}

// --------------------------------------------------------------------

static void add_round_key(const phk256_ctx *ctx,
                          phk256_state_t state,
                          const size_t round_index) {
    const uint8_t* round_key = ctx->round_keys[round_index];
    xor_arrays(state, state, round_key, PHK256_NUM_STATE_BYTES);
}

// --------------------------------------------------------------------

static void add_round_key_256(const phk256_256_ctx *ctx,
                              phk256_state_t state,
                              const size_t round_index) {
    const uint8_t* round_key = ctx->round_keys[round_index];
    xor_arrays(state, state, round_key, PHK256_NUM_STATE_BYTES);
}

// --------------------------------------------------------------------

static void add_round_constant(phk256_state_t state,
                               const size_t round_index) {
    const uint8_t* round_constant = PHK256_ROUND_CONSTANTS[round_index];
    xor_arrays(state, state, round_constant, PHK_NUM_SUBSTATE_BYTES);
}

// --------------------------------------------------------------------

static void add_round_tweakey(const phk256_ctx *ctx,
                              phk256_state_t state,
                              const size_t round_index) {
    add_round_key(ctx, state, round_index);
    add_round_tweak(ctx, state, round_index);
    add_round_constant(state, round_index);
}

// --------------------------------------------------------------------

static void add_round_tweakey_256(const phk256_256_ctx *ctx,
                                  phk256_state_t state,
                                  const size_t round_index) {
    add_round_key_256(ctx, state, round_index);
    add_round_tweak_256(ctx, state, round_index);
    add_round_constant(state, round_index);
}

// --------------------------------------------------------------------

static size_t get_round_index(const size_t step_index,
                              const size_t round_index) {
    return step_index * PHK_NUM_AES_ROUNDS_PER_STEP + round_index;
}

// --------------------------------------------------------------------

static void permute_words(uint8_t *state,
                          const uint8_t *permutation,
                          const size_t num_words) {
    uint8_t temp[4 * num_words];

    for (size_t i = 0; i < num_words; ++i) {
        const size_t to_index = permutation[i];
        uint8_t *word_start = &(state[4 * to_index]);
        uint8_t *word_to = &(temp[4 * i]);
        memcpy(word_to, word_start, 4);
    }

    memcpy(state, temp, PHK256_NUM_STATE_BYTES);
}

// --------------------------------------------------------------------

static void phk256_permute_state(phk256_state_t state) {
    permute_words(state, PHK256_PERMUTATION, PHK256_NUM_WORDS);
}

// --------------------------------------------------------------------

static void phk256_invert_permute_state(phk256_state_t state) {
    permute_words(state, PHK256_INVERSE_PERMUTATION, PHK256_NUM_WORDS);
}

// --------------------------------------------------------------------

static void phk256_tau(phk256_tweak_t tweak) {
    phk256_tweak_t result;

    for (size_t i = 0; i < PHK256_NUM_TWEAK_BYTES; ++i) {
        result[i] = tweak[PHK256_TAU_PERMUTATION[i]];
    }

    memcpy(tweak, result, PHK256_NUM_TWEAK_BYTES);
}

// --------------------------------------------------------------------

static void phk256_kappa(phk256_key_t key) {
    phk256_permute_state(key);

    for (size_t i = 0; i < PHK256_NUM_SUBSTATES; ++i) {
        uint8_t* substate = key + i * PHK_NUM_SUBSTATE_BYTES;
        phk256_tau(substate);
    }

    gf2_8_double_array(key, PHK_GF2_8_MODULUS, PHK256_NUM_STATE_BYTES);
}

// --------------------------------------------------------------------

static void phk256_set_key_encrypt_internal(phk256_state_t* round_keys,
                                            const phk256_key_t key) {
    phk256_key_t k;
    memcpy(k, key, PHK256_NUM_KEY_BYTES);

    uint8_t* round_key;

    for (size_t i = 0; i < PHK256_NUM_ROUNDS; ++i) {
        round_key = round_keys[i];
        memcpy(round_key, k, PHK256_NUM_STATE_BYTES);
        phk256_kappa(k);
    }

    round_key = round_keys[PHK256_NUM_ROUNDS];
    memcpy(round_key, k, PHK256_NUM_STATE_BYTES);
}

// --------------------------------------------------------------------
// Encrypt decrypt for Pholkos-256-128
// --------------------------------------------------------------------

static void phk256_encrypt_round(const phk256_ctx *ctx,
                                 phk256_state_t state,
                                 const size_t round_index) {
    for (size_t i = 0; i < PHK256_NUM_SUBSTATES; ++i) {
        const uint8_t* round_key = ctx->round_keys[round_index]
                                   + i * PHK_NUM_SUBSTATE_BYTES;
        uint8_t *substate = &(state[i * PHK_NUM_SUBSTATE_BYTES]);
        aes_encrypt_round(substate, substate, round_key);
    }

    add_round_tweak(ctx, state, round_index);
    add_round_constant(state, round_index);
}

// --------------------------------------------------------------------

static void phk256_encrypt_last_round(const phk256_ctx *ctx,
                                      phk256_state_t state,
                                      const size_t round_index) {
    for (size_t i = 0; i < PHK256_NUM_SUBSTATES; ++i) {
        const uint8_t *round_key = ctx->round_keys[round_index]
                                   + i * PHK_NUM_SUBSTATE_BYTES;
        uint8_t *substate = &(state[i * PHK_NUM_SUBSTATE_BYTES]);
        aes_encrypt_last_round(substate, substate, round_key);
    }

    add_round_tweak(ctx, state, round_index);
    add_round_constant(state, round_index);
}

// --------------------------------------------------------------------

static void phk256_encrypt_step(const phk256_ctx *ctx,
                                phk256_state_t state,
                                const size_t step_index) {
    for (size_t i = 0; i < PHK_NUM_AES_ROUNDS_PER_STEP; ++i) {
        const size_t round_index = get_round_index(step_index, i + 1);
        phk256_encrypt_round(ctx, state, round_index);
    }

    phk256_permute_state(state);
}

// --------------------------------------------------------------------

static void phk256_encrypt_last_step(const phk256_ctx *ctx,
                                     phk256_state_t state,
                                     const size_t step_index) {
    for (size_t i = 0; i < PHK_NUM_AES_ROUNDS_PER_STEP - 1; ++i) {
        const size_t round_index = get_round_index(step_index, i + 1);
        phk256_encrypt_round(ctx, state, round_index);
    }

    const size_t round_index = get_round_index(
        step_index, PHK_NUM_AES_ROUNDS_PER_STEP - 1
    );
    phk256_encrypt_last_round(ctx, state, round_index + 1);
}

// --------------------------------------------------------------------

static void phk256_decrypt_last_round(const phk256_ctx *ctx,
                                      phk256_state_t state,
                                      const size_t round_index) {
    add_round_tweak(ctx, state, round_index);
    add_round_constant(state, round_index);

    for (size_t i = 0; i < PHK256_NUM_SUBSTATES; ++i) {
        const uint8_t *round_key = ctx->round_keys[round_index]
                                   + i * PHK_NUM_SUBSTATE_BYTES;
        uint8_t *substate = state + i * PHK_NUM_SUBSTATE_BYTES;
        aes_decrypt_last_round(substate, substate, round_key);
    }
}

// --------------------------------------------------------------------

static void phk256_decrypt_round(const phk256_ctx *ctx,
                                 phk256_state_t state,
                                 const size_t round_index) {
    add_round_tweak(ctx, state, round_index);
    add_round_constant(state, round_index);

    for (size_t i = 0; i < PHK256_NUM_SUBSTATES; ++i) {
        const uint8_t *round_key = ctx->round_keys[round_index]
                                   + i * PHK_NUM_SUBSTATE_BYTES;
        uint8_t *substate = &(state[i * PHK_NUM_SUBSTATE_BYTES]);
        aes_decrypt_round(substate, substate, round_key);
    }
}

// --------------------------------------------------------------------

static void phk256_decrypt_step(const phk256_ctx *ctx,
                                phk256_state_t state,
                                const size_t step_index) {
    phk256_invert_permute_state(state);

    for (int i = PHK_NUM_AES_ROUNDS_PER_STEP - 1; i >= 0; --i) {
        const size_t round_index = get_round_index(step_index, (size_t)i + 1);
        phk256_decrypt_round(ctx, state, round_index);
    }
}

// --------------------------------------------------------------------

static void phk256_decrypt_last_step(const phk256_ctx *ctx,
                                     phk256_state_t state,
                                     const size_t step_index) { // 7
    size_t round_index = get_round_index(
        step_index, PHK_NUM_AES_ROUNDS_PER_STEP - 1
    );
    phk256_decrypt_last_round(ctx, state, round_index + 1);

    for (int i = PHK_NUM_AES_ROUNDS_PER_STEP - 2; i >= 0; --i) {
        round_index = get_round_index(step_index, (size_t)i + 1);
        phk256_decrypt_round(ctx, state, round_index);
    }
}

// --------------------------------------------------------------------
// Encrypt decrypt for Pholkos-256-256
// --------------------------------------------------------------------

static void phk256_256_encrypt_round(const phk256_256_ctx *ctx,
                                     phk256_state_t state,
                                     const size_t round_index) {
    for (size_t i = 0; i < PHK256_NUM_SUBSTATES; ++i) {
        const uint8_t* round_key = ctx->round_keys[round_index]
                                   + i * PHK_NUM_SUBSTATE_BYTES;
        uint8_t *substate = &(state[i * PHK_NUM_SUBSTATE_BYTES]);
        aes_encrypt_round(substate, substate, round_key);
    }

    add_round_tweak_256(ctx, state, round_index);
    add_round_constant(state, round_index);
}

// --------------------------------------------------------------------

static void phk256_256_encrypt_last_round(const phk256_256_ctx *ctx,
                                          phk256_state_t state,
                                          const size_t round_index) {
    for (size_t i = 0; i < PHK256_NUM_SUBSTATES; ++i) {
        const uint8_t *round_key = ctx->round_keys[round_index]
                                   + i * PHK_NUM_SUBSTATE_BYTES;
        uint8_t *substate = &(state[i * PHK_NUM_SUBSTATE_BYTES]);
        aes_encrypt_last_round(substate, substate, round_key);
    }

    add_round_tweak_256(ctx, state, round_index);
    add_round_constant(state, round_index);
}

// --------------------------------------------------------------------

static void phk256_256_encrypt_step(const phk256_256_ctx *ctx,
                                    phk256_state_t state,
                                    const size_t step_index) {
    for (size_t i = 0; i < PHK_NUM_AES_ROUNDS_PER_STEP; ++i) {
        const size_t round_index = get_round_index(step_index, i + 1);
        phk256_256_encrypt_round(ctx, state, round_index);
    }

    phk256_permute_state(state);
}

// --------------------------------------------------------------------

static void phk256_256_encrypt_last_step(const phk256_256_ctx *ctx,
                                         phk256_state_t state,
                                         const size_t step_index) {
    for (size_t i = 0; i < PHK_NUM_AES_ROUNDS_PER_STEP - 1; ++i) {
        const size_t round_index = get_round_index(step_index, i + 1);
        phk256_256_encrypt_round(ctx, state, round_index);
    }

    const size_t round_index = get_round_index(
        step_index, PHK_NUM_AES_ROUNDS_PER_STEP - 1
    );
    phk256_256_encrypt_last_round(ctx, state, round_index + 1);
}

// --------------------------------------------------------------------

static void phk256_256_decrypt_last_round(const phk256_256_ctx *ctx,
                                          phk256_state_t state,
                                          const size_t round_index) {
    add_round_tweak_256(ctx, state, round_index);
    add_round_constant(state, round_index);

    for (size_t i = 0; i < PHK256_NUM_SUBSTATES; ++i) {
        const uint8_t *round_key = ctx->round_keys[round_index]
                                   + i * PHK_NUM_SUBSTATE_BYTES;
        uint8_t *substate = state + i * PHK_NUM_SUBSTATE_BYTES;
        aes_decrypt_last_round(substate, substate, round_key);
    }
}

// --------------------------------------------------------------------

static void phk256_256_decrypt_round(const phk256_256_ctx *ctx,
                                     phk256_state_t state,
                                     const size_t round_index) {
    add_round_tweak_256(ctx, state, round_index);
    add_round_constant(state, round_index);

    for (size_t i = 0; i < PHK256_NUM_SUBSTATES; ++i) {
        const uint8_t *round_key = ctx->round_keys[round_index]
                                   + i * PHK_NUM_SUBSTATE_BYTES;
        uint8_t *substate = &(state[i * PHK_NUM_SUBSTATE_BYTES]);
        aes_decrypt_round(substate, substate, round_key);
    }
}

// --------------------------------------------------------------------

static void phk256_256_decrypt_step(const phk256_256_ctx *ctx,
                                    phk256_state_t state,
                                    const size_t step_index) {
    phk256_invert_permute_state(state);

    for (int i = PHK_NUM_AES_ROUNDS_PER_STEP - 1; i >= 0; --i) {
        const size_t round_index = get_round_index(step_index, (size_t)i + 1);
        phk256_256_decrypt_round(ctx, state, round_index);
    }
}

// --------------------------------------------------------------------

static void phk256_256_decrypt_last_step(const phk256_256_ctx *ctx,
                                         phk256_state_t state,
                                         const size_t step_index) { // 7
    size_t round_index = get_round_index(
        step_index, PHK_NUM_AES_ROUNDS_PER_STEP - 1
    );
    phk256_256_decrypt_last_round(ctx, state, round_index + 1);

    for (int i = PHK_NUM_AES_ROUNDS_PER_STEP - 2; i >= 0; --i) {
        round_index = get_round_index(step_index, (size_t)i + 1);
        phk256_256_decrypt_round(ctx, state, round_index);
    }
}

// --------------------------------------------------------------------
// API
// --------------------------------------------------------------------

void phk256_set_key_encrypt(phk256_ctx *ctx, const phk256_key_t key) {
    phk256_set_key_encrypt_internal(ctx->round_keys, key);
}

// --------------------------------------------------------------------

void phk256_set_key_decrypt(phk256_ctx *ctx, const phk256_key_t key) {
    phk256_set_key_encrypt_internal(ctx->round_keys, key);
}

// --------------------------------------------------------------------

void phk256_set_tweak_encrypt(phk256_ctx *ctx, const phk256_tweak_t tweak) {
    phk256_tweak_t t;
    memcpy(t, tweak, PHK256_NUM_TWEAK_BYTES);

    uint8_t* round_tweak;

    for (size_t i = 0; i < PHK256_NUM_ROUNDS; ++i) {
        round_tweak = ctx->round_tweaks[i];
        memcpy(round_tweak, t, PHK256_NUM_TWEAK_BYTES);

        phk256_tau(t);
    }

    round_tweak = ctx->round_tweaks[PHK256_NUM_ROUNDS];
    memcpy(round_tweak, t, PHK256_NUM_TWEAK_BYTES);
}

// --------------------------------------------------------------------

void phk256_set_tweak_decrypt(phk256_ctx *ctx, const phk256_tweak_t tweak) {
    phk256_set_tweak_encrypt(ctx, tweak);
}

// --------------------------------------------------------------------

void phk256_encrypt(const phk256_ctx *ctx, phk256_state_t state) {
    add_round_tweakey(ctx, state, 0);

    for (size_t i = 0; i < PHK256_NUM_STEPS - 1; ++i) {
        phk256_encrypt_step(ctx, state, i);
    }

    phk256_encrypt_last_step(ctx, state, PHK256_NUM_STEPS - 1);
}

// --------------------------------------------------------------------

void phk256_decrypt(const phk256_ctx *ctx, phk256_state_t state) {
    phk256_decrypt_last_step(ctx, state, PHK256_NUM_STEPS - 1);

    for (int i = PHK256_NUM_STEPS - 2; i >= 0; --i) {
        phk256_decrypt_step(ctx, state, (size_t)i);
    }

    add_round_tweakey(ctx, state, 0);
}

// --------------------------------------------------------------------

void print_round_tweakey(const phk256_ctx *ctx,
                         const size_t round_index) {
    phk256_state_t tmp;
    memcpy(tmp, ctx->round_keys[round_index], 32);
    add_round_tweak(ctx, tmp, round_index);
    add_round_constant(tmp, round_index);
    printf("roundtweakey %zu: ", round_index);
    print_hex(tmp, 32);
}

// --------------------------------------------------------------------

void print_round_key(const phk256_ctx *ctx,
                     const size_t round_index) {
    phk256_state_t tmp;
    memcpy(tmp, ctx->round_keys[round_index], 32);
    add_round_constant(tmp, round_index);
    printf("roundkey %zu: ", round_index);
    print_hex(tmp, 32);
}

// --------------------------------------------------------------------
// API for Pholkos-256-256
// --------------------------------------------------------------------

void phk256_256_set_key_encrypt(phk256_256_ctx *ctx, const phk256_key_t key) {
    phk256_set_key_encrypt_internal(ctx->round_keys, key);
}

// --------------------------------------------------------------------

void phk256_256_set_key_decrypt(phk256_256_ctx *ctx, const phk256_key_t key) {
    phk256_set_key_encrypt_internal(ctx->round_keys, key);
}

// --------------------------------------------------------------------

void phk256_256_set_tweak_encrypt(phk256_256_ctx *ctx,
                                  const phk256_256_tweak_t tweak) {
    phk256_256_tweak_t t;
    memcpy(t, tweak, PHK256_256_NUM_TWEAK_BYTES);

    uint8_t* round_tweak;

    for (size_t i = 0; i < PHK256_NUM_ROUNDS; ++i) {
        round_tweak = ctx->round_tweaks[i];
        memcpy(round_tweak, t, PHK256_256_NUM_TWEAK_BYTES);

        phk256_permute_state(t);
        phk256_tau(t);
        phk256_tau(&t[16]);
    }

    round_tweak = ctx->round_tweaks[PHK256_NUM_ROUNDS];
    memcpy(round_tweak, t, PHK256_256_NUM_TWEAK_BYTES);
}

// --------------------------------------------------------------------

void phk256_256_set_tweak_decrypt(phk256_256_ctx *ctx,
                                  const phk256_256_tweak_t tweak) {
    phk256_256_set_tweak_encrypt(ctx, tweak);
}

// --------------------------------------------------------------------

void phk256_256_encrypt(const phk256_256_ctx *ctx, phk256_state_t state) {
    add_round_tweakey_256(ctx, state, 0);

    for (size_t i = 0; i < PHK256_NUM_STEPS - 1; ++i) {
        phk256_256_encrypt_step(ctx, state, i);
    }

    phk256_256_encrypt_last_step(ctx, state, PHK256_NUM_STEPS - 1);
}

// --------------------------------------------------------------------

void phk256_256_decrypt(const phk256_256_ctx *ctx, phk256_state_t state) {
    phk256_256_decrypt_last_step(ctx, state, PHK256_NUM_STEPS - 1);

    for (int i = PHK256_NUM_STEPS - 2; i >= 0; --i) {
        phk256_256_decrypt_step(ctx, state, (size_t)i);
    }

    add_round_tweakey_256(ctx, state, 0);
}

// --------------------------------------------------------------------

void print_round_tweakey_phk256(const phk256_256_ctx *ctx,
                                const size_t round_index) {
    phk256_state_t tmp;
    const uint8_t* round_key = ctx->round_keys[round_index];
    const uint8_t* round_tweak = ctx->round_tweaks[round_index];

    xor_arrays(tmp, round_key, round_tweak, PHK256_NUM_STATE_BYTES);
    add_round_constant(tmp, round_index);

    printf("roundtweakey %zu: ", round_index);
    print_hex(tmp, PHK256_NUM_STATE_BYTES);
}

// --------------------------------------------------------------------

void print_round_key_phk256(const phk256_256_ctx *ctx,
                            const size_t round_index) {
    phk256_state_t tmp;
    memcpy(tmp, ctx->round_keys[round_index], PHK256_NUM_STATE_BYTES);
    add_round_constant(tmp, round_index);

    printf("roundkey %zu: ", round_index);
    print_hex(tmp, PHK256_NUM_STATE_BYTES);
}

/****************************************/
/* Compute pi to arbitrary precision    */
/* Author Roy Williams February 1994    */
/* Uses Machin's formula...             */
/* pi/4 = 4 arctan(1/5) - arctan(1/239) */
/****************************************/
/* compile with cc -O -o pi pi.c        */
/* run as "pi 1000"                     */
/****************************************/
/* The last few digits may be wrong.......... */

#include <stdio.h>
#include <cstdlib>

#include "pi.h"

// --------------------------------------------------------------------

void copy(int nblock, int *result, int *from) {
    int i;

    for (i = 0; i < nblock; i++) {
        result[i] = from[i];
    }
}

// --------------------------------------------------------------------

int zero(int nblock, int *result) {
    int i;

    for (i = 0; i < nblock; i++) {
        if (result[i]) {
            return 0;
        }
    }

    return 1;
}

// --------------------------------------------------------------------

void add(int nblock, int *result, int *increm) {
    int i;

    for (i = nblock - 1; i >= 0; i--) {
        result[i] += increm[i];

        if (result[i] >= BASE) {
            result[i] -= BASE;
            result[i - 1]++;
        }
    }
}

// --------------------------------------------------------------------

void sub(int nblock, int *result, int *decrem) {
    int i;

    for (i = nblock - 1; i >= 0; i--) {
        result[i] -= decrem[i];

        if (result[i] < 0) {
            result[i] += BASE;
            result[i - 1]--;
        }
    }
}

// --------------------------------------------------------------------

void mult(int nblock, int *result, int factor) {
    int i;
    int carry = 0;

    for (i = nblock - 1; i >= 0; i--) {
        result[i] *= factor;
        result[i] += carry;
        carry = result[i] / BASE;
        result[i] %= BASE;
    }
}

// --------------------------------------------------------------------

void div(int nblock, int *result, int denom) {
    int i, carry = 0;

    for (i = 0; i < nblock; i++) {
        result[i] += carry * BASE;
        carry = result[i] % denom;
        result[i] /= denom;
    }
}

// --------------------------------------------------------------------

void set(int nblock, int *result, int rhs) {
    int i;

    for (i = 0; i < nblock; i++) {
        result[i] = 0;
    }

    result[0] = rhs;
}

// --------------------------------------------------------------------

void print(int nblock, int *result) {
    int i, k;
    char s[10];
    printf("%1d.\n", result[0]);

    for (i = 1; i < nblock; i++) {
        sprintf(s, "%4d ", result[i]);

        for (k = 0; k < 5; k++) {
            if (s[k] == ' ') {
                s[k] = '0';
            }
        }

        printf("%c%c%c%c", s[0], s[1], s[2], s[3]);

        if (i % 15 == 0) {
            printf("\n");
        }
    }
    printf("\n");
}

// --------------------------------------------------------------------

void arctan(int nblock, int *result, int *w1, int *w2, int denom, int onestep) {
    int denom2 = denom * denom;
    int k = 1;

    set(nblock, result, 1);
    div(nblock, result, denom);
    copy(nblock, w1, result);

    do {
        if (onestep) {
            div(nblock, w1, denom2);
        } else {
            div(nblock, w1, denom);
            div(nblock, w1, denom);
        }

        copy(nblock, w2, w1);
        div(nblock, w2, 2 * k + 1);

        if (k % 2) {
            sub(nblock, result, w2);
        } else {
            add(nblock, result, w2);
        }

        k++;
    } while (!zero(nblock, w2));
}


#include <bitset>
#include <cstring>

#include "misc.h"

// --------------------------------------------------------------------

namespace misc {

    void print_progress_bar(std::ostream &out, uint8_t progress) {
        std::ios_base::fmtflags fmt(out.flags());
        out << "\u2563";

        for (size_t i(0); i < 20; ++i) {
            if (progress / 5 > i) {
                out << "\u2588";
            } else {
                out << "\u2591";
            }
        }

        out << "\u2560";
        out << ' ' << std::setw(3) << (int) progress << " %";
        out.flags(fmt);
    }

    // --------------------------------------------------------------------

    void print_buffer(uint8_t *buf, size_t len, bool only_words) {
        for (size_t i = 0; i < len; i += (only_words ? 4 : 1)) {
            std::cout << std::hex
                << std::setw(2)
                << std::setfill('0')
                << ((int)buf[i] / (only_words ? 4 : 1));

            if (only_words || (i % 16 == 15)) {
                std::cout << ' ';
            }
        }

        std::cout << std::endl;
    }

    // --------------------------------------------------------------------

    bool assert_equals(const uint8_t* expected,
                       const uint8_t* actual,
                       const size_t num_bytes) {
        return !memcmp(expected, actual, num_bytes);
    }

}

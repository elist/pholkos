#include "phk-1024.h"


void phk1024_load_constants() {
    phk1024_rc[0] = _mm_set_epi32(0x0684704c, 0xe620c00a, 0xb2c5fef0, 0x75817b9d);
    phk1024_rc[1] = _mm_set_epi32(0x8b66b4e1, 0x88f3a06b, 0x640f6ba4, 0x2f08f717);
    phk1024_rc[2] = _mm_set_epi32(0x3402de2d, 0x53f28498, 0xcf029d60, 0x9f029114);
    phk1024_rc[3] = _mm_set_epi32(0x0ed6eae6, 0x2e7b4f08, 0xbbf3bcaf, 0xfd5b4f79);
    phk1024_rc[4] = _mm_set_epi32(0xcbcfb0cb, 0x4872448b, 0x79eecd1c, 0xbe397044);
    phk1024_rc[5] = _mm_set_epi32(0x7eeacdee, 0x6e9032b7, 0x8d5335ed, 0x2b8a057b);
    phk1024_rc[6] = _mm_set_epi32(0x67c28f43, 0x5e2e7cd0, 0xe2412761, 0xda4fef1b);
    phk1024_rc[7] = _mm_set_epi32(0x2924d9b0, 0xafcacc07, 0x675ffde2, 0x1fc70b3b);
    phk1024_rc[8] = _mm_set_epi32(0xab4d63f1, 0xe6867fe9, 0xecdb8fca, 0xb9d465ee);
    phk1024_rc[9] = _mm_set_epi32(0x1c30bf84, 0xd4b7cd64, 0x5b2a404f, 0xad037e33);
    phk1024_rc[10] = _mm_set_epi32(0xb2cc0bb9, 0x941723bf, 0x69028b2e, 0x8df69800);
    phk1024_rc[11] = _mm_set_epi32(0xfa0478a6, 0xde6f5572, 0x4aaa9ec8, 0x5c9d2d8a);
    phk1024_rc[12] = _mm_set_epi32(0xdfb49f2b, 0x6b772a12, 0x0efa4f2e, 0x29129fd4);
    phk1024_rc[13] = _mm_set_epi32(0x1ea10344, 0xf449a236, 0x32d611ae, 0xbb6a12ee);
    phk1024_rc[14] = _mm_set_epi32(0xaf044988, 0x4b050084, 0x5f9600c9, 0x9ca8eca6);
    phk1024_rc[15] = _mm_set_epi32(0x21025ed8, 0x9d199c4f, 0x78a2c7e3, 0x27e593ec);
    phk1024_rc[16] = _mm_set_epi32(0xbf3aaaf8, 0xa759c9b7, 0xb9282ecd, 0x82d40173);
    phk1024_rc[17] = _mm_set_epi32(0x6260700d, 0x6186b017, 0x37f2efd9, 0x10307d6b);
    phk1024_rc[18] = _mm_set_epi32(0x5aca45c2, 0x21300443, 0x81c29153, 0xf6fc9ac6);
    phk1024_rc[19] = _mm_set_epi32(0x9223973c, 0x226b68bb, 0x2caf92e8, 0x36d1943a);
    phk1024_rc[20] = _mm_set_epi32(0xd3bf9238, 0x225886eb, 0x6cbab958, 0xe51071b4);
    phk1024_rc[21] = _mm_set_epi32(0xdb863ce5, 0xaef0c677, 0x933dfddd, 0x24e1128d);
    phk1024_rc[22] = _mm_set_epi32(0xbb606268, 0xffeba09c, 0x83e48de3, 0xcb2212b1);
    phk1024_rc[23] = _mm_set_epi32(0x734bd3dc, 0xe2e4d19c, 0x2db91a4e, 0xc72bf77d);
    phk1024_rc[24] = _mm_set_epi32(0x43bb47c3, 0x61301b43, 0x4b1415c4, 0x2cb3924e);
    phk1024_rc[25] = _mm_set_epi32(0xdba775a8, 0xe707eff6, 0x03b231dd, 0x16eb6899);
    phk1024_rc[26] = _mm_set_epi32(0x6df3614b, 0x3c755977, 0x8e5e2302, 0x7eca472c);
    phk1024_rc[27] = _mm_set_epi32(0xcda75a17, 0xd6de7d77, 0x6d1be5b9, 0xb88617f9);
    phk1024_rc[28] = _mm_set_epi32(0xec6b43f0, 0x6ba8e9aa, 0x9d6c069d, 0xa946ee5d);
}


void phk1024_256_load_constants() {
    phk1024_rc[0] = _mm_set_epi32(0x0684704c, 0xe620c00a, 0xb2c5fef0, 0x75817b9d);
    phk1024_rc[1] = _mm_set_epi32(0x8b66b4e1, 0x88f3a06b, 0x640f6ba4, 0x2f08f717);
    phk1024_rc[2] = _mm_set_epi32(0x3402de2d, 0x53f28498, 0xcf029d60, 0x9f029114);
    phk1024_rc[3] = _mm_set_epi32(0x0ed6eae6, 0x2e7b4f08, 0xbbf3bcaf, 0xfd5b4f79);
    phk1024_rc[4] = _mm_set_epi32(0xcbcfb0cb, 0x4872448b, 0x79eecd1c, 0xbe397044);
    phk1024_rc[5] = _mm_set_epi32(0x7eeacdee, 0x6e9032b7, 0x8d5335ed, 0x2b8a057b);
    phk1024_rc[6] = _mm_set_epi32(0x67c28f43, 0x5e2e7cd0, 0xe2412761, 0xda4fef1b);
    phk1024_rc[7] = _mm_set_epi32(0x2924d9b0, 0xafcacc07, 0x675ffde2, 0x1fc70b3b);
    phk1024_rc[8] = _mm_set_epi32(0xab4d63f1, 0xe6867fe9, 0xecdb8fca, 0xb9d465ee);
    phk1024_rc[9] = _mm_set_epi32(0x1c30bf84, 0xd4b7cd64, 0x5b2a404f, 0xad037e33);
    phk1024_rc[10] = _mm_set_epi32(0xb2cc0bb9, 0x941723bf, 0x69028b2e, 0x8df69800);
    phk1024_rc[11] = _mm_set_epi32(0xfa0478a6, 0xde6f5572, 0x4aaa9ec8, 0x5c9d2d8a);
    phk1024_rc[12] = _mm_set_epi32(0xdfb49f2b, 0x6b772a12, 0x0efa4f2e, 0x29129fd4);
    phk1024_rc[13] = _mm_set_epi32(0x1ea10344, 0xf449a236, 0x32d611ae, 0xbb6a12ee);
    phk1024_rc[14] = _mm_set_epi32(0xaf044988, 0x4b050084, 0x5f9600c9, 0x9ca8eca6);
    phk1024_rc[15] = _mm_set_epi32(0x21025ed8, 0x9d199c4f, 0x78a2c7e3, 0x27e593ec);
    phk1024_rc[16] = _mm_set_epi32(0xbf3aaaf8, 0xa759c9b7, 0xb9282ecd, 0x82d40173);
    phk1024_rc[17] = _mm_set_epi32(0x6260700d, 0x6186b017, 0x37f2efd9, 0x10307d6b);
    phk1024_rc[18] = _mm_set_epi32(0x5aca45c2, 0x21300443, 0x81c29153, 0xf6fc9ac6);
    phk1024_rc[19] = _mm_set_epi32(0x9223973c, 0x226b68bb, 0x2caf92e8, 0x36d1943a);
    phk1024_rc[20] = _mm_set_epi32(0xd3bf9238, 0x225886eb, 0x6cbab958, 0xe51071b4);
    phk1024_rc[21] = _mm_set_epi32(0xdb863ce5, 0xaef0c677, 0x933dfddd, 0x24e1128d);
    phk1024_rc[22] = _mm_set_epi32(0xbb606268, 0xffeba09c, 0x83e48de3, 0xcb2212b1);
    phk1024_rc[23] = _mm_set_epi32(0x734bd3dc, 0xe2e4d19c, 0x2db91a4e, 0xc72bf77d);
    phk1024_rc[24] = _mm_set_epi32(0x43bb47c3, 0x61301b43, 0x4b1415c4, 0x2cb3924e);
}

#define PHK1024_KS_ROUND0_X_ENC(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p) \
    PHK1024_KS_ROUND0(ctx->rk[i], ctx->rk[j], ctx->rk[k], ctx->rk[l], ctx->rk[m], ctx->rk[n], ctx->rk[o], ctx->rk[p], \
        ctx->rk[a], ctx->rk[b], ctx->rk[c], ctx->rk[d], ctx->rk[e], ctx->rk[f], ctx->rk[g], ctx->rk[h]) \
    ctx->rk[a] = _mm_xor_si128(ctx->rk[a], phk1024_rc[a >> 3]);

#define PHK1024_KS_ROUND1_X_ENC(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p) \
    PHK1024_KS_ROUND1(ctx->rk[i], ctx->rk[j], ctx->rk[k], ctx->rk[l], ctx->rk[m], ctx->rk[n], ctx->rk[o], ctx->rk[p], \
        ctx->rk[a], ctx->rk[b], ctx->rk[c], ctx->rk[d], ctx->rk[e], ctx->rk[f], ctx->rk[g], ctx->rk[h]) \
    ctx->rk[a] = _mm_xor_si128(ctx->rk[a], phk1024_rc[a >> 3]);

void phk1024_set_key_encrypt(struct phk1024_ctx *const ctx, const uint8_t *const key) {
    __m128i tmp;

    ctx->rt = _mm_setzero_si128();

    ctx->rk[0] = _mm_loadu_si128((__m128i*)key);
    ctx->rk[1] = _mm_loadu_si128((__m128i*)&key[16]);
    ctx->rk[2] = _mm_loadu_si128((__m128i*)&key[32]);
    ctx->rk[3] = _mm_loadu_si128((__m128i*)&key[48]);
    ctx->rk[4] = _mm_loadu_si128((__m128i*)&key[64]);
    ctx->rk[5] = _mm_loadu_si128((__m128i*)&key[80]);
    ctx->rk[6] = _mm_loadu_si128((__m128i*)&key[96]);
    ctx->rk[7] = _mm_loadu_si128((__m128i*)&key[112]);

    PHK1024_KS_ROUND0_X_ENC(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)
    PHK1024_KS_ROUND1_X_ENC(8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23)
    PHK1024_KS_ROUND0_X_ENC(16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31)
    PHK1024_KS_ROUND1_X_ENC(24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39)
    PHK1024_KS_ROUND0_X_ENC(32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47)
    PHK1024_KS_ROUND1_X_ENC(40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55)
    PHK1024_KS_ROUND0_X_ENC(48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63)
    PHK1024_KS_ROUND1_X_ENC(56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71)
    PHK1024_KS_ROUND0_X_ENC(64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79)
    PHK1024_KS_ROUND1_X_ENC(72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87)
    PHK1024_KS_ROUND0_X_ENC(80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95)
    PHK1024_KS_ROUND1_X_ENC(88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103)
    PHK1024_KS_ROUND0_X_ENC(96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111)
    PHK1024_KS_ROUND1_X_ENC(104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119)
    PHK1024_KS_ROUND0_X_ENC(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127)
    PHK1024_KS_ROUND1_X_ENC(120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135)
    PHK1024_KS_ROUND0_X_ENC(128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143)
    PHK1024_KS_ROUND1_X_ENC(136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151)
    PHK1024_KS_ROUND0_X_ENC(144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159)
    PHK1024_KS_ROUND1_X_ENC(152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167)
    PHK1024_KS_ROUND0_X_ENC(160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175)
    PHK1024_KS_ROUND1_X_ENC(168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183)
    PHK1024_KS_ROUND0_X_ENC(176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191)
    PHK1024_KS_ROUND1_X_ENC(184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199)
    PHK1024_KS_ROUND0_X_ENC(192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207)
    PHK1024_KS_ROUND1_X_ENC(200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215)
    PHK1024_KS_ROUND0_X_ENC(208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223)
    PHK1024_KS_ROUND1_X_ENC(216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231)

    ctx->rk[224] = _mm_xor_si128(ctx->rk[224], phk1024_rc[28]);
}

void phk1024_256_set_key_encrypt(struct phk1024_256_ctx *const ctx, const uint8_t *const key) {
    __m128i tmp;

    ctx->rt = _mm_setzero_si128();

    ctx->rk[0] = _mm_loadu_si128((__m128i*)key);
    ctx->rk[1] = _mm_loadu_si128((__m128i*)&key[16]);

    PHK1024_KS_EXPAND(ctx->rk[0], ctx->rk[1], ctx->rk[2], ctx->rk[3], ctx->rk[4], ctx->rk[5], ctx->rk[6], ctx->rk[7], ctx->rk[0], ctx->rk[1])

    PHK1024_KS_ROUND0_X_ENC(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)
    PHK1024_KS_ROUND1_X_ENC(8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23)
    PHK1024_KS_ROUND0_X_ENC(16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31)
    PHK1024_KS_ROUND1_X_ENC(24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39)
    PHK1024_KS_ROUND0_X_ENC(32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47)
    PHK1024_KS_ROUND1_X_ENC(40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55)
    PHK1024_KS_ROUND0_X_ENC(48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63)
    PHK1024_KS_ROUND1_X_ENC(56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71)
    PHK1024_KS_ROUND0_X_ENC(64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79)
    PHK1024_KS_ROUND1_X_ENC(72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87)
    PHK1024_KS_ROUND0_X_ENC(80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95)
    PHK1024_KS_ROUND1_X_ENC(88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103)
    PHK1024_KS_ROUND0_X_ENC(96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111)
    PHK1024_KS_ROUND1_X_ENC(104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119)
    PHK1024_KS_ROUND0_X_ENC(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127)
    PHK1024_KS_ROUND1_X_ENC(120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135)
    PHK1024_KS_ROUND0_X_ENC(128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143)
    PHK1024_KS_ROUND1_X_ENC(136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151)
    PHK1024_KS_ROUND0_X_ENC(144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159)
    PHK1024_KS_ROUND1_X_ENC(152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167)
    PHK1024_KS_ROUND0_X_ENC(160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175)
    PHK1024_KS_ROUND1_X_ENC(168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183)
    PHK1024_KS_ROUND0_X_ENC(176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191)
    PHK1024_KS_ROUND1_X_ENC(184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199)

    ctx->rk[192] = _mm_xor_si128(ctx->rk[192], phk1024_rc[24]);
}

#define PHK1024_KS_ROUND_X_DEC_FIRST(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p) \
    PHK1024_KS_ROUND0_X_ENC(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p)

#define PHK1024_KS_ROUND0_X_DEC(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p) \
    PHK1024_KS_ROUND0(ctx->rk[i], ctx->rk[j], ctx->rk[k], ctx->rk[l], ctx->rk[m], ctx->rk[n], ctx->rk[o], ctx->rk[p], \
        ctx->rk[a], ctx->rk[b], ctx->rk[c], ctx->rk[d], ctx->rk[e], ctx->rk[f], ctx->rk[g], ctx->rk[h]) \
    ctx->rk[a] = _mm_aesimc_si128(_mm_xor_si128(ctx->rk[a], phk1024_rc[a >> 3])); \
    ctx->rk[b] = _mm_aesimc_si128(ctx->rk[b]); \
    ctx->rk[c] = _mm_aesimc_si128(ctx->rk[c]); \
    ctx->rk[d] = _mm_aesimc_si128(ctx->rk[d]); \
    ctx->rk[e] = _mm_aesimc_si128(ctx->rk[e]); \
    ctx->rk[f] = _mm_aesimc_si128(ctx->rk[f]); \
    ctx->rk[g] = _mm_aesimc_si128(ctx->rk[g]); \
    ctx->rk[h] = _mm_aesimc_si128(ctx->rk[h]);

#define PHK1024_KS_ROUND1_X_DEC(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p) \
    PHK1024_KS_ROUND1(ctx->rk[i], ctx->rk[j], ctx->rk[k], ctx->rk[l], ctx->rk[m], ctx->rk[n], ctx->rk[o], ctx->rk[p], \
        ctx->rk[a], ctx->rk[b], ctx->rk[c], ctx->rk[d], ctx->rk[e], ctx->rk[f], ctx->rk[g], ctx->rk[h]) \
    ctx->rk[a] = _mm_aesimc_si128(_mm_xor_si128(ctx->rk[a], phk1024_rc[a >> 3])); \
    ctx->rk[b] = _mm_aesimc_si128(ctx->rk[b]); \
    ctx->rk[c] = _mm_aesimc_si128(ctx->rk[c]); \
    ctx->rk[d] = _mm_aesimc_si128(ctx->rk[d]); \
    ctx->rk[e] = _mm_aesimc_si128(ctx->rk[e]); \
    ctx->rk[f] = _mm_aesimc_si128(ctx->rk[f]); \
    ctx->rk[g] = _mm_aesimc_si128(ctx->rk[g]); \
    ctx->rk[h] = _mm_aesimc_si128(ctx->rk[h]);

void phk1024_set_key_decrypt(struct phk1024_ctx *const ctx, const uint8_t *const key) {
    __m128i tmp;

    ctx->rt = _mm_setzero_si128();

    ctx->rk[0] = _mm_loadu_si128((__m128i*)key);
    ctx->rk[1] = _mm_loadu_si128((__m128i*)&key[16]);
    ctx->rk[2] = _mm_loadu_si128((__m128i*)&key[32]);
    ctx->rk[3] = _mm_loadu_si128((__m128i*)&key[48]);
    ctx->rk[4] = _mm_loadu_si128((__m128i*)&key[64]);
    ctx->rk[5] = _mm_loadu_si128((__m128i*)&key[80]);
    ctx->rk[6] = _mm_loadu_si128((__m128i*)&key[96]);
    ctx->rk[7] = _mm_loadu_si128((__m128i*)&key[112]);

    PHK1024_KS_ROUND_X_DEC_FIRST(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)
    PHK1024_KS_ROUND1_X_DEC(8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23)
    PHK1024_KS_ROUND0_X_DEC(16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31)
    PHK1024_MIX0_ENC(ctx->rk[16], ctx->rk[17], ctx->rk[18], ctx->rk[19], ctx->rk[20], ctx->rk[21], ctx->rk[22], \
            ctx->rk[23], ctx->rk[16], ctx->rk[17], ctx->rk[18], ctx->rk[19], ctx->rk[20], ctx->rk[21], ctx->rk[22], \
            ctx->rk[23])
    PHK1024_KS_ROUND1_X_DEC(24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39)
    PHK1024_KS_ROUND0_X_DEC(32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47)
    PHK1024_MIX1_ENC(ctx->rk[32], ctx->rk[33], ctx->rk[34], ctx->rk[35], ctx->rk[36], ctx->rk[37], ctx->rk[38], \
            ctx->rk[39], ctx->rk[32], ctx->rk[33], ctx->rk[34], ctx->rk[35], ctx->rk[36], ctx->rk[37], ctx->rk[38], \
            ctx->rk[39])
    PHK1024_KS_ROUND1_X_DEC(40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55)
    PHK1024_KS_ROUND0_X_DEC(48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63)
    PHK1024_MIX0_ENC(ctx->rk[48], ctx->rk[49], ctx->rk[50], ctx->rk[51], ctx->rk[52], ctx->rk[53], ctx->rk[54], \
            ctx->rk[55], ctx->rk[48], ctx->rk[49], ctx->rk[50], ctx->rk[51], ctx->rk[52], ctx->rk[53], ctx->rk[54], \
            ctx->rk[55])
    PHK1024_KS_ROUND1_X_DEC(56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71)
    PHK1024_KS_ROUND0_X_DEC(64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79)
    PHK1024_MIX1_ENC(ctx->rk[64], ctx->rk[65], ctx->rk[66], ctx->rk[67], ctx->rk[68], ctx->rk[69], ctx->rk[70], \
            ctx->rk[71], ctx->rk[64], ctx->rk[65], ctx->rk[66], ctx->rk[67], ctx->rk[68], ctx->rk[69], ctx->rk[70], \
            ctx->rk[71])
    PHK1024_KS_ROUND1_X_DEC(72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87)
    PHK1024_KS_ROUND0_X_DEC(80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95)
    PHK1024_MIX0_ENC(ctx->rk[80], ctx->rk[81], ctx->rk[82], ctx->rk[83], ctx->rk[84], ctx->rk[85], ctx->rk[86], \
            ctx->rk[87], ctx->rk[80], ctx->rk[81], ctx->rk[82], ctx->rk[83], ctx->rk[84], ctx->rk[85], ctx->rk[86], \
            ctx->rk[87])
    PHK1024_KS_ROUND1_X_DEC(88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103)
    PHK1024_KS_ROUND0_X_DEC(96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111)
    PHK1024_MIX1_ENC(ctx->rk[96], ctx->rk[97], ctx->rk[98], ctx->rk[99], ctx->rk[100], ctx->rk[101], ctx->rk[102], \
            ctx->rk[103], ctx->rk[96], ctx->rk[97], ctx->rk[98], ctx->rk[99], ctx->rk[100], ctx->rk[101], ctx->rk[102], \
            ctx->rk[103])
    PHK1024_KS_ROUND1_X_DEC(104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119)
    PHK1024_KS_ROUND0_X_DEC(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127)
    PHK1024_MIX0_ENC(ctx->rk[112], ctx->rk[113], ctx->rk[114], ctx->rk[115], ctx->rk[116], ctx->rk[117], ctx->rk[118], \
            ctx->rk[119], ctx->rk[112], ctx->rk[113], ctx->rk[114], ctx->rk[115], ctx->rk[116], ctx->rk[117], ctx->rk[118], \
            ctx->rk[119])
    PHK1024_KS_ROUND1_X_DEC(120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135)
    PHK1024_KS_ROUND0_X_DEC(128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143)
    PHK1024_MIX1_ENC(ctx->rk[128], ctx->rk[129], ctx->rk[130], ctx->rk[131], ctx->rk[132], ctx->rk[133], ctx->rk[134], \
            ctx->rk[135], ctx->rk[128], ctx->rk[129], ctx->rk[130], ctx->rk[131], ctx->rk[132], ctx->rk[133], ctx->rk[134], \
            ctx->rk[135])
    PHK1024_KS_ROUND1_X_DEC(136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151)
    PHK1024_KS_ROUND0_X_DEC(144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159)
    PHK1024_MIX0_ENC(ctx->rk[144], ctx->rk[145], ctx->rk[146], ctx->rk[147], ctx->rk[148], ctx->rk[149], ctx->rk[150], \
            ctx->rk[151], ctx->rk[144], ctx->rk[145], ctx->rk[146], ctx->rk[147], ctx->rk[148], ctx->rk[149], ctx->rk[150], \
            ctx->rk[151])
    PHK1024_KS_ROUND1_X_DEC(152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167)
    PHK1024_KS_ROUND0_X_DEC(160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175)
    PHK1024_MIX1_ENC(ctx->rk[160], ctx->rk[161], ctx->rk[162], ctx->rk[163], ctx->rk[164], ctx->rk[165], ctx->rk[166], \
            ctx->rk[167], ctx->rk[160], ctx->rk[161], ctx->rk[162], ctx->rk[163], ctx->rk[164], ctx->rk[165], ctx->rk[166], \
            ctx->rk[167])
    PHK1024_KS_ROUND1_X_DEC(168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183)
    PHK1024_KS_ROUND0_X_DEC(176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191)
    PHK1024_MIX0_ENC(ctx->rk[176], ctx->rk[177], ctx->rk[178], ctx->rk[179], ctx->rk[180], ctx->rk[181], ctx->rk[182], \
            ctx->rk[183], ctx->rk[176], ctx->rk[177], ctx->rk[178], ctx->rk[179], ctx->rk[180], ctx->rk[181], ctx->rk[182], \
            ctx->rk[183])
    PHK1024_KS_ROUND1_X_DEC(184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199)
    PHK1024_KS_ROUND0_X_DEC(192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207)
    PHK1024_MIX1_ENC(ctx->rk[192], ctx->rk[193], ctx->rk[194], ctx->rk[195], ctx->rk[196], ctx->rk[197], ctx->rk[198], \
            ctx->rk[199], ctx->rk[192], ctx->rk[193], ctx->rk[194], ctx->rk[195], ctx->rk[196], ctx->rk[197], ctx->rk[198], \
            ctx->rk[199])
    PHK1024_KS_ROUND1_X_DEC(200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215)
    PHK1024_KS_ROUND0_X_DEC(208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223)
    PHK1024_MIX0_ENC(ctx->rk[208], ctx->rk[209], ctx->rk[210], ctx->rk[211], ctx->rk[212], ctx->rk[213], ctx->rk[214], \
            ctx->rk[215], ctx->rk[208], ctx->rk[209], ctx->rk[210], ctx->rk[211], ctx->rk[212], ctx->rk[213], ctx->rk[214], \
            ctx->rk[215])
    PHK1024_KS_ROUND1_X_DEC(216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231)

    ctx->rk[224] = _mm_xor_si128(ctx->rk[224], phk1024_rc[28]);
}

void phk1024_256_set_key_decrypt(struct phk1024_256_ctx *const ctx, const uint8_t *const key) {
    __m128i tmp;

    ctx->rt = _mm_setzero_si128();

    ctx->rk[0] = _mm_loadu_si128((__m128i*)key);
    ctx->rk[1] = _mm_loadu_si128((__m128i*)&key[16]);

    PHK1024_KS_EXPAND(ctx->rk[0], ctx->rk[1], ctx->rk[2], ctx->rk[3], ctx->rk[4], ctx->rk[5], ctx->rk[6], ctx->rk[7], ctx->rk[0], ctx->rk[1])

    PHK1024_KS_ROUND_X_DEC_FIRST(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)
    PHK1024_KS_ROUND1_X_DEC(8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23)
    PHK1024_KS_ROUND0_X_DEC(16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31)
    PHK1024_MIX0_ENC(ctx->rk[16], ctx->rk[17], ctx->rk[18], ctx->rk[19], ctx->rk[20], ctx->rk[21], ctx->rk[22], \
            ctx->rk[23], ctx->rk[16], ctx->rk[17], ctx->rk[18], ctx->rk[19], ctx->rk[20], ctx->rk[21], ctx->rk[22], \
            ctx->rk[23])
    PHK1024_KS_ROUND1_X_DEC(24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39)
    PHK1024_KS_ROUND0_X_DEC(32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47)
    PHK1024_MIX1_ENC(ctx->rk[32], ctx->rk[33], ctx->rk[34], ctx->rk[35], ctx->rk[36], ctx->rk[37], ctx->rk[38], \
            ctx->rk[39], ctx->rk[32], ctx->rk[33], ctx->rk[34], ctx->rk[35], ctx->rk[36], ctx->rk[37], ctx->rk[38], \
            ctx->rk[39])
    PHK1024_KS_ROUND1_X_DEC(40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55)
    PHK1024_KS_ROUND0_X_DEC(48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63)
    PHK1024_MIX0_ENC(ctx->rk[48], ctx->rk[49], ctx->rk[50], ctx->rk[51], ctx->rk[52], ctx->rk[53], ctx->rk[54], \
            ctx->rk[55], ctx->rk[48], ctx->rk[49], ctx->rk[50], ctx->rk[51], ctx->rk[52], ctx->rk[53], ctx->rk[54], \
            ctx->rk[55])
    PHK1024_KS_ROUND1_X_DEC(56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71)
    PHK1024_KS_ROUND0_X_DEC(64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79)
    PHK1024_MIX1_ENC(ctx->rk[64], ctx->rk[65], ctx->rk[66], ctx->rk[67], ctx->rk[68], ctx->rk[69], ctx->rk[70], \
            ctx->rk[71], ctx->rk[64], ctx->rk[65], ctx->rk[66], ctx->rk[67], ctx->rk[68], ctx->rk[69], ctx->rk[70], \
            ctx->rk[71])
    PHK1024_KS_ROUND1_X_DEC(72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87)
    PHK1024_KS_ROUND0_X_DEC(80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95)
    PHK1024_MIX0_ENC(ctx->rk[80], ctx->rk[81], ctx->rk[82], ctx->rk[83], ctx->rk[84], ctx->rk[85], ctx->rk[86], \
            ctx->rk[87], ctx->rk[80], ctx->rk[81], ctx->rk[82], ctx->rk[83], ctx->rk[84], ctx->rk[85], ctx->rk[86], \
            ctx->rk[87])
    PHK1024_KS_ROUND1_X_DEC(88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103)
    PHK1024_KS_ROUND0_X_DEC(96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111)
    PHK1024_MIX1_ENC(ctx->rk[96], ctx->rk[97], ctx->rk[98], ctx->rk[99], ctx->rk[100], ctx->rk[101], ctx->rk[102], \
            ctx->rk[103], ctx->rk[96], ctx->rk[97], ctx->rk[98], ctx->rk[99], ctx->rk[100], ctx->rk[101], ctx->rk[102], \
            ctx->rk[103])
    PHK1024_KS_ROUND1_X_DEC(104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119)
    PHK1024_KS_ROUND0_X_DEC(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127)
    PHK1024_MIX0_ENC(ctx->rk[112], ctx->rk[113], ctx->rk[114], ctx->rk[115], ctx->rk[116], ctx->rk[117], ctx->rk[118], \
            ctx->rk[119], ctx->rk[112], ctx->rk[113], ctx->rk[114], ctx->rk[115], ctx->rk[116], ctx->rk[117], ctx->rk[118], \
            ctx->rk[119])
    PHK1024_KS_ROUND1_X_DEC(120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135)
    PHK1024_KS_ROUND0_X_DEC(128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143)
    PHK1024_MIX1_ENC(ctx->rk[128], ctx->rk[129], ctx->rk[130], ctx->rk[131], ctx->rk[132], ctx->rk[133], ctx->rk[134], \
            ctx->rk[135], ctx->rk[128], ctx->rk[129], ctx->rk[130], ctx->rk[131], ctx->rk[132], ctx->rk[133], ctx->rk[134], \
            ctx->rk[135])
    PHK1024_KS_ROUND1_X_DEC(136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151)
    PHK1024_KS_ROUND0_X_DEC(144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159)
    PHK1024_MIX0_ENC(ctx->rk[144], ctx->rk[145], ctx->rk[146], ctx->rk[147], ctx->rk[148], ctx->rk[149], ctx->rk[150], \
            ctx->rk[151], ctx->rk[144], ctx->rk[145], ctx->rk[146], ctx->rk[147], ctx->rk[148], ctx->rk[149], ctx->rk[150], \
            ctx->rk[151])
    PHK1024_KS_ROUND1_X_DEC(152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167)
    PHK1024_KS_ROUND0_X_DEC(160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175)
    PHK1024_MIX1_ENC(ctx->rk[160], ctx->rk[161], ctx->rk[162], ctx->rk[163], ctx->rk[164], ctx->rk[165], ctx->rk[166], \
            ctx->rk[167], ctx->rk[160], ctx->rk[161], ctx->rk[162], ctx->rk[163], ctx->rk[164], ctx->rk[165], ctx->rk[166], \
            ctx->rk[167])
    PHK1024_KS_ROUND1_X_DEC(168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183)
    PHK1024_KS_ROUND0_X_DEC(176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191)
    PHK1024_MIX0_ENC(ctx->rk[176], ctx->rk[177], ctx->rk[178], ctx->rk[179], ctx->rk[180], ctx->rk[181], ctx->rk[182], \
            ctx->rk[183], ctx->rk[176], ctx->rk[177], ctx->rk[178], ctx->rk[179], ctx->rk[180], ctx->rk[181], ctx->rk[182], \
            ctx->rk[183])
    PHK1024_KS_ROUND1_X_DEC(184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199)

    ctx->rk[192] = _mm_xor_si128(ctx->rk[192], phk1024_rc[24]);
}

void phk1024_set_tweak_encrypt(struct phk1024_ctx *const ctx, const uint8_t *const tweak) {
    // The function tau has a period of 13, which is why we jump in steps of 13
    __m128i tmp = ctx->rt;

    ctx->rt = _mm_loadu_si128((__m128i*)tweak);

    tmp = _mm_xor_si128(tmp, ctx->rt);

    PHK1024_TWEAK_WRITE(tmp, 0)
    PHK1024_TWEAK_WRITE(tmp, 13)
    PHK1024_TWEAK_WRITE(tmp, 26)
    PHK1024_TWEAK_MIX_WRITE(1)
    PHK1024_TWEAK_WRITE(tmp, 14)
    PHK1024_TWEAK_WRITE(tmp, 27)
    PHK1024_TWEAK_MIX_WRITE(2)
    PHK1024_TWEAK_WRITE(tmp, 15)
    PHK1024_TWEAK_WRITE(tmp, 28)
    PHK1024_TWEAK_MIX_WRITE(3)
    PHK1024_TWEAK_WRITE(tmp, 16)
    PHK1024_TWEAK_MIX_WRITE(4)
    PHK1024_TWEAK_WRITE(tmp, 17)
    PHK1024_TWEAK_MIX_WRITE(5)
    PHK1024_TWEAK_WRITE(tmp, 18)
    PHK1024_TWEAK_MIX_WRITE(6)
    PHK1024_TWEAK_WRITE(tmp, 19)
    PHK1024_TWEAK_MIX_WRITE(7)
    PHK1024_TWEAK_WRITE(tmp, 20)
    PHK1024_TWEAK_MIX_WRITE(8)
    PHK1024_TWEAK_WRITE(tmp, 21)
    PHK1024_TWEAK_MIX_WRITE(9)
    PHK1024_TWEAK_WRITE(tmp, 22)
    PHK1024_TWEAK_MIX_WRITE(10)
    PHK1024_TWEAK_WRITE(tmp, 23)
    PHK1024_TWEAK_MIX_WRITE(11)
    PHK1024_TWEAK_WRITE(tmp, 24)
    PHK1024_TWEAK_MIX_WRITE(12)
    PHK1024_TWEAK_WRITE(tmp, 25)
}

void phk1024_256_set_tweak_encrypt(struct phk1024_256_ctx *const ctx, const uint8_t *const tweak) {
    __m128i tmp = ctx->rt;

    ctx->rt = _mm_loadu_si128((__m128i*)tweak);

    tmp = _mm_xor_si128(tmp, ctx->rt);

    PHK1024_TWEAK_WRITE(tmp, 0)
    PHK1024_TWEAK_WRITE(tmp, 13)
    PHK1024_TWEAK_MIX_WRITE(1)
    PHK1024_TWEAK_WRITE(tmp, 14)
    PHK1024_TWEAK_MIX_WRITE(2)
    PHK1024_TWEAK_WRITE(tmp, 15)
    PHK1024_TWEAK_MIX_WRITE(3)
    PHK1024_TWEAK_WRITE(tmp, 16)
    PHK1024_TWEAK_MIX_WRITE(4)
    PHK1024_TWEAK_WRITE(tmp, 17)
    PHK1024_TWEAK_MIX_WRITE(5)
    PHK1024_TWEAK_WRITE(tmp, 18)
    PHK1024_TWEAK_MIX_WRITE(6)
    PHK1024_TWEAK_WRITE(tmp, 19)
    PHK1024_TWEAK_MIX_WRITE(7)
    PHK1024_TWEAK_WRITE(tmp, 20)
    PHK1024_TWEAK_MIX_WRITE(8)
    PHK1024_TWEAK_WRITE(tmp, 21)
    PHK1024_TWEAK_MIX_WRITE(9)
    PHK1024_TWEAK_WRITE(tmp, 22)
    PHK1024_TWEAK_MIX_WRITE(10)
    PHK1024_TWEAK_WRITE(tmp, 23)
    PHK1024_TWEAK_MIX_WRITE(11)
    PHK1024_TWEAK_WRITE(tmp, 24)
    PHK1024_TWEAK_MIX_WRITE(12)
}

void phk1024_set_tweak_decrypt(struct phk1024_ctx *const ctx, const uint8_t *const tweak) {
    __m128i tmp0 = ctx->rt;
    __m128i tmp1;

    ctx->rt = _mm_loadu_si128((__m128i*)tweak);

    tmp0 = _mm_xor_si128(tmp0, ctx->rt);

    PHK1024_TWEAK_WRITE(tmp0, 0)
    PHK1024_TWEAK_IMC_WRITE(tmp1, tmp0, 13)
    PHK1024_TWEAK_WRITE(tmp1, 26)

    PHK1024_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 1)
    PHK1024_TWEAK_WRITE(tmp1, 14)
    PHK1024_TWEAK_WRITE(tmp1, 27)

    PHK1024_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 2)
    PHK1024_TWEAK_WRITE(tmp1, 15)
    PHK1024_TWEAK_WRITE(tmp0, 28)

    PHK1024_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 3)
    PHK1024_TWEAK_WRITE(tmp1, 16)

    PHK1024_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 4)
    PHK1024_TWEAK_WRITE(tmp1, 17)

    PHK1024_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 5)
    PHK1024_TWEAK_WRITE(tmp1, 18)

    PHK1024_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 6)
    PHK1024_TWEAK_WRITE(tmp1, 19)

    PHK1024_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 7)
    PHK1024_TWEAK_WRITE(tmp1, 20)

    PHK1024_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 8)
    PHK1024_TWEAK_WRITE(tmp1, 21)

    PHK1024_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 9)
    PHK1024_TWEAK_WRITE(tmp1, 22)

    PHK1024_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 10)
    PHK1024_TWEAK_WRITE(tmp1, 23)

    PHK1024_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 11)
    PHK1024_TWEAK_WRITE(tmp1, 24)

    PHK1024_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 12)
    PHK1024_TWEAK_WRITE(tmp1, 25)

}


void phk1024_256_set_tweak_decrypt(struct phk1024_256_ctx *const ctx, const uint8_t *const tweak) {
    __m128i tmp0 = ctx->rt;
    __m128i tmp1;

    ctx->rt = _mm_loadu_si128((__m128i*)tweak);

    tmp0 = _mm_xor_si128(tmp0, ctx->rt);

    PHK1024_TWEAK_WRITE(tmp0, 0)
    PHK1024_TWEAK_IMC_WRITE(tmp1, tmp0, 13)

    PHK1024_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 1)
    PHK1024_TWEAK_WRITE(tmp1, 14)

    PHK1024_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 2)
    PHK1024_TWEAK_WRITE(tmp1, 15)

    PHK1024_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 3)
    PHK1024_TWEAK_WRITE(tmp1, 16)

    PHK1024_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 4)
    PHK1024_TWEAK_WRITE(tmp1, 17)

    PHK1024_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 5)
    PHK1024_TWEAK_WRITE(tmp1, 18)

    PHK1024_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 6)
    PHK1024_TWEAK_WRITE(tmp1, 19)

    PHK1024_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 7)
    PHK1024_TWEAK_WRITE(tmp1, 20)

    PHK1024_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 8)
    PHK1024_TWEAK_WRITE(tmp1, 21)

    PHK1024_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 9)
    PHK1024_TWEAK_WRITE(tmp1, 22)

    PHK1024_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 10)
    PHK1024_TWEAK_WRITE(tmp1, 23)

    PHK1024_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 11)
    PHK1024_TWEAK_WRITE(tmp0, 24)

    PHK1024_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 12)

}

#define PHK1024_ENC_STEP_X0(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p) \
    PHK1024_STEP0_ENC(state[0], state[1], state[2], state[3], \
            state[4], state[5], state[6], state[7], \
            ctx->rk[a], ctx->rk[b], ctx->rk[c], ctx->rk[d], \
            ctx->rk[e], ctx->rk[f], ctx->rk[g], ctx->rk[h], \
            ctx->rk[i], ctx->rk[j], ctx->rk[k], ctx->rk[l], \
            ctx->rk[m], ctx->rk[n], ctx->rk[o], ctx->rk[p])

#define PHK1024_ENC_STEP_X1(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p) \
    PHK1024_STEP1_ENC(state[0], state[1], state[2], state[3], \
            state[4], state[5], state[6], state[7], \
            ctx->rk[a], ctx->rk[b], ctx->rk[c], ctx->rk[d], \
            ctx->rk[e], ctx->rk[f], ctx->rk[g], ctx->rk[h], \
            ctx->rk[i], ctx->rk[j], ctx->rk[k], ctx->rk[l], \
            ctx->rk[m], ctx->rk[n], ctx->rk[o], ctx->rk[p])

void phk1024_encrypt(const struct phk1024_ctx * const ctx, uint8_t * const buf) {
    __m128i state[PHK1024_SUBSTATES], tmp;

    // init plaintext and add initial round key
    state[0] = _mm_loadu_si128((__m128i*)buf);
    state[1] = _mm_loadu_si128((__m128i*)&buf[16]);
    state[2] = _mm_loadu_si128((__m128i*)&buf[32]);
    state[3] = _mm_loadu_si128((__m128i*)&buf[48]);
    state[4] = _mm_loadu_si128((__m128i*)&buf[64]);
    state[5] = _mm_loadu_si128((__m128i*)&buf[80]);
    state[6] = _mm_loadu_si128((__m128i*)&buf[96]);
    state[7] = _mm_loadu_si128((__m128i*)&buf[112]);

    state[0] = _mm_xor_si128(state[0], ctx->rk[0]);
    state[1] = _mm_xor_si128(state[1], ctx->rk[1]);
    state[2] = _mm_xor_si128(state[2], ctx->rk[2]);
    state[3] = _mm_xor_si128(state[3], ctx->rk[3]);
    state[4] = _mm_xor_si128(state[4], ctx->rk[4]);
    state[5] = _mm_xor_si128(state[5], ctx->rk[5]);
    state[6] = _mm_xor_si128(state[6], ctx->rk[6]);
    state[7] = _mm_xor_si128(state[7], ctx->rk[7]);

    PHK1024_ENC_STEP_X0(8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23)
    PHK1024_ENC_STEP_X1(24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39)
    PHK1024_ENC_STEP_X0(40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55)
    PHK1024_ENC_STEP_X1(56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71)
    PHK1024_ENC_STEP_X0(72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87)
    PHK1024_ENC_STEP_X1(88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103)
    PHK1024_ENC_STEP_X0(104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119)
    PHK1024_ENC_STEP_X1(120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135)
    PHK1024_ENC_STEP_X0(136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151)
    PHK1024_ENC_STEP_X1(152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167)
    PHK1024_ENC_STEP_X0(168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183)
    PHK1024_ENC_STEP_X1(184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199)
    PHK1024_ENC_STEP_X0(200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215)
    PHK1024_STEP_ENC_LAST(state[0], state[1], state[2], state[3],
                       state[4], state[5], state[6], state[7],
                       ctx->rk[216], ctx->rk[217], ctx->rk[218], ctx->rk[219],
                       ctx->rk[220], ctx->rk[221], ctx->rk[222], ctx->rk[223],
                      ctx->rk[224], ctx->rk[225], ctx->rk[226], ctx->rk[227],
                      ctx->rk[228], ctx->rk[229], ctx->rk[230], ctx->rk[231])

    _mm_storeu_si128((__m128i*)buf, state[0]);
    _mm_storeu_si128((__m128i*)&buf[16], state[1]);
    _mm_storeu_si128((__m128i*)&buf[32], state[2]);
    _mm_storeu_si128((__m128i*)&buf[48], state[3]);
    _mm_storeu_si128((__m128i*)&buf[64], state[4]);
    _mm_storeu_si128((__m128i*)&buf[80], state[5]);
    _mm_storeu_si128((__m128i*)&buf[96], state[6]);
    _mm_storeu_si128((__m128i*)&buf[112], state[7]);
}

void phk1024_256_encrypt(const struct phk1024_256_ctx * const ctx, uint8_t * const buf) {
    __m128i state[PHK1024_SUBSTATES], tmp;

    // init plaintext and add initial round key
    state[0] = _mm_loadu_si128((__m128i*)buf);
    state[1] = _mm_loadu_si128((__m128i*)&buf[16]);
    state[2] = _mm_loadu_si128((__m128i*)&buf[32]);
    state[3] = _mm_loadu_si128((__m128i*)&buf[48]);
    state[4] = _mm_loadu_si128((__m128i*)&buf[64]);
    state[5] = _mm_loadu_si128((__m128i*)&buf[80]);
    state[6] = _mm_loadu_si128((__m128i*)&buf[96]);
    state[7] = _mm_loadu_si128((__m128i*)&buf[112]);

    state[0] = _mm_xor_si128(state[0], ctx->rk[0]);
    state[1] = _mm_xor_si128(state[1], ctx->rk[1]);
    state[2] = _mm_xor_si128(state[2], ctx->rk[2]);
    state[3] = _mm_xor_si128(state[3], ctx->rk[3]);
    state[4] = _mm_xor_si128(state[4], ctx->rk[4]);
    state[5] = _mm_xor_si128(state[5], ctx->rk[5]);
    state[6] = _mm_xor_si128(state[6], ctx->rk[6]);
    state[7] = _mm_xor_si128(state[7], ctx->rk[7]);

    PHK1024_ENC_STEP_X0(8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23)
    PHK1024_ENC_STEP_X1(24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39)
    PHK1024_ENC_STEP_X0(40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55)
    PHK1024_ENC_STEP_X1(56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71)
    PHK1024_ENC_STEP_X0(72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87)
    PHK1024_ENC_STEP_X1(88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103)
    PHK1024_ENC_STEP_X0(104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119)
    PHK1024_ENC_STEP_X1(120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135)
    PHK1024_ENC_STEP_X0(136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151)
    PHK1024_ENC_STEP_X1(152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167)
    PHK1024_ENC_STEP_X0(168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183)
    PHK1024_STEP_ENC_LAST(state[0], state[1], state[2], state[3],
                       state[4], state[5], state[6], state[7],
                       ctx->rk[184], ctx->rk[185], ctx->rk[186], ctx->rk[187],
                       ctx->rk[188], ctx->rk[189], ctx->rk[190], ctx->rk[191],
                      ctx->rk[192], ctx->rk[193], ctx->rk[194], ctx->rk[195],
                      ctx->rk[196], ctx->rk[197], ctx->rk[198], ctx->rk[199])

    _mm_storeu_si128((__m128i*)buf, state[0]);
    _mm_storeu_si128((__m128i*)&buf[16], state[1]);
    _mm_storeu_si128((__m128i*)&buf[32], state[2]);
    _mm_storeu_si128((__m128i*)&buf[48], state[3]);
    _mm_storeu_si128((__m128i*)&buf[64], state[4]);
    _mm_storeu_si128((__m128i*)&buf[80], state[5]);
    _mm_storeu_si128((__m128i*)&buf[96], state[6]);
    _mm_storeu_si128((__m128i*)&buf[112], state[7]);
}

#define PHK1024_DEC_STEP_X0(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p) \
    PHK1024_STEP0_DEC(state[0], state[1], state[2], state[3], \
            state[4], state[5], state[6], state[7], \
            ctx->rk[a], ctx->rk[b], ctx->rk[c], ctx->rk[d], \
            ctx->rk[e], ctx->rk[f], ctx->rk[g], ctx->rk[h], \
            ctx->rk[i], ctx->rk[j], ctx->rk[k], ctx->rk[l], \
            ctx->rk[m], ctx->rk[n], ctx->rk[o], ctx->rk[p])

#define PHK1024_DEC_STEP_X1(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p) \
    PHK1024_STEP1_DEC(state[0], state[1], state[2], state[3], \
            state[4], state[5], state[6], state[7], \
            ctx->rk[a], ctx->rk[b], ctx->rk[c], ctx->rk[d], \
            ctx->rk[e], ctx->rk[f], ctx->rk[g], ctx->rk[h], \
            ctx->rk[i], ctx->rk[j], ctx->rk[k], ctx->rk[l], \
            ctx->rk[m], ctx->rk[n], ctx->rk[o], ctx->rk[p])

void phk1024_decrypt(const struct phk1024_ctx * const ctx, uint8_t * const buf) {
    __m128i state[PHK1024_SUBSTATES], tmp;

    // init ciphertext
    state[0] = _mm_loadu_si128((__m128i*)buf);
    state[1] = _mm_loadu_si128((__m128i*)&buf[16]);
    state[2] = _mm_loadu_si128((__m128i*)&buf[32]);
    state[3] = _mm_loadu_si128((__m128i*)&buf[48]);
    state[4] = _mm_loadu_si128((__m128i*)&buf[64]);
    state[5] = _mm_loadu_si128((__m128i*)&buf[80]);
    state[6] = _mm_loadu_si128((__m128i*)&buf[96]);
    state[7] = _mm_loadu_si128((__m128i*)&buf[112]);

    // remove last round key
    state[0] = _mm_xor_si128(state[0], ctx->rk[224]);
    state[1] = _mm_xor_si128(state[1], ctx->rk[225]);
    state[2] = _mm_xor_si128(state[2], ctx->rk[226]);
    state[3] = _mm_xor_si128(state[3], ctx->rk[227]);
    state[4] = _mm_xor_si128(state[4], ctx->rk[228]);
    state[5] = _mm_xor_si128(state[5], ctx->rk[229]);
    state[6] = _mm_xor_si128(state[6], ctx->rk[230]);
    state[7] = _mm_xor_si128(state[7], ctx->rk[231]);

    PHK1024_DEC_STEP_X0(208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223)
    PHK1024_DEC_STEP_X1(192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207)
    PHK1024_DEC_STEP_X0(176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191)
    PHK1024_DEC_STEP_X1(160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175)
    PHK1024_DEC_STEP_X0(144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159)
    PHK1024_DEC_STEP_X1(128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143)
    PHK1024_DEC_STEP_X0(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127)
    PHK1024_DEC_STEP_X1(96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111)
    PHK1024_DEC_STEP_X0(80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95)
    PHK1024_DEC_STEP_X1(64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79)
    PHK1024_DEC_STEP_X0(48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63)
    PHK1024_DEC_STEP_X1(32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47)
    PHK1024_DEC_STEP_X0(16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31)
    PHK1024_STEP_DEC_LAST(state[0], state[1], state[2], state[3],
                          state[4], state[5], state[6], state[7],
                          ctx->rk[0], ctx->rk[1], ctx->rk[2], ctx->rk[3],
                          ctx->rk[4], ctx->rk[5], ctx->rk[6], ctx->rk[7],
                          ctx->rk[8], ctx->rk[9], ctx->rk[10], ctx->rk[11],
                          ctx->rk[12], ctx->rk[13], ctx->rk[14], ctx->rk[15])

    _mm_storeu_si128((__m128i*)buf, state[0]);
    _mm_storeu_si128((__m128i*)&buf[16], state[1]);
    _mm_storeu_si128((__m128i*)&buf[32], state[2]);
    _mm_storeu_si128((__m128i*)&buf[48], state[3]);
    _mm_storeu_si128((__m128i*)&buf[64], state[4]);
    _mm_storeu_si128((__m128i*)&buf[80], state[5]);
    _mm_storeu_si128((__m128i*)&buf[96], state[6]);
    _mm_storeu_si128((__m128i*)&buf[112], state[7]);
}

void phk1024_256_decrypt(const struct phk1024_256_ctx * const ctx, uint8_t * const buf) {
    __m128i state[PHK1024_SUBSTATES], tmp;

    // init ciphertext
    state[0] = _mm_loadu_si128((__m128i*)buf);
    state[1] = _mm_loadu_si128((__m128i*)&buf[16]);
    state[2] = _mm_loadu_si128((__m128i*)&buf[32]);
    state[3] = _mm_loadu_si128((__m128i*)&buf[48]);
    state[4] = _mm_loadu_si128((__m128i*)&buf[64]);
    state[5] = _mm_loadu_si128((__m128i*)&buf[80]);
    state[6] = _mm_loadu_si128((__m128i*)&buf[96]);
    state[7] = _mm_loadu_si128((__m128i*)&buf[112]);

    // remove last round key
    state[0] = _mm_xor_si128(state[0], ctx->rk[192]);
    state[1] = _mm_xor_si128(state[1], ctx->rk[193]);
    state[2] = _mm_xor_si128(state[2], ctx->rk[194]);
    state[3] = _mm_xor_si128(state[3], ctx->rk[195]);
    state[4] = _mm_xor_si128(state[4], ctx->rk[196]);
    state[5] = _mm_xor_si128(state[5], ctx->rk[197]);
    state[6] = _mm_xor_si128(state[6], ctx->rk[198]);
    state[7] = _mm_xor_si128(state[7], ctx->rk[199]);

    PHK1024_DEC_STEP_X0(176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191)
    PHK1024_DEC_STEP_X1(160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175)
    PHK1024_DEC_STEP_X0(144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159)
    PHK1024_DEC_STEP_X1(128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143)
    PHK1024_DEC_STEP_X0(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127)
    PHK1024_DEC_STEP_X1(96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111)
    PHK1024_DEC_STEP_X0(80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95)
    PHK1024_DEC_STEP_X1(64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79)
    PHK1024_DEC_STEP_X0(48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63)
    PHK1024_DEC_STEP_X1(32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47)
    PHK1024_DEC_STEP_X0(16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31)
    PHK1024_STEP_DEC_LAST(state[0], state[1], state[2], state[3],
                          state[4], state[5], state[6], state[7],
                          ctx->rk[0], ctx->rk[1], ctx->rk[2], ctx->rk[3],
                          ctx->rk[4], ctx->rk[5], ctx->rk[6], ctx->rk[7],
                          ctx->rk[8], ctx->rk[9], ctx->rk[10], ctx->rk[11],
                          ctx->rk[12], ctx->rk[13], ctx->rk[14], ctx->rk[15])

    _mm_storeu_si128((__m128i*)buf, state[0]);
    _mm_storeu_si128((__m128i*)&buf[16], state[1]);
    _mm_storeu_si128((__m128i*)&buf[32], state[2]);
    _mm_storeu_si128((__m128i*)&buf[48], state[3]);
    _mm_storeu_si128((__m128i*)&buf[64], state[4]);
    _mm_storeu_si128((__m128i*)&buf[80], state[5]);
    _mm_storeu_si128((__m128i*)&buf[96], state[6]);
    _mm_storeu_si128((__m128i*)&buf[112], state[7]);
}

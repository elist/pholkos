#include "ctrt_phk.h"
#include <stdio.h>


void ctrt_phk_256(uint8_t * const key, uint8_t * const tweak, \
                  uint8_t * const nonce, uint8_t * buf, int length) {
  __m128i tmp, tmp1, tmp2, tmp3, tmp4;
  size_t original_length = length;

  phk256_ctx ctx1, ctx2, ctx3, ctx4;
  uint8_t nonce1[PHK_SUBSTATE_SIZE * PHK256_SUBSTATES];
  uint8_t nonce2[PHK_SUBSTATE_SIZE * PHK256_SUBSTATES];
  uint8_t nonce3[PHK_SUBSTATE_SIZE * PHK256_SUBSTATES];
  uint8_t nonce4[PHK_SUBSTATE_SIZE * PHK256_SUBSTATES];

  phk256_load_constants(&ctx1);
  phk256_set_key_encrypt(&ctx1, key);

  phk256_set_tweak_encrypt(&ctx1, tweak);

  ctx2.rt = ctx1.rt;
  ctx3.rt = ctx1.rt;
  ctx4.rt = ctx1.rt;

  std::memcpy(ctx2.rk, ctx1.rk, (PHK256_STEPS * PHK_AES_ROUNDS + 1) * PHK256_SUBSTATES * PHK_SUBSTATE_SIZE);
  std::memcpy(ctx3.rk, ctx1.rk, (PHK256_STEPS * PHK_AES_ROUNDS + 1) * PHK256_SUBSTATES * PHK_SUBSTATE_SIZE);
  std::memcpy(ctx4.rk, ctx1.rk, (PHK256_STEPS * PHK_AES_ROUNDS + 1) * PHK256_SUBSTATES * PHK_SUBSTATE_SIZE);

  __m128i int_tweak = _mm_loadu_si128((__m128i*) tweak);
  __m128i ctr1 = int_tweak, ctr2 = int_tweak, ctr3 = int_tweak, ctr4 = int_tweak;

  //increment_counter_three_times(int_tweak, ctr2, ctr3, ctr4);
  PHK_INC_THREE_CTRS(int_tweak, ctr2, ctr3, ctr4);

  while (length >= 4) {
    std::memcpy(nonce1, nonce, PHK_SUBSTATE_SIZE * PHK256_SUBSTATES);
    std::memcpy(nonce2, nonce, PHK_SUBSTATE_SIZE * PHK256_SUBSTATES);
    std::memcpy(nonce3, nonce, PHK_SUBSTATE_SIZE * PHK256_SUBSTATES);
    std::memcpy(nonce4, nonce, PHK_SUBSTATE_SIZE * PHK256_SUBSTATES);



    phk256_update_4tweak_encrypt(&ctx1, _mm_xor_si128(ctx1.rt, ctr1),\
                                 &ctx2, _mm_xor_si128(ctx2.rt, ctr2),\
                                 &ctx3, _mm_xor_si128(ctx3.rt, ctr3),\
                                 &ctx4, _mm_xor_si128(ctx4.rt, ctr4));

    // ENCRYPT FOUR BLOCKS
    phk256x4_encrypt_diff_tweakeys(&ctx1, nonce1, &ctx2, nonce2, \
                                   &ctx3, nonce3, &ctx4, nonce4);

    // XOR encryptions onto buf
    size_t offset = (original_length - length) * PHK256_NUM_STATE_BYTES;
    for (size_t iter = 0; iter < PHK256_NUM_STATE_BYTES; ++iter) {
      buf[offset + iter] = buf[offset + iter] ^ nonce1[iter];
      buf[offset + PHK256_NUM_STATE_BYTES + iter] = buf[offset + PHK256_NUM_STATE_BYTES + iter] ^ nonce2[iter];
      buf[offset + 2*PHK256_NUM_STATE_BYTES + iter] = buf[offset + 2*PHK256_NUM_STATE_BYTES + iter] ^ nonce3[iter];
      buf[offset + 3*PHK256_NUM_STATE_BYTES + iter] = buf[offset + 3*PHK256_NUM_STATE_BYTES + iter] ^ nonce4[iter];
    }


    length -= 4;
    int_tweak = ctr1;
    PHK_INC_FOUR_CTRS(int_tweak, ctr1, ctr2, ctr3, ctr4)

  }

  if (length == 3) {

  } else if (length == 2) {

  } else if (length == 1) {

  }




}



#include <algorithm>

#include "phk-256.h"
#include "phk-512.h"
#include "phk-1024.h"

#include "misc.h"


typedef void (*enc_function)(const void * const ctx, uint8_t * const buf);
typedef void (*key_function)(const void * const ctx, uint8_t * const buf);
typedef void (*tweak_function)(const void * const ctx, uint8_t * const buf);

void create_test_vectors(void * const ctx, enc_function enc,  tweak_function set_tweak, key_function set_key, size_t state_size) {
    uint8_t *key(phk_create_buffer(state_size));
    uint8_t *state(phk_create_buffer(state_size));
    uint8_t *tweak(phk_create_buffer(PHK_TWEAK_SIZE));

    std::fill(key, &key[state_size], 0);
    std::fill(state, &state[state_size], 0);
    std::fill(tweak, &tweak[PHK_TWEAK_SIZE], 0);

    std::cout << "Tweak:      ";
    misc::print_buffer(tweak, PHK_TWEAK_SIZE, false);
    std::cout << "Key:        ";
    misc::print_buffer(key, state_size, false);
    std::cout << "Plaintext:  ";
    misc::print_buffer(state, state_size, false);

    set_key(ctx, key);
    set_tweak(ctx, tweak);

    enc(ctx, state);

    std::cout << "Ciphertext: ";
    misc::print_buffer(state, state_size, false);

    for (size_t i = 0; i < PHK_TWEAK_SIZE; ++i) {
        tweak[i] = i;
    }

    std::cout << std::endl;

    std::fill(state, &state[state_size], 0);

    std::cout << "Tweak:      ";
    misc::print_buffer(tweak, PHK_TWEAK_SIZE, false);
    std::cout << "Key:        ";
    misc::print_buffer(key, state_size, false);
    std::cout << "Plaintext:  ";
    misc::print_buffer(state, state_size, false);

    set_tweak(ctx, tweak);

    enc(ctx, state);

    std::cout << "Ciphertext: ";
    misc::print_buffer(state, state_size, false);

    std::cout << std::endl;

    std::cout << "Tweak:      ";
    misc::print_buffer(tweak, PHK_TWEAK_SIZE, false);
    std::cout << "Key:        ";
    misc::print_buffer(state, state_size, false);
    std::cout << "Plaintext:  ";
    misc::print_buffer(key, state_size, false);

    set_key(ctx, state);
    set_tweak(ctx, tweak);

    enc(ctx, key);

    std::cout << "Ciphertext: ";
    misc::print_buffer(key, state_size, false);

    std::cout << std::endl;

    std::cout << "Tweak:      ";
    misc::print_buffer(tweak, PHK_TWEAK_SIZE, false);
    std::cout << "Key:        ";
    misc::print_buffer(key, state_size, false);
    std::cout << "Plaintext:  ";
    misc::print_buffer(state, state_size, false);

    set_key(ctx, key);
    set_tweak(ctx, tweak);

    enc(ctx, state);

    std::cout << "Ciphertext: ";
    misc::print_buffer(state, state_size, false);

    delete[] key;
    delete[] tweak;
    delete[] state;
}

int main() {
    std::cout << "Phk-256" << std::endl << std::endl;

    phk256_ctx ctx256;
    phk256_load_constants(&ctx256);

    create_test_vectors(&ctx256, (enc_function)phk256_encrypt,
                        (tweak_function)phk256_set_tweak_encrypt, (key_function)phk256_set_key_encrypt,
            PHK256_SUBSTATES * PHK_SUBSTATE_SIZE);

    std::cout << std::endl << "Phk-512" << std::endl << std::endl;

    phk512_load_constants();
    phk512_ctx ctx512;

    create_test_vectors(&ctx512, (enc_function)phk512_encrypt,
                        (tweak_function)phk512_set_tweak_encrypt, (key_function)phk512_set_key_encrypt,
                        PHK512_SUBSTATES * PHK_SUBSTATE_SIZE);

    std::cout << std::endl << "Phk-1024" << std::endl << std::endl;

    phk1024_load_constants();
    phk1024_ctx ctx1024;

    create_test_vectors(&ctx1024, (enc_function)phk1024_encrypt,
                        (tweak_function)phk1024_set_tweak_encrypt, (key_function)phk1024_set_key_encrypt,
                        PHK1024_SUBSTATES * PHK_SUBSTATE_SIZE);

    return 0;
}

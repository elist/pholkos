#include <iostream>
#include <iomanip>

#include "phk-256.h"
#include "phk-512.h"
#include "phk-1024.h"
#include "misc.h"

typedef void (*enc_function)(const void * const ctx, uint8_t * const buf);
typedef void (*dec_function)(const void * const ctx, uint8_t * const buf);

bool _mm_eq(__m128i lhs, __m128i rhs) {
  if (1 == _mm_test_all_ones(_mm_cmpeq_epi32(lhs, rhs))) {
    return true;
  } else {
    misc::print_m128i(lhs, "lhs");
    misc::print_m128i(rhs, "rhs");
    return false;
  }
}

bool test_big_addition() {



  std::cout << "Testing single addition" << std::endl;
  __m128i lhs, rhs, rhs2, rhs3, rhs4, res, res2, res3, res4, tmp, tmp1, tmp2, tmp3, tmp4;
  bool test = true;

  lhs = _mm_set_epi32(0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff);
  rhs = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000001);
  res = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000000);
  test &= _mm_eq(misc::add_128si(lhs, rhs), res);

  lhs = _mm_set_epi32(0x00000000, 0x00000000, 0xffffffff, 0xffffffff);
  rhs = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000001);
  res = _mm_set_epi32(0x00000000, 0x00000001, 0x00000000, 0x00000000);
  test &= _mm_eq(misc::add_128si(lhs, rhs), res);

  lhs = _mm_set_epi32(0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff);
  rhs = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000002);
  res = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000001);
  test &= _mm_eq(misc::add_128si(lhs, rhs), res);

  lhs = _mm_set_epi32(0x00000000, 0x00000000, 0xffffffff, 0xffffffff);
  rhs = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000002);
  res = _mm_set_epi32(0x00000000, 0x00000001, 0x00000000, 0x00000001);
  test &= _mm_eq(misc::add_128si(lhs, rhs), res);

  lhs = _mm_set_epi32(0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff);
  rhs = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000003);
  res = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000002);
  test &= _mm_eq(misc::add_128si(lhs, rhs), res);

  lhs = _mm_set_epi32(0x00000000, 0x00000000, 0xffffffff, 0xffffffff);
  rhs = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000003);
  res = _mm_set_epi32(0x00000000, 0x00000001, 0x00000000, 0x00000002);
  test &= _mm_eq(misc::add_128si(lhs, rhs), res);

  lhs = _mm_set_epi32(0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff);
  rhs = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000004);
  res = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000003);
  test &= _mm_eq(misc::add_128si(lhs, rhs), res);

  lhs = _mm_set_epi32(0x00000000, 0x00000000, 0xffffffff, 0xffffffff);
  rhs = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000004);
  res = _mm_set_epi32(0x00000000, 0x00000001, 0x00000000, 0x00000003);
  test &= _mm_eq(misc::add_128si(lhs, rhs), res);

  lhs = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0xffffffff);
  rhs = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000000);
  res = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0xffffffff);
  test &= _mm_eq(misc::add_128si(lhs, rhs), res);

  lhs = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0xffffffff);
  rhs = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000004);
  res = _mm_set_epi32(0x00000000, 0x00000000, 0x00000001, 0x00000003);
  test &= _mm_eq(misc::add_128si(lhs, rhs), res);

  lhs = _mm_set_epi32(0x00000000, 0x00000000, 0x80000000, 0xffffffff);
  rhs = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000004);
  res = _mm_set_epi32(0x00000000, 0x00000000, 0x80000001, 0x00000003);
  test &= _mm_eq(misc::add_128si(lhs, rhs), res);

  lhs = _mm_set_epi32(0x00000000, 0x00000000, 0xffffffff, 0xffffffff);
  rhs = _mm_set_epi32(0x00000000, 0x00000000, 0x80000000, 0x00000001);
  res = _mm_set_epi32(0x00000000, 0x00000001, 0x80000000, 0x00000000);
  test &= _mm_eq(misc::add_128si(lhs, rhs), res);

  if (!test) {
    std::cout << "single addition failed!" << std::endl;
    return false;
  }


  std::cout << "Testing three additions" << std::endl;

  lhs = _mm_set_epi32(0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff);
  res = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000000);
  res2 = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000001);
  res3 = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000002);
  PHK_INC_THREE_CTRS(lhs, rhs, rhs2, rhs3)
  test &= _mm_eq(rhs, res);
  test &= _mm_eq(rhs2, res2);
  test &= _mm_eq(rhs3, res3);

  lhs = _mm_set_epi32(0x00000000, 0x00000000, 0xffffffff, 0xffffffff);
  res = _mm_set_epi32(0x00000000, 0x00000001, 0x00000000, 0x00000000);
  res2 = _mm_set_epi32(0x00000000, 0x00000001, 0x00000000, 0x00000001);
  res3 = _mm_set_epi32(0x00000000, 0x00000001, 0x00000000, 0x00000002);
  PHK_INC_THREE_CTRS(lhs, rhs, rhs2, rhs3)
  test &= _mm_eq(rhs, res);
  test &= _mm_eq(rhs2, res2);
  test &= _mm_eq(rhs3, res3);

  lhs = _mm_set_epi32(0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff);
  res = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000000);
  res2 = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000001);
  res3 = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000002);
  PHK_INC_THREE_CTRS(lhs, rhs, rhs2, rhs3)
  test &= _mm_eq(rhs, res);
  test &= _mm_eq(rhs2, res2);
  test &= _mm_eq(rhs3, res3);

  lhs = _mm_set_epi32(0x00000000, 0x00030000, 0xffffffff, 0xfffffffe);
  res = _mm_set_epi32(0x00000000, 0x00030000, 0xffffffff, 0xffffffff);
  res2 = _mm_set_epi32(0x00000000, 0x00030001, 0x00000000, 0x00000000);
  res3 = _mm_set_epi32(0x00000000, 0x00030001, 0x00000000, 0x00000001);
  PHK_INC_THREE_CTRS(lhs, rhs, rhs2, rhs3)
  test &= _mm_eq(rhs, res);
  test &= _mm_eq(rhs2, res2);
  test &= _mm_eq(rhs3, res3);


  lhs = _mm_set_epi32(0x00000000, 0x00000001, 0x00000000, 0x00000000);
  res = _mm_set_epi32(0x00000000, 0x00000001, 0x00000000, 0x00000001);
  res2 = _mm_set_epi32(0x00000000, 0x00000001, 0x00000000, 0x00000002);
  res3 = _mm_set_epi32(0x00000000, 0x00000001, 0x00000000, 0x00000003);
  PHK_INC_THREE_CTRS(lhs, rhs, rhs2, rhs3)
  test &= _mm_eq(rhs, res);
  test &= _mm_eq(rhs2, res2);
  test &= _mm_eq(rhs3, res3);

  if (!test) {
    std::cout << "three additions failed!" << std::endl;
    return false;
  }

  std::cout << "Testing four additions" << std::endl;

  lhs = _mm_set_epi32(0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff);
  res = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000003);
  res2 = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000004);
  res3 = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000005);
  res4 = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000006);
  PHK_INC_FOUR_CTRS(lhs, rhs, rhs2, rhs3, rhs4)
  test &= _mm_eq(rhs, res);
  test &= _mm_eq(rhs2, res2);
  test &= _mm_eq(rhs3, res3);
  test &= _mm_eq(rhs4, res4);

  lhs = _mm_set_epi32(0x00000000, 0x00000000, 0xffffffff, 0xffffffff);
  res = _mm_set_epi32(0x00000000, 0x00000001, 0x00000000, 0x00000003);
  res2 = _mm_set_epi32(0x00000000, 0x00000001, 0x00000000, 0x00000004);
  res3 = _mm_set_epi32(0x00000000, 0x00000001, 0x00000000, 0x00000005);
  res4 = _mm_set_epi32(0x00000000, 0x00000001, 0x00000000, 0x00000006);
  PHK_INC_FOUR_CTRS(lhs, rhs, rhs2, rhs3, rhs4)
  test &= _mm_eq(rhs, res);
  test &= _mm_eq(rhs2, res2);
  test &= _mm_eq(rhs3, res3);
  test &= _mm_eq(rhs4, res4);

  lhs = _mm_set_epi32(0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff);
  res = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000003);
  res2 = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000004);
  res3 = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000005);
  res4 = _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000006);
  PHK_INC_FOUR_CTRS(lhs, rhs, rhs2, rhs3, rhs4)
  test &= _mm_eq(rhs, res);
  test &= _mm_eq(rhs2, res2);
  test &= _mm_eq(rhs3, res3);
  test &= _mm_eq(rhs4, res4);

  lhs = _mm_set_epi32(0x00000000, 0x00030000, 0xffffffff, 0xfffffffb);
  res = _mm_set_epi32(0x00000000, 0x00030000, 0xffffffff, 0xffffffff);
  res2 = _mm_set_epi32(0x00000000, 0x00030001, 0x00000000, 0x00000000);
  res3 = _mm_set_epi32(0x00000000, 0x00030001, 0x00000000, 0x00000001);
  res4 = _mm_set_epi32(0x00000000, 0x00030001, 0x00000000, 0x00000002);
  PHK_INC_FOUR_CTRS(lhs, rhs, rhs2, rhs3, rhs4)
  test &= _mm_eq(rhs, res);
  test &= _mm_eq(rhs2, res2);
  test &= _mm_eq(rhs3, res3);
  test &= _mm_eq(rhs4, res4);


  lhs = _mm_set_epi32(0x00000000, 0x00000001, 0x00000000, 0x00000000);
  res = _mm_set_epi32(0x00000000, 0x00000001, 0x00000000, 0x00000004);
  res2 = _mm_set_epi32(0x00000000, 0x00000001, 0x00000000, 0x00000005);
  res3 = _mm_set_epi32(0x00000000, 0x00000001, 0x00000000, 0x00000006);
  res4 = _mm_set_epi32(0x00000000, 0x00000001, 0x00000000, 0x00000007);
  PHK_INC_FOUR_CTRS(lhs, rhs, rhs2, rhs3, rhs4)
  test &= _mm_eq(rhs, res);
  test &= _mm_eq(rhs2, res2);
  test &= _mm_eq(rhs3, res3);
  test &= _mm_eq(rhs4, res4);

  if (!test) {
    std::cout << "four additions failed!" << std::endl;
    return false;
  }

  return test;
}

bool test_ts_permutation() {
    uint8_t buf[16] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    __m128i t = _mm_loadu_si128((__m128i*)buf);

    PHK_TS_MIX(t);

    _mm_storeu_si128((__m128i *) buf, t);

    return buf[0] == 8 &&
        buf[1] == 1 &&
        buf[2] == 7 &&
        buf[3] == 15 &&
        buf[4] == 10 &&
        buf[5] == 4 &&
        buf[6] == 2 &&
        buf[7] == 3 &&
        buf[8] == 6 &&
        buf[9] == 9 &&
        buf[10] == 11 &&
        buf[11] == 0 &&
        buf[12] == 5 &&
        buf[13] == 12 &&
        buf[14] == 14 &&
        buf[15] == 13;
}

bool test_pi_256() {
    uint32_t buf[8] {0, 1, 2, 3, 4, 5, 6, 7};
    __m128i a = _mm_loadu_si128((__m128i*)buf);
    __m128i b = _mm_loadu_si128((__m128i*)&buf[4]);
    __m128i tmp;

    PHK256_MIX_ENC(a, b, a, b);

    _mm_storeu_si128((__m128i *) buf, a);
    _mm_storeu_si128((__m128i *) &buf[4], b);

    return buf[0] == 0 &&
           buf[1] == 5 &&
           buf[2] == 2 &&
           buf[3] == 7 &&
           buf[4] == 4 &&
           buf[5] == 1 &&
           buf[6] == 6 &&
           buf[7] == 3;
}

bool test_pi_512() {
    uint32_t buf[16] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    __m128i a = _mm_loadu_si128((__m128i*)buf);
    __m128i b = _mm_loadu_si128((__m128i*)&buf[4]);
    __m128i c = _mm_loadu_si128((__m128i*)&buf[8]);
    __m128i d = _mm_loadu_si128((__m128i*)&buf[12]);
    __m128i tmp;

    PHK512_MIX_ENC(a, b, c, d, a, b, c, d);

    _mm_storeu_si128((__m128i *) buf, a);
    _mm_storeu_si128((__m128i *) &buf[4], b);
    _mm_storeu_si128((__m128i *) &buf[8], c);
    _mm_storeu_si128((__m128i *) &buf[12], d);

    return buf[0] == 0 &&
           buf[1] == 5 &&
           buf[2] == 10 &&
           buf[3] == 15 &&
           buf[4] == 4 &&
           buf[5] == 9 &&
           buf[6] == 14 &&
           buf[7] == 3 &&
           buf[8] == 8 &&
           buf[9] == 13 &&
           buf[10] == 2 &&
           buf[11] == 7 &&
           buf[12] == 12 &&
           buf[13] == 1 &&
           buf[14] == 6 &&
           buf[15] == 11;
}

bool test_pi_1024_0() {
    uint32_t buf[32] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
                      16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31};

    __m128i a = _mm_loadu_si128((__m128i*)buf);
    __m128i b = _mm_loadu_si128((__m128i*)&buf[4]);
    __m128i c = _mm_loadu_si128((__m128i*)&buf[8]);
    __m128i d = _mm_loadu_si128((__m128i*)&buf[12]);
    __m128i e = _mm_loadu_si128((__m128i*)&buf[16]);
    __m128i f = _mm_loadu_si128((__m128i*)&buf[20]);
    __m128i g = _mm_loadu_si128((__m128i*)&buf[24]);
    __m128i h = _mm_loadu_si128((__m128i*)&buf[28]);
    __m128i tmp;

    PHK1024_MIX0_ENC(a, b, c, d, e, f, g, h, a, b, c, d, e, f, g, h);

    _mm_storeu_si128((__m128i *) buf, a);
    _mm_storeu_si128((__m128i *) &buf[4], b);
    _mm_storeu_si128((__m128i *) &buf[8], c);
    _mm_storeu_si128((__m128i *) &buf[12], d);
    _mm_storeu_si128((__m128i *) &buf[16], e);
    _mm_storeu_si128((__m128i *) &buf[20], f);
    _mm_storeu_si128((__m128i *) &buf[24], g);
    _mm_storeu_si128((__m128i *) &buf[28], h);

    return buf[0] == 0 &&
           buf[1] == 9 &&
           buf[2] == 18 &&
           buf[3] == 27 &&
           buf[4] == 4 &&
           buf[5] == 13 &&
           buf[6] == 22 &&
           buf[7] == 31 &&
           buf[8] == 8 &&
           buf[9] == 17 &&
           buf[10] == 26 &&
           buf[11] == 3 &&
           buf[12] == 12 &&
           buf[13] == 21 &&
           buf[14] == 30 &&
           buf[15] == 7 &&
           buf[16] == 16 &&
           buf[17] == 25 &&
           buf[18] == 2 &&
           buf[19] == 11 &&
           buf[20] == 20 &&
           buf[21] == 29 &&
           buf[22] == 6 &&
           buf[23] == 15 &&
           buf[24] == 24 &&
           buf[25] == 1 &&
           buf[26] == 10 &&
           buf[27] == 19 &&
           buf[28] == 28 &&
           buf[29] == 5 &&
           buf[30] == 14 &&
           buf[31] == 23;
}

bool test_pi_1024_1() {
    uint32_t buf[32] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
                      16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31};

    __m128i a = _mm_loadu_si128((__m128i*)buf);
    __m128i b = _mm_loadu_si128((__m128i*)&buf[4]);
    __m128i c = _mm_loadu_si128((__m128i*)&buf[8]);
    __m128i d = _mm_loadu_si128((__m128i*)&buf[12]);
    __m128i e = _mm_loadu_si128((__m128i*)&buf[16]);
    __m128i f = _mm_loadu_si128((__m128i*)&buf[20]);
    __m128i g = _mm_loadu_si128((__m128i*)&buf[24]);
    __m128i h = _mm_loadu_si128((__m128i*)&buf[28]);
    __m128i tmp;

    PHK1024_MIX1_ENC(a, b, c, d, e, f, g, h, a, b, c, d, e, f, g, h);

    _mm_storeu_si128((__m128i *) buf, a);
    _mm_storeu_si128((__m128i *) &buf[4], b);
    _mm_storeu_si128((__m128i *) &buf[8], c);
    _mm_storeu_si128((__m128i *) &buf[12], d);
    _mm_storeu_si128((__m128i *) &buf[16], e);
    _mm_storeu_si128((__m128i *) &buf[20], f);
    _mm_storeu_si128((__m128i *) &buf[24], g);
    _mm_storeu_si128((__m128i *) &buf[28], h);

    return buf[0] == 0 &&
           buf[1] == 5 &&
           buf[2] == 2 &&
           buf[3] == 7 &&
           buf[4] == 4 &&
           buf[5] == 9 &&
           buf[6] == 6 &&
           buf[7] == 11 &&
           buf[8] == 8 &&
           buf[9] == 13 &&
           buf[10] == 10 &&
           buf[11] == 15 &&
           buf[12] == 12 &&
           buf[13] == 17 &&
           buf[14] == 14 &&
           buf[15] == 19 &&
           buf[16] == 16 &&
           buf[17] == 21 &&
           buf[18] == 18 &&
           buf[19] == 23 &&
           buf[20] == 20 &&
           buf[21] == 25 &&
           buf[22] == 22 &&
           buf[23] == 27 &&
           buf[24] == 24 &&
           buf[25] == 29 &&
           buf[26] == 26 &&
           buf[27] == 31 &&
           buf[28] == 28 &&
           buf[29] == 1 &&
           buf[30] == 30 &&
           buf[31] == 3;
}

bool test_enc_dec(void * const ctx_enc, void * const ctx_dec, enc_function enc, dec_function dec, size_t state_size) {
    uint8_t *buf(new uint8_t[state_size * 2]);

    for (size_t i = 0; i < state_size; ++i) {
        buf[i] = i;
        buf[i + state_size] = i;
    }

    enc(ctx_enc, buf);
    dec(ctx_dec, buf);

    for (size_t i = 0; i < state_size; ++i) {
        if (buf[i] != buf[i + state_size]) {
            return false;
        }
    }

    delete[] buf;

    return true;
}

bool test_equiv(void * const ctx_enc0,
                void * const ctx_enc1,
                enc_function enc0,
                enc_function enc1,
                size_t state_size0,
                size_t state_size1) {
    uint8_t *buf(new uint8_t[state_size0 + state_size1]);

    size_t min_state_size(std::min(state_size0, state_size1));

    for (size_t i = 0; i < (state_size0 + state_size1); ++i) {
        buf[i] = i % min_state_size;
    }

    enc0(ctx_enc0, buf);
    enc1(ctx_enc1, &buf[state_size0]);

    for (size_t i = 0; i < (state_size0 + state_size1); ++i) {
        if (buf[i] != buf[i % min_state_size]) {
            return false;
        }
    }

    delete[] buf;

    return true;
}

int main() {
    test_big_addition();

    return 0;


    uint8_t *buf(phk_create_buffer(2 * PHK1024_SUBSTATES * PHK_SUBSTATE_SIZE));
    uint8_t *zero_buf(phk_create_buffer(PHK_SUBSTATE_SIZE));

    for (size_t i(0); i < 2 * PHK1024_SUBSTATES * PHK_SUBSTATE_SIZE; ++i) {
        buf[i] = i;
    }

    if (test_ts_permutation()) {
        std::cout << "\u2713 Tweak permutation works." << std::endl;
    } else {
        std::cerr << "\u2717 Tweak permutation does not work." << std::endl;
    }

    std::cout << std::endl << "Phk-256" << std::endl;

    if (test_pi_256()) {
        std::cout << "\u2713 PHK-256 permutation works." << std::endl;
    } else {
        std::cerr << "\u2717 PHK-256 permutation does not work." << std::endl;
    }


    phk256_ctx ctx256_enc0;
    phk256_ctx ctx256_enc1;
    phk256_ctx ctx256_dec;

    phk256_load_constants(&ctx256_enc0);
    phk256_load_constants(&ctx256_enc1);
    phk256_load_constants(&ctx256_dec);

    phk256_set_key_encrypt(&ctx256_enc0, &buf[32]);
    phk256_set_key_encrypt(&ctx256_enc1, &buf[32]);
    phk256_set_key_decrypt(&ctx256_dec, &buf[32]);

    if (test_enc_dec((void*)&ctx256_enc0, (void*)&ctx256_dec,
            (enc_function)phk256_encrypt, (dec_function)phk256_decrypt,
            PHK256_SUBSTATES * PHK_SUBSTATE_SIZE)) {
        std::cout << "\u2713 Encryption and decryption are compliant." << std::endl;
    } else {
        std::cerr << "\u2717 Encryption and decryption are not compliant." << std::endl;
    }

    phk256_set_tweak_encrypt(&ctx256_enc0, &buf[65]);
    phk256_set_tweak_decrypt(&ctx256_dec, &buf[65]);

    if (test_enc_dec((void*)&ctx256_enc0, (void*)&ctx256_dec,
                     (enc_function)phk256_encrypt, (dec_function)phk256_decrypt,
                     PHK256_SUBSTATES * PHK_SUBSTATE_SIZE)) {
        std::cout << "\u2713 Tweak works." << std::endl;
    } else {
        std::cerr << "\u2717 Tweak does not work." << std::endl;
    }

    phk256_set_tweak_encrypt(&ctx256_enc0, zero_buf);

    if (test_equiv((void*)&ctx256_enc0, (void*)&ctx256_enc1, (enc_function)phk256_encrypt, (dec_function)phk256_encrypt,
                   PHK256_SUBSTATES * PHK_SUBSTATE_SIZE, PHK256_SUBSTATES * PHK_SUBSTATE_SIZE)) {
        std::cout << "\u2713 Tweak reset works." << std::endl;
    } else {
        std::cerr << "\u2717 Tweak reset does not work." << std::endl;
    }

    std::cout << std::endl << "Phk-256x2" << std::endl;

    phk256_load_constants(&ctx256_enc0);
    phk256_load_constants(&ctx256_dec);

    phk256_set_key_encrypt(&ctx256_enc0, &buf[32]);
    phk256_set_key_decrypt(&ctx256_dec, &buf[32]);

    if (test_enc_dec((void*)&ctx256_enc0, (void*)&ctx256_dec,
                     (enc_function)phk256x2_encrypt, (dec_function)phk256x2_decrypt,
                     PHK256_SUBSTATES * PHK_SUBSTATE_SIZE)) {
        std::cout << "\u2713 Encryption and decryption are compliant." << std::endl;
    } else {
        std::cerr << "\u2717 Encryption and decryption are not compliant." << std::endl;
    }

    if (test_equiv((void*)&ctx256_enc0, (void*)&ctx256_enc0, (enc_function)phk256_encrypt, (dec_function)phk256x2_encrypt,
                     PHK256_SUBSTATES * PHK_SUBSTATE_SIZE, 2 * PHK256_SUBSTATES * PHK_SUBSTATE_SIZE)) {
        std::cout << "\u2713 Encryption equivalent with Phk-256." << std::endl;
    } else {
        std::cerr << "\u2717 Encryption not equivalent with Phk-256." << std::endl;
    }

    std::cout << std::endl << "Phk-256x4" << std::endl;

    phk256_load_constants(&ctx256_enc0);
    phk256_load_constants(&ctx256_dec);

    phk256_set_key_encrypt(&ctx256_enc0, &buf[32]);
    phk256_set_key_decrypt(&ctx256_dec, &buf[32]);

    if (test_enc_dec((void*)&ctx256_enc0, (void*)&ctx256_dec,
                     (enc_function)phk256x4_encrypt, (dec_function)phk256x4_decrypt,
                     PHK256_SUBSTATES * PHK_SUBSTATE_SIZE)) {
        std::cout << "\u2713 Encryption and decryption are compliant." << std::endl;
    } else {
        std::cerr << "\u2717 Encryption and decryption are not compliant." << std::endl;
    }

    if (test_equiv((void*)&ctx256_enc0, (void*)&ctx256_enc0, (enc_function)phk256_encrypt, (dec_function)phk256x4_encrypt,
                   PHK256_SUBSTATES * PHK_SUBSTATE_SIZE, 4 * PHK256_SUBSTATES * PHK_SUBSTATE_SIZE)) {
        std::cout << "\u2713 Encryption equivalent with Phk-256." << std::endl;
    } else {
        std::cerr << "\u2717 Encryption not equivalent with Phk-256." << std::endl;
    }

    std::cout << std::endl << "Phk-512" << std::endl;

    if (test_pi_512()) {
        std::cout << "\u2713 PHK-512 permutation works." << std::endl;
    } else {
        std::cerr << "\u2717 PHK-512 permutation does not work." << std::endl;
    }

    phk512_load_constants();

    phk512_ctx ctx512_enc0;
    phk512_ctx ctx512_enc1;
    phk512_ctx ctx512_dec;

    phk512_set_key_encrypt(&ctx512_enc0, &buf[64]);
    phk512_set_key_encrypt(&ctx512_enc1, &buf[64]);
    phk512_set_key_decrypt(&ctx512_dec, &buf[64]);

    if (test_enc_dec((void*)&ctx512_enc0, (void*)&ctx512_dec,
                     (enc_function)phk512_encrypt, (dec_function)phk512_decrypt,
                     PHK512_SUBSTATES * PHK_SUBSTATE_SIZE)) {
        std::cout << "\u2713 Encryption and decryption are compliant." << std::endl;
    } else {
        std::cerr << "\u2717 Encryption and decryption are not compliant." << std::endl;
    }

    phk512_set_tweak_encrypt(&ctx512_enc0, &buf[65]);
    phk512_set_tweak_decrypt(&ctx512_dec, &buf[65]);

    if (test_enc_dec((void*)&ctx512_enc0, (void*)&ctx512_dec,
                     (enc_function)phk512_encrypt, (dec_function)phk512_decrypt,
                     PHK512_SUBSTATES * PHK_SUBSTATE_SIZE)) {
        std::cout << "\u2713 Tweak works." << std::endl;
    } else {
        std::cerr << "\u2717 Tweak does not work." << std::endl;
    }

    phk512_set_tweak_encrypt(&ctx512_enc0, zero_buf);

    if (test_equiv((void*)&ctx512_enc0, (void*)&ctx512_enc1, (enc_function)phk512_encrypt, (dec_function)phk512_encrypt,
                   PHK512_SUBSTATES * PHK_SUBSTATE_SIZE, PHK512_SUBSTATES * PHK_SUBSTATE_SIZE)) {
        std::cout << "\u2713 Tweak reset works." << std::endl;
    } else {
        std::cerr << "\u2717 Tweak reset does not work." << std::endl;
    }

    std::cout << std::endl << "Phk-512x2" << std::endl;

    phk512_load_constants();

    phk512_set_key_encrypt(&ctx512_enc0, &buf[64]);
    phk512_set_key_decrypt(&ctx512_dec, &buf[64]);

    phk512_set_tweak_encrypt(&ctx512_enc0, &buf[64]);
    phk512_set_tweak_decrypt(&ctx512_dec, &buf[64]);

    if (test_enc_dec((void*)&ctx512_enc0, (void*)&ctx512_dec,
                     (enc_function)phk512x2_encrypt, (dec_function)phk512x2_decrypt,
                     2 * PHK512_SUBSTATES * PHK_SUBSTATE_SIZE)) {
        std::cout << "\u2713 Encryption and decryption are compliant." << std::endl;
    } else {
        std::cerr << "\u2717 Encryption and decryption are not compliant." << std::endl;
    }

    if (test_equiv((void*)&ctx512_enc0, (void*)&ctx512_enc0, (enc_function)phk512_encrypt, (dec_function)phk512x2_encrypt,
                   PHK512_SUBSTATES * PHK_SUBSTATE_SIZE, 2 * PHK512_SUBSTATES * PHK_SUBSTATE_SIZE)) {
        std::cout << "\u2713 Encryption equivalent with Phk-512." << std::endl;
    } else {
        std::cerr << "\u2717 Encryption not equivalent with Phk-512." << std::endl;
    }

    std::cout << std::endl << "Phk-1024" << std::endl;

    if (test_pi_1024_0()) {
        std::cout << "\u2713 PHK-1024 permutation 0 works." << std::endl;
    } else {
        std::cerr << "\u2717 PHK-1024 permutation 0 does not work." << std::endl;
    }

    if (test_pi_1024_1()) {
        std::cout << "\u2713 PHK-1024 permutation 1 works." << std::endl;
    } else {
        std::cerr << "\u2717 PHK-1024 permutation 1 does not work." << std::endl;
    }

    phk1024_load_constants();

    phk1024_ctx ctx1024_enc0;
    phk1024_ctx ctx1024_enc1;
    phk1024_ctx ctx1024_dec;

    phk1024_set_key_encrypt(&ctx1024_enc0, &buf[128]);
    phk1024_set_key_encrypt(&ctx1024_enc1, &buf[128]);
    phk1024_set_key_decrypt(&ctx1024_dec, &buf[128]);

    if (test_enc_dec((void*)&ctx1024_enc0, (void*)&ctx1024_dec,
                     (enc_function)phk1024_encrypt, (dec_function)phk1024_decrypt,
                     PHK1024_SUBSTATES * PHK_SUBSTATE_SIZE)) {
        std::cout << "\u2713 Encryption and decryption are compliant." << std::endl;
    } else {
        std::cerr << "\u2717 Encryption and decryption are not compliant." << std::endl;
    }

    phk1024_set_tweak_encrypt(&ctx1024_enc0, &buf[129]);
    phk1024_set_tweak_decrypt(&ctx1024_dec, &buf[129]);

    if (test_enc_dec((void*)&ctx1024_enc0, (void*)&ctx1024_dec,
                     (enc_function)phk1024_encrypt, (dec_function)phk1024_decrypt,
                     PHK1024_SUBSTATES * PHK_SUBSTATE_SIZE)) {
        std::cout << "\u2713 Tweak works." << std::endl;
    } else {
        std::cerr << "\u2717 Tweak does not work." << std::endl;
    }

    phk1024_set_tweak_encrypt(&ctx1024_enc0, zero_buf);

    if (test_equiv((void*)&ctx1024_enc0, (void*)&ctx1024_enc1, (enc_function)phk1024_encrypt, (dec_function)phk1024_encrypt,
                   PHK1024_SUBSTATES * PHK_SUBSTATE_SIZE, PHK1024_SUBSTATES * PHK_SUBSTATE_SIZE)) {
        std::cout << "\u2713 Tweak reset works." << std::endl;
    } else {
        std::cerr << "\u2717 Tweak reset does not work." << std::endl;
    }

    delete[] buf;
    delete[] zero_buf;

    return 0;
}

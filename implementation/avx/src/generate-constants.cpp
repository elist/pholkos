#include <cstdint>
#include <algorithm>
#include <cmath>
#include <iostream>
#include <iomanip>

#include "pi.h"


int main(int argc, char **argv) {
    int number_constants = 0;

    if (argc == 2)
        number_constants = atoi(argv[1]);
    else {
        fprintf(stderr, "Usage: %s number_constants\n", argv[0]);
    }

    if (number_constants < 1) number_constants = 1;

    int nblock = (number_constants + 1) * 32;
    int *tot = (int *) malloc(nblock * sizeof(int));
    int *t1 = (int *) malloc(nblock * sizeof(int));
    int *t2 = (int *) malloc(nblock * sizeof(int));
    int *t3 = (int *) malloc(nblock * sizeof(int));

    if (!tot || !t1 || !t2 || !t3) {
        fprintf(stderr, "Not enough memory\n");
        exit(1);
    }

    arctan(nblock, tot, t1, t2, 5, 1);
    mult(nblock, tot, 4);
    arctan(nblock, t3, t1, t2, 239, 2);
    sub(nblock, tot, t3);
    mult(nblock, tot, 4);

    uint8_t constant[16];

    for (size_t i = 0; i < number_constants; ++i) {
        std::fill(constant, &constant[16], 0);

        for (size_t n = 0; n < 128; ++n) {
            constant[n / 8] |= (((tot[i * 32 + (127 - n) / 4 + 1] / (int)std::pow(10, 3 - ((127 - n) % 4))) & 1) << (7 - (n % 8)));
        }

        std::cout << "rc[" << std::dec << i << "] = _mm_set_epi32(0x";

        for (size_t n = 0; n < 4; ++n) {
            std::cout << std::hex << std::setw(2) << std::setfill('0') << (int)constant[n];
        }

        std::cout << ", 0x";

        for (size_t n = 4; n < 8; ++n) {
            std::cout << std::hex << std::setw(2) << std::setfill('0') << (int)constant[n];
        }

        std::cout << ", 0x";

        for (size_t n = 8; n < 12; ++n) {
            std::cout << std::hex << std::setw(2) << std::setfill('0') << (int)constant[n];
        }

        std::cout << ", 0x";

        for (size_t n = 12; n < 16; ++n) {
            std::cout << std::hex << std::setw(2) << std::setfill('0') << (int)constant[n];
        }

        std::cout << ");" << std::endl;
    }

    free(tot);
    free(t1);
    free(t2);
    free(t3);
}

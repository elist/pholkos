

def write_set(i, l):
    #print(l)
    print("\tPHK_TS_MIX(r1)")
    print("\tPHK_TS_MIX(r2)")
    print("\tPHK_TS_MIX(r3)")
    print("\tPHK_TS_MIX(r4)")
    print("\tctx1->rk[{0}] = _mm_xor_si128(ctx1->rk[{0}], r1);".format(2*i))
    print("\tctx1->rk[{0}] = _mm_xor_si128(ctx1->rk[{0}], r1);".format(2*i + 1))
    print("\tctx2->rk[{0}] = _mm_xor_si128(ctx2->rk[{0}], r2);".format(2*i))
    print("\tctx2->rk[{0}] = _mm_xor_si128(ctx2->rk[{0}], r2);".format(2*i + 1))
    print("\tctx3->rk[{0}] = _mm_xor_si128(ctx3->rk[{0}], r3);".format(2*i))
    print("\tctx3->rk[{0}] = _mm_xor_si128(ctx3->rk[{0}], r3);".format(2*i + 1))
    print("\tctx4->rk[{0}] = _mm_xor_si128(ctx4->rk[{0}], r4);".format(2*i))
    print("\tctx4->rk[{0}] = _mm_xor_si128(ctx4->rk[{0}], r4);".format(2*i + 1))
    print()
    print()


PI = [11, 12, 1, 2, 15, 0, 5, 6, 3, 4, 9, 10, 7, 8, 13, 14]

def main():
    xs = list(range(16))
    for i in range(17):
        write_set(i, xs)
        tmp = xs[:]
        xs = [PI[x] for x in tmp]

if __name__ == '__main__':
    main()

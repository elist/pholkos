
#include <cstdint>
#include <cstring>
#include <iostream>
#include <fstream>
#include <string>
#include <jsoncpp/json/json.h>
#include <random>

#include "misc.h"

#include "phk-256.h"



uint8_t HalfByteToDez(char const& h) {
  uint8_t ret = 0;
  switch (h) {
    case '0' : return 0;
    case '1' : return 1;
    case '2' : return 2;
    case '3' : return 3;
    case '4' : return 4;
    case '5' : return 5;
    case '6' : return 6;
    case '7' : return 7;
    case '8' : return 8;
    case '9' : return 9;
    case 'a' : return 10;
    case 'b' : return 11;
    case 'c' : return 12;
    case 'd' : return 13;
    case 'e' : return 14;
    case 'f' : return 15;
  }
  return ret;
}


uint8_t ByteToDez(char lhs, char rhs) {
  return 16 * HalfByteToDez(lhs) + HalfByteToDez(rhs);
}




void HexToDez(std::string const& hexstr, uint8_t* const& out) {
  uint64_t len = (hexstr.length() + 1) / 2;
  if (hexstr.length() % 2 == 1) {
    out[0] = HalfByteToDez(static_cast<char>(hexstr[0]));
    for (uint64_t i = 1; i <= len; ++i) {
      out[i] = ByteToDez(hexstr[(i << 1)-1], hexstr[(i << 1)]);
    }
  } else {
    for (uint64_t i = 0; i < len; ++i) {
      out[i] = ByteToDez(hexstr[i << 1], hexstr[(i << 1)+1]);
    }
  }
}

bool test_phk_256_avx() {
  bool ret = true;
  std::ifstream test_file("../testcases/phk256.json", std::ifstream::binary);

  if (!test_file.good()) {
    std::cout << "\033[91mTEST FILE ";
    std::cout << " NOT FOUND\n";
    std::cout << "You either changed the directory structure or called the";
    std::cout << " program from the wrong directory!\n\033[0m";
    return false;
  }

  Json::Value tests;

  test_file >> tests;

  for (auto test : tests) {
    std::string tmp = test["Plaintext"].asString();
    uint8_t plaintext[32];
    char* tmp2 = &tmp[0u];
    HexToDez(tmp2, plaintext);

    tmp = test["Key"].asString();
    uint8_t key[32];
    tmp2 = &tmp[0u];
    HexToDez(tmp2, key);
    misc::print_buffer(key, 32, false);

    tmp = test["Tweak"].asString();
    uint8_t tweak[16];
    tmp2 = &tmp[0u];
    HexToDez(tmp2, tweak);

    tmp = test["Ciphertext"].asString();
    uint8_t ciphertext[32];
    tmp2 = &tmp[0u];
    HexToDez(tmp2, ciphertext);

    phk256_ctx ctx;
    uint8_t test_val[32];
    phk256_load_constants(&ctx);
    phk256_set_key_encrypt(&ctx, key);
    for(int c = 0; c <34; c = c+2) {
      misc::print_key(ctx.rk[c], ctx.rk[c+1], "roundkey " + std::to_string(c/2)); 
    }
    phk256_set_tweak_encrypt(&ctx, tweak);
    for(int c = 0; c <34; c+=2) {
      misc::print_key(ctx.rk[c], ctx.rk[c+1], "roundtweakey " + std::to_string(c/2)); 
    }
    std::memcpy(test_val, plaintext, 32);
    phk256_encrypt(&ctx, test_val);

    // memcmp returns 0 if values are equal
    bool was_successful = memcmp(ciphertext, test_val, 32) == 0;
    if (!was_successful) {
      std::cout << "Encryption failed!" << std::endl;
      std::cout << "Expected: "<< std::hex;
      for (int i=0; i < 32; ++i) {
        std::cout << std::setw(2) << std::setfill('0') << (int) ciphertext[i];
      }
      std::cout << std::endl;
      std::cout << "But was: "<< std::hex;
      for (int i=0; i < 32; ++i) {
        std::cout << std::setw(2) << std::setfill('0') << (int) test_val[i];
      }
      std::cout << std::endl;
      ret = false;
    }

    phk256_set_key_decrypt(&ctx, key);
    phk256_set_tweak_decrypt(&ctx, tweak);
    std::memcpy(test_val, ciphertext, 32);
    phk256_decrypt(&ctx, test_val);

    was_successful = memcmp(plaintext, test_val, 32) == 0;
    if (!was_successful) {
      std::cout << "Decryption failed!" << std::endl;
      std::cout << "Expected: "<< std::hex;
      for (int i=0; i < 32; ++i) {
        std::cout << std::setw(2) << std::setfill('0') << (int) plaintext[i];
      }
      std::cout << std::endl;
      std::cout << "But was: "<< std::hex;
      for (int i=0; i < 32; ++i) {
        std::cout << std::setw(2) << std::setfill('0') << (int) test_val[i];
      }
      std::cout << std::endl;
      ret = false;
    }
  }
  return ret;
}

bool compare_rks(phk256_ctx *const lhs, phk256_ctx *rhs) {
  for (size_t i = 0; i < 34; ++i) {
    if (not (_mm_test_all_zeros(_mm_xor_si128(lhs->rk[i], rhs->rk[i]),\
                                          _mm_set_epi32(0xffffffff, 0xffffffff,\
                                                        0xffffffff, 0xffffffff\
                                            )))) {
      uint8_t conv[16];
      std::cout<<"Failed in round "<<i<<std::endl;
      _mm_store_si128((__m128i *) conv, lhs->rk[i]);
      misc::print_hex(conv, 16); 
      _mm_store_si128((__m128i *) conv, rhs->rk[i]);
      misc::print_hex(conv, 16); 
      return false;
    }
  }
  return true;
}

bool test_tweak_update() {
  std::cout<<"testing tweak update" << std::endl;
  std::random_device dev;
  std::default_random_engine gen(dev());
  std::uniform_int_distribution<uint8_t> dist(0, UINT8_MAX);
  std::string hex;
  for (size_t i = 0; i < 1000000; ++i) {
    phk256_ctx ctx;
    phk256_ctx ctx_compare;
    phk256_load_constants(&ctx);
    uint8_t *tweak(phk_create_buffer(16));
    for (size_t j = 0; j <16; ++j) {
      tweak[j] = dist(gen);
    }
    uint8_t key[32];
    for (size_t j = 0; j <32; ++j) {
      key[j] = dist(gen);
    }
    phk256_set_key_encrypt(&ctx, key);
    phk256_set_tweak_encrypt(&ctx, tweak);

    uint8_t *tweak_diff(phk_create_buffer(16));
    for (size_t j = 0; j <16; ++j) {
      tweak_diff[j] = dist(gen);
    }
    phk256_update_tweak_encrypt(&ctx, tweak_diff);

    phk256_load_constants(&ctx_compare);
    phk256_set_key_encrypt(&ctx_compare, key);

    for (size_t j = 0; j <16; ++j) {
      tweak[j] = tweak[j] ^ tweak_diff[j];
    }
    phk256_set_tweak_encrypt(&ctx_compare, tweak);
    if (not compare_rks(&ctx, &ctx_compare)){
      delete[] tweak;
      std::cout << "Update tweak failed!"<<std::endl;
      return false;
    }
    delete[] tweak;
  }

  return true;

}

// ---------------------------------------------------------

int main () {
  bool success = true;
  success &= test_phk_256_avx();
  success &= test_tweak_update();
  std::cout << (success ? "All tests passed." : "Failure.") <<std::endl;
  return 0;

}

// ---------------------------------------------------------

#include <x86intrin.h>
#include <immintrin.h>
#include <smmintrin.h>
#include <iostream>
#include <algorithm>
#include <random>

#include "misc.h"
#include "phk-256.h"
#include "phk-512.h"
#include "phk-1024.h"
#include "saturnin.h"
#include "simpira.h"

// ---------------------------------------------------------------------

const size_t NUM_TIMER_SAMPLES(1000000);
const size_t PRE_WARM_UP_RUNS(1024);
const size_t NUM_CONTINUOUS_RUNS(1024);
const size_t NUM_REPETITIONS(1024);
const size_t MEDIAN_REPETITIONS(NUM_REPETITIONS >> 1);

// ---------------------------------------------------------------------
void deb_print(int i) {
  std::cout << "DEBUG " << (int) i << std::endl;
}

typedef void (*encrypt_function)(const void *const ctx, uint8_t *const buffer);

typedef void (*key_function)(void *const ctx, const uint8_t *const key);

typedef void (*tweak_function)(void *const ctx, const uint8_t *const tweak);

typedef void (*tweak_update_function)(void *const ctx, uint8_t const tweak);

typedef void (*simpira_encrypt_function)(const simpira_context_t* ctx,
                                         const uint8_t *plaintext,
                                         uint8_t *ciphertext);

// ---------------------------------------------------------------------

inline uint64_t getTime() {
    return __rdtsc();
}

// ---------------------------------------------------------------------

uint64_t getTimerOverhead() {
    uint64_t minOverhead(0xffffffffffffffff);
    uint64_t t0, t1;

    for (size_t i = 0; i < NUM_TIMER_SAMPLES; ++i) {
        t0 = getTime();
        t1 = getTime();

        if (minOverhead > (t1 - t0)) {
            minOverhead = t1 - t0;
        }
    }

    return minOverhead;
}

// ---------------------------------------------------------------------

void printState(__m128i a, __m128i b, __m128i c, __m128i d) {
    uint32_t words[4];

    _mm_storeu_si128((__m128i *) words, a);

    for (uint8_t i = 0; i < 4; ++i) {
        std::cout << words[i] << ' ';
    }

    _mm_storeu_si128((__m128i *) words, b);

    for (uint8_t i = 0; i < 4; ++i) {
        std::cout << words[i] << ' ';
    }

    _mm_storeu_si128((__m128i *) words, c);

    for (uint8_t i = 0; i < 4; ++i) {
        std::cout << words[i] << ' ';
    }

    _mm_storeu_si128((__m128i *) words, d);

    for (uint8_t i = 0; i < 4; ++i) {
        std::cout << words[i] << ' ';
    }

    std::cout << std::endl;
}

// ---------------------------------------------------------------------

uint64_t sumState(__m128i a, __m128i b, __m128i c, __m128i d) {
    uint32_t words[4];
    uint64_t sum(0);

    _mm_storeu_si128((__m128i *) words, a);

    for (uint8_t i = 0; i < 4; ++i) {
        sum += words[i];
    }

    _mm_storeu_si128((__m128i *) words, b);

    for (uint8_t i = 0; i < 4; ++i) {
        sum += words[i];
    }

    _mm_storeu_si128((__m128i *) words, c);

    for (uint8_t i = 0; i < 4; ++i) {
        sum += words[i];
    }

    _mm_storeu_si128((__m128i *) words, d);

    for (uint8_t i = 0; i < 4; ++i) {
        sum += words[i];
    }

    return sum;
}

// ---------------------------------------------------------------------

double benchmarkKey(void *const ctx, key_function key, size_t key_size,
                    const char *const prefix) {
    uint8_t progress(0);
    std::cout << prefix << " ";
    misc::print_progress_bar(std::cout, progress);
    std::cout.flush();

    uint64_t start;
    uint64_t end;
    uint64_t timerOverhead(getTimerOverhead());

    std::random_device dev;
    std::default_random_engine gen(dev());
    std::uniform_int_distribution<uint8_t> dist(0, UINT8_MAX);

    double results[NUM_REPETITIONS];
    uint8_t check_sum(0);

    uint8_t *buf(phk_create_buffer(key_size * NUM_CONTINUOUS_RUNS));

    for (size_t rep(0); rep < NUM_REPETITIONS; ++rep) {
        // init buffer for keying
        for (size_t i = 0; i < key_size * NUM_CONTINUOUS_RUNS; ++i) {
            buf[i] = dist(gen);
        }

        start = getTime();

        for (size_t i = 0; i < PRE_WARM_UP_RUNS; ++i) {
            key(ctx, &buf[i * key_size]);
        }

        end = getTime();

        start = getTime();

        for (size_t i = 0; i < NUM_CONTINUOUS_RUNS; ++i) {
            key(ctx, &buf[i * key_size]);
        }

        end = getTime();

        results[rep] =
            ((double) (end - start - timerOverhead)) / NUM_CONTINUOUS_RUNS;

        for (size_t i = 0; i < NUM_CONTINUOUS_RUNS; ++i) {
            check_sum ^= buf[i * key_size];
        }

        if (((rep + 1) * 100 / NUM_REPETITIONS) > progress) {
            progress = ((rep + 1) * 100 / NUM_REPETITIONS);
            std::cout << "\r" << prefix << " ";
            misc::print_progress_bar(std::cout, progress);
            std::cout.flush();
        }
    }

    std::sort(results, results + NUM_REPETITIONS);
    std::cout << " " << results[MEDIAN_REPETITIONS] << " c ("
              << (results[MEDIAN_REPETITIONS] / key_size)
              << " cpb) [" << (int) check_sum << "]" << std::endl;

    delete[] buf;

    return results[MEDIAN_REPETITIONS];
}

// ---------------------------------------------------------------------

//ID 256-128 = 0, 256-256 = 1, 512-128 = 2, 1024-128 = 4
double benchmarkTweak(void * ctx, tweak_function tweak, size_t key_size,
                      const char *const prefix, size_t ID) {
    uint8_t progress(0);
    std::cout << prefix << " ";
    misc::print_progress_bar(std::cout, progress);
    std::cout.flush();

    uint64_t start;
    uint64_t end;
    uint64_t timerOverhead(getTimerOverhead());

    std::random_device dev;
    std::default_random_engine gen(dev());
    std::uniform_int_distribution<uint8_t> dist(0, UINT8_MAX);

    double results[NUM_REPETITIONS];
    uint8_t check_sum(0);

    uint8_t *buffer(phk_create_buffer(key_size * NUM_CONTINUOUS_RUNS));

    for (size_t num_repetitions(0);
         num_repetitions < NUM_REPETITIONS; ++num_repetitions) {
        // init buffer for keying
        for (size_t i = 0; i < key_size * NUM_CONTINUOUS_RUNS; ++i) {
            buffer[i] = dist(gen);
        }

        start = getTime();

        for (size_t i = 0; i < PRE_WARM_UP_RUNS; ++i) {
            tweak(ctx, &buffer[i * key_size]);
        }

        end = getTime();

        start = getTime();

        for (size_t i = 0; i < NUM_CONTINUOUS_RUNS; ++i) {
            tweak(ctx, &buffer[i * key_size]);
        }

        end = getTime();

        results[num_repetitions] =
            ((double) (end - start - timerOverhead)) / NUM_CONTINUOUS_RUNS;

        uint8_t tmpx[key_size];
        if (ID == 0){
          _mm_storeu_si128((__m128i*)tmpx, ((phk256_ctx *) ctx)->rk[32]);
        } else if(ID == 1) {
          _mm_storeu_si128((__m128i*)tmpx, ((phk256_256_ctx *) ctx)->rk[32]);
        } else if(ID == 2) {
          _mm_storeu_si128((__m128i*)tmpx, ((phk512_ctx *) ctx)->rk[32]);
        } else if(ID == 4) {
          _mm_storeu_si128((__m128i*)tmpx, ((phk1024_ctx *) ctx)->rk[32]);
        } else {
          std::cout << "INVALID ID" << std::endl;
          return 0;
        }
        check_sum ^= tmpx[0];

        for (size_t i = 0; i < NUM_CONTINUOUS_RUNS; ++i) {
            check_sum ^= buffer[i * key_size];
        }

        if (((num_repetitions + 1) * 100 / NUM_REPETITIONS) > progress) {
            progress = ((num_repetitions + 1) * 100 / NUM_REPETITIONS);
            std::cout << "\r" << prefix << " ";
            misc::print_progress_bar(std::cout, progress);
            std::cout.flush();
        }
    }

    std::sort(results, results + NUM_REPETITIONS);
    std::cout << " " << results[MEDIAN_REPETITIONS] << " c ("
              << (results[MEDIAN_REPETITIONS] / key_size)
              << " cpb) [" << (int) check_sum << "]" << std::endl;

    delete[] buffer;

    return results[MEDIAN_REPETITIONS];
}


// ---------------------------------------------------------------------

double benchmarkTweakMode(struct phk256_ctx *ctx, tweak_update_function tweak, size_t key_size,
                      const char *const prefix) {
    uint8_t progress(0);
    std::cout << prefix << " ";
    misc::print_progress_bar(std::cout, progress);
    std::cout.flush();

    uint64_t start;
    uint64_t end;
    uint64_t timerOverhead(getTimerOverhead());

    std::random_device dev;
    std::default_random_engine gen(dev());
    std::uniform_int_distribution<uint8_t> dist(0, UINT8_MAX);

    double results[NUM_REPETITIONS];
    uint8_t check_sum(0);

    uint8_t *tweak_diff(phk_create_buffer(NUM_CONTINUOUS_RUNS*16));
    deb_print(1);

    for (size_t num_repetitions(0);
         num_repetitions < NUM_REPETITIONS; ++num_repetitions) {
        for (size_t i = 0; i <  NUM_CONTINUOUS_RUNS*16; ++i) {
            tweak_diff[i] = dist(gen);
        }
        start = getTime();

    deb_print(2);
        for (size_t i = 0; i < NUM_CONTINUOUS_RUNS; ++i) {
            tweak(ctx, tweak_diff[16 * i]);
        }

    deb_print(3);
        end = getTime();

        start = getTime();

        for (size_t i = 0; i < NUM_CONTINUOUS_RUNS; ++i) {
            tweak(ctx, tweak_diff[16*i]);
        }

        end = getTime();


        results[num_repetitions] =
            ((double) (end - start - timerOverhead)) / NUM_CONTINUOUS_RUNS;

        for (size_t i = 0; i < NUM_CONTINUOUS_RUNS; ++i) {
            check_sum ^= tweak_diff[i];
        }

        if (((num_repetitions + 1) * 100 / NUM_REPETITIONS) > progress) {
            progress = ((num_repetitions + 1) * 100 / NUM_REPETITIONS);
            std::cout << "\r" << prefix << " ";
            misc::print_progress_bar(std::cout, progress);
            std::cout.flush();
        }
    }

    std::sort(results, results + NUM_REPETITIONS);
    std::cout << " " << results[MEDIAN_REPETITIONS] << " c ("
              << (results[MEDIAN_REPETITIONS] / key_size)
              << " cpb) [" << (int) check_sum << "]" << std::endl;


    return results[MEDIAN_REPETITIONS];
}

// ---------------------------------------------------------------------

double benchmarkEncryption(const void *const ctx, encrypt_function enc,
                           size_t state_size,
                           const char *const prefix) {
    uint8_t progress(0);
    std::cout << prefix << " ";
    misc::print_progress_bar(std::cout, progress);
    std::cout.flush();

    uint64_t start;
    uint64_t end;
    uint64_t timerOverhead(getTimerOverhead());

    std::random_device dev;
    std::default_random_engine gen(dev());
    std::uniform_int_distribution<uint8_t> dist(0, UINT8_MAX);

    double results[NUM_REPETITIONS];
    uint8_t check_sum(0);

    uint8_t *buffer(phk_create_buffer(state_size * NUM_CONTINUOUS_RUNS));

    for (size_t num_repetitions(0);
         num_repetitions < NUM_REPETITIONS; ++num_repetitions) {
        // init buffer for encryption
        for (size_t i = 0; i < state_size * NUM_CONTINUOUS_RUNS; ++i) {
            buffer[i] = dist(gen);
        }

        start = getTime();

        for (size_t i = 0; i < PRE_WARM_UP_RUNS; ++i) {
            enc(ctx, &buffer[i * state_size]);
        }

        end = getTime();

        start = getTime();

        for (size_t i = 0; i < NUM_CONTINUOUS_RUNS; ++i) {
            enc(ctx, &buffer[i * state_size]);
        }

        end = getTime();

        results[num_repetitions] =
            ((double) (end - start - timerOverhead)) / NUM_CONTINUOUS_RUNS;

        for (size_t i = 0; i < NUM_CONTINUOUS_RUNS; ++i) {
            check_sum ^= buffer[i * state_size];
        }

        if (((num_repetitions + 1) * 100 / NUM_REPETITIONS) > progress) {
            progress = ((num_repetitions + 1) * 100 / NUM_REPETITIONS);
            std::cout << "\r" << prefix << " ";
            misc::print_progress_bar(std::cout, progress);
            std::cout.flush();
        }
    }

    std::sort(results, results + NUM_REPETITIONS);
    std::cout << " " << results[MEDIAN_REPETITIONS] << " c ("
              << (results[MEDIAN_REPETITIONS] / state_size)
              << " cpb) [" << (int) check_sum << "]" << std::endl;

    delete[] buffer;

    return results[MEDIAN_REPETITIONS];
}

// ---------------------------------------------------------------------

double benchmarkSimpira(simpira_encrypt_function enc,
                        size_t state_size,
                        const char *const prefix) {
    uint8_t progress(0);
    std::cout << prefix << " ";
    misc::print_progress_bar(std::cout, progress);
    std::cout.flush();

    uint64_t start;
    uint64_t end;
    uint64_t timerOverhead(getTimerOverhead());

    std::random_device dev;
    std::default_random_engine gen(dev());
    std::uniform_int_distribution<uint8_t> dist(0, UINT8_MAX);

    double results[NUM_REPETITIONS];
    uint8_t check_sum(0);
    uint8_t *buffer(phk_create_buffer(state_size * NUM_CONTINUOUS_RUNS));

    uint8_t key[16] = {
        0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f
    };
    uint8_t ciphertext[64];

    simpira_context_t ctx;
    simpira256_set_key(&ctx, key);

    for (size_t num_repetitions(0);
         num_repetitions < NUM_REPETITIONS; ++num_repetitions) {
        // init buffer for encryption
        for (size_t i = 0; i < state_size * NUM_CONTINUOUS_RUNS; ++i) {
            buffer[i] = dist(gen);
        }

        start = getTime();

        for (size_t i = 0; i < PRE_WARM_UP_RUNS; ++i) {
            enc(&ctx, &buffer[i * state_size], ciphertext);
        }

        end = getTime();
        start = getTime();

        for (size_t i = 0; i < NUM_CONTINUOUS_RUNS; ++i) {
            enc(&ctx, &buffer[i * state_size], ciphertext);
        }

        end = getTime();

        results[num_repetitions] =
            ((double) (end - start - timerOverhead)) / NUM_CONTINUOUS_RUNS;

        for (size_t i = 0; i < 64; ++i) {
            check_sum ^= ciphertext[i * state_size];
        }

        if (((num_repetitions + 1) * 100 / NUM_REPETITIONS) > progress) {
            progress = ((num_repetitions + 1) * 100 / NUM_REPETITIONS);
            std::cout << "\r" << prefix << " ";
            misc::print_progress_bar(std::cout, progress);
            std::cout.flush();
        }
    }

    std::sort(results, results + NUM_REPETITIONS);
    std::cout << " " << results[MEDIAN_REPETITIONS] << " c ("
              << (results[MEDIAN_REPETITIONS] / state_size)
              << " cpb) [" << (int) check_sum << "]" << std::endl;

    delete[] buffer;

    return results[MEDIAN_REPETITIONS];
}

// ---------------------------------------------------------------------

double benchmarkGf(const char *const prefix) {
    uint8_t progress(0);
    std::cout << prefix << " ";
    misc::print_progress_bar(std::cout, progress);
    std::cout.flush();

    uint64_t start;
    uint64_t end;
    uint64_t timerOverhead(getTimerOverhead());

    std::random_device dev;
    std::default_random_engine gen(dev());
    std::uniform_int_distribution<uint8_t> dist(0, UINT8_MAX);

    double results[NUM_REPETITIONS];
    uint8_t check_sum(0);

    __m128i tmp;
    __m128i a = _mm_set_epi32(0x00010203, 0x04050607, 0x08090a0b, 0x0c0d0e0f);

    for (size_t num_repetitions(0);
         num_repetitions < NUM_REPETITIONS; ++num_repetitions) {
        start = getTime();

        for (size_t i = 0; i < PRE_WARM_UP_RUNS; ++i) {
            GF2P8MUL2(a)
        }

        end = getTime();

        start = getTime();

        for (size_t i = 0; i < NUM_CONTINUOUS_RUNS; ++i) {
            GF2P8MUL2(a)
        }

        end = getTime();

        results[num_repetitions] =
            ((double) (end - start - timerOverhead)) / NUM_CONTINUOUS_RUNS;

        if (((num_repetitions + 1) * 100 / NUM_REPETITIONS) > progress) {
            progress = ((num_repetitions + 1) * 100 / NUM_REPETITIONS);
            std::cout << "\r" << prefix << " ";
            misc::print_progress_bar(std::cout, progress);
            std::cout.flush();
        }
    }

    check_sum = _mm_extract_epi8(a, 0x5);

    std::sort(results, results + NUM_REPETITIONS);
    std::cout << " " << results[MEDIAN_REPETITIONS] << " c ("
              << (results[MEDIAN_REPETITIONS] / 16)
              << " cpb) [" << (int) check_sum << "]" << std::endl;

    return results[MEDIAN_REPETITIONS];
}

// ---------------------------------------------------------------------

int main(int argc, char **argv) {
    std::cout << "Miscellaneous Functions" << std::endl << std::endl;

    benchmarkGf("GF2(8) Multiplication");

    std::cout << std::endl << "Phk Benchmark" << std::endl << std::endl;

    uint8_t *key(phk_create_buffer(PHK1024_SUBSTATES * 16));
    std::fill(key, &key[PHK1024_SUBSTATES * 16], 0);

    phk256_ctx ctx256;
    phk256_256_ctx ctx256_256;
    phk512_ctx ctx512;
    phk1024_ctx ctx1024;

    phk256_load_constants(&ctx256);
    phk256_256_load_constants(&ctx256_256);
    phk512_load_constants();
    phk1024_load_constants();

    benchmarkKey(&ctx256, (key_function) phk256_set_key_encrypt,
                 PHK256_SUBSTATES * 16, "Phk-256  Key-Schedule Enc        ");
    benchmarkKey(&ctx256_256, (key_function) phk256_256_set_key_encrypt,
                 PHK256_SUBSTATES * 16, "Phk-256-256  Key-Schedule Enc        ");
//    benchmarkKey(&ctx512, (key_function) phk512_set_key_encrypt,
//                 PHK512_SUBSTATES * 16, "Phk-512  Key-Schedule Enc        ");
    benchmarkKey(&ctx512, (key_function) phk512_set_key256_encrypt,
                 PHK512_SUBSTATES * 16, "Phk-512  256-Bit Key-Schedule Enc");
//    benchmarkKey(&ctx1024, (key_function) phk1024_set_key_encrypt,
//                 PHK1024_SUBSTATES * 16, "Phk-1024 Key-Schedule Enc        ");
    benchmarkKey(&ctx1024, (key_function) phk1024_256_set_key_encrypt,
                 PHK1024_SUBSTATES * 16, "Phk-1024 256-Bit Key-Schedule Enc");
//
//    std::cout << std::endl;
//
    benchmarkKey(&ctx256, (key_function) phk256_set_key_decrypt,
                 PHK256_SUBSTATES * 16, "Phk-256  Key-Schedule Dec        ");
//    benchmarkKey(&ctx512, (key_function) phk512_set_key_decrypt,
//                 PHK512_SUBSTATES * 16, "Phk-512  Key-Schedule Dec        ");
    benchmarkKey(&ctx512, (key_function) phk512_set_key256_decrypt,
                 PHK512_SUBSTATES * 16, "Phk-512  256-Bit Key-Schedule Dec");
//    benchmarkKey(&ctx1024, (key_function) phk1024_set_key_decrypt,
//                 PHK1024_SUBSTATES * 16, "Phk-1024 Key-Schedule Dec        ");
    benchmarkKey(&ctx1024, (key_function) phk1024_256_set_key_decrypt,
                 PHK1024_SUBSTATES * 16, "Phk-1024 256-Bit Key-Schedule Dec");
//
//    std::cout << std::endl;
//
    benchmarkTweak(&ctx256, (tweak_function) phk256_set_tweak_encrypt,
                   16, "Phk-256  Retweak Enc", 0);
    benchmarkTweak(&ctx256_256, (tweak_function) phk256_set_256_tweak_encrypt,
                   32, "Phk-256 256 Retweak Enc", 1);
    benchmarkTweak(&ctx512, (key_function) phk512_set_tweak_encrypt,
                   PHK512_SUBSTATES * 16, "Phk-512  Retweak Enc", 2);
    benchmarkTweak(&ctx1024, (key_function) phk1024_set_tweak_encrypt,
                   PHK1024_SUBSTATES * 16, "Phk-1024 Retweak Enc", 4);
//
//    std::cout << std::endl;
//
    benchmarkTweak(&ctx256, (key_function) phk256_set_tweak_decrypt,
                   PHK256_SUBSTATES * 16, "Phk-256  Retweak Dec", 0);
    benchmarkTweak(&ctx256_256, (key_function) phk256_set_256_tweak_decrypt,
                   PHK256_SUBSTATES * 16, "Phk-256-256  Retweak Dec", 1);
    benchmarkTweak(&ctx512, (key_function) phk512_set_tweak_decrypt,
                   PHK512_SUBSTATES * 16, "Phk-512  Retweak Dec", 2);
    benchmarkTweak(&ctx1024, (key_function) phk1024_set_tweak_decrypt,
                   PHK1024_SUBSTATES * 16, "Phk-1024 Retweak Dec", 4);
//
//    benchmarkTweakMode(&ctx256, (tweak_update_function) phk256_update_tweak_encrypt,
//                   PHK256_SUBSTATES * 16, "Phk-256  Update tweak encr");
//    std::cout << std::endl;

    phk256_set_key_encrypt(&ctx256, key);
    phk256_256_set_key_encrypt(&ctx256_256, key);
    phk512_set_key_encrypt(&ctx512, key);
    phk1024_set_key_encrypt(&ctx1024, key);

    benchmarkEncryption(&ctx256, (encrypt_function) phk256_encrypt,
                        PHK256_SUBSTATES * 16, "Phk-256   Encryption");
    benchmarkEncryption(&ctx256, (encrypt_function) phk256x2_encrypt,
                        2 * PHK256_SUBSTATES * 16, "Phk-256x2 Encryption");
    benchmarkEncryption(&ctx256, (encrypt_function) phk256x4_encrypt,
                        4 * PHK256_SUBSTATES * 16, "Phk-256x4 Encryption");
    benchmarkEncryption(&ctx256_256, (encrypt_function) phk256_256_encrypt,
                        PHK256_SUBSTATES * 16, "Phk-256 256 Encryption");
    benchmarkEncryption(&ctx512, (encrypt_function) phk512_encrypt,
                        PHK512_SUBSTATES * 16, "Phk-512   Encryption");
    benchmarkEncryption(&ctx512, (encrypt_function) phk512x2_encrypt,
                        2 * PHK512_SUBSTATES * 16, "Phk-512x2 Encryption");
    benchmarkEncryption(&ctx1024, (encrypt_function) phk1024_encrypt,
                        PHK1024_SUBSTATES * 16, "Phk-1024  Encryption");
    benchmarkEncryption(&ctx1024, (encrypt_function) phk1024_256_encrypt,
                        PHK1024_SUBSTATES * 16, "Phk-1024 256 Encryption");

    std::cout << std::endl;

    benchmarkSimpira(simpira256_encrypt, 32, "Simpira-256 Encryption");
    benchmarkSimpira(simpira512_encrypt, 64, "Simpira-512 Encryption");

    std::cout << std::endl;

    benchmarkEncryption(&ctx256, (encrypt_function) phk256_decrypt,
                        PHK256_SUBSTATES * 16, "Phk-256   Decryption");
    benchmarkEncryption(&ctx256, (encrypt_function) phk256x2_decrypt,
                        2 * PHK256_SUBSTATES * 16, "Phk-256x2 Decryption");
    benchmarkEncryption(&ctx256, (encrypt_function) phk256x4_decrypt,
                        4 * PHK256_SUBSTATES * 16, "Phk-256x4 Decryption");
    benchmarkEncryption(&ctx512, (encrypt_function) phk512_decrypt,
                        PHK512_SUBSTATES * 16, "Phk-512   Decryption");
    benchmarkEncryption(&ctx512, (encrypt_function) phk512x2_decrypt,
                        2 * PHK512_SUBSTATES * 16, "Phk-512x2 Decryption");
    benchmarkEncryption(&ctx1024, (encrypt_function) phk1024_decrypt,
                        PHK1024_SUBSTATES * 16, "Phk-1024  Decryption");


    delete[] key;

    return 0;
}

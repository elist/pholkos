
#include <cstdint>
#include <cstring>
#include <iostream>
#include <fstream>
#include <string>
#include <jsoncpp/json/json.h>
#include <random>

#include "misc.h"

#include "phk-256.h"
#include "ctrt_phk.h"



uint8_t HalfByteToDez(char const& h) {
  uint8_t ret = 0;
  switch (h) {
    case '0' : return 0;
    case '1' : return 1;
    case '2' : return 2;
    case '3' : return 3;
    case '4' : return 4;
    case '5' : return 5;
    case '6' : return 6;
    case '7' : return 7;
    case '8' : return 8;
    case '9' : return 9;
    case 'a' : return 10;
    case 'b' : return 11;
    case 'c' : return 12;
    case 'd' : return 13;
    case 'e' : return 14;
    case 'f' : return 15;
  }
  return ret;
}


uint8_t ByteToDez(char lhs, char rhs) {
  return 16 * HalfByteToDez(lhs) + HalfByteToDez(rhs);
}




void HexToDez(std::string const& hexstr, uint8_t* const& out) {
  uint64_t len = (hexstr.length() + 1) / 2;
  if (hexstr.length() % 2 == 1) {
    out[0] = HalfByteToDez(static_cast<char>(hexstr[0]));
    for (uint64_t i = 1; i <= len; ++i) {
      out[i] = ByteToDez(hexstr[(i << 1)-1], hexstr[(i << 1)]);
    }
  } else {
    for (uint64_t i = 0; i < len; ++i) {
      out[i] = ByteToDez(hexstr[i << 1], hexstr[(i << 1)+1]);
    }
  }
}


bool test_ctrt_phk256_128_avx() {
std::cout<<"reading file\n";
    bool ret = true;
    std::ifstream test_file("../testcases/ctrt-phk256-128.json",
                            std::ifstream::binary);

    if (!test_file.good()) {
        std::cout << "\033[91mTEST FILE ";
        std::cout << " NOT FOUND\n";
        std::cout << "You either changed the directory structure or called the";
        std::cout << " program from the wrong directory!\n\033[0m";
        return false;
    }

    Json::Value tests;

    test_file >> tests;

    int num_tests = tests.size();

    for (auto test : tests) {

        std::cout << "-----------------" << std::endl;
        std::string tmp = test["Plaintext"].asString();
        int size = tmp.size() / 2;
        uint8_t plaintext[size];
        uint8_t ciphertext[size];
        char* tmp2 = &tmp[0u];
        HexToDez(tmp2, plaintext);
        HexToDez(tmp2, ciphertext);

        tmp = test["Ciphertext"].asString();
        uint8_t expected[size];
        tmp2 = &tmp[0u];
        HexToDez(tmp2, expected);

        tmp = test["Key"].asString();
        uint8_t key[PHK256_NUM_KEY_BYTES];
        tmp2 = &tmp[0u];
        HexToDez(tmp2, key);

        tmp = test["Tweak"].asString();
        uint8_t tweak[PHK_TWEAK_SIZE];
        tmp2 = &tmp[0u];
        HexToDez(tmp2, tweak);

        tmp = test["Nonce"].asString();
        uint8_t nonce[PHK256_NUM_STATE_BYTES];
        tmp2 = &tmp[0u];
        HexToDez(tmp2, nonce);


        ctrt_phk_256(key, tweak, nonce, ciphertext, size / PHK256_NUM_STATE_BYTES);

        // memcmp returns 0 if values are equal
        bool was_successful = memcmp(ciphertext, expected, size) == 0;

        if (!was_successful) {
            std::cout << "\033[91mCTRT encryption failed!\033[0m" << std::endl;
            std::cout << "Expected:\n" << std::hex;

            for (int i = 0; i < size; ++i) {
                std::cout << std::setw(2) << std::setfill('0')
                          << (int) expected[i];
                if((i+1)%32 == 0)
                  std::cout<<std::endl;
            }

            std::cout << std::endl;
            std::cout << "But was:\n" << std::hex;

            for (int i = 0; i < size; ++i) {
                std::cout << std::setw(2) << std::setfill('0')
                          << (int) ciphertext[i];
                if((i+1)%32 == 0)
                  std::cout<<std::endl;
            }

            std::cout << std::endl;
            ret = false;
        }

    }

    return ret;
}


// ---------------------------------------------------------

int main() {
    bool success = true;
    success &= test_ctrt_phk256_128_avx();
    std::cout << (success ? "All tests passed." : "Failure.") << std::endl;
    return 0;
}


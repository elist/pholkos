
#include <cstdint>
#include <cstring>
#include <iostream>
#include <fstream>
#include <string>
#include <jsoncpp/json/json.h>

#include "misc.h"

#include "phk-1024.h"

uint8_t HalfByteToDez(char const &h) {
    uint8_t ret = 0;
    switch (h) {
        case '0' :
            return 0;
        case '1' :
            return 1;
        case '2' :
            return 2;
        case '3' :
            return 3;
        case '4' :
            return 4;
        case '5' :
            return 5;
        case '6' :
            return 6;
        case '7' :
            return 7;
        case '8' :
            return 8;
        case '9' :
            return 9;
        case 'a' :
            return 10;
        case 'b' :
            return 11;
        case 'c' :
            return 12;
        case 'd' :
            return 13;
        case 'e' :
            return 14;
        case 'f' :
            return 15;
    }
    return ret;
}

uint8_t ByteToDez(char lhs, char rhs) {
    return 16 * HalfByteToDez(lhs) + HalfByteToDez(rhs);
}

void HexToDez(std::string const &hexstr, uint8_t *const &out) {
    uint64_t len = (hexstr.length() + 1) / 2;
    if (hexstr.length() % 2 == 1) {
        out[0] = HalfByteToDez(static_cast<char>(hexstr[0]));
        
        for (uint64_t i = 1; i <= len; ++i) {
            out[i] = ByteToDez(hexstr[(i << 1) - 1], hexstr[(i << 1)]);
        }
    } else {
        for (uint64_t i = 0; i < len; ++i) {
            out[i] = ByteToDez(hexstr[i << 1], hexstr[(i << 1) + 1]);
        }
    }
}


bool test_phk_1024_ref() {
    bool ret = true;
    std::ifstream test_file("../testcases/phk1024-1024.json",
                            std::ifstream::binary);
    
    if (!test_file.good()) {
        std::cout << "\033[91mTEST FILE ";
        std::cout << " NOT FOUND\n";
        std::cout << "You either changed the directory structure or called the";
        std::cout << " program from the wrong directory!\n\033[0m";
        return false;
    }
    
    Json::Value tests;
    
    test_file >> tests;
    
    for (auto test : tests) {
        std::cout << "------------------" << std::endl;
        std::string tmp = test["Plaintext"].asString();
        uint8_t plaintext[128];
        char *tmp2 = &tmp[0u];
        HexToDez(tmp2, plaintext);
        
        tmp = test["Key"].asString();
        uint8_t key[128];
        tmp2 = &tmp[0u];
        HexToDez(tmp2, key);
        
        tmp = test["Tweak"].asString();
        uint8_t tweak[16];
        tmp2 = &tmp[0u];
        HexToDez(tmp2, tweak);
        
        tmp = test["Ciphertext"].asString();
        uint8_t ciphertext[128];
        tmp2 = &tmp[0u];
        HexToDez(tmp2, ciphertext);
        
        phk1024_ctx ctx;
        uint8_t test_val[128];
        phk1024_load_constants();
        phk1024_set_key_encrypt(&ctx, key);
        phk1024_set_tweak_encrypt(&ctx, tweak);
        std::memcpy(test_val, plaintext, 128);
        phk1024_encrypt(&ctx, test_val);
        
        // memcmp returns 0 if values are equal
        bool was_successful = memcmp(ciphertext, test_val, 128) == 0;
        
        if (!was_successful) {
            std::cout << "\033[91mEncryption failed!\033[0m" << std::endl;
            std::cout << "Expected: " << std::hex;
            
            for (int i = 0; i < 128; ++i) {
                std::cout << std::setw(2) << std::setfill('0')
                          << (int) ciphertext[i];
            }
            
            std::cout << std::endl;
            std::cout << "But was: " << std::hex;
            
            for (int i = 0; i < 128; ++i) {
                std::cout << std::setw(2) << std::setfill('0')
                          << (int) test_val[i];
            }
            
            std::cout << std::endl;
            ret = false;
        }
        
        phk1024_load_constants();
        phk1024_set_key_decrypt(&ctx, key);
        phk1024_set_tweak_decrypt(&ctx, tweak);
        std::memcpy(test_val, ciphertext, 128);
        phk1024_decrypt(&ctx, test_val);
        
        was_successful = memcmp(plaintext, test_val, 128) == 0;
        
        if (!was_successful) {
            std::cout << "\033[91mDecryption failed!\033[0m" << std::endl;
            std::cout << "Expected: " << std::hex;
            
            for (int i = 0; i < 128; ++i) {
                std::cout << std::setw(2) << std::setfill('0')
                          << (int) plaintext[i];
            }
            
            std::cout << std::endl;
            std::cout << "But was: " << std::hex;
            
            for (int i = 0; i < 128; ++i) {
                std::cout << std::setw(2) << std::setfill('0')
                          << (int) test_val[i];
            }
            
            std::cout << std::endl;
            ret = false;
        }
        
        std::cout << "------------------" << std::endl;
    }
    
    return ret;
}

// ---------------------------------------------------------

bool test_phk_1024_256_ref() {
    bool ret = true;
    std::ifstream test_file("../testcases/phk1024-256.json",
                            std::ifstream::binary);
    
    if (!test_file.good()) {
        std::cout << "\033[91mTEST FILE ";
        std::cout << " NOT FOUND\n";
        std::cout << "You either changed the directory structure or called the";
        std::cout << " program from the wrong directory!\n\033[0m";
        return false;
    }
    
    Json::Value tests;
    
    test_file >> tests;
    
    for (auto test : tests) {
        std::cout << "------------------" << std::endl;
        std::string tmp = test["Plaintext"].asString();
        uint8_t plaintext[128];
        char *tmp2 = &tmp[0u];
        HexToDez(tmp2, plaintext);
        
        tmp = test["Key"].asString();
        uint8_t key[32];
        tmp2 = &tmp[0u];
        HexToDez(tmp2, key);
        
        tmp = test["Tweak"].asString();
        uint8_t tweak[16];
        tmp2 = &tmp[0u];
        HexToDez(tmp2, tweak);
        
        tmp = test["Ciphertext"].asString();
        uint8_t ciphertext[128];
        tmp2 = &tmp[0u];
        HexToDez(tmp2, ciphertext);
        
        phk1024_256_ctx ctx;
        uint8_t test_val[128];
        phk1024_256_load_constants();
        phk1024_256_set_key_encrypt(&ctx, key);
        phk1024_256_set_tweak_encrypt(&ctx, tweak);
        std::memcpy(test_val, plaintext, 128);
        phk1024_256_encrypt(&ctx, test_val);
        
        // memcmp returns 0 if values are equal
        bool was_successful = memcmp(ciphertext, test_val, 128) == 0;
        
        if (!was_successful) {
            std::cout << "\033[91mEncryption failed!\033[0m" << std::endl;
            std::cout << "Expected: " << std::hex;
            
            for (int i = 0; i < 128; ++i) {
                std::cout << std::setw(2) << std::setfill('0')
                          << (int) ciphertext[i];
            }
            
            std::cout << std::endl;
            std::cout << "But was: " << std::hex;
            
            for (int i = 0; i < 128; ++i) {
                std::cout << std::setw(2) << std::setfill('0')
                          << (int) test_val[i];
            }
            
            std::cout << std::endl;
            ret = false;
        }
        
        phk1024_256_load_constants();
        phk1024_256_set_key_decrypt(&ctx, key);
        phk1024_256_set_tweak_decrypt(&ctx, tweak);
        std::memcpy(test_val, ciphertext, 128);
        phk1024_256_decrypt(&ctx, test_val);
        
        was_successful = memcmp(plaintext, test_val, 128) == 0;
        
        if (!was_successful) {
            std::cout << "\033[91mDecryption failed!\033[0m" << std::endl;
            std::cout << "Expected: " << std::hex;
            
            for (int i = 0; i < 128; ++i) {
                std::cout << std::setw(2) << std::setfill('0')
                          << (int) plaintext[i];
            }
            
            std::cout << std::endl;
            std::cout << "But was: " << std::hex;
            
            for (int i = 0; i < 128; ++i) {
                std::cout << std::setw(2) << std::setfill('0')
                          << (int) test_val[i];
            }
            
            std::cout << std::endl;
            ret = false;
        }
        
        std::cout << "------------------" << std::endl;
    }
    
    return ret;
}

// ---------------------------------------------------------

int main() {
    bool success = true;
    success &= test_phk_1024_ref();
    success &= test_phk_1024_256_ref();
    std::cout << (success ? "All tests passed." : "Failure.") << std::endl;
    return 0;
}

#include "phk-512.h"
#include <cstdint>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <string>
#include <iostream>
#include <cstdio>

void phk512_load_constants() {
    phk512_rc[0] = _mm_set_epi32(0x0684704c,0xe620c00a,0xb2c5fef0,0x75817b9d);
    phk512_rc[1] = _mm_set_epi32(0x8b66b4e1,0x88f3a06b,0x640f6ba4,0x2f08f717);
    phk512_rc[2] = _mm_set_epi32(0x3402de2d,0x53f28498,0xcf029d60,0x9f029114);
    phk512_rc[3] = _mm_set_epi32(0x0ed6eae6,0x2e7b4f08,0xbbf3bcaf,0xfd5b4f79);
    phk512_rc[4] = _mm_set_epi32(0xcbcfb0cb,0x4872448b,0x79eecd1c,0xbe397044);
    phk512_rc[5] = _mm_set_epi32(0x7eeacdee,0x6e9032b7,0x8d5335ed,0x2b8a057b);
    phk512_rc[6] = _mm_set_epi32(0x67c28f43,0x5e2e7cd0,0xe2412761,0xda4fef1b);
    phk512_rc[7] = _mm_set_epi32(0x2924d9b0,0xafcacc07,0x675ffde2,0x1fc70b3b);
    phk512_rc[8] = _mm_set_epi32(0xab4d63f1,0xe6867fe9,0xecdb8fca,0xb9d465ee);
    phk512_rc[9] = _mm_set_epi32(0x1c30bf84,0xd4b7cd64,0x5b2a404f,0xad037e33);
    phk512_rc[10] = _mm_set_epi32(0xb2cc0bb9,0x941723bf,0x69028b2e,0x8df69800);
    phk512_rc[11] = _mm_set_epi32(0xfa0478a6,0xde6f5572,0x4aaa9ec8,0x5c9d2d8a);
    phk512_rc[12] = _mm_set_epi32(0xdfb49f2b,0x6b772a12,0x0efa4f2e,0x29129fd4);
    phk512_rc[13] = _mm_set_epi32(0x1ea10344,0xf449a236,0x32d611ae,0xbb6a12ee);
    phk512_rc[14] = _mm_set_epi32(0xaf044988,0x4b050084,0x5f9600c9,0x9ca8eca6);
    phk512_rc[15] = _mm_set_epi32(0x21025ed8,0x9d199c4f,0x78a2c7e3,0x27e593ec);
    phk512_rc[16] = _mm_set_epi32(0xbf3aaaf8,0xa759c9b7,0xb9282ecd,0x82d40173);
    phk512_rc[17] = _mm_set_epi32(0x6260700d, 0x6186b017, 0x37f2efd9, 0x10307d6b);
    phk512_rc[18] = _mm_set_epi32(0x5aca45c2, 0x21300443, 0x81c29153, 0xf6fc9ac6);
    phk512_rc[19] = _mm_set_epi32(0x9223973c, 0x226b68bb, 0x2caf92e8, 0x36d1943a);
    phk512_rc[20] = _mm_set_epi32(0xd3bf9238, 0x225886eb, 0x6cbab958, 0xe51071b4);

}

#define PHK512_KS_ROUND_X_ENC(a, b, c, d, e, f, g, h) \
    PHK512_KS_ROUND(ctx->rk[e], ctx->rk[f], ctx->rk[g], ctx->rk[h], ctx->rk[a], ctx->rk[b], ctx->rk[c], ctx->rk[d]) \
    ctx->rk[a] = _mm_xor_si128(ctx->rk[a], phk512_rc[a >> 2]);

void phk512_set_key_encrypt(struct phk512_ctx *const ctx, const uint8_t *const key) {
    __m128i tmp;

    ctx->rt = _mm_setzero_si128();

    ctx->rk[0] = _mm_loadu_si128((__m128i*)key);
    ctx->rk[1] = _mm_loadu_si128((__m128i*)&key[16]);
    ctx->rk[2] = _mm_loadu_si128((__m128i*)&key[32]);
    ctx->rk[3] = _mm_loadu_si128((__m128i*)&key[48]);

    PHK512_KS_ROUND_X_ENC(0, 1, 2, 3, 4, 5, 6, 7)
    PHK512_KS_ROUND_X_ENC(4, 5, 6, 7, 8, 9, 10, 11)
    PHK512_KS_ROUND_X_ENC(8, 9, 10, 11, 12, 13, 14, 15)
    PHK512_KS_ROUND_X_ENC(12, 13, 14, 15, 16, 17, 18, 19)
    PHK512_KS_ROUND_X_ENC(16, 17, 18, 19, 20, 21, 22, 23)
    PHK512_KS_ROUND_X_ENC(20, 21, 22, 23, 24, 25, 26, 27)
    PHK512_KS_ROUND_X_ENC(24, 25, 26, 27, 28, 29, 30, 31)
    PHK512_KS_ROUND_X_ENC(28, 29, 30, 31, 32, 33, 34, 35)
    PHK512_KS_ROUND_X_ENC(32, 33, 34, 35, 36, 37, 38, 39)
    PHK512_KS_ROUND_X_ENC(36, 37, 38, 39, 40, 41, 42, 43)
    PHK512_KS_ROUND_X_ENC(40, 41, 42, 43, 44, 45, 46, 47)
    PHK512_KS_ROUND_X_ENC(44, 45, 46, 47, 48, 49, 50, 51)
    PHK512_KS_ROUND_X_ENC(48, 49, 50, 51, 52, 53, 54, 55)
    PHK512_KS_ROUND_X_ENC(52, 53, 54, 55, 56, 57, 58, 59)
    PHK512_KS_ROUND_X_ENC(56, 57, 58, 59, 60, 61, 62, 63)
    PHK512_KS_ROUND_X_ENC(60, 61, 62, 63, 64, 65, 66, 67)
    PHK512_KS_ROUND_X_ENC(64, 65, 66, 67, 68, 69, 70, 71)
    PHK512_KS_ROUND_X_ENC(68, 69, 70, 71, 72, 73, 74, 75)
    PHK512_KS_ROUND_X_ENC(72, 73, 74, 75, 76, 77, 78, 79)
    PHK512_KS_ROUND_X_ENC(76, 77, 78, 79, 80, 81, 82, 83)

    ctx->rk[80] = _mm_xor_si128(ctx->rk[80], phk512_rc[20]);
}

void phk512_set_key256_encrypt(struct phk512_ctx *const ctx, const uint8_t *const key) {
    __m128i tmp;

    ctx->rt = _mm_setzero_si128();

    ctx->rk[0] = _mm_loadu_si128((__m128i*)key);
    ctx->rk[1] = _mm_loadu_si128((__m128i*)&key[16]);

    PHK512_KS_EXPAND(ctx->rk[0], ctx->rk[1], ctx->rk[2], \
                      ctx->rk[3], ctx->rk[0], ctx->rk[1])

    PHK512_KS_ROUND_X_ENC(0, 1, 2, 3, 4, 5, 6, 7)
    PHK512_KS_ROUND_X_ENC(4, 5, 6, 7, 8, 9, 10, 11)
    PHK512_KS_ROUND_X_ENC(8, 9, 10, 11, 12, 13, 14, 15)
    PHK512_KS_ROUND_X_ENC(12, 13, 14, 15, 16, 17, 18, 19)
    PHK512_KS_ROUND_X_ENC(16, 17, 18, 19, 20, 21, 22, 23)
    PHK512_KS_ROUND_X_ENC(20, 21, 22, 23, 24, 25, 26, 27)
    PHK512_KS_ROUND_X_ENC(24, 25, 26, 27, 28, 29, 30, 31)
    PHK512_KS_ROUND_X_ENC(28, 29, 30, 31, 32, 33, 34, 35)
    PHK512_KS_ROUND_X_ENC(32, 33, 34, 35, 36, 37, 38, 39)
    PHK512_KS_ROUND_X_ENC(36, 37, 38, 39, 40, 41, 42, 43)
    PHK512_KS_ROUND_X_ENC(40, 41, 42, 43, 44, 45, 46, 47)
    PHK512_KS_ROUND_X_ENC(44, 45, 46, 47, 48, 49, 50, 51)
    PHK512_KS_ROUND_X_ENC(48, 49, 50, 51, 52, 53, 54, 55)
    PHK512_KS_ROUND_X_ENC(52, 53, 54, 55, 56, 57, 58, 59)
    PHK512_KS_ROUND_X_ENC(56, 57, 58, 59, 60, 61, 62, 63)
    PHK512_KS_ROUND_X_ENC(60, 61, 62, 63, 64, 65, 66, 67)
    PHK512_KS_ROUND_X_ENC(64, 65, 66, 67, 68, 69, 70, 71)
    PHK512_KS_ROUND_X_ENC(68, 69, 70, 71, 72, 73, 74, 75)
    PHK512_KS_ROUND_X_ENC(72, 73, 74, 75, 76, 77, 78, 79)
    PHK512_KS_ROUND_X_ENC(76, 77, 78, 79, 80, 81, 82, 83)

    ctx->rk[80] = _mm_xor_si128(ctx->rk[80], phk512_rc[20]);
}


#define PHK512_KS_ROUND_X_DEC_FIRST(a, b, c, d, e, f, g, h) \
    PHK512_KS_ROUND(ctx->rk[e], ctx->rk[f], ctx->rk[g], ctx->rk[h], ctx->rk[a], ctx->rk[b], ctx->rk[c], ctx->rk[d]) \
    ctx->rk[a] = _mm_xor_si128(ctx->rk[a], phk512_rc[a >> 2]);

#define PHK512_KS_ROUND_X_DEC(a, b, c, d, e, f, g, h) \
    PHK512_KS_ROUND(ctx->rk[e], ctx->rk[f], ctx->rk[g], ctx->rk[h], ctx->rk[a], ctx->rk[b], ctx->rk[c], ctx->rk[d]) \
    ctx->rk[a] = _mm_aesimc_si128(_mm_xor_si128(ctx->rk[a], phk512_rc[a >> 2])); \
    ctx->rk[b] = _mm_aesimc_si128(ctx->rk[b]); \
    ctx->rk[c] = _mm_aesimc_si128(ctx->rk[c]); \
    ctx->rk[d] = _mm_aesimc_si128(ctx->rk[d]);

void phk512_set_key_decrypt(struct phk512_ctx *const ctx, const uint8_t *const key) {
    __m128i tmp;

    ctx->rt = _mm_setzero_si128();

    ctx->rk[0] = _mm_loadu_si128((__m128i*)key);
    ctx->rk[1] = _mm_loadu_si128((__m128i*)&key[16]);
    ctx->rk[2] = _mm_loadu_si128((__m128i*)&key[32]);
    ctx->rk[3] = _mm_loadu_si128((__m128i*)&key[48]);
//foobar
//    PHK512_KS_EXPAND(ctx->rk[0], ctx->rk[1], ctx->rk[2], ctx->rk[3], ctx->rk[0], ctx->rk[1])

    PHK512_KS_ROUND_X_DEC_FIRST(0, 1, 2, 3, 4, 5, 6, 7)
    PHK512_KS_ROUND_X_DEC(4, 5, 6, 7, 8, 9, 10, 11)
    PHK512_KS_ROUND_X_DEC(8, 9, 10, 11, 12, 13, 14, 15)
    PHK512_MIX_ENC(ctx->rk[8], ctx->rk[9], ctx->rk[10], ctx->rk[11], ctx->rk[8], ctx->rk[9], ctx->rk[10], ctx->rk[11])
    PHK512_KS_ROUND_X_DEC(12, 13, 14, 15, 16, 17, 18, 19)
    PHK512_KS_ROUND_X_DEC(16, 17, 18, 19, 20, 21, 22, 23)
    PHK512_MIX_ENC(ctx->rk[16], ctx->rk[17], ctx->rk[18], ctx->rk[19], ctx->rk[16], ctx->rk[17], ctx->rk[18], ctx->rk[19])
    PHK512_KS_ROUND_X_DEC(20, 21, 22, 23, 24, 25, 26, 27)
    PHK512_KS_ROUND_X_DEC(24, 25, 26, 27, 28, 29, 30, 31)
    PHK512_MIX_ENC(ctx->rk[24], ctx->rk[25], ctx->rk[26], ctx->rk[27], ctx->rk[24], ctx->rk[25], ctx->rk[26], ctx->rk[27])
    PHK512_KS_ROUND_X_DEC(28, 29, 30, 31, 32, 33, 34, 35)
    PHK512_KS_ROUND_X_DEC(32, 33, 34, 35, 36, 37, 38, 39)
    PHK512_MIX_ENC(ctx->rk[32], ctx->rk[33], ctx->rk[34], ctx->rk[35], ctx->rk[32], ctx->rk[33], ctx->rk[34], ctx->rk[35])
    PHK512_KS_ROUND_X_DEC(36, 37, 38, 39, 40, 41, 42, 43)
    PHK512_KS_ROUND_X_DEC(40, 41, 42, 43, 44, 45, 46, 47)
    PHK512_MIX_ENC(ctx->rk[40], ctx->rk[41], ctx->rk[42], ctx->rk[43], ctx->rk[40], ctx->rk[41], ctx->rk[42], ctx->rk[43])
    PHK512_KS_ROUND_X_DEC(44, 45, 46, 47, 48, 49, 50, 51)
    PHK512_KS_ROUND_X_DEC(48, 49, 50, 51, 52, 53, 54, 55)
    PHK512_MIX_ENC(ctx->rk[48], ctx->rk[49], ctx->rk[50], ctx->rk[51], ctx->rk[48], ctx->rk[49], ctx->rk[50], ctx->rk[51])
    PHK512_KS_ROUND_X_DEC(52, 53, 54, 55, 56, 57, 58, 59)
    PHK512_KS_ROUND_X_DEC(56, 57, 58, 59, 60, 61, 62, 63)
    PHK512_MIX_ENC(ctx->rk[56], ctx->rk[57], ctx->rk[58], ctx->rk[59], ctx->rk[56], ctx->rk[57], ctx->rk[58], ctx->rk[59])
    PHK512_KS_ROUND_X_DEC(60, 61, 62, 63, 64, 65, 66, 67)
    PHK512_KS_ROUND_X_DEC(64, 65, 66, 67, 68, 69, 70, 71)
    PHK512_MIX_ENC(ctx->rk[64], ctx->rk[65], ctx->rk[66], ctx->rk[67], ctx->rk[64], ctx->rk[65], ctx->rk[66], ctx->rk[67])
    PHK512_KS_ROUND_X_DEC(68, 69, 70, 71, 72, 73, 74, 75)
    PHK512_KS_ROUND_X_DEC(72, 73, 74, 75, 76, 77, 78, 79)
    PHK512_MIX_ENC(ctx->rk[72], ctx->rk[73], ctx->rk[74], ctx->rk[75], ctx->rk[72], ctx->rk[73], ctx->rk[74], ctx->rk[75])
    PHK512_KS_ROUND_X_DEC(76, 77, 78, 79, 80, 81, 82, 83)

    ctx->rk[80] = _mm_xor_si128(ctx->rk[80], phk512_rc[20]);
}

void phk512_set_key256_decrypt(struct phk512_ctx *const ctx, const uint8_t *const key) {
    __m128i tmp;

    ctx->rt = _mm_setzero_si128();

    ctx->rk[0] = _mm_loadu_si128((__m128i*)key);
    ctx->rk[1] = _mm_loadu_si128((__m128i*)&key[16]);

    PHK512_KS_EXPAND(ctx->rk[0], ctx->rk[1], ctx->rk[2], ctx->rk[3], ctx->rk[0], ctx->rk[1])

    PHK512_KS_ROUND_X_DEC_FIRST(0, 1, 2, 3, 4, 5, 6, 7)
    PHK512_KS_ROUND_X_DEC(4, 5, 6, 7, 8, 9, 10, 11)
    PHK512_KS_ROUND_X_DEC(8, 9, 10, 11, 12, 13, 14, 15)
    PHK512_MIX_ENC(ctx->rk[8], ctx->rk[9], ctx->rk[10], ctx->rk[11], ctx->rk[8], ctx->rk[9], ctx->rk[10], ctx->rk[11])
    PHK512_KS_ROUND_X_DEC(12, 13, 14, 15, 16, 17, 18, 19)
    PHK512_KS_ROUND_X_DEC(16, 17, 18, 19, 20, 21, 22, 23)
    PHK512_MIX_ENC(ctx->rk[16], ctx->rk[17], ctx->rk[18], ctx->rk[19], ctx->rk[16], ctx->rk[17], ctx->rk[18], ctx->rk[19])
    PHK512_KS_ROUND_X_DEC(20, 21, 22, 23, 24, 25, 26, 27)
    PHK512_KS_ROUND_X_DEC(24, 25, 26, 27, 28, 29, 30, 31)
    PHK512_MIX_ENC(ctx->rk[24], ctx->rk[25], ctx->rk[26], ctx->rk[27], ctx->rk[24], ctx->rk[25], ctx->rk[26], ctx->rk[27])
    PHK512_KS_ROUND_X_DEC(28, 29, 30, 31, 32, 33, 34, 35)
    PHK512_KS_ROUND_X_DEC(32, 33, 34, 35, 36, 37, 38, 39)
    PHK512_MIX_ENC(ctx->rk[32], ctx->rk[33], ctx->rk[34], ctx->rk[35], ctx->rk[32], ctx->rk[33], ctx->rk[34], ctx->rk[35])
    PHK512_KS_ROUND_X_DEC(36, 37, 38, 39, 40, 41, 42, 43)
    PHK512_KS_ROUND_X_DEC(40, 41, 42, 43, 44, 45, 46, 47)
    PHK512_MIX_ENC(ctx->rk[40], ctx->rk[41], ctx->rk[42], ctx->rk[43], ctx->rk[40], ctx->rk[41], ctx->rk[42], ctx->rk[43])
    PHK512_KS_ROUND_X_DEC(44, 45, 46, 47, 48, 49, 50, 51)
    PHK512_KS_ROUND_X_DEC(48, 49, 50, 51, 52, 53, 54, 55)
    PHK512_MIX_ENC(ctx->rk[48], ctx->rk[49], ctx->rk[50], ctx->rk[51], ctx->rk[48], ctx->rk[49], ctx->rk[50], ctx->rk[51])
    PHK512_KS_ROUND_X_DEC(52, 53, 54, 55, 56, 57, 58, 59)
    PHK512_KS_ROUND_X_DEC(56, 57, 58, 59, 60, 61, 62, 63)
    PHK512_MIX_ENC(ctx->rk[56], ctx->rk[57], ctx->rk[58], ctx->rk[59], ctx->rk[56], ctx->rk[57], ctx->rk[58], ctx->rk[59])
    PHK512_KS_ROUND_X_DEC(60, 61, 62, 63, 64, 65, 66, 67)
    PHK512_KS_ROUND_X_DEC(64, 65, 66, 67, 68, 69, 70, 71)
    PHK512_MIX_ENC(ctx->rk[64], ctx->rk[65], ctx->rk[66], ctx->rk[67], ctx->rk[64], ctx->rk[65], ctx->rk[66], ctx->rk[67])
    PHK512_KS_ROUND_X_DEC(68, 69, 70, 71, 72, 73, 74, 75)
    PHK512_KS_ROUND_X_DEC(72, 73, 74, 75, 76, 77, 78, 79)
    PHK512_MIX_ENC(ctx->rk[72], ctx->rk[73], ctx->rk[74], ctx->rk[75], ctx->rk[72], ctx->rk[73], ctx->rk[74], ctx->rk[75])
    PHK512_KS_ROUND_X_DEC(76, 77, 78, 79, 80, 81, 82, 83)

    ctx->rk[80] = _mm_xor_si128(ctx->rk[80], phk512_rc[20]);
}

void phk512_set_tweak_encrypt(struct phk512_ctx *const ctx, const uint8_t *const tweak) {
    __m128i tmp = ctx->rt;

    ctx->rt = _mm_loadu_si128((__m128i*)tweak);

    tmp = _mm_xor_si128(tmp, ctx->rt);

    PHK512_TWEAK_WRITE(tmp, 0)
    PHK512_TWEAK_WRITE(tmp, 13)
    PHK512_TWEAK_MIX_WRITE(1)
    PHK512_TWEAK_WRITE(tmp, 14)
    PHK512_TWEAK_MIX_WRITE(2)
    PHK512_TWEAK_WRITE(tmp, 15)
    PHK512_TWEAK_MIX_WRITE(3)
    PHK512_TWEAK_WRITE(tmp, 16)
    PHK512_TWEAK_MIX_WRITE(4)
    PHK512_TWEAK_WRITE(tmp, 17)
    PHK512_TWEAK_MIX_WRITE(5)
    PHK512_TWEAK_WRITE(tmp, 18)
    PHK512_TWEAK_MIX_WRITE(6)
    PHK512_TWEAK_WRITE(tmp, 19)
    PHK512_TWEAK_MIX_WRITE(7)
    PHK512_TWEAK_WRITE(tmp, 20)
    PHK512_TWEAK_MIX_WRITE(8)
    PHK512_TWEAK_MIX_WRITE(9)
    PHK512_TWEAK_MIX_WRITE(10)
    PHK512_TWEAK_MIX_WRITE(11)
    PHK512_TWEAK_MIX_WRITE(12)
}

void phk512_set_tweak_decrypt(struct phk512_ctx *const ctx, const uint8_t *const tweak) {
    __m128i tmp0 = ctx->rt;
    __m128i tmp1;

    ctx->rt = _mm_loadu_si128((__m128i*)tweak);

    tmp0 = _mm_xor_si128(tmp0, ctx->rt);

    PHK512_TWEAK_WRITE(tmp0, 0)
    PHK512_TWEAK_IMC_WRITE(tmp1, tmp0, 13)

    PHK512_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 1)
    PHK512_TWEAK_WRITE(tmp1, 14)

    PHK512_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 2)
    PHK512_TWEAK_WRITE(tmp1, 15)

    PHK512_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 3)
    PHK512_TWEAK_WRITE(tmp1, 16)

    PHK512_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 4)
    PHK512_TWEAK_WRITE(tmp1, 17)

    PHK512_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 5)
    PHK512_TWEAK_WRITE(tmp1, 18)

    PHK512_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 6)
    PHK512_TWEAK_WRITE(tmp1, 19)

    PHK512_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 7)
    PHK512_TWEAK_WRITE(tmp0, 20)


    PHK512_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 8)
    PHK512_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 9)
    PHK512_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 10)
    PHK512_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 11)
    PHK512_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 12)
}

#define PHK512_ENC_STEP_X(a, b, c, d, e, f, g, h) \
    PHK512_STEP_ENC(state[0], state[1], state[2], state[3], \
            ctx->rk[a], ctx->rk[b], ctx->rk[c], ctx->rk[d], \
            ctx->rk[e], ctx->rk[f], ctx->rk[g], ctx->rk[h])

void phk512_encrypt(const struct phk512_ctx * const ctx, uint8_t * const buf) {
    __m128i state[PHK512_SUBSTATES], tmp;

    // init plaintext and add initial round key
    state[0] = _mm_loadu_si128((__m128i*)buf);
    state[1] = _mm_loadu_si128((__m128i*)&buf[16]);
    state[2] = _mm_loadu_si128((__m128i*)&buf[32]);
    state[3] = _mm_loadu_si128((__m128i*)&buf[48]);

    state[0] = _mm_xor_si128(state[0], ctx->rk[0]);
    state[1] = _mm_xor_si128(state[1], ctx->rk[1]);
    state[2] = _mm_xor_si128(state[2], ctx->rk[2]);
    state[3] = _mm_xor_si128(state[3], ctx->rk[3]);

    PHK512_ENC_STEP_X(4, 5, 6, 7, 8, 9, 10, 11)
    PHK512_ENC_STEP_X(12, 13, 14, 15, 16, 17, 18, 19)
    PHK512_ENC_STEP_X(20, 21, 22, 23, 24, 25, 26, 27)
    PHK512_ENC_STEP_X(28, 29, 30, 31, 32, 33, 34, 35)
    PHK512_ENC_STEP_X(36, 37, 38, 39, 40, 41, 42, 43)
    PHK512_ENC_STEP_X(44, 45, 46, 47, 48, 49, 50, 51)
    PHK512_ENC_STEP_X(52, 53, 54, 55, 56, 57, 58, 59)
    PHK512_ENC_STEP_X(60, 61, 62, 63, 64, 65, 66, 67)
    PHK512_ENC_STEP_X(68, 69, 70, 71, 72, 73, 74, 75)
    PHK512_STEP_ENC_LAST(state[0], state[1], state[2], state[3],
            ctx->rk[76], ctx->rk[77], ctx->rk[78], ctx->rk[79],
            ctx->rk[80], ctx->rk[81], ctx->rk[82], ctx->rk[83])

    _mm_storeu_si128((__m128i*)buf, state[0]);
    _mm_storeu_si128((__m128i*)&buf[16], state[1]);
    _mm_storeu_si128((__m128i*)&buf[32], state[2]);
    _mm_storeu_si128((__m128i*)&buf[48], state[3]);
}

#define PHK512X2_ENC_STEP_X(a, b, c, d, e, f, g, h) \
    PHK512X2_STEP_ENC(state[0], state[1], state[2], state[3], \
            state[4], state[5], state[6], state[7], \
            ctx->rk[a], ctx->rk[b], ctx->rk[c], ctx->rk[d], \
            ctx->rk[e], ctx->rk[f], ctx->rk[g], ctx->rk[h])

void phk512x2_encrypt(const struct phk512_ctx * const ctx, uint8_t * const buf) {
    __m128i state[PHK512_SUBSTATES << 2], tmp;

    // init plaintext and add initial round key
    state[0] = _mm_loadu_si128((__m128i*)buf);
    state[1] = _mm_loadu_si128((__m128i*)&buf[16]);
    state[2] = _mm_loadu_si128((__m128i*)&buf[32]);
    state[3] = _mm_loadu_si128((__m128i*)&buf[48]);
    state[4] = _mm_loadu_si128((__m128i*)&buf[64]);
    state[5] = _mm_loadu_si128((__m128i*)&buf[80]);
    state[6] = _mm_loadu_si128((__m128i*)&buf[96]);
    state[7] = _mm_loadu_si128((__m128i*)&buf[112]);

    state[0] = _mm_xor_si128(state[0], ctx->rk[0]);
    state[1] = _mm_xor_si128(state[1], ctx->rk[1]);
    state[2] = _mm_xor_si128(state[2], ctx->rk[2]);
    state[3] = _mm_xor_si128(state[3], ctx->rk[3]);
    state[4] = _mm_xor_si128(state[4], ctx->rk[0]);
    state[5] = _mm_xor_si128(state[5], ctx->rk[1]);
    state[6] = _mm_xor_si128(state[6], ctx->rk[2]);
    state[7] = _mm_xor_si128(state[7], ctx->rk[3]);

    PHK512X2_ENC_STEP_X(4, 5, 6, 7, 8, 9, 10, 11)
    PHK512X2_ENC_STEP_X(12, 13, 14, 15, 16, 17, 18, 19)
    PHK512X2_ENC_STEP_X(20, 21, 22, 23, 24, 25, 26, 27)
    PHK512X2_ENC_STEP_X(28, 29, 30, 31, 32, 33, 34, 35)
    PHK512X2_ENC_STEP_X(36, 37, 38, 39, 40, 41, 42, 43)
    PHK512X2_ENC_STEP_X(44, 45, 46, 47, 48, 49, 50, 51)
    PHK512X2_ENC_STEP_X(52, 53, 54, 55, 56, 57, 58, 59)
    PHK512X2_STEP_ENC_LAST(state[0], state[1], state[2], state[3],
                       state[4], state[5], state[6], state[7],
                     ctx->rk[60], ctx->rk[61], ctx->rk[62], ctx->rk[63],
                     ctx->rk[64], ctx->rk[65], ctx->rk[66], ctx->rk[67])

    _mm_storeu_si128((__m128i*)buf, state[0]);
    _mm_storeu_si128((__m128i*)&buf[16], state[1]);
    _mm_storeu_si128((__m128i*)&buf[32], state[2]);
    _mm_storeu_si128((__m128i*)&buf[48], state[3]);
    _mm_storeu_si128((__m128i*)&buf[64], state[4]);
    _mm_storeu_si128((__m128i*)&buf[80], state[5]);
    _mm_storeu_si128((__m128i*)&buf[96], state[6]);
    _mm_storeu_si128((__m128i*)&buf[112], state[7]);
}

#define PHK512_DEC_STEP_X(a, b, c, d, e, f, g, h) \
    PHK512_STEP_DEC(state[0], state[1], state[2], state[3], \
            ctx->rk[a], ctx->rk[b], ctx->rk[c], ctx->rk[d], \
            ctx->rk[e], ctx->rk[f], ctx->rk[g], ctx->rk[h])

void phk512_decrypt(const struct phk512_ctx * const ctx, uint8_t * const buf) {
    __m128i state[PHK512_SUBSTATES], tmp;

    // init ciphertext
    state[0] = _mm_loadu_si128((__m128i*)buf);
    state[1] = _mm_loadu_si128((__m128i*)&buf[16]);
    state[2] = _mm_loadu_si128((__m128i*)&buf[32]);
    state[3] = _mm_loadu_si128((__m128i*)&buf[48]);

    // remove last round key
    state[0] = _mm_xor_si128(state[0], ctx->rk[80]);
    state[1] = _mm_xor_si128(state[1], ctx->rk[81]);
    state[2] = _mm_xor_si128(state[2], ctx->rk[82]);
    state[3] = _mm_xor_si128(state[3], ctx->rk[83]);


    PHK512_DEC_STEP_X(72, 73, 74, 75, 76, 77, 78, 79)

    PHK512_DEC_STEP_X(64, 65, 66, 67, 68, 69, 70, 71)
    PHK512_DEC_STEP_X(56, 57, 58, 59, 60, 61, 62, 63)
    PHK512_DEC_STEP_X(48, 49, 50, 51, 52, 53, 54, 55)
    PHK512_DEC_STEP_X(40, 41, 42, 43, 44, 45, 46, 47)
    PHK512_DEC_STEP_X(32, 33, 34, 35, 36, 37, 38, 39)
    PHK512_DEC_STEP_X(24, 25, 26, 27, 28, 29, 30, 31)
    PHK512_DEC_STEP_X(16, 17, 18, 19, 20, 21, 22, 23)
    PHK512_DEC_STEP_X(8, 9, 10, 11, 12, 13, 14, 15)
    PHK512_STEP_DEC_LAST(state[0], state[1], state[2], state[3],
                         ctx->rk[0], ctx->rk[1], ctx->rk[2], ctx->rk[3],
                         ctx->rk[4], ctx->rk[5], ctx->rk[6], ctx->rk[7])

    _mm_storeu_si128((__m128i*)buf, state[0]);
    _mm_storeu_si128((__m128i*)&buf[16], state[1]);
    _mm_storeu_si128((__m128i*)&buf[32], state[2]);
    _mm_storeu_si128((__m128i*)&buf[48], state[3]);
}

#define PHK512X2_DEC_STEP_X(a, b, c, d, e, f, g, h) \
    PHK512X2_STEP_DEC(state[0], state[1], state[2], state[3], \
            state[4], state[5], state[6], state[7], \
            ctx->rk[a], ctx->rk[b], ctx->rk[c], ctx->rk[d], \
            ctx->rk[e], ctx->rk[f], ctx->rk[g], ctx->rk[h])

void phk512x2_decrypt(const struct phk512_ctx * const ctx, uint8_t * const buf) {
    __m128i state[PHK512_SUBSTATES << 1], tmp;

    // init ciphertext
    state[0] = _mm_loadu_si128((__m128i*)buf);
    state[1] = _mm_loadu_si128((__m128i*)&buf[16]);
    state[2] = _mm_loadu_si128((__m128i*)&buf[32]);
    state[3] = _mm_loadu_si128((__m128i*)&buf[48]);
    state[4] = _mm_loadu_si128((__m128i*)&buf[64]);
    state[5] = _mm_loadu_si128((__m128i*)&buf[80]);
    state[6] = _mm_loadu_si128((__m128i*)&buf[96]);
    state[7] = _mm_loadu_si128((__m128i*)&buf[112]);

    // remove last round key
    state[0] = _mm_xor_si128(state[0], ctx->rk[64]);
    state[1] = _mm_xor_si128(state[1], ctx->rk[65]);
    state[2] = _mm_xor_si128(state[2], ctx->rk[66]);
    state[3] = _mm_xor_si128(state[3], ctx->rk[67]);
    state[4] = _mm_xor_si128(state[4], ctx->rk[64]);
    state[5] = _mm_xor_si128(state[5], ctx->rk[65]);
    state[6] = _mm_xor_si128(state[6], ctx->rk[66]);
    state[7] = _mm_xor_si128(state[7], ctx->rk[67]);

    PHK512X2_DEC_STEP_X(56, 57, 58, 59, 60, 61, 62, 63)
    PHK512X2_DEC_STEP_X(48, 49, 50, 51, 52, 53, 54, 55)
    PHK512X2_DEC_STEP_X(40, 41, 42, 43, 44, 45, 46, 47)
    PHK512X2_DEC_STEP_X(32, 33, 34, 35, 36, 37, 38, 39)
    PHK512X2_DEC_STEP_X(24, 25, 26, 27, 28, 29, 30, 31)
    PHK512X2_DEC_STEP_X(16, 17, 18, 19, 20, 21, 22, 23)
    PHK512X2_DEC_STEP_X(8, 9, 10, 11, 12, 13, 14, 15)
    PHK512X2_STEP_DEC_LAST(state[0], state[1], state[2], state[3],
                           state[4], state[5], state[6], state[7],
                         ctx->rk[0], ctx->rk[1], ctx->rk[2], ctx->rk[3],
                         ctx->rk[4], ctx->rk[5], ctx->rk[6], ctx->rk[7])

    _mm_storeu_si128((__m128i*)buf, state[0]);
    _mm_storeu_si128((__m128i*)&buf[16], state[1]);
    _mm_storeu_si128((__m128i*)&buf[32], state[2]);
    _mm_storeu_si128((__m128i*)&buf[48], state[3]);
    _mm_storeu_si128((__m128i*)&buf[64], state[4]);
    _mm_storeu_si128((__m128i*)&buf[80], state[5]);
    _mm_storeu_si128((__m128i*)&buf[96], state[6]);
    _mm_storeu_si128((__m128i*)&buf[112], state[7]);
}

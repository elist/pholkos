// ---------------------------------------------------------------------
// Simpira v2
// ---------------------------------------------------------------------

#include "simpira.h"

// ---------------------------------------------------------------------

void simpira256_set_key(simpira_context_t *ctx,
                        const uint8_t key[16]) {
    ctx->key = vloadu(key);
}

// ---------------------------------------------------------------------

void simpira256_decrypt(const simpira_context_t *ctx,
                        const uint8_t ciphertext[32],
                        uint8_t plaintext[32]) {
    __m128i l = vxor(vloadu(ciphertext), ctx->key);
    __m128i r = vloadu((ciphertext + 16));
    
    r = simpira_round_2blocks(l, r, 15, 2);
    l = simpira_round_2blocks(r, l, 14, 2);
    r = simpira_round_2blocks(l, r, 13, 2);
    l = simpira_round_2blocks(r, l, 12, 2);
    r = simpira_round_2blocks(l, r, 11, 2);
    l = simpira_round_2blocks(r, l, 10, 2);
    r = simpira_round_2blocks(l, r, 9, 2);
    l = simpira_round_2blocks(r, l, 8, 2);
    r = simpira_round_2blocks(l, r, 7, 2);
    l = simpira_round_2blocks(r, l, 6, 2);
    r = simpira_round_2blocks(l, r, 5, 2);
    l = simpira_round_2blocks(r, l, 4, 2);
    r = simpira_round_2blocks(l, r, 3, 2);
    l = simpira_round_2blocks(r, l, 2, 2);
    r = simpira_round_2blocks(l, r, 1, 2);
    
    vstoreu(plaintext, vxor(l, ctx->key));
    vstoreu((plaintext + 16), r);
}

// ---------------------------------------------------------------------

void simpira256_encrypt(const simpira_context_t *ctx,
                        const uint8_t plaintext[32],
                        uint8_t ciphertext[32]) {
    __m128i l = vxor(vloadu(plaintext), ctx->key);
    __m128i r = vloadu((plaintext + 16));
    
    r = simpira_round_2blocks(l, r, 1, 2);
    l = simpira_round_2blocks(r, l, 2, 2);
    r = simpira_round_2blocks(l, r, 3, 2);
    l = simpira_round_2blocks(r, l, 4, 2);
    r = simpira_round_2blocks(l, r, 5, 2);
    l = simpira_round_2blocks(r, l, 6, 2);
    r = simpira_round_2blocks(l, r, 7, 2);
    l = simpira_round_2blocks(r, l, 8, 2);
    r = simpira_round_2blocks(l, r, 9, 2);
    l = simpira_round_2blocks(r, l, 10, 2);
    r = simpira_round_2blocks(l, r, 11, 2);
    l = simpira_round_2blocks(r, l, 12, 2);
    r = simpira_round_2blocks(l, r, 13, 2);
    l = simpira_round_2blocks(r, l, 14, 2);
    r = simpira_round_2blocks(l, r, 15, 2);
    
    vstoreu(ciphertext, vxor(l, ctx->key));
    vstoreu((ciphertext + 16), r);
}

// ---------------------------------------------------------------------

void simpira512_set_key(simpira_context_t *ctx,
                        const uint8_t key[16]) {
    ctx->key = vloadu(key);
}

// ---------------------------------------------------------------------

void simpira512_decrypt(const simpira_context_t *ctx,
                        const uint8_t ciphertext[64],
                        uint8_t plaintext[64]) {
    __m128i state[4];
    state[0] = vxor(vloadu((ciphertext + 48)), ctx->key);
    state[1] = vloadu(ciphertext);
    state[2] = vloadu((ciphertext + 16));
    state[3] = vloadu((ciphertext + 32));
    
    __m128i tmp0;
    __m128i tmp1;
    
    simpira_round_4blocks(state[0], state[1], state[2], state[3], tmp0, tmp1,
                          29, 4);
    simpira_round_4blocks(state[3], state[0], state[1], state[2], tmp0, tmp1,
                          27, 4);
    simpira_round_4blocks(state[2], state[3], state[0], state[1], tmp0, tmp1,
                          25, 4);
    simpira_round_4blocks(state[1], state[2], state[3], state[0], tmp0, tmp1,
                          23, 4);
    simpira_round_4blocks(state[0], state[1], state[2], state[3], tmp0, tmp1,
                          21, 4);
    simpira_round_4blocks(state[3], state[0], state[1], state[2], tmp0, tmp1,
                          19, 4);
    simpira_round_4blocks(state[2], state[3], state[0], state[1], tmp0, tmp1,
                          17, 4);
    simpira_round_4blocks(state[1], state[2], state[3], state[0], tmp0, tmp1,
                          15, 4);
    simpira_round_4blocks(state[0], state[1], state[2], state[3], tmp0, tmp1,
                          13, 4);
    simpira_round_4blocks(state[3], state[0], state[1], state[2], tmp0, tmp1,
                          11, 4);
    simpira_round_4blocks(state[2], state[3], state[0], state[1], tmp0, tmp1, 9,
                          4);
    simpira_round_4blocks(state[1], state[2], state[3], state[0], tmp0, tmp1, 7,
                          4);
    simpira_round_4blocks(state[0], state[1], state[2], state[3], tmp0, tmp1, 5,
                          4);
    simpira_round_4blocks(state[3], state[0], state[1], state[2], tmp0, tmp1, 3,
                          4);
    simpira_round_4blocks(state[2], state[3], state[0], state[1], tmp0, tmp1, 1,
                          4);
    
    vstoreu(plaintext, vxor(state[2], ctx->key));
    vstoreu((plaintext + 16), state[3]);
    vstoreu((plaintext + 32), state[0]);
    vstoreu((plaintext + 48), state[1]);
}

// ---------------------------------------------------------------------

void simpira512_encrypt(const simpira_context_t *ctx,
                        const uint8_t plaintext[64],
                        uint8_t ciphertext[64]) {
    __m128i state[4];
    state[0] = vxor(vloadu(plaintext), ctx->key);
    state[1] = vloadu((plaintext + 16));
    state[2] = vloadu((plaintext + 32));
    state[3] = vloadu((plaintext + 48));
    
    __m128i tmp0;
    __m128i tmp1;
    
    simpira_round_4blocks(state[0], state[1], state[2], state[3], tmp0, tmp1, 1,
                          4);
    simpira_round_4blocks(state[1], state[2], state[3], state[0], tmp0, tmp1, 3,
                          4);
    simpira_round_4blocks(state[2], state[3], state[0], state[1], tmp0, tmp1, 5,
                          4);
    simpira_round_4blocks(state[3], state[0], state[1], state[2], tmp0, tmp1, 7,
                          4);
    simpira_round_4blocks(state[0], state[1], state[2], state[3], tmp0, tmp1, 9,
                          4);
    simpira_round_4blocks(state[1], state[2], state[3], state[0], tmp0, tmp1,
                          11, 4);
    simpira_round_4blocks(state[2], state[3], state[0], state[1], tmp0, tmp1,
                          13, 4);
    simpira_round_4blocks(state[3], state[0], state[1], state[2], tmp0, tmp1,
                          15, 4);
    simpira_round_4blocks(state[0], state[1], state[2], state[3], tmp0, tmp1,
                          17, 4);
    simpira_round_4blocks(state[1], state[2], state[3], state[0], tmp0, tmp1,
                          19, 4);
    simpira_round_4blocks(state[2], state[3], state[0], state[1], tmp0, tmp1,
                          21, 4);
    simpira_round_4blocks(state[3], state[0], state[1], state[2], tmp0, tmp1,
                          23, 4);
    simpira_round_4blocks(state[0], state[1], state[2], state[3], tmp0, tmp1,
                          25, 4);
    simpira_round_4blocks(state[1], state[2], state[3], state[0], tmp0, tmp1,
                          27, 4);
    simpira_round_4blocks(state[2], state[3], state[0], state[1], tmp0, tmp1,
                          29, 4);
    
    vstoreu(ciphertext, vxor(state[3], ctx->key));
    vstoreu((ciphertext + 16), state[0]);
    vstoreu((ciphertext + 32), state[1]);
    vstoreu((ciphertext + 48), state[2]);
}

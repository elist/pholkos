#include "phk.h"

uint8_t *phk_create_buffer(size_t len) {
    return (uint8_t*)(new __m128i[(len + 15) >> 4]);
}

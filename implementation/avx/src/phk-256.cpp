#include "phk-256.h"
#include "misc.h"

void debg_print(int i) {
  std::cout << "DEBUG " << (int) i << std::endl;
}


void phk256_load_constants(struct phk256_ctx *const ctx) {
    ctx->rc[0] = _mm_set_epi32(0x0684704c,0xe620c00a,0xb2c5fef0,0x75817b9d);
    ctx->rc[1] = _mm_set_epi32(0x8b66b4e1,0x88f3a06b,0x640f6ba4,0x2f08f717);
    ctx->rc[2] = _mm_set_epi32(0x3402de2d,0x53f28498,0xcf029d60,0x9f029114);
    ctx->rc[3] = _mm_set_epi32(0x0ed6eae6,0x2e7b4f08,0xbbf3bcaf,0xfd5b4f79);
    ctx->rc[4] = _mm_set_epi32(0xcbcfb0cb,0x4872448b,0x79eecd1c,0xbe397044);
    ctx->rc[5] = _mm_set_epi32(0x7eeacdee,0x6e9032b7,0x8d5335ed,0x2b8a057b);
    ctx->rc[6] = _mm_set_epi32(0x67c28f43,0x5e2e7cd0,0xe2412761,0xda4fef1b);
    ctx->rc[7] = _mm_set_epi32(0x2924d9b0,0xafcacc07,0x675ffde2,0x1fc70b3b);
    ctx->rc[8] = _mm_set_epi32(0xab4d63f1,0xe6867fe9,0xecdb8fca,0xb9d465ee);
    ctx->rc[9] = _mm_set_epi32(0x1c30bf84,0xd4b7cd64,0x5b2a404f,0xad037e33);
    ctx->rc[10] = _mm_set_epi32(0xb2cc0bb9,0x941723bf,0x69028b2e,0x8df69800);
    ctx->rc[11] = _mm_set_epi32(0xfa0478a6,0xde6f5572,0x4aaa9ec8,0x5c9d2d8a);
    ctx->rc[12] = _mm_set_epi32(0xdfb49f2b,0x6b772a12,0x0efa4f2e,0x29129fd4);
    ctx->rc[13] = _mm_set_epi32(0x1ea10344,0xf449a236,0x32d611ae,0xbb6a12ee);
    ctx->rc[14] = _mm_set_epi32(0xaf044988,0x4b050084,0x5f9600c9,0x9ca8eca6);
    ctx->rc[15] = _mm_set_epi32(0x21025ed8,0x9d199c4f,0x78a2c7e3,0x27e593ec);
    ctx->rc[16] = _mm_set_epi32(0xbf3aaaf8,0xa759c9b7,0xb9282ecd,0x82d40173);
}


void phk256_256_load_constants(struct phk256_256_ctx *const ctx) {
    ctx->rc[0] = _mm_set_epi32(0x0684704c,0xe620c00a,0xb2c5fef0,0x75817b9d);
    ctx->rc[1] = _mm_set_epi32(0x8b66b4e1,0x88f3a06b,0x640f6ba4,0x2f08f717);
    ctx->rc[2] = _mm_set_epi32(0x3402de2d,0x53f28498,0xcf029d60,0x9f029114);
    ctx->rc[3] = _mm_set_epi32(0x0ed6eae6,0x2e7b4f08,0xbbf3bcaf,0xfd5b4f79);
    ctx->rc[4] = _mm_set_epi32(0xcbcfb0cb,0x4872448b,0x79eecd1c,0xbe397044);
    ctx->rc[5] = _mm_set_epi32(0x7eeacdee,0x6e9032b7,0x8d5335ed,0x2b8a057b);
    ctx->rc[6] = _mm_set_epi32(0x67c28f43,0x5e2e7cd0,0xe2412761,0xda4fef1b);
    ctx->rc[7] = _mm_set_epi32(0x2924d9b0,0xafcacc07,0x675ffde2,0x1fc70b3b);
    ctx->rc[8] = _mm_set_epi32(0xab4d63f1,0xe6867fe9,0xecdb8fca,0xb9d465ee);
    ctx->rc[9] = _mm_set_epi32(0x1c30bf84,0xd4b7cd64,0x5b2a404f,0xad037e33);
    ctx->rc[10] = _mm_set_epi32(0xb2cc0bb9,0x941723bf,0x69028b2e,0x8df69800);
    ctx->rc[11] = _mm_set_epi32(0xfa0478a6,0xde6f5572,0x4aaa9ec8,0x5c9d2d8a);
    ctx->rc[12] = _mm_set_epi32(0xdfb49f2b,0x6b772a12,0x0efa4f2e,0x29129fd4);
    ctx->rc[13] = _mm_set_epi32(0x1ea10344,0xf449a236,0x32d611ae,0xbb6a12ee);
    ctx->rc[14] = _mm_set_epi32(0xaf044988,0x4b050084,0x5f9600c9,0x9ca8eca6);
    ctx->rc[15] = _mm_set_epi32(0x21025ed8,0x9d199c4f,0x78a2c7e3,0x27e593ec);
    ctx->rc[16] = _mm_set_epi32(0xbf3aaaf8,0xa759c9b7,0xb9282ecd,0x82d40173);
}

#define PHK256_KS_ROUND_X_ENC(a, b, c, d) \
    PHK256_KS_ROUND(ctx->rk[c], ctx->rk[d], ctx->rk[a], ctx->rk[b]) \
    ctx->rk[a] = _mm_xor_si128(ctx->rk[a], ctx->rc[a >> 1]);

void phk256_set_key_encrypt(struct phk256_ctx *const ctx, const uint8_t *const key) {
    __m128i tmp;

    ctx->rt = _mm_setzero_si128();

    ctx->rk[0] = _mm_loadu_si128((__m128i*)key);
    ctx->rk[1] = _mm_loadu_si128((__m128i*)&key[16]);

    PHK256_KS_ROUND_X_ENC(0, 1, 2, 3)
    PHK256_KS_ROUND_X_ENC(2, 3, 4, 5)
    PHK256_KS_ROUND_X_ENC(4, 5, 6, 7)
    PHK256_KS_ROUND_X_ENC(6, 7, 8, 9)
    PHK256_KS_ROUND_X_ENC(8, 9, 10, 11)
    PHK256_KS_ROUND_X_ENC(10, 11, 12, 13)
    PHK256_KS_ROUND_X_ENC(12, 13, 14, 15)
    PHK256_KS_ROUND_X_ENC(14, 15, 16, 17)
    PHK256_KS_ROUND_X_ENC(16, 17, 18, 19)
    PHK256_KS_ROUND_X_ENC(18, 19, 20, 21)
    PHK256_KS_ROUND_X_ENC(20, 21, 22, 23)
    PHK256_KS_ROUND_X_ENC(22, 23, 24, 25)
    PHK256_KS_ROUND_X_ENC(24, 25, 26, 27)
    PHK256_KS_ROUND_X_ENC(26, 27, 28, 29)
    PHK256_KS_ROUND_X_ENC(28, 29, 30, 31)
    PHK256_KS_ROUND_X_ENC(30, 31, 32, 33)

    ctx->rk[32] = _mm_xor_si128(ctx->rk[32], ctx->rc[16]);
}

#define PHK256_KS_ROUND_X_DEC_FIRST(a, b, c, d) \
    PHK256_KS_ROUND(ctx->rk[c], ctx->rk[d], ctx->rk[a], ctx->rk[b]) \
    ctx->rk[a] = _mm_xor_si128(ctx->rk[a], ctx->rc[a >> 1]);

#define PHK256_KS_ROUND_X_DEC(a, b, c, d) \
    PHK256_KS_ROUND(ctx->rk[c], ctx->rk[d], ctx->rk[a], ctx->rk[b]) \
    ctx->rk[a] = _mm_aesimc_si128(_mm_xor_si128(ctx->rk[a], ctx->rc[a >> 1])); \
    ctx->rk[b] = _mm_aesimc_si128(ctx->rk[b]);

void phk256_set_key_decrypt(struct phk256_ctx *const ctx, const uint8_t *const key) {
    __m128i tmp;

    ctx->rt = _mm_setzero_si128();

    ctx->rk[0] = _mm_loadu_si128((__m128i*)key);
    ctx->rk[1] = _mm_loadu_si128((__m128i*)&key[16]);

    PHK256_KS_ROUND_X_DEC_FIRST(0, 1, 2, 3)
    PHK256_KS_ROUND_X_DEC(2, 3, 4, 5)
    PHK256_KS_ROUND_X_DEC(4, 5, 6, 7)
    PHK256_MIX_ENC(ctx->rk[4], ctx->rk[5], ctx->rk[4], ctx->rk[5])
    PHK256_KS_ROUND_X_DEC(6, 7, 8, 9)
    PHK256_KS_ROUND_X_DEC(8, 9, 10, 11)
    PHK256_MIX_ENC(ctx->rk[8], ctx->rk[9], ctx->rk[8], ctx->rk[9])
    PHK256_KS_ROUND_X_DEC(10, 11, 12, 13)
    PHK256_KS_ROUND_X_DEC(12, 13, 14, 15)
    PHK256_MIX_ENC(ctx->rk[12], ctx->rk[13], ctx->rk[12], ctx->rk[13])
    PHK256_KS_ROUND_X_DEC(14, 15, 16, 17)
    PHK256_KS_ROUND_X_DEC(16, 17, 18, 19)
    PHK256_MIX_ENC(ctx->rk[16], ctx->rk[17], ctx->rk[16], ctx->rk[17])
    PHK256_KS_ROUND_X_DEC(18, 19, 20, 21)
    PHK256_KS_ROUND_X_DEC(20, 21, 22, 23)
    PHK256_MIX_ENC(ctx->rk[20], ctx->rk[21], ctx->rk[20], ctx->rk[21])
    PHK256_KS_ROUND_X_DEC(22, 23, 24, 25)
    PHK256_KS_ROUND_X_DEC(24, 25, 26, 27)
    PHK256_MIX_ENC(ctx->rk[24], ctx->rk[25], ctx->rk[24], ctx->rk[25])
    PHK256_KS_ROUND_X_DEC(26, 27, 28, 29)
    PHK256_KS_ROUND_X_DEC(28, 29, 30, 31)
    PHK256_MIX_ENC(ctx->rk[28], ctx->rk[29], ctx->rk[28], ctx->rk[29])
    PHK256_KS_ROUND_X_DEC(30, 31, 32, 33)

    ctx->rk[32] = _mm_xor_si128(ctx->rk[32], ctx->rc[16]);
}


#define PHK256_256_KS_ROUND_X_DEC(a, b, c, d) \
    PHK256_KS_ROUND(ctx->rk[c], ctx->rk[d], ctx->rk[a], ctx->rk[b]) \
    ctx->rk[a] = _mm_xor_si128(ctx->rk[a], ctx->rc[a >> 1]);

void phk256_256_set_key_decrypt(struct phk256_256_ctx *const ctx,
								const uint8_t *const key) {
	__m128i tmp;
	
	ctx->rt[0] = _mm_setzero_si128();
	ctx->rt[1] = _mm_setzero_si128();
	
	ctx->rk[0] = _mm_loadu_si128((__m128i*)key);
	ctx->rk[1] = _mm_loadu_si128((__m128i*)&key[16]);
	
	PHK256_KS_ROUND_X_DEC_FIRST(0, 1, 2, 3)
	PHK256_256_KS_ROUND_X_DEC(2, 3, 4, 5)
	PHK256_256_KS_ROUND_X_DEC(4, 5, 6, 7)
	//PHK256_MIX_ENC(ctx->rk[4], ctx->rk[5], ctx->rk[4], ctx->rk[5])
	PHK256_256_KS_ROUND_X_DEC(6, 7, 8, 9)
	PHK256_256_KS_ROUND_X_DEC(8, 9, 10, 11)
	//PHK256_MIX_ENC(ctx->rk[8], ctx->rk[9], ctx->rk[8], ctx->rk[9])
	PHK256_256_KS_ROUND_X_DEC(10, 11, 12, 13)
	PHK256_256_KS_ROUND_X_DEC(12, 13, 14, 15)
	//PHK256_MIX_ENC(ctx->rk[12], ctx->rk[13], ctx->rk[12], ctx->rk[13])
	PHK256_256_KS_ROUND_X_DEC(14, 15, 16, 17)
	PHK256_256_KS_ROUND_X_DEC(16, 17, 18, 19)
	//PHK256_MIX_ENC(ctx->rk[16], ctx->rk[17], ctx->rk[16], ctx->rk[17])
	PHK256_256_KS_ROUND_X_DEC(18, 19, 20, 21)
	PHK256_256_KS_ROUND_X_DEC(20, 21, 22, 23)
	//PHK256_MIX_ENC(ctx->rk[20], ctx->rk[21], ctx->rk[20], ctx->rk[21])
	PHK256_256_KS_ROUND_X_DEC(22, 23, 24, 25)
	PHK256_256_KS_ROUND_X_DEC(24, 25, 26, 27)
	//PHK256_MIX_ENC(ctx->rk[24], ctx->rk[25], ctx->rk[24], ctx->rk[25])
	PHK256_256_KS_ROUND_X_DEC(26, 27, 28, 29)
	PHK256_256_KS_ROUND_X_DEC(28, 29, 30, 31)
	//PHK256_MIX_ENC(ctx->rk[28], ctx->rk[29], ctx->rk[28], ctx->rk[29])
	PHK256_256_KS_ROUND_X_DEC(30, 31, 32, 33)
	
	ctx->rk[32] = _mm_xor_si128(ctx->rk[32], ctx->rc[16]);
}

void phk256_set_tweak_encrypt(struct phk256_ctx *const ctx, const uint8_t *const tweak) {
    __m128i tmp = ctx->rt;

    ctx->rt = _mm_loadu_si128((__m128i*)tweak);

    tmp = _mm_xor_si128(tmp, ctx->rt);

    PHK256_TWEAK_WRITE(tmp, 0)
    PHK256_TWEAK_WRITE(tmp, 16)
    PHK256_TWEAK_MIX_WRITE(1)
    PHK256_TWEAK_MIX_WRITE(2)
    PHK256_TWEAK_MIX_WRITE(3)
    PHK256_TWEAK_MIX_WRITE(4)
    PHK256_TWEAK_MIX_WRITE(5)
    PHK256_TWEAK_MIX_WRITE(6)
    PHK256_TWEAK_MIX_WRITE(7)
    PHK256_TWEAK_MIX_WRITE(8)
    PHK256_TWEAK_MIX_WRITE(9)
    PHK256_TWEAK_MIX_WRITE(10)
    PHK256_TWEAK_MIX_WRITE(11)
    PHK256_TWEAK_MIX_WRITE(12)
    PHK256_TWEAK_MIX_WRITE(13)
    PHK256_TWEAK_MIX_WRITE(14)
    PHK256_TWEAK_MIX_WRITE(15)
}


void phk256_256_set_key_encrypt(struct phk256_256_ctx *const ctx, const uint8_t *const key) {
    __m128i tmp;

    ctx->rt[0] = _mm_setzero_si128();
    ctx->rt[1] = _mm_setzero_si128();

    ctx->rk[0] = _mm_loadu_si128((__m128i*)key);
    ctx->rk[1] = _mm_loadu_si128((__m128i*)&key[16]);

    PHK256_KS_ROUND_X_ENC(0, 1, 2, 3)
    PHK256_KS_ROUND_X_ENC(2, 3, 4, 5)
    PHK256_KS_ROUND_X_ENC(4, 5, 6, 7)
    PHK256_KS_ROUND_X_ENC(6, 7, 8, 9)
    PHK256_KS_ROUND_X_ENC(8, 9, 10, 11)
    PHK256_KS_ROUND_X_ENC(10, 11, 12, 13)
    PHK256_KS_ROUND_X_ENC(12, 13, 14, 15)
    PHK256_KS_ROUND_X_ENC(14, 15, 16, 17)
    PHK256_KS_ROUND_X_ENC(16, 17, 18, 19)
    PHK256_KS_ROUND_X_ENC(18, 19, 20, 21)
    PHK256_KS_ROUND_X_ENC(20, 21, 22, 23)
    PHK256_KS_ROUND_X_ENC(22, 23, 24, 25)
    PHK256_KS_ROUND_X_ENC(24, 25, 26, 27)
    PHK256_KS_ROUND_X_ENC(26, 27, 28, 29)
    PHK256_KS_ROUND_X_ENC(28, 29, 30, 31)
    PHK256_KS_ROUND_X_ENC(30, 31, 32, 33)

    ctx->rk[32] = _mm_xor_si128(ctx->rk[32], ctx->rc[16]);
}

void phk256_set_256_tweak_encrypt(struct phk256_256_ctx *const ctx, const uint8_t *const tweak) {
    __m128i tmp_256_twk;
    __m128i* tmp = ctx->rt;

    ctx->rt[0] = _mm_loadu_si128((__m128i*)tweak);
    ctx->rt[1] = _mm_loadu_si128((__m128i*)&tweak[16]);

    tmp[0] = ctx->rt[0];
    tmp[1] = ctx->rt[1];

    PHK256_256_TWEAK_WRITE(tmp, 0)
    PHK256_256_TWEAK_WRITE(tmp, 16)
    PHK256_256_TWEAK_MIX_WRITE(tmp, 1);
    PHK256_256_TWEAK_MIX_WRITE(tmp, 2);
    PHK256_256_TWEAK_MIX_WRITE(tmp, 3);
    PHK256_256_TWEAK_MIX_WRITE(tmp, 4);
    PHK256_256_TWEAK_MIX_WRITE(tmp, 5);
    PHK256_256_TWEAK_MIX_WRITE(tmp, 6);
    PHK256_256_TWEAK_MIX_WRITE(tmp, 7);
    PHK256_256_TWEAK_MIX_WRITE(tmp, 8);
    PHK256_256_TWEAK_MIX_WRITE(tmp, 9);
    PHK256_256_TWEAK_MIX_WRITE(tmp, 10);
    PHK256_256_TWEAK_MIX_WRITE(tmp, 11);
    PHK256_256_TWEAK_MIX_WRITE(tmp, 12);
    PHK256_256_TWEAK_MIX_WRITE(tmp, 13);
    PHK256_256_TWEAK_MIX_WRITE(tmp, 14);
    PHK256_256_TWEAK_MIX_WRITE(tmp, 15);
}

// tweak_diff = [15, 14, ..., 0]
void phk256_update_tweak_encrypt(struct phk256_ctx *ctx,\
                                  const uint8_t *const tweak_diff) {
debg_print(11);
// ICh weiß: tweak diff gibt seg fault. aber nicht warum
  ctx->rt = _mm_xor_si128(ctx->rt,_mm_loadu_si128((__m128i*)&tweak_diff));
debg_print(21);
  __m128i 	r0 = _mm_set_epi8(tweak_diff[15], tweak_diff[14], tweak_diff[13], tweak_diff[12],\
						tweak_diff[11], tweak_diff[10], tweak_diff[9], tweak_diff[8],\
						tweak_diff[7], tweak_diff[6], tweak_diff[5], tweak_diff[4],\
						tweak_diff[3], tweak_diff[2], tweak_diff[1], tweak_diff[0]);
	ctx->rk[0] = _mm_xor_si128(ctx->rk[0], r0);
debg_print(31);
	ctx->rk[1] = _mm_xor_si128(ctx->rk[1], r0);

	r0 = _mm_set_epi8(tweak_diff[14], tweak_diff[13], tweak_diff[8], tweak_diff[7],\
						tweak_diff[10], tweak_diff[9], tweak_diff[4], tweak_diff[3],\
						tweak_diff[6], tweak_diff[5], tweak_diff[0], tweak_diff[15],\
						tweak_diff[2], tweak_diff[1], tweak_diff[12], tweak_diff[11]);
	ctx->rk[2] = _mm_xor_si128(ctx->rk[2], r0);
	ctx->rk[3] = _mm_xor_si128(ctx->rk[3], r0);

	r0 = _mm_set_epi8(tweak_diff[13], tweak_diff[8], tweak_diff[3], tweak_diff[6],\
						tweak_diff[9], tweak_diff[4], tweak_diff[15], tweak_diff[2],\
						tweak_diff[5], tweak_diff[0], tweak_diff[11], tweak_diff[14],\
						tweak_diff[1], tweak_diff[12], tweak_diff[7], tweak_diff[10]);
	ctx->rk[4] = _mm_xor_si128(ctx->rk[4], r0);
	ctx->rk[5] = _mm_xor_si128(ctx->rk[5], r0);

	r0 = _mm_set_epi8(tweak_diff[8], tweak_diff[3], tweak_diff[2], tweak_diff[5],\
						tweak_diff[4], tweak_diff[15], tweak_diff[14], tweak_diff[1],\
						tweak_diff[0], tweak_diff[11], tweak_diff[10], tweak_diff[13],\
						tweak_diff[12], tweak_diff[7], tweak_diff[6], tweak_diff[9]);
	ctx->rk[6] = _mm_xor_si128(ctx->rk[6], r0);
	ctx->rk[7] = _mm_xor_si128(ctx->rk[7], r0);

	r0 = _mm_set_epi8(tweak_diff[3], tweak_diff[2], tweak_diff[1], tweak_diff[0],\
						tweak_diff[15], tweak_diff[14], tweak_diff[13], tweak_diff[12],\
						tweak_diff[11], tweak_diff[10], tweak_diff[9], tweak_diff[8],\
						tweak_diff[7], tweak_diff[6], tweak_diff[5], tweak_diff[4]);
	ctx->rk[8] = _mm_xor_si128(ctx->rk[8], r0);
	ctx->rk[9] = _mm_xor_si128(ctx->rk[9], r0);

	r0 = _mm_set_epi8(tweak_diff[2], tweak_diff[1], tweak_diff[12], tweak_diff[11],\
						tweak_diff[14], tweak_diff[13], tweak_diff[8], tweak_diff[7],\
						tweak_diff[10], tweak_diff[9], tweak_diff[4], tweak_diff[3],\
						tweak_diff[6], tweak_diff[5], tweak_diff[0], tweak_diff[15]);
	ctx->rk[10] = _mm_xor_si128(ctx->rk[10], r0);
	ctx->rk[11] = _mm_xor_si128(ctx->rk[11], r0);

	r0 = _mm_set_epi8(tweak_diff[1], tweak_diff[12], tweak_diff[7], tweak_diff[10],\
						tweak_diff[13], tweak_diff[8], tweak_diff[3], tweak_diff[6],\
						tweak_diff[9], tweak_diff[4], tweak_diff[15], tweak_diff[2],\
						tweak_diff[5], tweak_diff[0], tweak_diff[11], tweak_diff[14]);
	ctx->rk[12] = _mm_xor_si128(ctx->rk[12], r0);
	ctx->rk[13] = _mm_xor_si128(ctx->rk[13], r0);

	r0 = _mm_set_epi8(tweak_diff[12], tweak_diff[7], tweak_diff[6], tweak_diff[9],\
						tweak_diff[8], tweak_diff[3], tweak_diff[2], tweak_diff[5],\
						tweak_diff[4], tweak_diff[15], tweak_diff[14], tweak_diff[1],\
						tweak_diff[0], tweak_diff[11], tweak_diff[10], tweak_diff[13]);
	ctx->rk[14] = _mm_xor_si128(ctx->rk[14], r0);
	ctx->rk[15] = _mm_xor_si128(ctx->rk[15], r0);

	r0 = _mm_set_epi8(tweak_diff[7], tweak_diff[6], tweak_diff[5], tweak_diff[4],\
						tweak_diff[3], tweak_diff[2], tweak_diff[1], tweak_diff[0],\
						tweak_diff[15], tweak_diff[14], tweak_diff[13], tweak_diff[12],\
						tweak_diff[11], tweak_diff[10], tweak_diff[9], tweak_diff[8]);
	ctx->rk[16] = _mm_xor_si128(ctx->rk[16], r0);
	ctx->rk[17] = _mm_xor_si128(ctx->rk[17], r0);

	r0 = _mm_set_epi8(tweak_diff[6], tweak_diff[5], tweak_diff[0], tweak_diff[15],\
						tweak_diff[2], tweak_diff[1], tweak_diff[12], tweak_diff[11],\
						tweak_diff[14], tweak_diff[13], tweak_diff[8], tweak_diff[7],\
						tweak_diff[10], tweak_diff[9], tweak_diff[4], tweak_diff[3]);
	ctx->rk[18] = _mm_xor_si128(ctx->rk[18], r0);
	ctx->rk[19] = _mm_xor_si128(ctx->rk[19], r0);

	r0 = _mm_set_epi8(tweak_diff[5], tweak_diff[0], tweak_diff[11], tweak_diff[14],\
						tweak_diff[1], tweak_diff[12], tweak_diff[7], tweak_diff[10],\
						tweak_diff[13], tweak_diff[8], tweak_diff[3], tweak_diff[6],\
						tweak_diff[9], tweak_diff[4], tweak_diff[15], tweak_diff[2]);
	ctx->rk[20] = _mm_xor_si128(ctx->rk[20], r0);
	ctx->rk[21] = _mm_xor_si128(ctx->rk[21], r0);

	r0 = _mm_set_epi8(tweak_diff[0], tweak_diff[11], tweak_diff[10], tweak_diff[13],\
						tweak_diff[12], tweak_diff[7], tweak_diff[6], tweak_diff[9],\
						tweak_diff[8], tweak_diff[3], tweak_diff[2], tweak_diff[5],\
						tweak_diff[4], tweak_diff[15], tweak_diff[14], tweak_diff[1]);
	ctx->rk[22] = _mm_xor_si128(ctx->rk[22], r0);
	ctx->rk[23] = _mm_xor_si128(ctx->rk[23], r0);

	r0 = _mm_set_epi8(tweak_diff[11], tweak_diff[10], tweak_diff[9], tweak_diff[8],\
						tweak_diff[7], tweak_diff[6], tweak_diff[5], tweak_diff[4],\
						tweak_diff[3], tweak_diff[2], tweak_diff[1], tweak_diff[0],\
						tweak_diff[15], tweak_diff[14], tweak_diff[13], tweak_diff[12]);
	ctx->rk[24] = _mm_xor_si128(ctx->rk[24], r0);
	ctx->rk[25] = _mm_xor_si128(ctx->rk[25], r0);

	r0 = _mm_set_epi8(tweak_diff[10], tweak_diff[9], tweak_diff[4], tweak_diff[3],\
						tweak_diff[6], tweak_diff[5], tweak_diff[0], tweak_diff[15],\
						tweak_diff[2], tweak_diff[1], tweak_diff[12], tweak_diff[11],\
						tweak_diff[14], tweak_diff[13], tweak_diff[8], tweak_diff[7]);
	ctx->rk[26] = _mm_xor_si128(ctx->rk[26], r0);
	ctx->rk[27] = _mm_xor_si128(ctx->rk[27], r0);

	r0 = _mm_set_epi8(tweak_diff[9], tweak_diff[4], tweak_diff[15], tweak_diff[2],\
						tweak_diff[5], tweak_diff[0], tweak_diff[11], tweak_diff[14],\
						tweak_diff[1], tweak_diff[12], tweak_diff[7], tweak_diff[10],\
						tweak_diff[13], tweak_diff[8], tweak_diff[3], tweak_diff[6]);
	ctx->rk[28] = _mm_xor_si128(ctx->rk[28], r0);
	ctx->rk[29] = _mm_xor_si128(ctx->rk[29], r0);

	r0 = _mm_set_epi8(tweak_diff[4], tweak_diff[15], tweak_diff[14], tweak_diff[1],\
						tweak_diff[0], tweak_diff[11], tweak_diff[10], tweak_diff[13],\
						tweak_diff[12], tweak_diff[7], tweak_diff[6], tweak_diff[9],\
						tweak_diff[8], tweak_diff[3], tweak_diff[2], tweak_diff[5]);
	ctx->rk[30] = _mm_xor_si128(ctx->rk[30], r0);
	ctx->rk[31] = _mm_xor_si128(ctx->rk[31], r0);

debg_print(12);
	r0 = _mm_set_epi8(tweak_diff[15], tweak_diff[14], tweak_diff[13], tweak_diff[12],\
						tweak_diff[11], tweak_diff[10], tweak_diff[9], tweak_diff[8],\
						tweak_diff[7], tweak_diff[6], tweak_diff[5], tweak_diff[4],\
						tweak_diff[3], tweak_diff[2], tweak_diff[1], tweak_diff[0]);
	ctx->rk[32] = _mm_xor_si128(ctx->rk[32], r0);
	ctx->rk[33] = _mm_xor_si128(ctx->rk[33], r0);

}

// ---------------------------------------------------------

void phk256_256_update_tweak_encrypt(struct phk256_256_ctx *ctx,\
                                     const uint8_t *const tweak_diff) {
	// TODO
}

// ---------------------------------------------------------

// tweak_diff = [15, 14, ..., 0]
void phk256_update_4tweak_encrypt(struct phk256_ctx *ctx1, __m128i r1,\
                          struct phk256_ctx *ctx2, __m128i r2,\
                          struct phk256_ctx *ctx3, __m128i r3,\
                          struct phk256_ctx *ctx4, __m128i r4) {
  ctx1->rt = _mm_xor_si128(ctx1->rt, r1);
  ctx2->rt = _mm_xor_si128(ctx2->rt, r2);
  ctx3->rt = _mm_xor_si128(ctx3->rt, r3);
  ctx4->rt = _mm_xor_si128(ctx4->rt, r4);


	ctx1->rk[0] = _mm_xor_si128(ctx1->rk[0], r1);
	ctx1->rk[1] = _mm_xor_si128(ctx1->rk[1], r1);
	ctx2->rk[0] = _mm_xor_si128(ctx2->rk[0], r2);
	ctx2->rk[1] = _mm_xor_si128(ctx2->rk[1], r2);
	ctx3->rk[0] = _mm_xor_si128(ctx3->rk[0], r3);
	ctx3->rk[1] = _mm_xor_si128(ctx3->rk[1], r3);
	ctx4->rk[0] = _mm_xor_si128(ctx4->rk[0], r4);
	ctx4->rk[1] = _mm_xor_si128(ctx4->rk[1], r4);


	PHK_TS_MIX(r1)
	PHK_TS_MIX(r2)
	PHK_TS_MIX(r3)
	PHK_TS_MIX(r4)
	ctx1->rk[2] = _mm_xor_si128(ctx1->rk[2], r1);
	ctx1->rk[3] = _mm_xor_si128(ctx1->rk[3], r1);
	ctx2->rk[2] = _mm_xor_si128(ctx2->rk[2], r2);
	ctx2->rk[3] = _mm_xor_si128(ctx2->rk[3], r2);
	ctx3->rk[2] = _mm_xor_si128(ctx3->rk[2], r3);
	ctx3->rk[3] = _mm_xor_si128(ctx3->rk[3], r3);
	ctx4->rk[2] = _mm_xor_si128(ctx4->rk[2], r4);
	ctx4->rk[3] = _mm_xor_si128(ctx4->rk[3], r4);


	PHK_TS_MIX(r1)
	PHK_TS_MIX(r2)
	PHK_TS_MIX(r3)
	PHK_TS_MIX(r4)
	ctx1->rk[4] = _mm_xor_si128(ctx1->rk[4], r1);
	ctx1->rk[5] = _mm_xor_si128(ctx1->rk[5], r1);
	ctx2->rk[4] = _mm_xor_si128(ctx2->rk[4], r2);
	ctx2->rk[5] = _mm_xor_si128(ctx2->rk[5], r2);
	ctx3->rk[4] = _mm_xor_si128(ctx3->rk[4], r3);
	ctx3->rk[5] = _mm_xor_si128(ctx3->rk[5], r3);
	ctx4->rk[4] = _mm_xor_si128(ctx4->rk[4], r4);
	ctx4->rk[5] = _mm_xor_si128(ctx4->rk[5], r4);


	PHK_TS_MIX(r1)
	PHK_TS_MIX(r2)
	PHK_TS_MIX(r3)
	PHK_TS_MIX(r4)
	ctx1->rk[6] = _mm_xor_si128(ctx1->rk[6], r1);
	ctx1->rk[7] = _mm_xor_si128(ctx1->rk[7], r1);
	ctx2->rk[6] = _mm_xor_si128(ctx2->rk[6], r2);
	ctx2->rk[7] = _mm_xor_si128(ctx2->rk[7], r2);
	ctx3->rk[6] = _mm_xor_si128(ctx3->rk[6], r3);
	ctx3->rk[7] = _mm_xor_si128(ctx3->rk[7], r3);
	ctx4->rk[6] = _mm_xor_si128(ctx4->rk[6], r4);
	ctx4->rk[7] = _mm_xor_si128(ctx4->rk[7], r4);


	PHK_TS_MIX(r1)
	PHK_TS_MIX(r2)
	PHK_TS_MIX(r3)
	PHK_TS_MIX(r4)
	ctx1->rk[8] = _mm_xor_si128(ctx1->rk[8], r1);
	ctx1->rk[9] = _mm_xor_si128(ctx1->rk[9], r1);
	ctx2->rk[8] = _mm_xor_si128(ctx2->rk[8], r2);
	ctx2->rk[9] = _mm_xor_si128(ctx2->rk[9], r2);
	ctx3->rk[8] = _mm_xor_si128(ctx3->rk[8], r3);
	ctx3->rk[9] = _mm_xor_si128(ctx3->rk[9], r3);
	ctx4->rk[8] = _mm_xor_si128(ctx4->rk[8], r4);
	ctx4->rk[9] = _mm_xor_si128(ctx4->rk[9], r4);


	PHK_TS_MIX(r1)
	PHK_TS_MIX(r2)
	PHK_TS_MIX(r3)
	PHK_TS_MIX(r4)
	ctx1->rk[10] = _mm_xor_si128(ctx1->rk[10], r1);
	ctx1->rk[11] = _mm_xor_si128(ctx1->rk[11], r1);
	ctx2->rk[10] = _mm_xor_si128(ctx2->rk[10], r2);
	ctx2->rk[11] = _mm_xor_si128(ctx2->rk[11], r2);
	ctx3->rk[10] = _mm_xor_si128(ctx3->rk[10], r3);
	ctx3->rk[11] = _mm_xor_si128(ctx3->rk[11], r3);
	ctx4->rk[10] = _mm_xor_si128(ctx4->rk[10], r4);
	ctx4->rk[11] = _mm_xor_si128(ctx4->rk[11], r4);


	PHK_TS_MIX(r1)
	PHK_TS_MIX(r2)
	PHK_TS_MIX(r3)
	PHK_TS_MIX(r4)
	ctx1->rk[12] = _mm_xor_si128(ctx1->rk[12], r1);
	ctx1->rk[13] = _mm_xor_si128(ctx1->rk[13], r1);
	ctx2->rk[12] = _mm_xor_si128(ctx2->rk[12], r2);
	ctx2->rk[13] = _mm_xor_si128(ctx2->rk[13], r2);
	ctx3->rk[12] = _mm_xor_si128(ctx3->rk[12], r3);
	ctx3->rk[13] = _mm_xor_si128(ctx3->rk[13], r3);
	ctx4->rk[12] = _mm_xor_si128(ctx4->rk[12], r4);
	ctx4->rk[13] = _mm_xor_si128(ctx4->rk[13], r4);


	PHK_TS_MIX(r1)
	PHK_TS_MIX(r2)
	PHK_TS_MIX(r3)
	PHK_TS_MIX(r4)
	ctx1->rk[14] = _mm_xor_si128(ctx1->rk[14], r1);
	ctx1->rk[15] = _mm_xor_si128(ctx1->rk[15], r1);
	ctx2->rk[14] = _mm_xor_si128(ctx2->rk[14], r2);
	ctx2->rk[15] = _mm_xor_si128(ctx2->rk[15], r2);
	ctx3->rk[14] = _mm_xor_si128(ctx3->rk[14], r3);
	ctx3->rk[15] = _mm_xor_si128(ctx3->rk[15], r3);
	ctx4->rk[14] = _mm_xor_si128(ctx4->rk[14], r4);
	ctx4->rk[15] = _mm_xor_si128(ctx4->rk[15], r4);


	PHK_TS_MIX(r1)
	PHK_TS_MIX(r2)
	PHK_TS_MIX(r3)
	PHK_TS_MIX(r4)
	ctx1->rk[16] = _mm_xor_si128(ctx1->rk[16], r1);
	ctx1->rk[17] = _mm_xor_si128(ctx1->rk[17], r1);
	ctx2->rk[16] = _mm_xor_si128(ctx2->rk[16], r2);
	ctx2->rk[17] = _mm_xor_si128(ctx2->rk[17], r2);
	ctx3->rk[16] = _mm_xor_si128(ctx3->rk[16], r3);
	ctx3->rk[17] = _mm_xor_si128(ctx3->rk[17], r3);
	ctx4->rk[16] = _mm_xor_si128(ctx4->rk[16], r4);
	ctx4->rk[17] = _mm_xor_si128(ctx4->rk[17], r4);


	PHK_TS_MIX(r1)
	PHK_TS_MIX(r2)
	PHK_TS_MIX(r3)
	PHK_TS_MIX(r4)
	ctx1->rk[18] = _mm_xor_si128(ctx1->rk[18], r1);
	ctx1->rk[19] = _mm_xor_si128(ctx1->rk[19], r1);
	ctx2->rk[18] = _mm_xor_si128(ctx2->rk[18], r2);
	ctx2->rk[19] = _mm_xor_si128(ctx2->rk[19], r2);
	ctx3->rk[18] = _mm_xor_si128(ctx3->rk[18], r3);
	ctx3->rk[19] = _mm_xor_si128(ctx3->rk[19], r3);
	ctx4->rk[18] = _mm_xor_si128(ctx4->rk[18], r4);
	ctx4->rk[19] = _mm_xor_si128(ctx4->rk[19], r4);


	PHK_TS_MIX(r1)
	PHK_TS_MIX(r2)
	PHK_TS_MIX(r3)
	PHK_TS_MIX(r4)
	ctx1->rk[20] = _mm_xor_si128(ctx1->rk[20], r1);
	ctx1->rk[21] = _mm_xor_si128(ctx1->rk[21], r1);
	ctx2->rk[20] = _mm_xor_si128(ctx2->rk[20], r2);
	ctx2->rk[21] = _mm_xor_si128(ctx2->rk[21], r2);
	ctx3->rk[20] = _mm_xor_si128(ctx3->rk[20], r3);
	ctx3->rk[21] = _mm_xor_si128(ctx3->rk[21], r3);
	ctx4->rk[20] = _mm_xor_si128(ctx4->rk[20], r4);
	ctx4->rk[21] = _mm_xor_si128(ctx4->rk[21], r4);


	PHK_TS_MIX(r1)
	PHK_TS_MIX(r2)
	PHK_TS_MIX(r3)
	PHK_TS_MIX(r4)
	ctx1->rk[22] = _mm_xor_si128(ctx1->rk[22], r1);
	ctx1->rk[23] = _mm_xor_si128(ctx1->rk[23], r1);
	ctx2->rk[22] = _mm_xor_si128(ctx2->rk[22], r2);
	ctx2->rk[23] = _mm_xor_si128(ctx2->rk[23], r2);
	ctx3->rk[22] = _mm_xor_si128(ctx3->rk[22], r3);
	ctx3->rk[23] = _mm_xor_si128(ctx3->rk[23], r3);
	ctx4->rk[22] = _mm_xor_si128(ctx4->rk[22], r4);
	ctx4->rk[23] = _mm_xor_si128(ctx4->rk[23], r4);


	PHK_TS_MIX(r1)
	PHK_TS_MIX(r2)
	PHK_TS_MIX(r3)
	PHK_TS_MIX(r4)
	ctx1->rk[24] = _mm_xor_si128(ctx1->rk[24], r1);
	ctx1->rk[25] = _mm_xor_si128(ctx1->rk[25], r1);
	ctx2->rk[24] = _mm_xor_si128(ctx2->rk[24], r2);
	ctx2->rk[25] = _mm_xor_si128(ctx2->rk[25], r2);
	ctx3->rk[24] = _mm_xor_si128(ctx3->rk[24], r3);
	ctx3->rk[25] = _mm_xor_si128(ctx3->rk[25], r3);
	ctx4->rk[24] = _mm_xor_si128(ctx4->rk[24], r4);
	ctx4->rk[25] = _mm_xor_si128(ctx4->rk[25], r4);


	PHK_TS_MIX(r1)
	PHK_TS_MIX(r2)
	PHK_TS_MIX(r3)
	PHK_TS_MIX(r4)
	ctx1->rk[26] = _mm_xor_si128(ctx1->rk[26], r1);
	ctx1->rk[27] = _mm_xor_si128(ctx1->rk[27], r1);
	ctx2->rk[26] = _mm_xor_si128(ctx2->rk[26], r2);
	ctx2->rk[27] = _mm_xor_si128(ctx2->rk[27], r2);
	ctx3->rk[26] = _mm_xor_si128(ctx3->rk[26], r3);
	ctx3->rk[27] = _mm_xor_si128(ctx3->rk[27], r3);
	ctx4->rk[26] = _mm_xor_si128(ctx4->rk[26], r4);
	ctx4->rk[27] = _mm_xor_si128(ctx4->rk[27], r4);


	PHK_TS_MIX(r1)
	PHK_TS_MIX(r2)
	PHK_TS_MIX(r3)
	PHK_TS_MIX(r4)
	ctx1->rk[28] = _mm_xor_si128(ctx1->rk[28], r1);
	ctx1->rk[29] = _mm_xor_si128(ctx1->rk[29], r1);
	ctx2->rk[28] = _mm_xor_si128(ctx2->rk[28], r2);
	ctx2->rk[29] = _mm_xor_si128(ctx2->rk[29], r2);
	ctx3->rk[28] = _mm_xor_si128(ctx3->rk[28], r3);
	ctx3->rk[29] = _mm_xor_si128(ctx3->rk[29], r3);
	ctx4->rk[28] = _mm_xor_si128(ctx4->rk[28], r4);
	ctx4->rk[29] = _mm_xor_si128(ctx4->rk[29], r4);


	PHK_TS_MIX(r1)
	PHK_TS_MIX(r2)
	PHK_TS_MIX(r3)
	PHK_TS_MIX(r4)
	ctx1->rk[30] = _mm_xor_si128(ctx1->rk[30], r1);
	ctx1->rk[31] = _mm_xor_si128(ctx1->rk[31], r1);
	ctx2->rk[30] = _mm_xor_si128(ctx2->rk[30], r2);
	ctx2->rk[31] = _mm_xor_si128(ctx2->rk[31], r2);
	ctx3->rk[30] = _mm_xor_si128(ctx3->rk[30], r3);
	ctx3->rk[31] = _mm_xor_si128(ctx3->rk[31], r3);
	ctx4->rk[30] = _mm_xor_si128(ctx4->rk[30], r4);
	ctx4->rk[31] = _mm_xor_si128(ctx4->rk[31], r4);


	PHK_TS_MIX(r1)
	PHK_TS_MIX(r2)
	PHK_TS_MIX(r3)
	PHK_TS_MIX(r4)
	ctx1->rk[32] = _mm_xor_si128(ctx1->rk[32], r1);
	ctx1->rk[33] = _mm_xor_si128(ctx1->rk[33], r1);
	ctx2->rk[32] = _mm_xor_si128(ctx2->rk[32], r2);
	ctx2->rk[33] = _mm_xor_si128(ctx2->rk[33], r2);
	ctx3->rk[32] = _mm_xor_si128(ctx3->rk[32], r3);
	ctx3->rk[33] = _mm_xor_si128(ctx3->rk[33], r3);
	ctx4->rk[32] = _mm_xor_si128(ctx4->rk[32], r4);
	ctx4->rk[33] = _mm_xor_si128(ctx4->rk[33], r4);

}

//void phk256_update_tweak_encrypt(struct phk256_ctx *ctx,\
//                                  const uint8_t tweak_diff) {
//	__m128i r0 = _mm_set_epi8(tweak_diff, 0, 0, 0, 0, 0, 0, 0,\
//								0, 0, 0, 0, 0, 0, 0, 0);
//	ctx->rk[0] = _mm_xor_si128(ctx->rk[0], r0);
//	ctx->rk[1] = _mm_xor_si128(ctx->rk[1], r0);
//
//	r0 = _mm_set_epi8(0, 0, 0, 0, 0, 0, 0, 0,\
//								0, 0, 0, tweak_diff, 0, 0, 0, 0);
//	ctx->rk[2] = _mm_xor_si128(ctx->rk[2], r0);
//	ctx->rk[3] = _mm_xor_si128(ctx->rk[3], r0);
//
//	r0 = _mm_set_epi8(0, 0, 0, 0, 0, 0, tweak_diff, 0,\
//								0, 0, 0, 0, 0, 0, 0, 0);
//	ctx->rk[4] = _mm_xor_si128(ctx->rk[4], r0);
//	ctx->rk[5] = _mm_xor_si128(ctx->rk[5], r0);
//
//	r0 = _mm_set_epi8(0, 0, 0, 0, 0, tweak_diff, 0, 0,\
//								0, 0, 0, 0, 0, 0, 0, 0);
//	ctx->rk[6] = _mm_xor_si128(ctx->rk[6], r0);
//	ctx->rk[7] = _mm_xor_si128(ctx->rk[7], r0);
//
//	r0 = _mm_set_epi8(0, 0, 0, 0, tweak_diff, 0, 0, 0,\
//								0, 0, 0, 0, 0, 0, 0, 0);
//	ctx->rk[8] = _mm_xor_si128(ctx->rk[8], r0);
//	ctx->rk[9] = _mm_xor_si128(ctx->rk[9], r0);
//
//	r0 = _mm_set_epi8(0, 0, 0, 0, 0, 0, 0, 0,\
//								0, 0, 0, 0, 0, 0, 0, tweak_diff);
//	ctx->rk[10] = _mm_xor_si128(ctx->rk[10], r0);
//	ctx->rk[11] = _mm_xor_si128(ctx->rk[11], r0);
//
//	r0 = _mm_set_epi8(0, 0, 0, 0, 0, 0, 0, 0,\
//								0, 0, tweak_diff, 0, 0, 0, 0, 0);
//	ctx->rk[12] = _mm_xor_si128(ctx->rk[12], r0);
//	ctx->rk[13] = _mm_xor_si128(ctx->rk[13], r0);
//
//	r0 = _mm_set_epi8(0, 0, 0, 0, 0, 0, 0, 0,\
//								0, tweak_diff, 0, 0, 0, 0, 0, 0);
//	ctx->rk[14] = _mm_xor_si128(ctx->rk[14], r0);
//	ctx->rk[15] = _mm_xor_si128(ctx->rk[15], r0);
//
//	r0 = _mm_set_epi8(0, 0, 0, 0, 0, 0, 0, 0,\
//								tweak_diff, 0, 0, 0, 0, 0, 0, 0);
//	ctx->rk[16] = _mm_xor_si128(ctx->rk[16], r0);
//	ctx->rk[17] = _mm_xor_si128(ctx->rk[17], r0);
//
//	r0 = _mm_set_epi8(0, 0, 0, tweak_diff, 0, 0, 0, 0,\
//								0, 0, 0, 0, 0, 0, 0, 0);
//	ctx->rk[18] = _mm_xor_si128(ctx->rk[18], r0);
//	ctx->rk[19] = _mm_xor_si128(ctx->rk[19], r0);
//
//	r0 = _mm_set_epi8(0, 0, 0, 0, 0, 0, 0, 0,\
//								0, 0, 0, 0, 0, 0, tweak_diff, 0);
//	ctx->rk[20] = _mm_xor_si128(ctx->rk[20], r0);
//	ctx->rk[21] = _mm_xor_si128(ctx->rk[21], r0);
//
//	r0 = _mm_set_epi8(0, 0, 0, 0, 0, 0, 0, 0,\
//								0, 0, 0, 0, 0, tweak_diff, 0, 0);
//	ctx->rk[22] = _mm_xor_si128(ctx->rk[22], r0);
//	ctx->rk[23] = _mm_xor_si128(ctx->rk[23], r0);
//
//	r0 = _mm_set_epi8(0, 0, 0, 0, 0, 0, 0, 0,\
//								0, 0, 0, 0, tweak_diff, 0, 0, 0);
//	ctx->rk[24] = _mm_xor_si128(ctx->rk[24], r0);
//	ctx->rk[25] = _mm_xor_si128(ctx->rk[25], r0);
//
//	r0 = _mm_set_epi8(0, 0, 0, 0, 0, 0, 0, tweak_diff,\
//								0, 0, 0, 0, 0, 0, 0, 0);
//	ctx->rk[26] = _mm_xor_si128(ctx->rk[26], r0);
//	ctx->rk[27] = _mm_xor_si128(ctx->rk[27], r0);
//
//	r0 = _mm_set_epi8(0, 0, tweak_diff, 0, 0, 0, 0, 0,\
//								0, 0, 0, 0, 0, 0, 0, 0);
//	ctx->rk[28] = _mm_xor_si128(ctx->rk[28], r0);
//	ctx->rk[29] = _mm_xor_si128(ctx->rk[29], r0);
//
//	r0 = _mm_set_epi8(0, tweak_diff, 0, 0, 0, 0, 0, 0,\
//								0, 0, 0, 0, 0, 0, 0, 0);
//	ctx->rk[30] = _mm_xor_si128(ctx->rk[30], r0);
//	ctx->rk[31] = _mm_xor_si128(ctx->rk[31], r0);
//
//	r0 = _mm_set_epi8(tweak_diff, 0, 0, 0, 0, 0, 0, 0,\
//								0, 0, 0, 0, 0, 0, 0, 0);
//	ctx->rk[32] = _mm_xor_si128(ctx->rk[32], r0);
//	ctx->rk[33] = _mm_xor_si128(ctx->rk[33], r0);
//}

void phk256_set_tweak_decrypt(struct phk256_ctx *const ctx, const uint8_t *const tweak) {
    __m128i tmp0 = ctx->rt;
    __m128i tmp1;

    ctx->rt = _mm_loadu_si128((__m128i*)tweak);

    tmp0 = _mm_xor_si128(tmp0, ctx->rt);

    PHK256_TWEAK_WRITE(tmp0, 0)
    PHK256_TWEAK_WRITE(tmp0, 16)
    PHK256_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 1)
    PHK256_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 2)
    PHK256_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 3)
    PHK256_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 4)
    PHK256_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 5)
    PHK256_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 6)
    PHK256_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 7)
    PHK256_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 8)
    PHK256_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 9)
    PHK256_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 10)
    PHK256_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 11)
    PHK256_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 12)
    PHK256_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 13)
    PHK256_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 14)
    PHK256_TWEAK_MIX_IMC_WRITE(tmp1, tmp0, 15)
}

void phk256_set_256_tweak_decrypt(struct phk256_256_ctx *const ctx,
							      const uint8_t *const tweak) {
    __m128i tmp_256_twk;
    __m128i tmp[2];

    ctx->rt[0] = _mm_loadu_si128((__m128i*)tweak);
    ctx->rt[1] = _mm_loadu_si128((__m128i*)&tweak[16]);

    tmp[0] = ctx->rt[0];
    tmp[1] = ctx->rt[1];

    PHK256_256_TWEAK_WRITE(tmp, 0)
    PHK256_256_TWEAK_WRITE(tmp, 16)
    PHK256_256_TWEAK_MIX_IMC_WRITE(tmp, 1)
    PHK256_256_TWEAK_MIX_IMC_WRITE(tmp, 2)
    PHK256_256_TWEAK_MIX_IMC_WRITE(tmp, 3)
    PHK256_256_TWEAK_MIX_IMC_WRITE(tmp, 4)
    PHK256_256_TWEAK_MIX_IMC_WRITE(tmp, 5)
    PHK256_256_TWEAK_MIX_IMC_WRITE(tmp, 6)
    PHK256_256_TWEAK_MIX_IMC_WRITE(tmp, 7)
    PHK256_256_TWEAK_MIX_IMC_WRITE(tmp, 8)
    PHK256_256_TWEAK_MIX_IMC_WRITE(tmp, 9)
    PHK256_256_TWEAK_MIX_IMC_WRITE(tmp, 10)
    PHK256_256_TWEAK_MIX_IMC_WRITE(tmp, 11)
    PHK256_256_TWEAK_MIX_IMC_WRITE(tmp, 12)
    PHK256_256_TWEAK_MIX_IMC_WRITE(tmp, 13)
    PHK256_256_TWEAK_MIX_IMC_WRITE(tmp, 14)
    PHK256_256_TWEAK_MIX_IMC_WRITE(tmp, 15)

    for(int i = 2; i < 32; i++) {
      ctx->rk[i] = _mm_aesimc_si128(ctx->rk[i]);
    }

    for(int i = 4; i <= 28; i += 4) {
      tmp_256_twk = _mm_blend_epi32(ctx->rk[i], ctx->rk[i+1], 0b0101); \
      ctx->rk[i] = _mm_blend_epi32(ctx->rk[i], ctx->rk[i+1], 0b1010); \
      ctx->rk[i+1] = tmp_256_twk;
    }
    //PHK256_MIX_ENC(ctx>rk[4], ctx->rk[5], ctx->rk[4], ctx->rk[5])
    //PHK256_MIX_ENC(ctx->rk[8], ctx->rk[9], ctx->rk[8], ctx->rk[9])
    //PHK256_MIX_ENC(ctx->rk[12], ctx->rk[13], ctx->rk[12], ctx->rk[13])
    //PHK256_MIX_ENC(ctx->rk[16], ctx->rk[17], ctx->rk[16], ctx->rk[17])
    //PHK256_MIX_ENC(ctx->rk[20], ctx->rk[21], ctx->rk[20], ctx->rk[21])
    //PHK256_MIX_ENC(ctx->rk[24], ctx->rk[25], ctx->rk[24], ctx->rk[25])
    //PHK256_MIX_ENC(ctx->rk[28], ctx->rk[29], ctx->rk[28], ctx->rk[29])
    
}

#define PHK256_ENC_STEP_X(a, b, c, d) \
    PHK256_STEP_ENC(state[0], state[1], ctx->rk[a], ctx->rk[b], ctx->rk[c], ctx->rk[d])

void phk256_encrypt(const struct phk256_ctx * const ctx, uint8_t * const buf) {
    __m128i state[PHK256_SUBSTATES], tmp;

    // init plaintext and add initial round key
    state[0] = _mm_loadu_si128((__m128i*)buf);
    state[1] = _mm_loadu_si128((__m128i*)&buf[16]);

    state[0] = _mm_xor_si128(state[0], ctx->rk[0]);
    state[1] = _mm_xor_si128(state[1], ctx->rk[1]);

    PHK256_ENC_STEP_X(2, 3, 4, 5)
    PHK256_ENC_STEP_X(6, 7, 8, 9)
    PHK256_ENC_STEP_X(10, 11, 12, 13)
    PHK256_ENC_STEP_X(14, 15, 16, 17)
    PHK256_ENC_STEP_X(18, 19, 20, 21)
    PHK256_ENC_STEP_X(22, 23, 24, 25)
    PHK256_ENC_STEP_X(26, 27, 28, 29)
    PHK256_STEP_ENC_LAST(state[0], state[1], ctx->rk[30], ctx->rk[31], ctx->rk[32], ctx->rk[33])

    _mm_storeu_si128((__m128i*)buf, state[0]);
    _mm_storeu_si128((__m128i*)&buf[16], state[1]);
}

void phk256_256_encrypt(const struct phk256_256_ctx * const ctx, uint8_t * const buf) {
    __m128i state[PHK256_SUBSTATES], tmp;

    // init plaintext and add initial round key
    state[0] = _mm_loadu_si128((__m128i*)buf);
    state[1] = _mm_loadu_si128((__m128i*)&buf[16]);

    state[0] = _mm_xor_si128(state[0], ctx->rk[0]);
    state[1] = _mm_xor_si128(state[1], ctx->rk[1]);

    PHK256_ENC_STEP_X(2, 3, 4, 5)
    PHK256_ENC_STEP_X(6, 7, 8, 9)
    PHK256_ENC_STEP_X(10, 11, 12, 13)
    PHK256_ENC_STEP_X(14, 15, 16, 17)
    PHK256_ENC_STEP_X(18, 19, 20, 21)
    PHK256_ENC_STEP_X(22, 23, 24, 25)
    PHK256_ENC_STEP_X(26, 27, 28, 29)
    PHK256_STEP_ENC_LAST(state[0], state[1], ctx->rk[30], ctx->rk[31], ctx->rk[32], ctx->rk[33])

    _mm_storeu_si128((__m128i*)buf, state[0]);
    _mm_storeu_si128((__m128i*)&buf[16], state[1]);
}

#define PHK256X2_ENC_STEP_X(a, b, c, d) \
    PHK256X2_STEP_ENC(state[0], state[1], state[2], state[3], \
            ctx->rk[a], ctx->rk[b], ctx->rk[c], ctx->rk[d])

void phk256x2_encrypt(const struct phk256_ctx * const ctx, uint8_t * const buf) {
    __m128i state[PHK256_SUBSTATES << 1], tmp;

    // init plaintext and add initial round key
    state[0] = _mm_loadu_si128((__m128i*)buf);
    state[1] = _mm_loadu_si128((__m128i*)&buf[16]);
    state[2] = _mm_loadu_si128((__m128i*)&buf[32]);
    state[3] = _mm_loadu_si128((__m128i*)&buf[48]);

    state[0] = _mm_xor_si128(state[0], ctx->rk[0]);
    state[1] = _mm_xor_si128(state[1], ctx->rk[1]);
    state[2] = _mm_xor_si128(state[2], ctx->rk[0]);
    state[3] = _mm_xor_si128(state[3], ctx->rk[1]);

    PHK256X2_ENC_STEP_X(2, 3, 4, 5)
    PHK256X2_ENC_STEP_X(6, 7, 8, 9)
    PHK256X2_ENC_STEP_X(10, 11, 12, 13)
    PHK256X2_ENC_STEP_X(14, 15, 16, 17)
    PHK256X2_ENC_STEP_X(18, 19, 20, 21)
    PHK256X2_ENC_STEP_X(22, 23, 24, 25)
    PHK256X2_ENC_STEP_X(26, 27, 28, 29)
    PHK256X2_STEP_ENC_LAST(state[0], state[1], state[2], state[3],
                           ctx->rk[30], ctx->rk[31], ctx->rk[32], ctx->rk[33])

    _mm_storeu_si128((__m128i*)buf, state[0]);
    _mm_storeu_si128((__m128i*)&buf[16], state[1]);
    _mm_storeu_si128((__m128i*)&buf[32], state[2]);
    _mm_storeu_si128((__m128i*)&buf[48], state[3]);
}

#define PHK256X4_ENC_STEP_X(a, b, c, d) \
    PHK256X4_STEP_ENC(state[0], state[1], state[2], state[3], \
            state[4], state[5], state[6], state[7], \
            ctx->rk[a], ctx->rk[b], ctx->rk[c], ctx->rk[d])

void phk256x4_encrypt(const struct phk256_ctx * const ctx, uint8_t * const buf) {
    __m128i state[PHK256_SUBSTATES << 2], tmp;

    // init plaintext and add initial round key
    state[0] = _mm_loadu_si128((__m128i*)buf);
    state[1] = _mm_loadu_si128((__m128i*)&buf[16]);
    state[2] = _mm_loadu_si128((__m128i*)&buf[32]);
    state[3] = _mm_loadu_si128((__m128i*)&buf[48]);
    state[4] = _mm_loadu_si128((__m128i*)&buf[64]);
    state[5] = _mm_loadu_si128((__m128i*)&buf[80]);
    state[6] = _mm_loadu_si128((__m128i*)&buf[96]);
    state[7] = _mm_loadu_si128((__m128i*)&buf[112]);

    state[0] = _mm_xor_si128(state[0], ctx->rk[0]);
    state[1] = _mm_xor_si128(state[1], ctx->rk[1]);
    state[2] = _mm_xor_si128(state[2], ctx->rk[0]);
    state[3] = _mm_xor_si128(state[3], ctx->rk[1]);
    state[4] = _mm_xor_si128(state[4], ctx->rk[0]);
    state[5] = _mm_xor_si128(state[5], ctx->rk[1]);
    state[6] = _mm_xor_si128(state[6], ctx->rk[0]);
    state[7] = _mm_xor_si128(state[7], ctx->rk[1]);

    PHK256X4_ENC_STEP_X(2, 3, 4, 5)
    PHK256X4_ENC_STEP_X(6, 7, 8, 9)
    PHK256X4_ENC_STEP_X(10, 11, 12, 13)
    PHK256X4_ENC_STEP_X(14, 15, 16, 17)
    PHK256X4_ENC_STEP_X(18, 19, 20, 21)
    PHK256X4_ENC_STEP_X(22, 23, 24, 25)
    PHK256X4_ENC_STEP_X(26, 27, 28, 29)
    PHK256X4_STEP_ENC_LAST(state[0], state[1], state[2], state[3],
                           state[4], state[5], state[6], state[7],
                           ctx->rk[30], ctx->rk[31], ctx->rk[32], ctx->rk[33])

    _mm_storeu_si128((__m128i*)buf, state[0]);
    _mm_storeu_si128((__m128i*)&buf[16], state[1]);
    _mm_storeu_si128((__m128i*)&buf[32], state[2]);
    _mm_storeu_si128((__m128i*)&buf[48], state[3]);
    _mm_storeu_si128((__m128i*)&buf[64], state[4]);
    _mm_storeu_si128((__m128i*)&buf[80], state[5]);
    _mm_storeu_si128((__m128i*)&buf[96], state[6]);
    _mm_storeu_si128((__m128i*)&buf[112], state[7]);
}

#define PHK256X4_STEP_ENC_DIFF_TWEAKKEYS(a, b, c, d, ka0, ka1, ka2, ka3, \
                                                     kb0, kb1, kb2, kb3, \
                                                     kc0, kc1, kc2, kc3, \
                                                     kd0, kd1, kd2, kd3) \
  PHK256_ROUND_ENC(a[0], a[1], ka0, ka1) \
  PHK256_ROUND_ENC(a[0], a[1], ka2, ka3) \
  PHK256_ROUND_ENC(b[0], b[1], kb0, kb1) \
  PHK256_ROUND_ENC(b[0], b[1], kb2, kb3) \
  PHK256_ROUND_ENC(c[0], c[1], kc0, kc1) \
  PHK256_ROUND_ENC(c[0], c[1], kc2, kc3) \
  PHK256_ROUND_ENC(d[0], d[1], kd0, kd1) \
  PHK256_ROUND_ENC(d[0], d[1], kd2, kd3) \
  PHK256_MIX_ENC(a[0], a[1], a[0], a[1]) \
  PHK256_MIX_ENC(b[0], b[1], b[0], b[1]) \
  PHK256_MIX_ENC(c[0], c[1], c[0], c[1]) \
  PHK256_MIX_ENC(d[0], d[1], d[0], d[1])

void phk256x4_encrypt_diff_tweakeys(struct phk256_ctx * ctx1, uint8_t* buf1, \
                                     struct phk256_ctx * ctx2, uint8_t* buf2, \
                                     struct phk256_ctx * ctx3, uint8_t* buf3, \
                                     struct phk256_ctx * ctx4, uint8_t* buf4) {
  
  __m128i state1[2], state2[2], state3[2], state4[2], tmp;
  state1[0] = _mm_loadu_si128((__m128i*) &buf1[0]);
  state1[1] = _mm_loadu_si128((__m128i*) &buf1[16]);
  state2[0] = _mm_loadu_si128((__m128i*) &buf2[0]);
  state2[1] = _mm_loadu_si128((__m128i*) &buf2[16]);
  state3[0] = _mm_loadu_si128((__m128i*) &buf3[0]);
  state3[1] = _mm_loadu_si128((__m128i*) &buf3[16]);
  state4[0] = _mm_loadu_si128((__m128i*) &buf4[0]);
  state4[1] = _mm_loadu_si128((__m128i*) &buf4[16]);

  //add initial roundkey
  state1[0] = _mm_xor_si128(state1[0], ctx1->rk[0]);
  state1[1] = _mm_xor_si128(state1[1], ctx1->rk[1]);
  state2[0] = _mm_xor_si128(state2[0], ctx2->rk[0]);
  state2[1] = _mm_xor_si128(state2[1], ctx2->rk[1]);
  state3[0] = _mm_xor_si128(state3[0], ctx3->rk[0]);
  state3[1] = _mm_xor_si128(state3[1], ctx3->rk[1]);
  state4[0] = _mm_xor_si128(state4[0], ctx4->rk[0]);
  state4[1] = _mm_xor_si128(state4[1], ctx4->rk[1]);

  // 7 normal steps = 14 normal rounds
  //step1
  PHK256X4_STEP_ENC_DIFF_TWEAKKEYS(state1, state2, state3, state4, \
                          ctx1->rk[2], ctx1->rk[3], ctx1->rk[4], ctx1->rk[5], \
                          ctx2->rk[2], ctx2->rk[3], ctx2->rk[4], ctx2->rk[5], \
                          ctx3->rk[2], ctx3->rk[3], ctx3->rk[4], ctx3->rk[5], \
                          ctx4->rk[2], ctx4->rk[3], ctx4->rk[4], ctx4->rk[5])
  //step2
  PHK256X4_STEP_ENC_DIFF_TWEAKKEYS(state1, state2, state3, state4, \
                          ctx1->rk[6], ctx1->rk[7], ctx1->rk[8], ctx1->rk[9], \
                          ctx2->rk[6], ctx2->rk[7], ctx2->rk[8], ctx2->rk[9], \
                          ctx3->rk[6], ctx3->rk[7], ctx3->rk[8], ctx3->rk[9], \
                          ctx4->rk[6], ctx4->rk[7], ctx4->rk[8], ctx4->rk[9])
  //step3
  PHK256X4_STEP_ENC_DIFF_TWEAKKEYS(state1, state2, state3, state4, \
                          ctx1->rk[10], ctx1->rk[11], ctx1->rk[12], ctx1->rk[13], \
                          ctx2->rk[10], ctx2->rk[11], ctx2->rk[12], ctx2->rk[13], \
                          ctx3->rk[10], ctx3->rk[11], ctx3->rk[12], ctx3->rk[13], \
                          ctx4->rk[10], ctx4->rk[11], ctx4->rk[12], ctx4->rk[13])
  //step4
  PHK256X4_STEP_ENC_DIFF_TWEAKKEYS(state1, state2, state3, state4, \
                          ctx1->rk[14], ctx1->rk[15], ctx1->rk[16], ctx1->rk[17], \
                          ctx2->rk[14], ctx2->rk[15], ctx2->rk[16], ctx2->rk[17], \
                          ctx3->rk[14], ctx3->rk[15], ctx3->rk[16], ctx3->rk[17], \
                          ctx4->rk[14], ctx4->rk[15], ctx4->rk[16], ctx4->rk[17])
  //step5
  PHK256X4_STEP_ENC_DIFF_TWEAKKEYS(state1, state2, state3, state4, \
                          ctx1->rk[18], ctx1->rk[19], ctx1->rk[20], ctx1->rk[21], \
                          ctx2->rk[18], ctx2->rk[19], ctx2->rk[20], ctx2->rk[21], \
                          ctx3->rk[18], ctx3->rk[19], ctx3->rk[20], ctx3->rk[21], \
                          ctx4->rk[18], ctx4->rk[19], ctx4->rk[20], ctx4->rk[21])
  //step6
  PHK256X4_STEP_ENC_DIFF_TWEAKKEYS(state1, state2, state3, state4, \
                          ctx1->rk[22], ctx1->rk[23], ctx1->rk[24], ctx1->rk[25], \
                          ctx2->rk[22], ctx2->rk[23], ctx2->rk[24], ctx2->rk[25], \
                          ctx3->rk[22], ctx3->rk[23], ctx3->rk[24], ctx3->rk[25], \
                          ctx4->rk[22], ctx4->rk[23], ctx4->rk[24], ctx4->rk[25])
  //step7
  PHK256X4_STEP_ENC_DIFF_TWEAKKEYS(state1, state2, state3, state4, \
                          ctx1->rk[26], ctx1->rk[27], ctx1->rk[28], ctx1->rk[29], \
                          ctx2->rk[26], ctx2->rk[27], ctx2->rk[28], ctx2->rk[29], \
                          ctx3->rk[26], ctx3->rk[27], ctx3->rk[28], ctx3->rk[29], \
                          ctx4->rk[26], ctx4->rk[27], ctx4->rk[28], ctx4->rk[29])
  // Final step, no mixing and no mix columns
  PHK256_ROUND_ENC(state1[0], state1[1], ctx1->rk[30], ctx1->rk[31])
  PHK256_ROUND_ENC(state2[0], state2[1], ctx2->rk[30], ctx2->rk[31])
  PHK256_ROUND_ENC(state3[0], state3[1], ctx3->rk[30], ctx3->rk[31])
  PHK256_ROUND_ENC(state4[0], state4[1], ctx4->rk[30], ctx4->rk[31])
  PHK256_ROUND_ENC_LAST(state1[0], state1[1], ctx1->rk[32], ctx1->rk[33])
  PHK256_ROUND_ENC_LAST(state2[0], state2[1], ctx2->rk[32], ctx2->rk[33])
  PHK256_ROUND_ENC_LAST(state3[0], state3[1], ctx3->rk[32], ctx3->rk[33])
  PHK256_ROUND_ENC_LAST(state4[0], state4[1], ctx4->rk[32], ctx4->rk[33])


  _mm_storeu_si128((__m128i*)buf1, state1[0]);
  _mm_storeu_si128((__m128i*)&buf1[16], state1[1]);
  _mm_storeu_si128((__m128i*)buf2, state2[0]);
  _mm_storeu_si128((__m128i*)&buf2[16], state2[1]);
  _mm_storeu_si128((__m128i*)buf3, state3[0]);
  _mm_storeu_si128((__m128i*)&buf3[16], state3[1]);
  _mm_storeu_si128((__m128i*)buf4, state4[0]);
  _mm_storeu_si128((__m128i*)&buf4[16], state4[1]);

}


#define PHK256_DEC_STEP_X(a, b, c, d) \
    PHK256_STEP_DEC(state[0], state[1], ctx->rk[a], ctx->rk[b], ctx->rk[c], ctx->rk[d])

void phk256_decrypt(const struct phk256_ctx * const ctx, uint8_t * const buf) {
    __m128i state[PHK256_SUBSTATES], tmp;

    // init ciphertext
    state[0] = _mm_loadu_si128((__m128i*)buf);
    state[1] = _mm_loadu_si128((__m128i*)&buf[16]);

    // remove last round key
    state[0] = _mm_xor_si128(state[0], ctx->rk[32]);
    state[1] = _mm_xor_si128(state[1], ctx->rk[33]);

    PHK256_DEC_STEP_X(28, 29, 30, 31)
    PHK256_DEC_STEP_X(24, 25, 26, 27)
    PHK256_DEC_STEP_X(20, 21, 22, 23)
    PHK256_DEC_STEP_X(16, 17, 18, 19)
    PHK256_DEC_STEP_X(12, 13, 14, 15)
    PHK256_DEC_STEP_X(8, 9, 10, 11)
    PHK256_DEC_STEP_X(4, 5, 6, 7)
    PHK256_STEP_DEC_LAST(state[0], state[1], ctx->rk[0], ctx->rk[1], ctx->rk[2], ctx->rk[3])

    _mm_storeu_si128((__m128i*)buf, state[0]);
    _mm_storeu_si128((__m128i*)&buf[16], state[1]);
}

void phk256_256_decrypt(const struct phk256_256_ctx * const ctx, uint8_t * const buf) {
	__m128i state[PHK256_SUBSTATES], tmp;
	
	// init ciphertext
	state[0] = _mm_loadu_si128((__m128i*)buf);
	state[1] = _mm_loadu_si128((__m128i*)&buf[16]);
	
	// remove last round key
	state[0] = _mm_xor_si128(state[0], ctx->rk[32]);
	state[1] = _mm_xor_si128(state[1], ctx->rk[33]);
	
	PHK256_DEC_STEP_X(28, 29, 30, 31)
	PHK256_DEC_STEP_X(24, 25, 26, 27)
	PHK256_DEC_STEP_X(20, 21, 22, 23)
	PHK256_DEC_STEP_X(16, 17, 18, 19)
	PHK256_DEC_STEP_X(12, 13, 14, 15)
	PHK256_DEC_STEP_X(8, 9, 10, 11)
	PHK256_DEC_STEP_X(4, 5, 6, 7)
	PHK256_STEP_DEC_LAST(state[0], state[1], ctx->rk[0], ctx->rk[1], ctx->rk[2], ctx->rk[3])
	
	_mm_storeu_si128((__m128i*)buf, state[0]);
	_mm_storeu_si128((__m128i*)&buf[16], state[1]);
}

#define PHK256X2_DEC_STEP_X(a, b, c, d) \
    PHK256X2_STEP_DEC(state[0], state[1], state[2], state[3], \
            ctx->rk[a], ctx->rk[b], ctx->rk[c], ctx->rk[d])

void phk256x2_decrypt(const struct phk256_ctx * const ctx, uint8_t * const buf) {
    __m128i state[PHK256_SUBSTATES << 1], tmp;

    // init ciphertext
    state[0] = _mm_loadu_si128((__m128i*)buf);
    state[1] = _mm_loadu_si128((__m128i*)&buf[16]);
    state[2] = _mm_loadu_si128((__m128i*)&buf[32]);
    state[3] = _mm_loadu_si128((__m128i*)&buf[48]);

    // remove last round key
    state[0] = _mm_xor_si128(state[0], ctx->rk[32]);
    state[1] = _mm_xor_si128(state[1], ctx->rk[33]);
    state[2] = _mm_xor_si128(state[2], ctx->rk[32]);
    state[3] = _mm_xor_si128(state[3], ctx->rk[33]);

    PHK256X2_DEC_STEP_X(28, 29, 30, 31)
    PHK256X2_DEC_STEP_X(24, 25, 26, 27)
    PHK256X2_DEC_STEP_X(20, 21, 22, 23)
    PHK256X2_DEC_STEP_X(16, 17, 18, 19)
    PHK256X2_DEC_STEP_X(12, 13, 14, 15)
    PHK256X2_DEC_STEP_X(8, 9, 10, 11)
    PHK256X2_DEC_STEP_X(4, 5, 6, 7)
    PHK256X2_STEP_DEC_LAST(state[0], state[1], state[2], state[3],
                           ctx->rk[0], ctx->rk[1], ctx->rk[2], ctx->rk[3])

    _mm_storeu_si128((__m128i*)buf, state[0]);
    _mm_storeu_si128((__m128i*)&buf[16], state[1]);
    _mm_storeu_si128((__m128i*)&buf[32], state[2]);
    _mm_storeu_si128((__m128i*)&buf[48], state[3]);
}

#define PHK256X4_DEC_STEP_X(a, b, c, d) \
    PHK256X4_STEP_DEC(state[0], state[1], state[2], state[3], \
            state[4], state[5], state[6], state[7], \
            ctx->rk[a], ctx->rk[b], ctx->rk[c], ctx->rk[d])

void phk256x4_decrypt(const struct phk256_ctx * const ctx, uint8_t * const buf) {
    __m128i state[PHK256_SUBSTATES << 2], tmp;

    // init ciphertext
    state[0] = _mm_loadu_si128((__m128i*)buf);
    state[1] = _mm_loadu_si128((__m128i*)&buf[16]);
    state[2] = _mm_loadu_si128((__m128i*)&buf[32]);
    state[3] = _mm_loadu_si128((__m128i*)&buf[48]);
    state[4] = _mm_loadu_si128((__m128i*)&buf[64]);
    state[5] = _mm_loadu_si128((__m128i*)&buf[80]);
    state[6] = _mm_loadu_si128((__m128i*)&buf[96]);
    state[7] = _mm_loadu_si128((__m128i*)&buf[112]);

    // remove last round key
    state[0] = _mm_xor_si128(state[0], ctx->rk[32]);
    state[1] = _mm_xor_si128(state[1], ctx->rk[33]);
    state[2] = _mm_xor_si128(state[2], ctx->rk[32]);
    state[3] = _mm_xor_si128(state[3], ctx->rk[33]);
    state[4] = _mm_xor_si128(state[4], ctx->rk[32]);
    state[5] = _mm_xor_si128(state[5], ctx->rk[33]);
    state[6] = _mm_xor_si128(state[6], ctx->rk[32]);
    state[7] = _mm_xor_si128(state[7], ctx->rk[33]);

    PHK256X4_DEC_STEP_X(28, 29, 30, 31)
    PHK256X4_DEC_STEP_X(24, 25, 26, 27)
    PHK256X4_DEC_STEP_X(20, 21, 22, 23)
    PHK256X4_DEC_STEP_X(16, 17, 18, 19)
    PHK256X4_DEC_STEP_X(12, 13, 14, 15)
    PHK256X4_DEC_STEP_X(8, 9, 10, 11)
    PHK256X4_DEC_STEP_X(4, 5, 6, 7)
    PHK256X4_STEP_DEC_LAST(state[0], state[1], state[2], state[3],
                           state[4], state[5], state[6], state[7],
                           ctx->rk[0], ctx->rk[1], ctx->rk[2], ctx->rk[3])

    _mm_storeu_si128((__m128i*)buf, state[0]);
    _mm_storeu_si128((__m128i*)&buf[16], state[1]);
    _mm_storeu_si128((__m128i*)&buf[32], state[2]);
    _mm_storeu_si128((__m128i*)&buf[48], state[3]);
    _mm_storeu_si128((__m128i*)&buf[64], state[4]);
    _mm_storeu_si128((__m128i*)&buf[80], state[5]);
    _mm_storeu_si128((__m128i*)&buf[96], state[6]);
    _mm_storeu_si128((__m128i*)&buf[112], state[7]);
}

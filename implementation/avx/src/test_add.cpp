
#include <cstdint>
#include <cstring>
#include <iostream>
#include <fstream>
#include <string>
#include <jsoncpp/json/json.h>
#include <random>
#include <emmintrin.h>

#include "misc.h"




uint8_t HalfByteToDez(char const& h) {
  uint8_t ret = 0;
  switch (h) {
    case '0' : return 0;
    case '1' : return 1;
    case '2' : return 2;
    case '3' : return 3;
    case '4' : return 4;
    case '5' : return 5;
    case '6' : return 6;
    case '7' : return 7;
    case '8' : return 8;
    case '9' : return 9;
    case 'a' : return 10;
    case 'b' : return 11;
    case 'c' : return 12;
    case 'd' : return 13;
    case 'e' : return 14;
    case 'f' : return 15;
  }
  return ret;
}


uint8_t ByteToDez(char lhs, char rhs) {
  return 16 * HalfByteToDez(lhs) + HalfByteToDez(rhs);
}




void HexToDez(std::string const& hexstr, uint8_t* const& out) {
  uint64_t len = (hexstr.length() + 1) / 2;
  if (hexstr.length() % 2 == 1) {
    out[0] = HalfByteToDez(static_cast<char>(hexstr[0]));
    for (uint64_t i = 1; i <= len; ++i) {
      out[i] = ByteToDez(hexstr[(i << 1)-1], hexstr[(i << 1)]);
    }
  } else {
    for (uint64_t i = 0; i < len; ++i) {
      out[i] = ByteToDez(hexstr[i << 1], hexstr[(i << 1)+1]);
    }
  }
}


bool test_ctrt_phk256_128_avx() {
std::cout<<"reading file\n";
    bool ret = true;
    std::ifstream test_file("../testcases/test-128-add.json",
                            std::ifstream::binary);

    if (!test_file.good()) {
        std::cout << "\033[91mTEST FILE ";
        std::cout << " NOT FOUND\n";
        std::cout << "You either changed the directory structure or called the";
        std::cout << " program from the wrong directory!\n\033[0m";
        return false;
    }

    Json::Value tests;

    test_file >> tests;

    int num_tests = tests.size();

    for (auto test : tests) {

        std::cout << "-----------------" << std::endl;
        std::string tmp = test["lhs"].asString();
        uint8_t lhs[16];
        uint8_t rhs;
        uint8_t res[16];
        uint8_t expected[16];
        char* tmp2 = &tmp[0u];
        HexToDez(tmp2, lhs);

        tmp = test["rhs"].asString();
        rhs = ByteToDez(tmp2[0], tmp2[1]);

        tmp = test["res"].asString();
        tmp2 = &tmp[0u];
        HexToDez(tmp2, expected);

        __m128i i_lhs = _mm_loadu_si128((__m128i*) lhs);        
        i_lhs = _mm_shuffle_epi8(i_lhs, _mm_set_epi8(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15));
        __m128i i_rhs = _mm_set_epi8(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,rhs);        

        __m128i i_res = misc::add_128si(i_lhs, i_rhs);
        i_res = _mm_shuffle_epi8(i_res, _mm_set_epi8(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15));

        _mm_storeu_si128((__m128i*)res, i_res);

        // memcmp returns 0 if values are equal
        bool was_successful = memcmp(res, expected, 16) == 0;

        if (!was_successful) {
            std::cout << "\033[91mCTRT encryption failed!\033[0m" << std::endl;
            std::cout << "Expected: " << std::hex;

            for (int i = 0; i < 16; ++i) {
                std::cout << std::setw(2) << std::setfill('0')
                          << (int) expected[i];
            }

            std::cout << std::endl;
            std::cout << "But was:  " << std::hex;

            for (int i = 0; i < 16; ++i) {
                std::cout << std::setw(2) << std::setfill('0')
                          << (int) res[i];
            }

            std::cout << std::endl;
            ret = false;
        }

    }

    return ret;
}


// ---------------------------------------------------------


int main() {
    bool success = true;
    success &= test_ctrt_phk256_128_avx();
    std::cout << (success ? "All tests passed." : "Failure.") << std::endl;
    return 0;
}


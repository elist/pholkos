#include <bitset>
#include <cstring>
#include "misc.h"

namespace misc {
    
    void print_progress_bar(std::ostream &out, uint8_t progress) {
        std::ios_base::fmtflags fmt(out.flags());
        out << "\u2563";

        for (size_t i(0); i < 20; ++i) {
            if (progress / 5 > i) {
                out << "\u2588";
            } else {
                out << "\u2591";
            }
        }

        out << "\u2560";

        out << ' ' << std::setw(3) << (int) progress << " %";
        out.flags(fmt);
    }

    // ---------------------------------------------------------

    void print_buffer(uint8_t  *buf, size_t len, bool only_words) {
        for (size_t i = 0; i < len; i += (only_words ? 4 : 1)) {
            std::cout << std::hex << std::setw(2) << std::setfill('0') << ((int)buf[i] / (only_words ? 4 : 1));

            if (only_words || (i % 16 == 15)) {
                std::cout << ' ';
            }
        }

        std::cout << std::endl;
    }
    
    // ---------------------------------------------------------
    
    void print_hex(const uint8_t* array, size_t num_bytes) {
        for (size_t i = 0; i < num_bytes; ++i) {
            printf("%02x  ", array[i]);
        }
        
        puts("");
    }
    
    // ---------------------------------------------------------
    
    bool assert_equals(const uint8_t* left,
                       const uint8_t* right,
                       size_t num_bytes) {
        return memcmp((void*)left, (void*)right, num_bytes) == 0;
    }

    // ---------------------------------------------------------
    
    void print_m128i(__m128i foo, std::string comment="") {
      uint8_t tmp[16];
      _mm_store_si128((__m128i*)tmp, foo);
      printf("%-10s: %02x  %02x  %02x  %02x  %02x  %02x  %02x  %02x  " \
              "%02x  %02x  %02x  %02x  %02x  %02x  %02x  %02x\n", comment.c_str(), \
            tmp[0], tmp[1], tmp[2], tmp[3], \
            tmp[4], tmp[5], tmp[6], tmp[7], \
            tmp[8], tmp[9], tmp[10], tmp[11], \
            tmp[12], tmp[13], tmp[14], tmp[15]);
    }

    
    void print_key(__m128i foo, __m128i bar, std::string comment="") {
      uint8_t tmp[16];
      _mm_store_si128((__m128i*)tmp, foo);
      printf("%-10s: %02x  %02x  %02x  %02x  %02x  %02x  %02x  %02x  " \
              "%02x  %02x  %02x  %02x  %02x  %02x  %02x  %02x ", comment.c_str(), \
            tmp[0], tmp[1], tmp[2], tmp[3], \
            tmp[4], tmp[5], tmp[6], tmp[7], \
            tmp[8], tmp[9], tmp[10], tmp[11], \
            tmp[12], tmp[13], tmp[14], tmp[15]);
      _mm_store_si128((__m128i*)tmp, bar);
      printf(" %02x  %02x  %02x  %02x  %02x  %02x  %02x  %02x  " \
              "%02x  %02x  %02x  %02x  %02x  %02x  %02x  %02x\n", \
            tmp[0], tmp[1], tmp[2], tmp[3], \
            tmp[4], tmp[5], tmp[6], tmp[7], \
            tmp[8], tmp[9], tmp[10], tmp[11], \
            tmp[12], tmp[13], tmp[14], tmp[15]);
    }

    // ---------------------------------------------------------

    // addition of two_mm_128si, where the rhs is at most 4!
    __m128i add_128si(__m128i lhs, __m128i rhs) {

      __m128i res;
      __m128i tmp1;
      // add 64 bit words 
      res = _mm_add_epi64(lhs, rhs);
      // check if overflow happened in the lower word and add 1 if and 0 if not to
      // the upper word

      // if the lower half of lhs is larger than of res, we had an overflow 
      // we need to flip 1st bit because comparison is signed
      __m128i flip = _mm_set_epi32(0x80000000, 0, 0x80000000, 0);
      tmp1 = _mm_cmpgt_epi64(_mm_xor_si128(lhs,flip), _mm_xor_si128(res,flip));

      // if there was an overflow, tmp1 is xx11 else xx00
      // we want to leave the lower half as is and set the upper to 1&tmp1
      tmp1 = _mm_unpacklo_epi64(_mm_setzero_si128(), tmp1);
      tmp1 = _mm_and_si128(tmp1, _mm_set_epi32(0,1,0,0));
      return _mm_add_epi64(res, tmp1);
    }

}


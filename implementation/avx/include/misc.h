#ifndef MISC_H
#define MISC_H

// ---------------------------------------------------------

#include <cstddef>
#include <cstdint>
#include <iostream>
#include <iomanip>
#include <math.h>
#include <limits>
#include <sstream>
#include <immintrin.h>

// ---------------------------------------------------------

namespace misc {
    
    void print_progress_bar(std::ostream &out, uint8_t progress);
    
    // ---------------------------------------------------------
    
    void print_buffer(uint8_t  *buf, size_t len, bool only_words);
    
    // ---------------------------------------------------------
    
    void print_hex(const uint8_t* array, size_t num_bytes);
    
    // ---------------------------------------------------------
    
    bool assert_equals(const uint8_t* left,
                       const uint8_t* right,
                       size_t num_bytes);

    // ---------------------------------------------------------

    void print_m128i(__m128i foo, std::string comment);

    // ---------------------------------------------------------

    void print_key(__m128i foo, __m128i bar, std::string comment);

    // ---------------------------------------------------------
    
    __m128i add_128si(__m128i lhs, __m128i rhs);
}

// ---------------------------------------------------------


#endif

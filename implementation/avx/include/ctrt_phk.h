#ifndef PHK_CTRT_H
#define PHK_CTRT_H

#include <cstring>

#include "phk-256.h"
#include "misc.h"


void ctrt_phk_256(uint8_t * const key, uint8_t * const tweak, uint8_t * const nonce, uint8_t * buf, int length);
  


#endif //PHK_CTRT

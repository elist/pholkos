// ---------------------------------------------------------------------
// Simpira v2
// ---------------------------------------------------------------------

#ifndef __SIMIPIRA_V2_H_
#define __SIMIPIRA_V2_H_

// ---------------------------------------------------------------------

#include <cstdint>
#include <emmintrin.h>
#include <immintrin.h> // AVX2 for blending 32-bit values
#include <smmintrin.h>
#include <tmmintrin.h> // SSSE3 for pshufb
#include <wmmintrin.h>

// ---------------------------------------------------------------------

#define simpira_const(c, b)  _mm_setr_epi32(c^b, c^b^0x10, c^b^0x20, c^b^0x30)
#define vaesenc(x, y)        _mm_aesenc_si128(x,y)
#define vxor(x, y)           _mm_xor_si128(x,y)
#define vloadu(x)            _mm_loadu_si128((__m128i*)x)
#define vstoreu(x, y)        _mm_storeu_si128((__m128i*)x, y)

#define simpira_round_2blocks(input, output, c, b)  \
    vaesenc(vaesenc(input, simpira_const(c, b)), output)

#define simpira_round_4blocks(x0, x1, x2, x3, tmp0, tmp1, c, b) do { \
    tmp0 = vaesenc(x0, simpira_const(c, b)); \
    tmp1 = vaesenc(x2, simpira_const(c+1, b)); \
    x1 = vaesenc(tmp0, x1); \
    x3 = vaesenc(tmp1, x3); \
    } while (0)

// ---------------------------------------------------------------------

typedef struct {
    __m128i key;
} simpira_context_t;

// ---------------------------------------------------------------------

void simpira256_set_key(simpira_context_t *ctx,
                        const uint8_t key[16]);

// ---------------------------------------------------------------------

void simpira256_decrypt(const simpira_context_t *ctx,
                        const uint8_t ciphertext[32],
                        uint8_t plaintext[32]);

// ---------------------------------------------------------------------

void simpira256_encrypt(const simpira_context_t *ctx,
                        const uint8_t plaintext[32],
                        uint8_t ciphertext[32]);

// ---------------------------------------------------------------------

void simpira512_set_key(simpira_context_t *ctx,
                        const uint8_t key[16]);

// ---------------------------------------------------------------------

void simpira512_decrypt(const simpira_context_t *ctx,
                        const uint8_t ciphertext[64],
                        uint8_t plaintext[64]);

// ---------------------------------------------------------------------

void simpira512_encrypt(const simpira_context_t *ctx,
                        const uint8_t plaintext[64],
                        uint8_t ciphertext[64]);

// ---------------------------------------------------------------------

#endif // __SIMIPIRA_V2_H_

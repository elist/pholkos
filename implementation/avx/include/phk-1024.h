#include "phk-512.h"


#ifndef PHK_1024_H
#define PHK_1024_H


#define PHK1024_SUBSTATES 8
#define PHK1024_STEPS 14
#define PHK1024_256_STEPS 12


#define PHK1024_ROUND_ENC(a, b, c, d, e, f, g, h, k0, k1, k2, k3, k4, k5, k6, k7) \
    PHK512_ROUND_ENC(a, b, c, d, k0, k1, k2, k3) \
    PHK512_ROUND_ENC(e, f, g, h, k4, k5, k6, k7)

#define PHK1024_ROUND_ENC_LAST(a, b, c, d, e, f, g, h, k0, k1, k2, k3, k4, k5, k6, k7) \
    PHK512_ROUND_ENC_LAST(a, b, c, d, k0, k1, k2, k3) \
    PHK512_ROUND_ENC_LAST(e, f, g, h, k4, k5, k6, k7)

#define PHK1024_ROUND_DEC(a, b, c, d, e, f, g, h, k0, k1, k2, k3, k4, k5, k6, k7) \
    PHK512_ROUND_DEC(a, b, c, d, k0, k1, k2, k3) \
    PHK512_ROUND_DEC(e, f, g, h, k4, k5, k6, k7)

#define PHK1024_ROUND_DEC_LAST(a, b, c, d, e, f, g, h, k0, k1, k2, k3, k4, k5, k6, k7) \
    PHK512_ROUND_DEC_LAST(a, b, c, d, k0, k1, k2, k3) \
    PHK512_ROUND_DEC_LAST(e, f, g, h, k4, k5, k6, k7)

// ( 0 9 18 27 4 13 22 31 8 17 26 3 12 21 30 7 16 25 2 11 20 29 6 15 24 1 10 19 28 5 14 23 )
#define PHK1024_MIX0_ENC(a_out, b_out, c_out, d_out, e_out, f_out, g_out, h_out, \
        a_in, b_in, c_in, d_in, e_in, f_in, g_in, h_in) \
    PHK512_MIX_ENC(a_out, c_out, e_out, g_out, a_in, c_in, e_in, g_in) \
    PHK512_MIX_ENC(b_out, d_out, f_out, h_out, b_in, d_in, f_in, h_in)

// ( 0 25 18 11 4 29 22 15 8 1 26 19 12 5 30 23 16 09 2 27 20 13 6 31 24 17 10 3 28 21 14 7 )
#define PHK1024_MIX0_DEC(a_out, b_out, c_out, d_out, e_out, f_out, g_out, h_out, \
        a_in, b_in, c_in, d_in, e_in, f_in, g_in, h_in) \
    PHK512_MIX_DEC(a_out, c_out, e_out, g_out, a_in, c_in, e_in, g_in) \
    PHK512_MIX_DEC(b_out, d_out, f_out, h_out, b_in, d_in, f_in, h_in)

// ( 0 5 2 7 4 9 6 11 8 13 10 15 12 17 14 19 16 21 18 23 20 25 22 27 24 29 26 31 28 1 30 3 )
#define PHK1024_MIX1_ENC(a_out, b_out, c_out, d_out, e_out, f_out, g_out, h_out, \
        a_in, b_in, c_in, d_in, e_in, f_in, g_in, h_in) \
    tmp = _mm_blend_epi32(h_in, a_in, 0b1010); \
    a_out = _mm_blend_epi32(a_in, b_in, 0b1010); \
    b_out = _mm_blend_epi32(b_in, c_in, 0b1010); \
    c_out = _mm_blend_epi32(c_in, d_in, 0b1010); \
    d_out = _mm_blend_epi32(d_in, e_in, 0b1010); \
    e_out = _mm_blend_epi32(e_in, f_in, 0b1010); \
    f_out = _mm_blend_epi32(f_in, g_in, 0b1010); \
    g_out = _mm_blend_epi32(g_in, h_in, 0b1010); \
    h_out = tmp;

// ( 0 29 2 31 4 1 6 3 8 5 10 7 12 9 14 11 16 13 18 15 20 17 22 19 24 21 26 23 28 25 30 27 )
#define PHK1024_MIX1_DEC(a_out, b_out, c_out, d_out, e_out, f_out, g_out, h_out, \
        a_in, b_in, c_in, d_in, e_in, f_in, g_in, h_in) \
    tmp = _mm_blend_epi32(a_in, h_in, 0b1010); \
    h_out = _mm_blend_epi32(h_in, g_in, 0b1010); \
    g_out = _mm_blend_epi32(g_in, f_in, 0b1010); \
    f_out = _mm_blend_epi32(f_in, e_in, 0b1010); \
    e_out = _mm_blend_epi32(e_in, d_in, 0b1010); \
    d_out = _mm_blend_epi32(d_in, c_in, 0b1010); \
    c_out = _mm_blend_epi32(c_in, b_in, 0b1010); \
    b_out = _mm_blend_epi32(b_in, a_in, 0b1010); \
    a_out = tmp;

#define PHK1024_STEP0_ENC(a, b, c, d, e, f, g, h, k0, k1, k2, k3, k4, k5, k6, k7, k8, k9, k10, k11, k12, k13, k14, k15) \
    PHK1024_ROUND_ENC(a, b, c, d, e, f, g, h, k0, k1, k2, k3, k4, k5, k6, k7) \
    PHK1024_ROUND_ENC(a, b, c, d, e, f, g, h, k8, k9, k10, k11, k12, k13, k14, k15) \
    PHK1024_MIX0_ENC(a, b, c, d, e, f, g, h, a, b, c, d, e, f, g, h)

#define PHK1024_STEP0_DEC(a, b, c, d, e, f, g, h, k0, k1, k2, k3, k4, k5, k6, k7, k8, k9, k10, k11, k12, k13, k14, k15) \
    PHK1024_ROUND_DEC(a, b, c, d, e, f, g, h, k8, k9, k10, k11, k12, k13, k14, k15) \
    PHK1024_ROUND_DEC(a, b, c, d, e, f, g, h, k0, k1, k2, k3, k4, k5, k6, k7) \
    PHK1024_MIX0_DEC(a, b, c, d, e, f, g, h, a, b, c, d, e, f, g, h) \

#define PHK1024_STEP1_ENC(a, b, c, d, e, f, g, h, k0, k1, k2, k3, k4, k5, k6, k7, k8, k9, k10, k11, k12, k13, k14, k15) \
    PHK1024_ROUND_ENC(a, b, c, d, e, f, g, h, k0, k1, k2, k3, k4, k5, k6, k7) \
    PHK1024_ROUND_ENC(a, b, c, d, e, f, g, h, k8, k9, k10, k11, k12, k13, k14, k15) \
    PHK1024_MIX1_ENC(a, b, c, d, e, f, g, h, a, b, c, d, e, f, g, h) \

#define PHK1024_STEP1_DEC(a, b, c, d, e, f, g, h, k0, k1, k2, k3, k4, k5, k6, k7, k8, k9, k10, k11, k12, k13, k14, k15) \
    PHK1024_ROUND_DEC(a, b, c, d, e, f, g, h, k8, k9, k10, k11, k12, k13, k14, k15) \
    PHK1024_ROUND_DEC(a, b, c, d, e, f, g, h, k0, k1, k2, k3, k4, k5, k6, k7) \
    PHK1024_MIX1_DEC(a, b, c, d, e, f, g, h, a, b, c, d, e, f, g, h)

#define PHK1024_STEP_ENC_LAST(a, b, c, d, e, f, g, h, k0, k1, k2, k3, k4, k5, k6, k7, k8, k9, k10, k11, k12, k13, k14, k15) \
    PHK1024_ROUND_ENC(a, b, c, d, e, f, g, h, k0, k1, k2, k3, k4, k5, k6, k7) \
    PHK1024_ROUND_ENC_LAST(a, b, c, d, e, f, g, h, k8, k9, k10, k11, k12, k13, k14, k15)

#define PHK1024_STEP_DEC_LAST(a, b, c, d, e, f, g, h, k0, k1, k2, k3, k4, k5, k6, k7, k8, k9, k10, k11, k12, k13, k14, k15) \
    PHK1024_ROUND_DEC(a, b, c, d, e, f, g, h, k8, k9, k10, k11, k12, k13, k14, k15) \
    PHK1024_ROUND_DEC_LAST(a, b, c, d, e, f, g, h, k0, k1, k2, k3, k4, k5, k6, k7)

#define PHK1024_KS_EXPAND(a_out, b_out, c_out, d_out, e_out, f_out, g_out, h_out, a_in, b_in) \
    a_out = a_in; \
    b_out = b_in; \
    PHK_KS_EXPAND_A(c_out, d_out, a_in, b_in) \
    PHK_KS_EXPAND_B(e_out, f_out, a_in, b_in) \
    PHK_KS_EXPAND_C(g_out, h_out, a_in, b_in)

#define PHK1024_KS_EXPAND_ALTERNATIVE(a_out, b_out, c_out, d_out, e_out, f_out, g_out, h_out, a_in, b_in) \
    a_out = a_in; \
    b_out = b_in; \
    c_out = _mm_xor_si128(a_in, b_in); \
    d_out = c_out; \
    c_out = _mm_xor_si128(c_out, _mm_alignr_epi8(b_in, a_in, 4));  \
    d_out = _mm_xor_si128(d_out, _mm_alignr_epi8(a_in, b_in, 4)); \
    e_out = _mm_xor_si128(a_out, _mm_alignr_epi8(a_out, a_out, 2)); \
    f_out = _mm_xor_si128(b_out, _mm_alignr_epi8(b_out, b_out, 2)); \
    g_out = _mm_xor_si128(c_out, _mm_alignr_epi8(c_out, c_out, 2)); \
    h_out = _mm_xor_si128(d_out, _mm_alignr_epi8(d_out, d_out, 2)); \
    e_out = _mm_xor_si128(e_out, _mm_alignr_epi8(e_out, e_out, 1)); \
    f_out = _mm_xor_si128(f_out, _mm_alignr_epi8(f_out, f_out, 1)); \
    g_out = _mm_xor_si128(g_out, _mm_alignr_epi8(g_out, g_out, 1)); \
    h_out = _mm_xor_si128(h_out, _mm_alignr_epi8(h_out, h_out, 1)); \
    e_out = _mm_shuffle_epi8(e_out, _mm_set_epi32(0x0c0c0c0c, 0x08080808, 0x04040404, 0x00000000)); \
    f_out = _mm_shuffle_epi8(f_out, _mm_set_epi32(0x0c0c0c0c, 0x08080808, 0x04040404, 0x00000000)); \
    g_out = _mm_shuffle_epi8(g_out, _mm_set_epi32(0x0c0c0c0c, 0x08080808, 0x04040404, 0x00000000)); \
    h_out = _mm_shuffle_epi8(h_out, _mm_set_epi32(0x0c0c0c0c, 0x08080808, 0x04040404, 0x00000000)); \
    e_out = _mm_xor_si128(a_out, e_out); \
    f_out = _mm_xor_si128(b_out, f_out); \
    g_out = _mm_xor_si128(c_out, g_out); \
    h_out = _mm_xor_si128(d_out, h_out);

#define PHK1024_KS_MIX0(a_out, b_out, c_out, d_out, e_out, f_out, g_out, h_out, \
        a_in, b_in, c_in, d_in, e_in, f_in, g_in, h_in) \
    PHK1024_MIX0_ENC(a_out, b_out, c_out, d_out, e_out, f_out, g_out, h_out, \
        a_in, b_in, c_in, d_in, e_in, f_in, g_in, h_in)

#define PHK1024_KS_MIX1(a_out, b_out, c_out, d_out, e_out, f_out, g_out, h_out, \
        a_in, b_in, c_in, d_in, e_in, f_in, g_in, h_in) \
    PHK1024_MIX1_ENC(a_out, b_out, c_out, d_out, e_out, f_out, g_out, h_out, \
        a_in, b_in, c_in, d_in, e_in, f_in, g_in, h_in)

#define PHK1024_KS_TAU(a, b, c, d, e, f, g, h) \
    PHK_TS_MIX(a) \
    PHK_TS_MIX(b) \
    PHK_TS_MIX(c) \
    PHK_TS_MIX(d) \
    PHK_TS_MIX(e) \
    PHK_TS_MIX(f) \
    PHK_TS_MIX(g) \
    PHK_TS_MIX(h)

#define PHK1024_KS_ROUND0(a_out, b_out, c_out, d_out, e_out, f_out, g_out, h_out, \
        a_in, b_in, c_in, d_in, e_in, f_in, g_in, h_in) \
    PHK1024_KS_MIX0(a_out, b_out, c_out, d_out, e_out, f_out, g_out, h_out, \
        a_in, b_in, c_in, d_in, e_in, f_in, g_in, h_in) \
    PHK1024_KS_TAU(a_out, b_out, c_out, d_out, e_out, f_out, g_out, h_out) \
    GF2P8MUL2(a_out) \
    GF2P8MUL2(b_out) \
    GF2P8MUL2(c_out) \
    GF2P8MUL2(d_out) \
    GF2P8MUL2(e_out) \
    GF2P8MUL2(f_out) \
    GF2P8MUL2(g_out) \
    GF2P8MUL2(h_out) \
    // GF2P8MUL2x8(a_out, b_out, c_out, d_out, e_out, f_out, g_out, h_out)

#define PHK1024_KS_ROUND1(a_out, b_out, c_out, d_out, e_out, f_out, g_out, h_out, \
        a_in, b_in, c_in, d_in, e_in, f_in, g_in, h_in) \
    PHK1024_KS_MIX1(a_out, b_out, c_out, d_out, e_out, f_out, g_out, h_out, \
        a_in, b_in, c_in, d_in, e_in, f_in, g_in, h_in) \
    PHK1024_KS_TAU(a_out, b_out, c_out, d_out, e_out, f_out, g_out, h_out) \
    GF2P8MUL2(a_out) \
    GF2P8MUL2(b_out) \
    GF2P8MUL2(c_out) \
    GF2P8MUL2(d_out) \
    GF2P8MUL2(e_out) \
    GF2P8MUL2(f_out) \
    GF2P8MUL2(g_out) \
    GF2P8MUL2(h_out) \
    // GF2P8MUL2x8(a_out, b_out, c_out, d_out, e_out, f_out, g_out, h_out)

#define PHK1024_TWEAK_WRITE(tmp_in, i) \
    ctx->rk[i * PHK1024_SUBSTATES] = _mm_xor_si128(ctx->rk[i * PHK1024_SUBSTATES], tmp_in); \
    ctx->rk[i * PHK1024_SUBSTATES + 1] = _mm_xor_si128(ctx->rk[i * PHK1024_SUBSTATES + 1], tmp_in); \
    ctx->rk[i * PHK1024_SUBSTATES + 2] = _mm_xor_si128(ctx->rk[i * PHK1024_SUBSTATES + 2], tmp_in); \
    ctx->rk[i * PHK1024_SUBSTATES + 3] = _mm_xor_si128(ctx->rk[i * PHK1024_SUBSTATES + 3], tmp_in); \
    ctx->rk[i * PHK1024_SUBSTATES + 4] = _mm_xor_si128(ctx->rk[i * PHK1024_SUBSTATES + 4], tmp_in); \
    ctx->rk[i * PHK1024_SUBSTATES + 5] = _mm_xor_si128(ctx->rk[i * PHK1024_SUBSTATES + 5], tmp_in); \
    ctx->rk[i * PHK1024_SUBSTATES + 6] = _mm_xor_si128(ctx->rk[i * PHK1024_SUBSTATES + 6], tmp_in); \
    ctx->rk[i * PHK1024_SUBSTATES + 7] = _mm_xor_si128(ctx->rk[i * PHK1024_SUBSTATES + 7], tmp_in);

#define PHK1024_TWEAK_MIX_WRITE(i) \
    PHK_TS_MIX(tmp) \
    PHK1024_TWEAK_WRITE(tmp, i)

#define PHK1024_TWEAK_IMC_WRITE(tmp_out, tmp_in, i) \
    tmp_out = _mm_aesimc_si128(tmp_in); \
    PHK1024_TWEAK_WRITE(tmp_out, i);

#define PHK1024_TWEAK_MIX_IMC_WRITE(tmp_out, tmp_in, i) \
    tmp_in = PHK_TS_MIX(tmp_in) \
    PHK1024_TWEAK_IMC_WRITE(tmp_out, tmp_in, i)

static __m128i phk1024_rc[PHK1024_STEPS * PHK_AES_ROUNDS + 1];

struct phk1024_ctx {
    __m128i rk[(PHK1024_STEPS * PHK_AES_ROUNDS + 1) * PHK1024_SUBSTATES];
    __m128i rt;
};

struct phk1024_256_ctx {
    __m128i rk[(PHK1024_256_STEPS * PHK_AES_ROUNDS + 1) * PHK1024_SUBSTATES];
    __m128i rt;
};

void phk1024_load_constants();

void phk1024_set_key_encrypt(struct phk1024_ctx *const ctx, const uint8_t *const key);
void phk1024_set_key_decrypt(struct phk1024_ctx *const ctx, const uint8_t *const key);

void phk1024_set_tweak_encrypt(struct phk1024_ctx * const ctx, const uint8_t * const tweak);
void phk1024_set_tweak_decrypt(struct phk1024_ctx * const ctx, const uint8_t * const tweak);

void phk1024_256_load_constants();

void phk1024_256_set_key_encrypt(struct phk1024_256_ctx *const ctx, const uint8_t *const key);
void phk1024_256_set_key_decrypt(struct phk1024_256_ctx *const ctx, const uint8_t *const key);

void phk1024_256_set_tweak_encrypt(struct phk1024_256_ctx * const ctx, const uint8_t * const tweak);
void phk1024_256_set_tweak_decrypt(struct phk1024_256_ctx * const ctx, const uint8_t * const tweak);

// Pholkos-1024-1024 consists of 14 Steps
void phk1024_encrypt(const struct phk1024_ctx * const ctx, uint8_t * const buf);
// Pholkos-1024-256 consists of 12 Steps
void phk1024_256_encrypt(const struct phk1024_256_ctx * const ctx, uint8_t * const buf);
// Pholkos-1024-perm consists of 16 Steps
void phk1024_perm_encrypt(const struct phk1024_ctx * const ctx, uint8_t * const buf);

// Pholkos-1024-1024 consists of 14 Steps
void phk1024_decrypt(const struct phk1024_ctx * const ctx, uint8_t * const buf);
// Pholkos-1024-256 consists of 12 Steps
void phk1024_256_decrypt(const struct phk1024_256_ctx * const ctx, uint8_t * const buf);
// Pholkos-1024-perm consists of 16 Steps
void phk1024_perm_decrypt(const struct phk1024_ctx * const ctx, uint8_t * const buf);


#endif // PHK_1024_H

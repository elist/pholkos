#include "phk-256.h"

// ---------------------------------------------------------------------

#ifndef PHK_512_H
#define PHK_512_H

// ---------------------------------------------------------------------

#define PHK512_SUBSTATES 4
#define PHK512_STEPS 10


#define PHK512_ROUND_ENC(a, b, c, d, k0, k1, k2, k3) \
    PHK256_ROUND_ENC(a, b, k0, k1) \
    PHK256_ROUND_ENC(c, d, k2, k3)

#define PHK512_ROUND_ENC_LAST(a, b, c, d, k0, k1, k2, k3) \
    PHK256_ROUND_ENC_LAST(a, b, k0, k1) \
    PHK256_ROUND_ENC_LAST(c, d, k2, k3)

#define PHK512_ROUND_DEC(a, b, c, d, k0, k1, k2, k3) \
    PHK256_ROUND_DEC(a, b, k0, k1) \
    PHK256_ROUND_DEC(c, d, k2, k3)

#define PHK512_ROUND_DEC_LAST(a, b, c, d, k0, k1, k2, k3) \
    PHK256_ROUND_DEC_LAST(a, b, k0, k1) \
    PHK256_ROUND_DEC_LAST(c, d, k2, k3)

// ( 0 5 10 15 4 9 14 3 8 13 2 7 12 1 6 11 )
#define PHK512_MIX_ENC(a_out, b_out, c_out, d_out, a_in, b_in, c_in, d_in) \
    tmp = _mm_blend_epi32(a_in, b_in, 0b0101); \
    a_out = _mm_blend_epi32(a_in, b_in, 0b1010); \
    b_out = _mm_blend_epi32(c_in, d_in, 0b0101); \
    c_out = _mm_blend_epi32(c_in, d_in, 0b1010); \
    d_out = _mm_blend_epi32(tmp, b_out, 0b1001); \
    b_out = _mm_blend_epi32(b_out, tmp, 0b1001); \
    tmp = _mm_blend_epi32(a_out, c_out, 0b1100); \
    c_out = _mm_blend_epi32(a_out, c_out, 0b0011); \
    a_out = tmp;
// ( 0 5 10 15 4 9 14 3 8 13 2 7 12 1 6 11 )
//#define PHK512_MIX_ENC(a_out, b_out, c_out, d_out, a_in, b_in, c_in, d_in) \
//    tmp = _mm_unpacklo_epi32(a_in, b_in); \
//    a_out = _mm_unpackhi_epi32(a_in, b_in); \
//    b_out = _mm_unpacklo_epi32(c_in, d_in); \
//    c_out = _mm_unpackhi_epi32(c_in, d_in); \
//    d_out = _mm_unpacklo_epi32(a_out, c_out); \
//    a_out = _mm_unpackhi_epi32(a_out, c_out); \
//    c_out = _mm_unpackhi_epi32(b_out, tmp); \
//    b_out = _mm_unpacklo_epi32(b_out, tmp);

// ( 0 13 10 7 4 1 14 11 8 5 2 15 12 9 6 3 )
#define PHK512_MIX_DEC(a_out, b_out, c_out, d_out, a_in, b_in, c_in, d_in) \
    tmp = _mm_blend_epi32(a_in, d_in, 0b1010); \
    d_out = _mm_blend_epi32(a_in, d_in, 0b0101); \
    a_out = tmp; \
    tmp = _mm_blend_epi32(b_in, c_in, 0b1010); \
    c_out = _mm_blend_epi32(b_in, c_in, 0b0101); \
    b_out = tmp; \
    tmp = _mm_blend_epi32(a_out, c_out, 0b1100); \
    c_out = _mm_blend_epi32(a_out, c_out, 0b0011); \
    a_out = tmp; \
    tmp = _mm_blend_epi32(b_out, d_out, 0b1001); \
    b_out = _mm_blend_epi32(b_out, d_out, 0b0110); \
    d_out = tmp;

#define PHK512_STEP_ENC(a, b, c, d, k0, k1, k2, k3, k4, k5, k6, k7) \
    PHK512_ROUND_ENC(a, b, c, d, k0, k1, k2, k3) \
    PHK512_ROUND_ENC(a, b, c, d, k4, k5, k6, k7) \
    PHK512_MIX_ENC(a, b, c, d, a, b, c, d)

#define PHK512_STEP_DEC(a, b, c, d, k0, k1, k2, k3, k4, k5, k6, k7) \
    PHK512_ROUND_DEC(a, b, c, d, k4, k5, k6, k7) \
    PHK512_ROUND_DEC(a, b, c, d, k0, k1, k2, k3) \
    PHK512_MIX_DEC(a, b, c, d, a, b, c, d)

#define PHK512_STEP_ENC_LAST(a, b, c, d, k0, k1, k2, k3, k4, k5, k6, k7) \
    PHK512_ROUND_ENC(a, b, c, d, k0, k1, k2, k3) \
    PHK512_ROUND_ENC_LAST(a, b, c, d, k4, k5, k6, k7)

#define PHK512_STEP_DEC_LAST(a, b, c, d, k0, k1, k2, k3, k4, k5, k6, k7) \
    PHK512_ROUND_DEC(a, b, c, d, k4, k5, k6, k7) \
    PHK512_ROUND_DEC_LAST(a, b, c, d, k0, k1, k2, k3)

#define PHK512X2_STEP_ENC(a, b, c, d, e, f, g, h, k0, k1, k2, k3, k4, k5, k6, k7) \
    PHK512_ROUND_ENC(a, b, c, d, k0, k1, k2, k3) \
    PHK512_ROUND_ENC(e, f, g, h, k0, k1, k2, k3) \
    PHK512_ROUND_ENC(a, b, c, d, k4, k5, k6, k7) \
    PHK512_ROUND_ENC(e, f, g, h, k4, k5, k6, k7) \
    PHK512_MIX_ENC(a, b, c, d, a, b, c, d) \
    PHK512_MIX_ENC(e, f, g, h, e, f, g, h)

#define PHK512X2_STEP_DEC(a, b, c, d, e, f, g, h, k0, k1, k2, k3, k4, k5, k6, k7) \
    PHK512_ROUND_DEC(a, b, c, d, k4, k5, k6, k7) \
    PHK512_ROUND_DEC(e, f, g, h, k4, k5, k6, k7) \
    PHK512_ROUND_DEC(a, b, c, d, k0, k1, k2, k3) \
    PHK512_ROUND_DEC(e, f, g, h, k0, k1, k2, k3) \
    PHK512_MIX_DEC(a, b, c, d, a, b, c, d) \
    PHK512_MIX_DEC(e, f, g, h, e, f, g, h)

#define PHK512X2_STEP_ENC_LAST(a, b, c, d, e, f, g, h, k0, k1, k2, k3, k4, k5, k6, k7) \
    PHK512_ROUND_ENC(a, b, c, d, k0, k1, k2, k3) \
    PHK512_ROUND_ENC(e, f, g, h, k0, k1, k2, k3) \
    PHK512_ROUND_ENC_LAST(a, b, c, d, k4, k5, k6, k7) \
    PHK512_ROUND_ENC_LAST(e, f, g, h, k4, k5, k6, k7)

#define PHK512X2_STEP_DEC_LAST(a, b, c, d, e, f, g, h, k0, k1, k2, k3, k4, k5, k6, k7) \
    PHK512_ROUND_DEC(a, b, c, d, k4, k5, k6, k7) \
    PHK512_ROUND_DEC(e, f, g, h, k4, k5, k6, k7) \
    PHK512_ROUND_DEC_LAST(a, b, c, d, k0, k1, k2, k3) \
    PHK512_ROUND_DEC_LAST(e, f, g, h, k0, k1, k2, k3)

/*
* 0 1 1 1  1 1 1 1
* 1 0 1 1  1 1 1 1
* 1 1 0 1  1 1 1 1
* 1 1 1 0  1 1 1 1
 *
* 1 1 1 1  0 1 1 1
* 1 1 1 1  1 0 1 1
* 1 1 1 1  1 1 0 1
* 1 1 1 1  1 1 1 0
*/
#define PHK512_KS_EXPAND(a_out, b_out, c_out, d_out, a_in, b_in) \
    a_out = a_in; \
    b_out = b_in; \
    PHK_KS_EXPAND_A(c_out, d_out, a_in, b_in)

/*
 * 1 1 0 0  1 0 0 0
 * 0 1 1 0  0 1 0 0
 * 0 0 1 1  0 0 1 0
 * 0 0 0 1  1 0 0 1
 *
 * 1 0 0 0  1 1 0 0
 * 0 1 0 0  0 1 1 0
 * 0 0 1 0  0 0 1 1
 * 1 0 0 1  0 0 0 1
 */
#define PHK512_KS_EXPAND_ALTERNATIVE(a_out, b_out, c_out, d_out, a_in, b_in) \
    a_out = a_in; \
    b_out = b_in; \
    c_out = _mm_xor_si128(a_in, b_in); \
    d_out = c_out; \
    c_out = _mm_xor_si128(c_out, _mm_alignr_epi8(a_in, b_in, 12));  \
    d_out = _mm_xor_si128(d_out, _mm_alignr_epi8(b_in, a_in, 12));

#define PHK512_KS_MIX(a_out, b_out, c_out, d_out, a_in, b_in, c_in, d_in) \
    PHK512_MIX_ENC(a_out, b_out, c_out, d_out, a_in, b_in, c_in, d_in)

#define PHK512_KS_TAU(a, b, c, d) \
    PHK_TS_MIX(a) \
    PHK_TS_MIX(b) \
    PHK_TS_MIX(c) \
    PHK_TS_MIX(d)

#define PHK512_KS_ROUND(a_out, b_out, c_out, d_out, a_in, b_in, c_in, d_in) \
    PHK512_KS_MIX(a_out, b_out, c_out, d_out, a_in, b_in, c_in, d_in) \
    PHK512_KS_TAU(a_out, b_out, c_out, d_out) \
    GF2P8MUL2(a_out) \
    GF2P8MUL2(b_out) \
    GF2P8MUL2(c_out) \
    GF2P8MUL2(d_out) \
    // GF2P8MUL2x4(a_out, b_out, c_out, d_out)

#define PHK512_TWEAK_WRITE(tmp_in, i) \
    ctx->rk[i * PHK512_SUBSTATES] = \
                      _mm_xor_si128(ctx->rk[i * PHK512_SUBSTATES], tmp_in); \
    ctx->rk[i * PHK512_SUBSTATES + 1] = \
                  _mm_xor_si128(ctx->rk[i * PHK512_SUBSTATES + 1], tmp_in); \
    ctx->rk[i * PHK512_SUBSTATES + 2] = \
                  _mm_xor_si128(ctx->rk[i * PHK512_SUBSTATES + 2], tmp_in); \
    ctx->rk[i * PHK512_SUBSTATES + 3] = \
                  _mm_xor_si128(ctx->rk[i * PHK512_SUBSTATES + 3], tmp_in);

#define PHK512_TWEAK_MIX_WRITE(i) \
    PHK_TS_MIX(tmp) \
    PHK512_TWEAK_WRITE(tmp, i)

#define PHK512_TWEAK_IMC_WRITE(tmp_out, tmp_in, i) \
    tmp_out = _mm_aesimc_si128(tmp_in); \
    PHK512_TWEAK_WRITE(tmp_out, i);

#define PHK512_TWEAK_MIX_IMC_WRITE(tmp_out, tmp_in, i) \
    tmp_in = PHK_TS_MIX(tmp_in) \
    PHK512_TWEAK_IMC_WRITE(tmp_out, tmp_in, i)

// ---------------------------------------------------------------------

static __m128i phk512_rc[PHK512_STEPS * PHK_AES_ROUNDS + 1];

struct phk512_ctx {
    __m128i rk[(PHK512_STEPS * PHK_AES_ROUNDS + 1) * PHK512_SUBSTATES];
    __m128i rt;
};

// ---------------------------------------------------------------------

void phk512_load_constants();

// ---------------------------------------------------------------------

void phk512_set_key_encrypt(struct phk512_ctx *const ctx, const uint8_t *const key);
void phk512_set_key_decrypt(struct phk512_ctx *const ctx, const uint8_t *const key);

// ---------------------------------------------------------------------

void phk512_set_key256_encrypt(struct phk512_ctx *const ctx, const uint8_t *const key);
void phk512_set_key256_decrypt(struct phk512_ctx *const ctx, const uint8_t *const key);

// ---------------------------------------------------------------------

void phk512_set_tweak_encrypt(struct phk512_ctx *const ctx, const uint8_t *const tweak);
void phk512_set_tweak_decrypt(struct phk512_ctx *const ctx, const uint8_t *const tweak);

// ---------------------------------------------------------------------

void phk512_encrypt(const struct phk512_ctx * const ctx, uint8_t * const buf);
void phk512x2_encrypt(const struct phk512_ctx * const ctx, uint8_t * const buf);

// ---------------------------------------------------------------------

void phk512_decrypt(const struct phk512_ctx * const ctx, uint8_t * const buf);
void phk512x2_decrypt(const struct phk512_ctx * const ctx, uint8_t * const buf);

// ---------------------------------------------------------------------

#endif // PHK_512_H

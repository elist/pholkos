#include <cstdint>
#include <immintrin.h>
#include <emmintrin.h>


#ifndef PHK_H
#define PHK_H


#define PHK_AES_ROUNDS 2
#define PHK_SUBSTATE_SIZE 16
#define PHK_TWEAK_SIZE 16

#define GF2P8MUL2(a) \
    tmp = _mm_blendv_epi8(_mm_setzero_si128(), _mm_set_epi32(0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b), a); \
    a = _mm_add_epi8(a, a); \
    a = _mm_xor_si128(a, tmp);

#define GF2P8MUL2x2(a, b) \
    tmp = _mm_blendv_epi8(_mm_setzero_si128(), _mm_set_epi32(0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b), a); \
    tmp1 = _mm_blendv_epi8(_mm_setzero_si128(), _mm_set_epi32(0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b), b); \
    a = _mm_add_epi8(a, a); \
    b = _mm_add_epi8(b, b); \
    a = _mm_xor_si128(a, tmp); \
    b = _mm_xor_si128(b, tmp1);

#define GF2P8MUL2x4(a, b, c, d) \
    tmp = _mm_blendv_epi8(_mm_setzero_si128(), _mm_set_epi32(0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b), a); \
    tmp1 = _mm_blendv_epi8(_mm_setzero_si128(), _mm_set_epi32(0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b), b); \
    tmp2 = _mm_blendv_epi8(_mm_setzero_si128(), _mm_set_epi32(0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b), c); \
    tmp3 = _mm_blendv_epi8(_mm_setzero_si128(), _mm_set_epi32(0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b), d); \
    a = _mm_add_epi8(a, a); \
    b = _mm_add_epi8(b, b); \
    c = _mm_add_epi8(c, c); \
    d = _mm_add_epi8(d, d);

#define GF2P8MUL2x8(a, b, c, d, e, f, g, h) \
    tmp = _mm_blendv_epi8(_mm_setzero_si128(), _mm_set_epi32(0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b), a); \
    tmp1 = _mm_blendv_epi8(_mm_setzero_si128(), _mm_set_epi32(0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b), b); \
    tmp2 = _mm_blendv_epi8(_mm_setzero_si128(), _mm_set_epi32(0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b), c); \
    tmp3 = _mm_blendv_epi8(_mm_setzero_si128(), _mm_set_epi32(0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b), d); \
    tmp4 = _mm_blendv_epi8(_mm_setzero_si128(), _mm_set_epi32(0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b), e); \
    tmp5 = _mm_blendv_epi8(_mm_setzero_si128(), _mm_set_epi32(0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b), f); \
    tmp6 = _mm_blendv_epi8(_mm_setzero_si128(), _mm_set_epi32(0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b), g); \
    tmp7 = _mm_blendv_epi8(_mm_setzero_si128(), _mm_set_epi32(0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b, 0x1b1b1b1b), h); \
    a = _mm_add_epi8(a, a); \
    b = _mm_add_epi8(b, b); \
    c = _mm_add_epi8(c, c); \
    d = _mm_add_epi8(d, d); \
    e = _mm_add_epi8(e, e); \
    f = _mm_add_epi8(f, f); \
    g = _mm_add_epi8(g, g); \
    h = _mm_add_epi8(h, h);

#define PHK_KS_EXPAND_A(a_out, b_out, a_in, b_in) \
    tmp = _mm_xor_si128(a_in, b_in); \
    a_out = _mm_alignr_epi8(b_in, a_in, 4); \
    b_out = _mm_alignr_epi8(a_in, b_in, 4); \
    a_out = _mm_xor_si128(tmp, a_out);  \
    b_out = _mm_xor_si128(tmp, b_out);

#define PHK_KS_EXPAND_B(a_out, b_out, a_in, b_in) \
    tmp = _mm_xor_si128(a_in, b_in); \
    a_out = _mm_alignr_epi8(b_in, a_in, 8); \
    b_out = _mm_alignr_epi8(a_in, b_in, 8); \
    a_out = _mm_xor_si128(tmp, a_out);  \
    b_out = _mm_xor_si128(tmp, b_out);

#define PHK_KS_EXPAND_C(a_out, b_out, a_in, b_in) \
    tmp = _mm_xor_si128(a_in, b_in); \
    a_out = _mm_alignr_epi8(b_in, a_in, 12); \
    b_out = _mm_alignr_epi8(a_in, b_in, 12); \
    a_out = _mm_xor_si128(tmp, a_out);  \
    b_out = _mm_xor_si128(tmp, b_out);

#define PHK_TS_MIX(t) \
    t = _mm_shuffle_epi8(t, _mm_set_epi8(14, 13, 8, 7, 10, 9, 4, 3, 6, 5, 0,15, 2, 1, 12, 11));
    // t = _mm_shuffle_epi8(t, _mm_set_epi8(11, 12, 1, 2, 15, 0, 5, 6, 3, 4, 9, 10, 7, 8, 13, 14));

//    t = _mm_shuffle_epi8(t, _mm_set_epi32(0x0d0e0c05, 0x000b0906, 0x0302040a, 0x0f070108));

const __m128i FLIP = _mm_set_epi32(0x80000000, 0, 0x80000000, 0);
const __m128i CARRY = _mm_set_epi32(0, 1, 0, 0);
const __m128i ZERO = _mm_setzero_si128();
const __m128i ONE = _mm_set_epi8(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1);
const __m128i TWO = _mm_set_epi8(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2);
const __m128i THREE = _mm_set_epi8(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3);
const __m128i FOUR = _mm_set_epi8(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4);
const __m128i FIVE = _mm_set_epi8(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5);
const __m128i SIX = _mm_set_epi8(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6);
const __m128i SEVEN = _mm_set_epi8(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7);
const __m128i REVERSE = _mm_set_epi8(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15);
//const __m128i REVERSE = _mm_set_epi8(15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0);

#define PHK_INC_THREE_CTRS(t, c1, c2, c3) \
   t = _mm_shuffle_epi8(t, REVERSE); \
   c1 = _mm_add_epi64(t, ONE); c2 = _mm_add_epi64(t, TWO); \
   c3 = _mm_add_epi64(t, THREE); \
   tmp = _mm_xor_si128(t, FLIP); tmp1 = _mm_xor_si128(c1, FLIP); \
   tmp2 = _mm_xor_si128(c2, FLIP); tmp3 = _mm_xor_si128(c3, FLIP); \
   tmp1 = _mm_cmpgt_epi64(tmp, tmp1); tmp2 = _mm_cmpgt_epi64(tmp, tmp2); \
   tmp3 = _mm_cmpgt_epi64(tmp, tmp3); \
   tmp1 = _mm_unpacklo_epi64(ZERO, tmp1); \
   tmp2 = _mm_unpacklo_epi64(ZERO, tmp2); \
   tmp3 = _mm_unpacklo_epi64(ZERO, tmp3); \
   tmp1 = _mm_and_si128(CARRY, tmp1); \
   tmp2 = _mm_and_si128(CARRY, tmp2); \
   tmp3 = _mm_and_si128(CARRY, tmp3); \
   c1 = _mm_add_epi64(c1, tmp1); \
   c2 = _mm_add_epi64(c2, tmp2); \
   c3 = _mm_add_epi64(c3, tmp3); \
   c1 = _mm_shuffle_epi8(c1, REVERSE); \
   c2 = _mm_shuffle_epi8(c2, REVERSE); \
   c3 = _mm_shuffle_epi8(c3, REVERSE); \

#define PHK_INC_FOUR_CTRS(t, c0, c1, c2, c3) \
   t = _mm_shuffle_epi8(t, REVERSE); \
   c1 = _mm_add_epi64(t, FIVE); c2 = _mm_add_epi64(t, SIX); \
   c3 = _mm_add_epi64(t, SEVEN); c0 = _mm_add_epi64(t, FOUR);\
   tmp = _mm_xor_si128(t, FLIP); tmp1 = _mm_xor_si128(c1, FLIP); \
   tmp2 = _mm_xor_si128(c2, FLIP); tmp3 = _mm_xor_si128(c3, FLIP); \
   tmp4 = _mm_xor_si128(c0, FLIP); \
   tmp1 = _mm_cmpgt_epi64(tmp, tmp1); tmp2 = _mm_cmpgt_epi64(tmp, tmp2); \
   tmp3 = _mm_cmpgt_epi64(tmp, tmp3); tmp4 = _mm_cmpgt_epi64(tmp, tmp4); \
   tmp4 = _mm_unpacklo_epi64(ZERO, tmp4); \
   tmp1 = _mm_unpacklo_epi64(ZERO, tmp1); \
   tmp2 = _mm_unpacklo_epi64(ZERO, tmp2); \
   tmp3 = _mm_unpacklo_epi64(ZERO, tmp3); \
   tmp4 = _mm_and_si128(CARRY, tmp4); \
   tmp1 = _mm_and_si128(CARRY, tmp1); \
   tmp2 = _mm_and_si128(CARRY, tmp2); \
   tmp3 = _mm_and_si128(CARRY, tmp3); \
   c0 = _mm_add_epi64(c0, tmp4); \
   c1 = _mm_add_epi64(c1, tmp1); \
   c2 = _mm_add_epi64(c2, tmp2); \
   c3 = _mm_add_epi64(c3, tmp3); \
   c0 = _mm_shuffle_epi8(c0, REVERSE); \
   c1 = _mm_shuffle_epi8(c1, REVERSE); \
   c2 = _mm_shuffle_epi8(c2, REVERSE); \
   c3 = _mm_shuffle_epi8(c3, REVERSE);



uint8_t *phk_create_buffer(size_t len);

#endif // PHK_H

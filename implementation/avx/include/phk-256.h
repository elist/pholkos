#include "phk.h"


#ifndef PHK_256_H
#define PHK_256_H


#define PHK256_SUBSTATES 2
#define PHK256_STEPS 8
#define PHK256_NUM_KEY_BYTES 32
#define PHK256_NUM_TWEAK_BYTES 16
#define PHK256_256_NUM_TWEAK_BYTES 32
#define PHK256_NUM_STATE_BYTES 32

#define PHK256_ROUND_ENC(a, b, k0, k1) \
    a = _mm_aesenc_si128(a, k0); \
    b = _mm_aesenc_si128(b, k1);

#define PHK256_ROUND_ENC_LAST(a, b, k0, k1) \
    a = _mm_aesenclast_si128(a, k0); \
    b = _mm_aesenclast_si128(b, k1);

#define PHK256_ROUND_DEC(a, b, k0, k1) \
    a = _mm_aesdec_si128(a, k0); \
    b = _mm_aesdec_si128(b, k1);

#define PHK256_ROUND_DEC_LAST(a, b, k0, k1) \
    a = _mm_aesdeclast_si128(a, k0); \
    b = _mm_aesdeclast_si128(b, k1);

// ( 0 5 2 7 4 1 6 3 )
#define PHK256_MIX_ENC(a_out, b_out, a_in, b_in) \
    tmp = _mm_blend_epi32(a_in, b_in, 0b0101); \
    a_out = _mm_blend_epi32(a_in, b_in, 0b1010); \
    b_out = tmp;

// ( 0 5 2 7 4 1 6 3 )
#define PHK256_MIX_DEC(a_out, b_out, a_in, b_in) \
    PHK256_MIX_ENC(a_out, b_out, a_in, b_in)

#define PHK256_STEP_ENC(a, b, k0, k1, k2, k3) \
    PHK256_ROUND_ENC(a, b, k0, k1) \
    PHK256_ROUND_ENC(a, b, k2, k3) \
    PHK256_MIX_ENC(a, b, a, b)

#define PHK256_STEP_DEC(a, b, k0, k1, k2, k3) \
    PHK256_ROUND_DEC(a, b, k2, k3) \
    PHK256_ROUND_DEC(a, b, k0, k1) \
    PHK256_MIX_DEC(a, b, a, b)


#define PHK256_STEP_ENC_LAST(a, b, k0, k1, k2, k3) \
    PHK256_ROUND_ENC(a, b, k0, k1) \
    PHK256_ROUND_ENC_LAST(a, b, k2, k3)

#define PHK256_STEP_DEC_LAST(a, b, k0, k1, k2, k3) \
    PHK256_ROUND_DEC(a, b, k2, k3) \
    PHK256_ROUND_DEC_LAST(a, b, k0, k1)

#define PHK256X2_STEP_ENC(a, b, c, d, k0, k1, k2, k3) \
    PHK256_ROUND_ENC(a, b, k0, k1) \
    PHK256_ROUND_ENC(c, d, k0, k1) \
    PHK256_ROUND_ENC(a, b, k2, k3) \
    PHK256_ROUND_ENC(c, d, k2, k3) \
    PHK256_MIX_ENC(a, b, a, b) \
    PHK256_MIX_ENC(c, d, c, d)

#define PHK256X2_STEP_DEC(a, b, c, d, k0, k1, k2, k3) \
    PHK256_ROUND_DEC(a, b, k2, k3) \
    PHK256_ROUND_DEC(c, d, k2, k3) \
    PHK256_ROUND_DEC(a, b, k0, k1) \
    PHK256_ROUND_DEC(c, d, k0, k1) \
    PHK256_MIX_DEC(a, b, a, b) \
    PHK256_MIX_DEC(c, d, c, d)

#define PHK256X2_STEP_ENC_LAST(a, b, c, d, k0, k1, k2, k3) \
    PHK256_ROUND_ENC(a, b, k0, k1) \
    PHK256_ROUND_ENC(c, d, k0, k1) \
    PHK256_ROUND_ENC_LAST(a, b, k2, k3) \
    PHK256_ROUND_ENC_LAST(c, d, k2, k3)

#define PHK256X2_STEP_DEC_LAST(a, b, c, d, k0, k1, k2, k3) \
    PHK256_ROUND_DEC(a, b, k2, k3) \
    PHK256_ROUND_DEC(c, d, k2, k3) \
    PHK256_ROUND_DEC_LAST(a, b, k0, k1) \
    PHK256_ROUND_DEC_LAST(c, d, k0, k1)

#define PHK256X4_STEP_ENC(a, b, c, d, e, f, g, h, k0, k1, k2, k3) \
    PHK256_ROUND_ENC(a, b, k0, k1) \
    PHK256_ROUND_ENC(c, d, k0, k1) \
    PHK256_ROUND_ENC(e, f, k0, k1) \
    PHK256_ROUND_ENC(g, h, k0, k1) \
    PHK256_ROUND_ENC(a, b, k2, k3) \
    PHK256_ROUND_ENC(c, d, k2, k3) \
    PHK256_ROUND_ENC(e, f, k2, k3) \
    PHK256_ROUND_ENC(g, h, k2, k3) \
    PHK256_MIX_ENC(a, b, a, b) \
    PHK256_MIX_ENC(c, d, c, d) \
    PHK256_MIX_ENC(e, f, e, f) \
    PHK256_MIX_ENC(g, h, g, h)

#define PHK256X4_STEP_DEC(a, b, c, d, e, f, g, h, k0, k1, k2, k3) \
    PHK256_ROUND_DEC(a, b, k2, k3) \
    PHK256_ROUND_DEC(c, d, k2, k3) \
    PHK256_ROUND_DEC(e, f, k2, k3) \
    PHK256_ROUND_DEC(g, h, k2, k3) \
    PHK256_ROUND_DEC(a, b, k0, k1) \
    PHK256_ROUND_DEC(c, d, k0, k1) \
    PHK256_ROUND_DEC(e, f, k0, k1) \
    PHK256_ROUND_DEC(g, h, k0, k1) \
    PHK256_MIX_DEC(a, b, a, b) \
    PHK256_MIX_DEC(c, d, c, d) \
    PHK256_MIX_DEC(e, f, e, f) \
    PHK256_MIX_DEC(g, h, g, h)

#define PHK256X4_STEP_ENC_LAST(a, b, c, d, e, f, g, h, k0, k1, k2, k3) \
    PHK256_ROUND_ENC(a, b, k0, k1) \
    PHK256_ROUND_ENC(c, d, k0, k1) \
    PHK256_ROUND_ENC(e, f, k0, k1) \
    PHK256_ROUND_ENC(g, h, k0, k1) \
    PHK256_ROUND_ENC_LAST(a, b, k2, k3) \
    PHK256_ROUND_ENC_LAST(c, d, k2, k3) \
    PHK256_ROUND_ENC_LAST(e, f, k2, k3) \
    PHK256_ROUND_ENC_LAST(g, h, k2, k3)

#define PHK256X4_STEP_DEC_LAST(a, b, c, d, e, f, g, h, k0, k1, k2, k3) \
    PHK256_ROUND_DEC(a, b, k2, k3) \
    PHK256_ROUND_DEC(c, d, k2, k3) \
    PHK256_ROUND_DEC(e, f, k2, k3) \
    PHK256_ROUND_DEC(g, h, k2, k3) \
    PHK256_ROUND_DEC_LAST(a, b, k0, k1) \
    PHK256_ROUND_DEC_LAST(c, d, k0, k1) \
    PHK256_ROUND_DEC_LAST(e, f, k0, k1) \
    PHK256_ROUND_DEC_LAST(g, h, k0, k1)

#define PHK256_KS_MIX(a_out, b_out, a_in, b_in) \
    PHK256_MIX_ENC(a_out, b_out, a_in, b_in)

#define PHK256_KS_TAU(a, b) \
    PHK_TS_MIX(a) \
    PHK_TS_MIX(b)

#define PHK256_KS_ROUND(a_out, b_out, a_in, b_in) \
    PHK256_KS_MIX(a_out, b_out, a_in, b_in) \
    PHK256_KS_TAU(a_out, b_out) \
    GF2P8MUL2(a_out) \
    GF2P8MUL2(b_out) \
    // GF2P8MUL2x2(a_out, b_out)

/* 256-128 tweak mix stuff */

#define PHK256_TWEAK_WRITE(tmp_in, i) \
    ctx->rk[i * PHK256_SUBSTATES] = _mm_xor_si128(ctx->rk[i * PHK256_SUBSTATES], tmp_in); \
    ctx->rk[i * PHK256_SUBSTATES + 1] = _mm_xor_si128(ctx->rk[i * PHK256_SUBSTATES + 1], tmp_in);

#define PHK256_TWEAK_MIX_WRITE(i) \
    PHK_TS_MIX(tmp) \
    PHK256_TWEAK_WRITE(tmp, i)

#define PHK256_TWEAK_IMC_WRITE(tmp_out, tmp_in, i) \
    tmp_out = _mm_aesimc_si128(tmp_in); \
    PHK256_TWEAK_WRITE(tmp_out, i);

#define PHK256_TWEAK_MIX_IMC_WRITE(tmp_out, tmp_in, i) \
    tmp_in = PHK_TS_MIX(tmp_in) \
    PHK256_TWEAK_IMC_WRITE(tmp_out, tmp_in, i)

/* 256-256 tweak mix stuff */
#define PHK256_256_TWEAK_WRITE(tmp, i) \
    ctx->rk[i * PHK256_SUBSTATES] = _mm_xor_si128(ctx->rk[i * PHK256_SUBSTATES], tmp[0]); \
    ctx->rk[i * PHK256_SUBSTATES + 1] = _mm_xor_si128(ctx->rk[i * PHK256_SUBSTATES + 1], tmp[1]);

#define PHK256_256_TWEAK_MIX_WRITE(tmp, i) \
    tmp_256_twk = _mm_blend_epi32(tmp[0], tmp[1], 0b0101); \
    tmp[0] = _mm_blend_epi32(tmp[0], tmp[1], 0b1010); \
    tmp[1] = tmp_256_twk;\
    PHK256_256_TS_MIX(tmp) \
    PHK256_256_TWEAK_WRITE(tmp, i)

#define PHK256_256_TWEAK_IMC_WRITE(tmp, i) \
    PHK256_256_TWEAK_WRITE(tmp, i);/* \
    ctx->rk[i * PHK256_SUBSTATES] = _mm_aesimc_si128(ctx->rk[i * PHK256_SUBSTATES]);\
    ctx->rk[i * PHK256_SUBSTATES + 1] = _mm_aesimc_si128(ctx->rk[i * PHK256_SUBSTATES + 1]);*/

#define PHK256_256_TWEAK_MIX_IMC_WRITE(tmp, i) \
    tmp_256_twk = _mm_blend_epi32(tmp[0], tmp[1], 0b0101); \
    tmp[0] = _mm_blend_epi32(tmp[0], tmp[1], 0b1010); \
    tmp[1] = tmp_256_twk;\
    PHK256_256_TS_MIX(tmp) \
    PHK256_256_TWEAK_IMC_WRITE(tmp, i)

#define PHK256_256_TS_MIX(t) \
    t[0] = _mm_shuffle_epi8(t[0], _mm_set_epi8(14, 13, 8, 7, 10, 9, 4, 3, 6, 5, 0,15, 2, 1, 12, 11));\
    t[1] = _mm_shuffle_epi8(t[1], _mm_set_epi8(14, 13, 8, 7, 10, 9, 4, 3, 6, 5, 0,15, 2, 1, 12, 11));

struct phk256_ctx {
    __m128i rk[(PHK256_STEPS * PHK_AES_ROUNDS + 1) * PHK256_SUBSTATES];
    __m128i rt;
    __m128i rc[PHK256_STEPS * PHK_AES_ROUNDS + 1];
};

struct phk256_256_ctx {
    __m128i rk[(PHK256_STEPS * PHK_AES_ROUNDS + 1) * PHK256_SUBSTATES];
    __m128i rt[2];
    __m128i rc[PHK256_STEPS * PHK_AES_ROUNDS + 1];
};

void phk256_load_constants(struct phk256_ctx *const ctx);
void phk256_256_load_constants(struct phk256_256_ctx *const ctx);

void phk256_set_key_encrypt(struct phk256_ctx *const ctx, const uint8_t *const key);
void phk256_set_key_decrypt(struct phk256_ctx *const ctx, const uint8_t *const key);

void phk256_set_tweak_encrypt(struct phk256_ctx *const ctx, const uint8_t *const tweak);
void phk256_set_tweak_decrypt(struct phk256_ctx *const ctx, const uint8_t *const tweak);

void phk256_set_256_tweak_encrypt(struct phk256_256_ctx *const ctx, const uint8_t *const tweak);
void phk256_set_256_tweak_decrypt(struct phk256_256_ctx *const ctx, const uint8_t *const tweak);
void phk256_256_set_key_encrypt(struct phk256_256_ctx *const ctx, const uint8_t *const key);
void phk256_256_set_key_decrypt(struct phk256_256_ctx *const ctx, const uint8_t *const key);

void phk256_update_tweak_encrypt(struct phk256_ctx *ctx, const uint8_t *const tweak_diff);
void phk256_256_update_tweak_encrypt(struct phk256_256_ctx *ctx, const uint8_t *const tweak_diff);
void phk256_update_4tweak_encrypt(struct phk256_ctx *ctx1, __m128i tweak_diff1,\
                          struct phk256_ctx *ctx2, __m128i tweak_diff2,\
                          struct phk256_ctx *ctx3, __m128i tweak_diff3,\
                          struct phk256_ctx *ctx4, __m128i tweak_diff4);

void phk256_encrypt(const struct phk256_ctx * const ctx, uint8_t * const buf);
void phk256x2_encrypt(const struct phk256_ctx * const ctx, uint8_t * const buf);
void phk256x4_encrypt(const struct phk256_ctx * const ctx, uint8_t * const buf);
void phk256_256_encrypt(const struct phk256_256_ctx * const ctx, uint8_t * const buf);

// pipelined block encryption with different tweakeys
void phk256x4_encrypt_diff_tweakeys(struct phk256_ctx * ctx1, uint8_t* buf1, \
                                     struct phk256_ctx * ctx2, uint8_t* buf2, \
                                     struct phk256_ctx * ctx3, uint8_t* buf3, \
                                     struct phk256_ctx * ctx4, uint8_t* buf4);

void phk256_decrypt(const struct phk256_ctx * const ctx, uint8_t * const buf);
void phk256x2_decrypt(const struct phk256_ctx * const ctx, uint8_t * const buf);
void phk256x4_decrypt(const struct phk256_ctx * const ctx, uint8_t * const buf);
void phk256_256_decrypt(const struct phk256_256_ctx * const ctx, uint8_t * const buf);

#endif // PHK_256_H

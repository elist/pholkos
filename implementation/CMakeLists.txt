# ----------------------------------------------------------
# Common
# ----------------------------------------------------------

cmake_minimum_required(VERSION 3.7 FATAL_ERROR)

# ----------------------------------------------------------
# Project variables and paths
# ----------------------------------------------------------

# Project Name
project(phk-implementation)

# Store Variables
set(PROJECT_SOURCES_ROOT_DIR "${CMAKE_CURRENT_SOURCE_DIR}")
set(PROJECT_REF_DIR "${PROJECT_SOURCES_ROOT_DIR}/ref")
set(PROJECT_REF_SOURCES_DIR "${PROJECT_REF_DIR}/src")
set(PROJECT_REF_INCLUDE_DIR "${PROJECT_REF_DIR}/include")
set(PROJECT_REF_TESTS_DIR "${PROJECT_REF_DIR}/tests")
set(PROJECT_REF_PROGRAMS_DIR "${PROJECT_REF_DIR}/programs")
set(PROJECT_REF_INCLUDE_DIRECTORIES ${PROJECT_REF_SOURCES_DIR} ${PROJECT_REF_INCLUDE_DIR})

set(PROJECT_AVX_DIR "${PROJECT_SOURCES_ROOT_DIR}/avx")
set(PROJECT_AVX_SOURCES_DIR "${PROJECT_AVX_DIR}/src")
set(PROJECT_AVX_INCLUDE_DIR "${PROJECT_AVX_DIR}/include")
set(PROJECT_AVX_TESTS_DIR "${PROJECT_AVX_DIR}/tests")
set(PROJECT_AVX_PROGRAMS_DIR "${PROJECT_AVX_DIR}/programs")
set(PROJECT_AVX_INCLUDE_DIRECTORIES ${PROJECT_AVX_SOURCES_DIR} ${PROJECT_AVX_INCLUDE_DIR})

set(CMAKE_BINARY_DIR "${PROJECT_SOURCES_ROOT_DIR}/bin")
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_DEBUG "${CMAKE_BINARY_DIR}/lib")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}")
set(EXECUTABLE_OUTPUT_PATH "${CMAKE_BINARY_DIR}")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG "${CMAKE_BINARY_DIR}")

# Add include paths
include_directories(${INCLUDE_DIRECTORIES})

file(GLOB PROJECT_AVX_CXX "${PROJECT_AVX_SOURCES_DIR}/*.cpp")
file(GLOB PROJECT_AVX_C "${PROJECT_AVX_SOURCES_DIR}/*.c")

file(GLOB PROJECT_REF_CXX "${PROJECT_REF_SOURCES_DIR}/*.cpp")
file(GLOB PROJECT_REF_C "${PROJECT_REF_SOURCES_DIR}/*.c")

# ----------------------------------------------------------
# Compilation Properties
# ----------------------------------------------------------

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_COMPILER "clang++")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -W -Wall -Wextra -pedantic -std=c++11 -mavx2 -march=native -O3 -funroll-loops")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS} -DNDEBUG -msse4.1 -mavx2 -mavx -maes -O3 -std=c++11 -march=native")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS} -DDEBUG -Wconversion -Wsign-conversion -fsanitize=alignment,address,undefined")

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_COMPILER "clang")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -W -Wall -Wextra -pedantic -std=c11 -mavx2 -march=native -O3")
set(CMAKE_C_FLAGS_RELEASE "${CMAKE_CXX_FLAGS} -DNDEBUG -msse4.1 -mavx2 -mavx -maes -O3 -std=c++11 -march=native")
set(CMAKE_C_FLAGS_DEBUG "${CMAKE_CXX_FLAGS} -DDEBUG -Wconversion -Wsign-conversion -fsanitize=alignment,address,undefined")

set(CMAKE_BUILD_TYPE Release)

if (NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Debug)
endif (NOT CMAKE_BUILD_TYPE)

# Logging
message("Using build type ${CMAKE_BUILD_TYPE}")

# ----------------------------------------------------------
# Libraries
# ----------------------------------------------------------

add_library(phk-avx
        ${PROJECT_AVX_INCLUDE_DIR}/phk.h
        ${PROJECT_AVX_SOURCES_DIR}/phk.cpp
        ${PROJECT_AVX_INCLUDE_DIR}/phk-256.h
        ${PROJECT_AVX_SOURCES_DIR}/phk-256.cpp
        ${PROJECT_AVX_INCLUDE_DIR}/phk-512.h
        ${PROJECT_AVX_SOURCES_DIR}/phk-512.cpp
        ${PROJECT_AVX_INCLUDE_DIR}/phk-1024.h
        ${PROJECT_AVX_SOURCES_DIR}/phk-1024.cpp)

# ----------------------------------------------------------
# Building AVX Executables
# ----------------------------------------------------------

add_executable(benchmark-phk-avx
        ${PROJECT_AVX_SOURCES_DIR}/benchmark.cpp
        ${PROJECT_AVX_INCLUDE_DIR}/misc.h
        ${PROJECT_AVX_SOURCES_DIR}/misc.cpp
        ${PROJECT_AVX_INCLUDE_DIR}/simpira.h
        ${PROJECT_AVX_SOURCES_DIR}/simpira.cpp)

add_executable(test-add-avx
    ${PROJECT_AVX_SOURCES_DIR}/test_add.cpp
    ${PROJECT_AVX_INCLUDE_DIR}/misc.h
    ${PROJECT_AVX_SOURCES_DIR}/misc.cpp)

add_executable(test-phk-avx
    ${PROJECT_AVX_SOURCES_DIR}/test.cpp
    ${PROJECT_AVX_INCLUDE_DIR}/misc.h
    ${PROJECT_AVX_SOURCES_DIR}/misc.cpp)

add_executable(test-phk-256-256-avx
        ${PROJECT_AVX_SOURCES_DIR}/test_phk-256-256.cpp
        ${PROJECT_AVX_INCLUDE_DIR}/misc.h
        ${PROJECT_AVX_SOURCES_DIR}/misc.cpp)

add_executable(test-phk-256-avx
        ${PROJECT_AVX_SOURCES_DIR}/test_phk-256.cpp
        ${PROJECT_AVX_INCLUDE_DIR}/misc.h
        ${PROJECT_AVX_SOURCES_DIR}/misc.cpp)

add_executable(test-ctrt-avx
        ${PROJECT_AVX_SOURCES_DIR}/test_ctrt.cpp
        ${PROJECT_AVX_SOURCES_DIR}/ctrt_phk.cpp
        ${PROJECT_AVX_INCLUDE_DIR}/ctrt_phk.h
        ${PROJECT_AVX_INCLUDE_DIR}/misc.h
        ${PROJECT_AVX_SOURCES_DIR}/misc.cpp)


add_executable(test-phk-512-avx
        ${PROJECT_AVX_SOURCES_DIR}/test_phk-512.cpp
        ${PROJECT_AVX_INCLUDE_DIR}/misc.h
        ${PROJECT_AVX_SOURCES_DIR}/misc.cpp)

add_executable(test-phk-1024-avx
        ${PROJECT_AVX_SOURCES_DIR}/test_phk-1024.cpp
        ${PROJECT_AVX_INCLUDE_DIR}/misc.h
        ${PROJECT_AVX_SOURCES_DIR}/misc.cpp)

add_executable(test-simpira-avx
        ${PROJECT_AVX_SOURCES_DIR}/test_simpira.cpp
        ${PROJECT_AVX_INCLUDE_DIR}/misc.h
        ${PROJECT_AVX_SOURCES_DIR}/misc.cpp
        ${PROJECT_AVX_INCLUDE_DIR}/simpira.h
        ${PROJECT_AVX_SOURCES_DIR}/simpira.cpp)

add_executable(create-test-vectors-phk-avx
        ${PROJECT_AVX_SOURCES_DIR}/create-test-vectors.cpp
        ${PROJECT_AVX_INCLUDE_DIR}/misc.h
        ${PROJECT_AVX_SOURCES_DIR}/misc.cpp)

add_executable(generate-constants-phk-avx
        ${PROJECT_AVX_SOURCES_DIR}/generate-constants.cpp
        ${PROJECT_AVX_INCLUDE_DIR}/pi.h
        ${PROJECT_AVX_SOURCES_DIR}/pi.cpp)

target_include_directories(phk-avx PRIVATE ${PROJECT_AVX_INCLUDE_DIR})
target_include_directories(benchmark-phk-avx PRIVATE ${PROJECT_AVX_INCLUDE_DIR})
target_include_directories(test-add-avx PRIVATE ${PROJECT_AVX_INCLUDE_DIR})
target_include_directories(test-phk-avx PRIVATE ${PROJECT_AVX_INCLUDE_DIR})
target_include_directories(test-phk-256-256-avx PRIVATE ${PROJECT_AVX_INCLUDE_DIR})
target_include_directories(test-phk-256-avx PRIVATE ${PROJECT_AVX_INCLUDE_DIR})
target_include_directories(test-ctrt-avx PRIVATE ${PROJECT_AVX_INCLUDE_DIR})
target_include_directories(test-phk-512-avx PRIVATE ${PROJECT_AVX_INCLUDE_DIR})
target_include_directories(test-phk-1024-avx PRIVATE ${PROJECT_AVX_INCLUDE_DIR})
target_include_directories(test-simpira-avx PRIVATE ${PROJECT_AVX_INCLUDE_DIR})
target_include_directories(create-test-vectors-phk-avx PRIVATE ${PROJECT_AVX_INCLUDE_DIR})
target_include_directories(generate-constants-phk-avx PRIVATE ${PROJECT_AVX_INCLUDE_DIR})

target_link_libraries(benchmark-phk-avx phk-avx)
target_link_libraries(test-add-avx -ljsoncpp)
target_link_libraries(test-phk-avx phk-avx -ljsoncpp)
target_link_libraries(test-phk-256-256-avx phk-avx -ljsoncpp)
target_link_libraries(test-phk-256-avx phk-avx -ljsoncpp)
target_link_libraries(test-ctrt-avx phk-avx -ljsoncpp)
target_link_libraries(test-phk-512-avx phk-avx -ljsoncpp)
target_link_libraries(test-phk-1024-avx phk-avx -ljsoncpp)
target_link_libraries(create-test-vectors-phk-avx phk-avx)

# ----------------------------------------------------------
# Building Reference Executables
# ----------------------------------------------------------

add_executable(test-ctrt-ref
        ${PROJECT_REF_TESTS_DIR}/test-ctrt.cpp
        ${PROJECT_REF_INCLUDE_DIR}/aes.h
        ${PROJECT_REF_INCLUDE_DIR}/misc.h
        ${PROJECT_REF_INCLUDE_DIR}/utils.h
        ${PROJECT_REF_INCLUDE_DIR}/phk.h
        ${PROJECT_REF_INCLUDE_DIR}/phk-256.h
        ${PROJECT_REF_INCLUDE_DIR}/ctrt.h
        ${PROJECT_REF_SOURCES_DIR}/ctrt.cpp
        ${PROJECT_REF_SOURCES_DIR}/phk-256.c
        ${PROJECT_REF_SOURCES_DIR}/aes.c
        ${PROJECT_REF_SOURCES_DIR}/misc.cpp
        ${PROJECT_REF_SOURCES_DIR}/utils.c)

add_executable(test-phk-256-256-ref
        ${PROJECT_REF_TESTS_DIR}/test-phk-256-256.cpp
        ${PROJECT_REF_INCLUDE_DIR}/aes.h
        ${PROJECT_REF_INCLUDE_DIR}/misc.h
        ${PROJECT_REF_INCLUDE_DIR}/utils.h
        ${PROJECT_REF_INCLUDE_DIR}/phk.h
        ${PROJECT_REF_INCLUDE_DIR}/phk-256.h
        ${PROJECT_REF_SOURCES_DIR}/phk-256.c
        ${PROJECT_REF_SOURCES_DIR}/aes.c
        ${PROJECT_REF_SOURCES_DIR}/misc.cpp
        ${PROJECT_REF_SOURCES_DIR}/utils.c)

add_executable(test-phk-256-ref
        ${PROJECT_REF_TESTS_DIR}/test-phk-256.cpp
        ${PROJECT_REF_INCLUDE_DIR}/aes.h
        ${PROJECT_REF_INCLUDE_DIR}/misc.h
        ${PROJECT_REF_INCLUDE_DIR}/utils.h
        ${PROJECT_REF_INCLUDE_DIR}/phk.h
        ${PROJECT_REF_INCLUDE_DIR}/phk-256.h
        ${PROJECT_REF_SOURCES_DIR}/phk-256.c
        ${PROJECT_REF_SOURCES_DIR}/aes.c
        ${PROJECT_REF_SOURCES_DIR}/misc.cpp
        ${PROJECT_REF_SOURCES_DIR}/utils.c)

add_executable(test-phk-512-ref
        ${PROJECT_REF_TESTS_DIR}/test-phk-512.cpp
        ${PROJECT_REF_INCLUDE_DIR}/aes.h
        ${PROJECT_REF_INCLUDE_DIR}/misc.h
        ${PROJECT_REF_INCLUDE_DIR}/utils.h
        ${PROJECT_REF_INCLUDE_DIR}/phk.h
        ${PROJECT_REF_INCLUDE_DIR}/phk-512.h
        ${PROJECT_REF_SOURCES_DIR}/phk-512.c
        ${PROJECT_REF_SOURCES_DIR}/phk.c
        ${PROJECT_REF_SOURCES_DIR}/aes.c
        ${PROJECT_REF_SOURCES_DIR}/misc.cpp
        ${PROJECT_REF_SOURCES_DIR}/utils.c)

add_executable(test-phk-1024-ref
        ${PROJECT_REF_TESTS_DIR}/test-phk-1024.cpp
        ${PROJECT_REF_INCLUDE_DIR}/aes.h
        ${PROJECT_REF_INCLUDE_DIR}/misc.h
        ${PROJECT_REF_INCLUDE_DIR}/utils.h
        ${PROJECT_REF_INCLUDE_DIR}/phk.h
        ${PROJECT_REF_INCLUDE_DIR}/phk-1024.h
        ${PROJECT_REF_SOURCES_DIR}/phk-1024.c
        ${PROJECT_REF_SOURCES_DIR}/phk.c
        ${PROJECT_REF_SOURCES_DIR}/aes.c
        ${PROJECT_REF_SOURCES_DIR}/misc.cpp
        ${PROJECT_REF_SOURCES_DIR}/utils.c)

add_executable(test-aes-round
        ${PROJECT_REF_TESTS_DIR}/test-aes-round.cpp
        ${PROJECT_REF_INCLUDE_DIR}/aes.h
        ${PROJECT_REF_INCLUDE_DIR}/misc.h
        ${PROJECT_REF_INCLUDE_DIR}/utils.h
        ${PROJECT_REF_SOURCES_DIR}/misc.cpp
        ${PROJECT_REF_SOURCES_DIR}/aes.c
        ${PROJECT_REF_SOURCES_DIR}/utils.c)

add_executable(create-test-vectors-phk-ref
        ${PROJECT_REF_SOURCES_DIR}/create-tests-ref.cpp
        ${PROJECT_REF_INCLUDE_DIR}/aes.h
        ${PROJECT_REF_INCLUDE_DIR}/misc.h
        ${PROJECT_REF_INCLUDE_DIR}/utils.h
        ${PROJECT_REF_INCLUDE_DIR}/phk.h
        ${PROJECT_REF_INCLUDE_DIR}/phk-256.h
        ${PROJECT_REF_INCLUDE_DIR}/phk-512.h
        ${PROJECT_REF_INCLUDE_DIR}/phk-1024.h
        ${PROJECT_REF_INCLUDE_DIR}/ctrt.h
        ${PROJECT_REF_SOURCES_DIR}/ctrt.cpp
        ${PROJECT_REF_SOURCES_DIR}/phk-1024.c
        ${PROJECT_REF_SOURCES_DIR}/phk-512.c
        ${PROJECT_REF_SOURCES_DIR}/phk-256.c
        ${PROJECT_REF_SOURCES_DIR}/phk.c
        ${PROJECT_REF_SOURCES_DIR}/aes.c
        ${PROJECT_REF_SOURCES_DIR}/misc.cpp
        ${PROJECT_REF_SOURCES_DIR}/utils.c)

target_include_directories(test-ctrt-ref PRIVATE ${PROJECT_REF_INCLUDE_DIR})
target_include_directories(test-phk-256-256-ref PRIVATE ${PROJECT_REF_INCLUDE_DIR})
target_include_directories(test-phk-256-ref PRIVATE ${PROJECT_REF_INCLUDE_DIR})
target_include_directories(test-phk-512-ref PRIVATE ${PROJECT_REF_INCLUDE_DIR})
target_include_directories(test-phk-1024-ref PRIVATE ${PROJECT_REF_INCLUDE_DIR})
target_include_directories(test-aes-round PRIVATE ${PROJECT_REF_INCLUDE_DIR})
target_include_directories(create-test-vectors-phk-ref PRIVATE ${PROJECT_REF_INCLUDE_DIR})

target_link_libraries(test-ctrt-ref -ljsoncpp)
target_link_libraries(test-phk-256-256-ref -ljsoncpp)
target_link_libraries(test-phk-256-ref -ljsoncpp)
target_link_libraries(test-phk-512-ref -ljsoncpp)
target_link_libraries(test-phk-1024-ref -ljsoncpp)
target_link_libraries(test-aes-round)
target_link_libraries(create-test-vectors-phk-ref)

